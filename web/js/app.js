//function include(scriptUrl) {
//    document.write('<script src="' + scriptUrl + '"></script>');
//}

var path = location.pathname.split('/');
//window.baseUrl = location.protocol + '//' + location.hostname + ':' + location.port + '/' + path[1] ;
window.baseUrl = location.protocol + '//' + location.hostname + '/' + path[1];

/*--- No submit when press enter ---*/
$(function () {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});


/*--- Profile scripts ---*/
(function($) {

    // tabs plugin
    var $profile = $('.profile .edit');
    if ($profile.length) {
        include(window.baseUrl + '/js/tabs.js');
        $(function () {
            $profile.lightTabs();
        });
    }

    // phone mask
    //var $phone = $('.phoneMask');
    //if ($phone.length) {
    //    include(window.baseUrl + '/js/jquery.mask.js');
    //    $(function() {
    //        $phone.mask('000000000000');
    //    });
    //}

    // photobox
    var $gallery = $('#gallery');
    if ($gallery.length) {
        include(window.baseUrl + '/js/jquery.photobox.js');
        $(function () {
            $gallery.photobox('a', {time: 0});
        });
    }
})(jQuery);


 //Choosen
(function() {
    var $chosenSelect = $('.chosenSelect');
    if ($chosenSelect.length) {
        include(window.baseUrl + '/js/chosen.jquery.min.js');
        $(function() {
            $chosenSelect.chosen({width: "100%"});
        });
    }
})(jQuery);


/*--- User wall scripts ---*/
(function($){
    var $o = $('.user-wall');
    if ($o.length) {
        $o.find('.addAttaches').on('click', function() {
            $o.find('.attachForm').show();
        });
    }
})(jQuery);


/*--- Authmodal scripts ---*/
(function($) {

    $(function() {
        var $o = $('#authMadal');
        if ($o.length) {
            $o.find('.enterBtn').on('click', function() {
                $('button[type=submit]').trigger('click');
            });

            if ($o.find('.help-block').text()) {
                $o.modal('show');
            }
        }
    });
})(jQuery);


/*--- Registration form scripts ---*/
(function($) {
    var $o = $('#registration-form');

    if ($o.length) {
        var
            $types = $('.roleTypes input[type=radio]'),
            currentType = $('.roleTypes input[type=radio]:checked').val();

        inputHider(currentType);
        radioHider(currentType);

        $types.on('change', function() {
            var currentType = $(this).val();

            $('.roleInfo div:nth-of-type('+ currentType +')')
                .show()
                .siblings()
                .hide();

            inputHider(currentType);
            radioHider(currentType);
        });
    }

    function inputHider(currentType) {
        $('.formInputs input, .formInputs textarea').each(function() {
            var $this = $(this),
                inputType  = $this.data('type');

            if (typeof inputType != 'undefined' && currentType != inputType) {
                $this
                    .closest('.form-group')
                    .hide();
            } else if (currentType == inputType) {
                $this
                    .closest('.form-group')
                    .show();
            }
        })
    }

    function radioHider(currentType) {
        var $radios = $('.formInputs .radioList');

        if (currentType == 2 || currentType == 3) {
            $radios.each(function() {
                $(this).show();
            });
        } else {
            $radios.each(function() {
                $(this).hide();
            });
        }
    }

})(jQuery);


/*--- Get registration cities ---*/
//(function($) {
//    include("//code.jquery.com/ui/1.11.4/jquery-ui.js");
//    var citiesList = $('#citiesList');
//
//    $(function() {
//        citiesList.autocomplete({
//            source: []
//        });
//
//        $('#citiesList').on('keyup', function () {
//            var $this = $(this),
//                city = $this.val();
//
//            if (city.length > 2) {
//                var params = {
//                    'countryId': $('#countriesList option:selected').val(),
//                    'city': city
//                };
//
//                var res = $.ajax({
//                    url: window.baseUrl + '/ajax/get-autocomplete',
//                    type: 'POST',
//                    dataType: 'json',
//                    data: params
//                });
//
//                res.done(function (data) {
//                    var availableTags = [];
//                    for (var i = 0, len = data.cities.length; i < len; i++) {
//                        availableTags.push(data.cities[i].title);
//                    }
//
//                    citiesList.autocomplete({
//                        source: availableTags,
//                        select: function() {
//                            console.log($(this).val());
//                        }
//                    });
//                });
//            }
//        });
//
//    });
//})(jQuery);



/*--- Create board msg ---*/
(function($) {
    $(function() {
        $('.noApproved').on('click', function() {
            $('.noApprovedMsg').show();
        })
    });
})(jQuery);



/*--- Info modal ---*/
(function($) {
    $(function() {
        console.log($.cookie('server'));

        if ($.cookie('server') == '62.16.20.1' && !$.cookie('info')) {
            var $infoModal = $("#infoModal");
            $infoModal.modal();
            $infoModal.on('hidden.bs.modal', function () {
                $.cookie('info', '1', { expires: 366, path: '/' });
            })
        }
    })
})(jQuery);

(function ($) {
    $(function () {
        $('.UploadAvatar').on('click', function () {

            $('#uploadform-imagefile').trigger('click');
            $('#uploadform-boardfile').trigger('click');
        });
    });
})(jQuery);


$(document).ready(function () {

    $('.deliveryPopup').click(function(event){ // нажатие на кнопку - выпадает модальное окно
        event.preventDefault();


        var url = window.baseUrl + '/dress';
        var clickedbtn = $(this);
        var dressId = clickedbtn.data("dress");
        console.log(dressId);

        var modalContainer = $('#startDelivery');

        //var modalBody = modalContainer.find('.modal-body');
        modalContainer.modal({show:true});
        $.ajax({
            url: url,
            type: "GET",
            data: {'dressId':dressId},
            success: function (data) {
                $('.modal-body').html(data);
                modalContainer.modal({show:true});
            }
        });



    });
});

/*--- Get cities ---*/
//(function($) {
//    include(window.baseUrl + '/js/chosen.jquery.min.js');
//
//    $(function() {
//        $('#countriesList').on('change', function() {
//
//            var $this = $(this),
//                params = {
//                    'countryId': $this.val()
//                };
//
//            var res = $.ajax({
//                url: window.baseUrl + '/ajax/get-regions',
//                type: 'POST',
//                dataType: 'json',
//                data: params
//            });
//
//            res.done(function(data) {
//                var regions = $('#regionsList'),
//                    cities = $('#citiesList'),
//                    source = $('#regions-template').html(),
//                    template = Handlebars.compile(source),
//                    html = template(data);
//
//                regions
//                    .empty()
//                    .chosen("destroy")
//                    .append(html)
//                    .chosen({width: "100%"});
//
//                cities
//                    .empty()
//                    .chosen("destroy")
//                    .chosen({width: "100%"});
//            });
//        });
//
//        $('#regionsList').on('change', function() {
//
//            var $this = $(this),
//                params = {
//                    'regionId': $this.val()
//                };
//
//            var res = $.ajax({
//                url: window.baseUrl + '/ajax/get-cities',
//                type: 'POST',
//                dataType: 'json',
//                data: params
//            });
//
//            res.done(function(data) {
//                var cities = $('#citiesList'),
//                    source = $("#cities-template").html(),
//                    template = Handlebars.compile(source),
//                    html = template(data);
//
//                cities
//                    .empty()
//                    .chosen("destroy")
//                    .append(html)
//                    .chosen({width: "100%"});
//
//
//            });
//        });
//    });
//
//})(jQuery);


/*--- Masonry ---*/
//(function($) {
//
//    include(window.baseUrl + '/js/masonry.js');
//
//    $(function () {
//        $('.grid').masonry({
//            itemSelector: '.item'
//        });
//    });
//
//})(jQuery);