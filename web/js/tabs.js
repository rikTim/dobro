/**
 * Плагин для переключения вкладок
 */

(function($) {
    jQuery.fn.lightTabs = function() {

        var createTabs = function() {
            tabs = this;
            i = 0;

            showPage = function(i) {
                $(tabs)
                    .find('.btn-group button')
                    .eq(i)
                    .addClass('active')
                    .siblings()
                    .removeClass('active');

                $(tabs).find('.form').hide();
                $(tabs)
                    .find('.form')
                    .eq(i)
                    .show()
            };

            showPage(0);

            $(tabs).find('.btn-group button').each(function(index, element) {
                $(element).attr("data-page", i);
                i++;
            });

            $(tabs).find('.tabBtn').click(function() {
                showPage(parseInt($(this).attr("data-page")));
            });
        };

        return this.each(createTabs);
    };
})(jQuery);