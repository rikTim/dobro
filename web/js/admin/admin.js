function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}

var path = location.pathname.split('/');
//window.baseUrl = location.protocol + '//' + location.hostname + ':' + location.port + '/' + path[1] + '/web';
window.baseUrl = location.protocol + '//' + location.hostname + '/' + path[1];

$(function () {
    /*========================================================
     Add video
     ========================================================*/
    (function ($) {
        var $videoAdd = $('.video-add');
        $videoAdd.find('.addVideo').on('click', function() {
            var videoBlock = $videoAdd.find('.link-video').last().clone();
            videoBlock.find('input').each(function(){
                $(this).val('');
            });
            videoBlock.appendTo('.links-video');
        });
    })(jQuery);


    /*========================================================
     Delete  Avatar
     =========================================================*/
    $(function() {
        $('.content .form-active .delete-photo').on('click', function () {

            var $this = $(this);
            var avatarId = $(this).data('id');
            var csrfToken = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                type: 'POST',
                url: '../delavatar',
                data: {
                    'avatarId': avatarId,
                    _csrf: csrfToken
                },
                success: function () {
                    $this.parents('.image-user').remove();
                }
            });
        });
    });


    /*========================================================
     Img Rotate
     =========================================================*/
    $(function () {
        $('.img-rotate').on('click', function () {
            var id = $(this).data('id');
            var side = $(this).data('side');
            $.ajax({
                type: 'POST',
                url: '../rotate-img',
                data: {
                    id: id,
                    side: side
                },
                success: function () {
                    var angle = null;
                    if (side == 'left') {
                        angle = -90;
                    } else {
                        angle = 90;
                    }
                    var src = $('#img-' + id).attr('src');
                    $('#img-' + id).attr('src', src + '?' + new Date().getTime());
                }
            });
        });
    });

    /*========================================================
     Img Set WM
     =========================================================*/
    $(function () {
        $('.img-set-wm').on('click', function () {
            var id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '../set-watermark',
                data: {
                    id: id
                },
                success: function () {
                    var src = $('#img-' + id).attr('src');
                    $('#img-' + id).attr('src', src + '?' + new Date().getTime());
                }
            });
        });
    });

});

$(document).ready(function () {

    $('.deliveryPopup').click(function(event){ // нажатие на кнопку - выпадает модальное окно
        event.preventDefault();


        var url = window.baseUrl + '/shop/dress';
        var clickedbtn = $(this);
        var girlId = clickedbtn.data("girl");

        var modalContainer = $('#startDelivery');
        console.log(url);
        //var modalBody = modalContainer.find('.modal-body');
        modalContainer.modal({show:true});
        $.ajax({
            url: url,
            type: "GET",
            data: {'girlId':girlId},
            success: function (data) {
                $('.modal-body').html(data);
                modalContainer.modal({show:true});
            }
        });



    });



    //$(document).on("submit", '.signup-form', function (e) {
    //        e.preventDefault();
    //        var form = $(this);
    //        var type = $(document.activeElement,this)[0].name;
    //        var girlId = $(this).data("girl");
    //        var url = 'index.php?r=process%2Fstart&girlId='+girlId+'&type='+type;
    //        var id = $('.profile-btn').val();
    //
    //        $.ajax({
    //            url: url,
    //            type: "POST",
    //            data: form.serialize(),
    //            success: function (result) {
    //                var modalContainer = $('#startDelivery');
    //                var modalBody = modalContainer.find('.modal-body');
    //                var insidemodalBody = modalContainer.find('.gb-user-form');
    //
    //
    //                switch (result){
    //                    case 'true':
    //                        $("#"+girlId+".girl-block-view").addClass('green-background violet-border');
    //                        insidemodalBody.html(result).hide(); //
    //
    //                        $('#success').html("<div class='alert alert-success'>");
    //                        $('#success > .alert-success').append("<strong>Рассылка запущена</strong>");
    //                        $('#success > .alert-success').append('</div>');
    //                        //setTimeout(function() { // скрываем modal через 4 секунды
    //                        //    $("#startDelivery").modal('hide');
    //                        //}, 2000);
    //                        break;
    //                    case 'false':
    //                        insidemodalBody.html(result).hide(); //
    //
    //                        $('#success').html("<div class='alert alert-success'>");
    //                        $('#success > .alert-success').append("<strong>Шаблон сохранён</strong>");
    //                        $('#success > .alert-success').append('</div>');
    //                        //setTimeout(function() { // скрываем modal через 4 секунды
    //                        //    $("#startDelivery").modal('hide');
    //                        //}, 2000);
    //                        break;
    //                    default : modalBody.html(result).hide().fadeIn(); break;
    //                }
    //            }
    //        });
    //    }
    //
    //
    //);


});
