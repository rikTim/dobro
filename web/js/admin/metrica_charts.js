$(function () {
    var o = $('.metrica-chart');
    if (o.length > 0) {
        /**
         * Traffic Summary Line Chart
         */
        trafficSummary = $.parseJSON(trafficSummary);
        var trafficSummaryCanvas = document.getElementById('traffic-summary').getContext('2d');

        var visits = _.pluck(trafficSummary.data, 'visits');
        var visitors = _.pluck(trafficSummary.data, 'visitors');
        var new_visitors = _.pluck(trafficSummary.data, 'new_visitors');

        var labels = _.map(trafficSummary.data, function (value, key, list) {
            return value.date.substr(0, 4) + '.' + value.date.substr(4, 2) + '.' + value.date.substr(6, 2);
        });

        var trafficSummaryChart = new Chart(trafficSummaryCanvas).Line(
            {
                labels: labels,
                datasets: [
                    {
                        label: 'Visits',
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: visits
                    },
                    {
                        label: 'Visitors',
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: visitors
                    },
                    {
                        label: 'New Visitors',
                        fillColor: "rgba(255,195,188,0.2)",
                        strokeColor: "rgba(255,195,188,1)",
                        pointColor: "rgba(255,195,188,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(255,195,188,1)",
                        data: new_visitors
                    },
                ]
            },
            {
                responsive: true,
                multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
            }
        );

        /**
         * Traffic Deepness Bar Chart
         */
        trafficDeepness = $.parseJSON(trafficDeepness);
        var trafficDeepnessCanvas = document.getElementById('traffic-deepness').getContext('2d');

        labels = _.pluck(trafficDeepness.data_time, 'name');
        visits = _.pluck(trafficDeepness.data_time, 'visits');
        var visit_time = _.pluck(trafficDeepness.data_time, 'visit_time');

        var trafficDeepnessChart = new Chart(trafficDeepnessCanvas).Bar(
            {
                labels: labels,
                datasets: [
                    {
                        label: "Visits",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data: visits
                    },
                    {
                        label: "Visit Time",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: visit_time
                    }
                ]
            },
            {
                responsive: true,
                multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
            }
        );

        /**
         * Traffic Hourly Line Chart
         */
         trafficHourly = $.parseJSON(trafficHourly);
         var trafficHourlyCanvas = document.getElementById('traffic-hourly').getContext('2d');

         labels = _.pluck(trafficHourly.data, 'hours');
         var avg_visits = _.pluck(trafficHourly.data, 'avg_visits');
         var denial = _.pluck(trafficHourly.data, 'denial');

         var trafficHourlyChart = new Chart(trafficHourlyCanvas).Line(
             {
                 labels: labels,
                 datasets: [
                     {
                         label: 'Avg Visits',
                         fillColor: "rgba(220,220,220,0.2)",
                         strokeColor: "rgba(220,220,220,1)",
                         pointColor: "rgba(220,220,220,1)",
                         pointStrokeColor: "#fff",
                         pointHighlightFill: "#fff",
                         pointHighlightStroke: "rgba(220,220,220,1)",
                         data: avg_visits
                     },
                     {
                         label: 'Denial',
                         fillColor: "rgba(151,187,205,0.2)",
                         strokeColor: "rgba(151,187,205,1)",
                         pointColor: "rgba(151,187,205,1)",
                         pointStrokeColor: "#fff",
                         pointHighlightFill: "#fff",
                         pointHighlightStroke: "rgba(151,187,205,1)",
                         data: denial
                     }
                 ]
             },
             {
                 responsive: true,
                 multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
             }
         );

         /**
          * Traffic Load Chart
          */
          trafficLoad = $.parseJSON(trafficLoad);
          var trafficLoadCanvas = document.getElementById('traffic-load').getContext('2d');

          labels = _.pluck(trafficLoad.data, 'id');
          var max_rps = _.pluck(trafficLoad.data, 'max_rps');
          var max_users = _.pluck(trafficLoad.data, 'max_users');

          var trafficLoadChart = new Chart(trafficLoadCanvas).Bar(
              {
                  labels: labels,
                  datasets: [
                      {
                          label: "Max Rps",
                          fillColor: "rgba(220,220,220,0.5)",
                          strokeColor: "rgba(220,220,220,0.8)",
                          highlightFill: "rgba(220,220,220,0.75)",
                          highlightStroke: "rgba(220,220,220,1)",
                          data: max_rps
                      },
                      {
                          label: "Max Users",
                          fillColor: "rgba(151,187,205,0.5)",
                          strokeColor: "rgba(151,187,205,0.8)",
                          highlightFill: "rgba(151,187,205,0.75)",
                          highlightStroke: "rgba(151,187,205,1)",
                          data: max_users
                      }
                  ]
              },
              {
                  responsive: true,
                  multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
              }
          );

          /**
           * Geo Pie Chart
           */
           geo = $.parseJSON(geo);
           var geoVisitsCanvas = document.getElementById('geo-visits-chart').getContext('2d');

           var data = _.map(geo.data, function (value) {
               var color = getRandomColor();
               return {
                   value: value.visits,
                   label: value.name,
                   color: color,
                   highlight: color
               }
           });

           var geoVisitsChart = new Chart(geoVisitsCanvas).Pie(data);

           var geoViewsCanvas = document.getElementById('geo-views-chart').getContext('2d');

           data = _.map(geo.data, function (value) {
               var color = getRandomColor();
               return {
                   value: value.page_views,
                   label: value.name,
                   color: color,
                   highlight: color
               }
           });

           var geoViewsChart = new Chart(geoViewsCanvas).Doughnut(data);
    }
});

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
