var functions = {
    unactivateBtns: function () {
        $('.profile .tab-btn:not(.fast-edit)').each(function () {
            $(this).addClass('unactive');
        });
        return true;
    },
    hideSteps: function () {
        $('.profile .step').each(function () {
            $(this).hide();
        });
        return true;
    },
    stepBtns: function ($this, type) {
        var $container = $('.profile-form .container');
        var currentStep = $('.profile .btn-noshadow:not(.unactive)').data('step');
        var step = currentStep.split('_');
        var stepCount = $('.tab-btn').length;


        if (type == 'next') {
            var check = this.checkFields(currentStep);
            if (check) {
                var stepNum = parseInt(step[1]) + 1;

                if (stepNum == stepCount) {
                    $('.profile .save-profile').show();
                }

                $('.control-btns .prev-step').css('display', 'inline-block');
            } else {
                return false;
            }
        } else if (type == 'prev') {
            var stepNum = parseInt(step[1]) - 1;
        }

        var nextStep = 'span[data-step=' + step[0] + '_' + stepNum + ']';
        this.unactivateBtns();
        this.hideSteps();


        if (type == 'next' && stepNum == stepCount) {
            $this.hide();
        } else if (type == 'prev' && stepNum == 1) {
            $this.hide();
        }


        console.log(nextStep);

        $container.find(nextStep).removeClass('unactive');
        $container.find('.' + step[0] + '_' + stepNum).show();
        return true;
    },
    geoName: function (req, selector, isCity) {
        var $this = this;
        var lang = this.getCookie('lang');

        if (typeof lang == 'undefined') {
            lang = 'en';
        } else {
            lang = lang.toLowerCase();
        }

        $.ajax({
            url: req,
            type: 'GET',
            dataType: 'jsonp',
            success: function (data) {
                for (var i = 0; i <= data.response.count - 1; i++) {
                    selector.append('<option value="' + data.response.items[i].id + '">' + data.response.items[i].title + '</option>');
                }
                if (!isCity) {
                    selector.chosen({
                        width: "100%"
                    }).change(function () {
                        var cityReq = 'http://api.vk.com/method/database.getCities?v=5.5&country_id=' + $(this).val() + '&offset=0&need_all=0&count=1000&lang=' + lang;
                        var $citySelect = $('.city-list-chosen');
                        $citySelect.empty();
                        var isCity = true;
                        $this.geoName(cityReq, $citySelect, true);
                    });
                }
            }
        });
    },
    getGeo: function (req, selector) {
        $.ajax({
            url: req,
            type: 'GET',
            dataType: 'jsonp',
            success: function (data) {
                $(selector).val(data.response[0].title);
            }
        });
    },
    geoDropDown: function (countrySelector, сitySelector) {
        var $countrySelect = $('.country-list-chosen');
        var $citySelect = $('.city-list-chosen');
        var lang = this.getCookie('lang');

        if (typeof lang == 'undefined') {
            lang = 'en';
        } else {
            lang = lang.toLowerCase();
        }

        var selectedCountryId = $countrySelect.val();
        var selectedCityId = $citySelect.val();
        var reqCountry = 'http://api.vk.com/method/database.getCountriesById?v=5.5&country_ids=' + selectedCountryId + '&lang='+lang;
        var reqCity = 'http://api.vk.com/method/database.getCitiesById?v=5.5&city_ids=' + selectedCityId + '&lang='+lang;

        this.getGeo(reqCountry, countrySelector);
        this.getGeo(reqCity, сitySelector);
    },
    girlsHome: function (countrySelector, сitySelector, nameSelector) {
        var $countrySelect = $('.country-list-chosen');
        var $cityselect = $('#girls-cityid option:selected');
        var $nameselect = $('#girls-nameid option:selected');

        var lang = this.getCookie('lang');
        if (typeof lang == 'undefined') {
            lang = 'en';
        } else {
            lang = lang.toLowerCase();
        }

        var selectedCountryId = $countrySelect.val();
        var reqCountry = 'http://api.vk.com/method/database.getCountriesById?v=5.5&country_ids=' + selectedCountryId + '&lang='+lang;
        this.getGeo(reqCountry, countrySelector);
        $('.girlsCityId').val($cityselect.val());
        $('.girlsCity').val($cityselect.text());
        $('.girlsName').val($nameselect.text());
    },
    checkFields: function (currentStep) {
        var errors = [];
        $('.' + currentStep + ' .tmInput.required .form-item').each(function () {
            var $this = $(this);
            if ($this.val().length == 0) {
                errors.push($this.attr('id'));
            }
        });
        errors = errors.filter(function (element) {
            return !!element;
        });

        if (errors.length != 0) {
            for (var i = 0, len = errors.length; i < len; i++) {
                $('.profile-form #' + errors[i]).addClass('error');
            }
            return false;
        } else {
            return true;
        }
    },
    drowCharts: function (data, id) {
        var labels = [];
        var vals = [];
        var apVals = [];
        var reVals = [];

        if (data[0].length == 0) {
            $('.no-register-girls').show();
            $('#statistics').hide();
        } else {
            $('.input-data-block').show();
        }
        for (var i = 0, len = data.length; i < len; i++) {
            for (var j = 0, lenJ = data[i].length; j < lenJ; j++) {
                if ($.inArray(data[i][j]['registerdate'], labels) == -1) {
                    labels.push(data[i][j]['registerdate']);
                }
            }
        }
        for (var i = 0, len = data[0].length; i < len; i++) {
            vals.push(data[0][i]['count'])
        }
        for (var i = 0, len = data[1].length; i < len; i++) {
            apVals.push(data[1][i]['count'])
        }
        for (var i = 0, len = data[2].length; i < len; i++) {
            reVals.push(data[2][i]['count'])
        }

        var lineChartData = {
            labels: labels,
            datasets: [
                {
                    label: "Registered girls",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: vals
                }
                , {
                    label: "Approved girls",
                    fillColor: "rgba(34, 112, 36, 0.2)",
                    strokeColor: "rgba(34, 112, 36, 1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: apVals
                }
                , {
                    label: "Rejected girls",
                    fillColor: "rgba(216, 60, 34, 0.2)",
                    strokeColor: "rgba(216, 60, 34, 1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: reVals
                }
            ]

        };

        var canvas = document.getElementById(id);
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        window.myLine = new Chart(ctx).Bar(lineChartData, {
            responsive: true,
            multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

        });
    },
    drowStatistics: function (data, id) {
        var labels = [];
        var vals = [];

        for (var i = 0, len = data.length; i < len; i++) {
            for (var j = 0, lenJ = data[i].length; j < lenJ; j++) {
                if ($.inArray(data[i][j]['registerdate'], labels) == -1) {
                    labels.push(data[i][j]['registerdate']);
                }
            }
        }
        for (var i = 0, len = data[0].length; i < len; i++) {
            vals.push(data[0][i]['count'])
        }

        var lineChartData = {
            labels: labels,
            datasets: [
                {
                    label: "Registered girls",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: vals
                }
            ]

        };

        var canvas = document.getElementById(id);
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        window.myLine = new Chart(ctx).Bar(lineChartData, {
            responsive: true,
            multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

        });
    },
    getCookie : function(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },
    getGet : function(val) {
        var result = "Not found",
            tmp = [];
        location.search
            //.replace ( "?", "" )
            // this is better, there might be a question mark inside
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
            });
        return result;
    },
    setCookie : function (name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    },
    deleteCookie : function(name) {
        this.setCookie(name, "", {
            expires: -1
        })
    },



    include : function(scriptUrl) {
        document.write('<script src="' + scriptUrl + '"></script>');
    }

};