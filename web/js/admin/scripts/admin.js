$(function () {

    /*========================================================
     Date inputs
     =========================================================*/
    $('.charts .selectData').mask('00.00.0000');


    /*========================================================
     Draw charts
     =========================================================*/
    var o = $('.chart');
    if (o.length > 0) {
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'POST',
            url: 'getgirlstatistics',
            dataType: 'json',
            data: {
                _csrf: csrfToken
            },
            success: function (data) {
                chartsDataCounts('.girlsStatistic', data);
                functions.drowCharts(data, 'girlsStatistic');
            }
        });

        $.ajax({
            type: 'POST',
            url: 'getpartnerstatistics',
            dataType: 'json',
            data: {
                _csrf: csrfToken
            },
            success: function (data) {
                chartsDataCounts('.partnersStatistic', data);
                functions.drowCharts(data, 'partnersStatistic');
            }
        });
    }

    if ($('#registerStatistics').length > 0) {
        $.ajax({
            type: 'POST',
            url: 'getpartnerstatistics',
            dataType: 'json',
            data: {
                _csrf: csrfToken
            },
            success: function (data) {
                functions.drowStatistics(data, 'registerStatistics');
            }
        });
    }


    /*========================================================
     Delete girl photo
     =========================================================*/
    $('.content .form-active .delete-photo').on('click', function () {
        var $this = $(this);
        var photoId = $(this).data('id');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'POST',
            url: 'delphotos',
            data: {
                'photoId': photoId,
                _csrf: csrfToken
            },
            success: function () {
                $this.parents('.image-user').remove();
            }
        });
    });


    /*========================================================
     Update girl avatar
     =========================================================*/
    $('.content .form-active .set-avatar:not(.clicked)').click(function () {
        $(this).remove();
        var photoId = $(this).data('id');
        var girlAvatar = $('.girl-avatar').data('avatarid');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'POST',
            url: 'updatephotos',
            data: {
                'photoId': photoId,
                'girlAvatarId': girlAvatar,
                _csrf: csrfToken
            },
            success: function () {
                location.reload();
            }
        });
    });


    /*========================================================
     Rotate photo
     =========================================================*/
    $('.content .form-active .rotate-photo').click(function () {
        $(this).remove();
        var photoId = $(this).data('id');
        var girlAvatar = $('.girl-avatar').data('avatarid');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'POST',
            url: 'rotate-photo',
            data: {
                'photoId': photoId,
                'girlAvatarId': girlAvatar,
                _csrf: csrfToken
            },
            success: function () {
                //location.reload();
            }
        });
    });


    /*========================================================
     Send message admin
     ========================================================*/
    $('.send-message').on('click', function () {
        var $this = $(this);
        var text = $('.form-control.text-area').val();
        var userId = $this.data('userid');
        var typeId = $this.data('typeid');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");

        $.ajax({
            type: 'POST',
            url: 'addmessage',
            data: {
                _csrf: csrfToken,
                text: text,
                userId: userId,
                typeId: typeId
            },
            success: function () {
                location.reload();
            }
        });
    });


    /*========================================================
     Filter scripts
     =========================================================*/
    $('.filterBtn').on('click', function () {
        var $this = $(this);
        var filterFrom = $this.parent().find('.filterFrom').val();
        var filterTo = $this.parent().find('.filterTo').val();
        var url = $this.parent().find('.url').val();
        var id = $this.parents('.box-body').find('canvas').attr('id');
        var csrfToken = $('meta[name="csrf-token"]').attr("content");

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                'filterFrom': filterFrom,
                'filterTo': filterTo,
                _csrf: csrfToken
            },
            success: function (data) {
                chartsDataCounts('.'+id, data);
                functions.drowCharts(data, id);
            }
        });
    });


    /*========================================================
     Recount partner amount with month payment
     =========================================================*/
    $('.recountPartnerAmount').on('click', function () {
        var partnerId = $('.partnerId').text();
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'POST',
            data: {
                _csrf: csrfToken,
                'partnerId': partnerId
            },
            url: 'recountmoney',
            success: function () {
                location.href = 'balances';
            }
        });
    });


    /*========================================================
     Push girl to top dates
     =========================================================*/
    $(document).on('click', '#pushToTopDates', function(){
        var $this = $(this);

        var res = confirm('Отправить анкету на top-dates?');
        if (res) {
            $this.removeAttr('id');
            var girlId = $('.girlIdHidden').val();
            var csrfToken = $('meta[name="csrf-token"]').attr("content");

            $.ajax({
                type: 'POST',
                data: {
                    _csrf: csrfToken,
                    'girlId': girlId
                },
                url: 'push',
                beforeSend: function() {
                    $this.removeAttr('id');
                    $('#floatingCirclesG').css('display','inline-block');
                },
                success: function () {
                    $('#floatingCirclesG').hide();
                    $('.success-msg').show();
                    $this.hide();
                }
            })
        }
    });


    /* Fancy box
     ========================================================*/
    $('.profile-gallery-item').fancybox();
});


var chartsDataCounts = function(className ,data) {

    var newCount = countData(data[0]);
    var approveCount = countData(data[1]);
    var rejectedCount = countData(data[2]);
    var allCount = newCount + approveCount + rejectedCount;

    $(className + ' .newCount').text(newCount);
    $(className + ' .approveCount').text(approveCount);
    $(className + ' .rejectedCount').text(rejectedCount);
    $(className + ' .allCount').text(allCount);
};

var countData = function(data) {
    var sum = 0;
    for (var i = 0, len = data.length; i < len; i++) {
        sum += Number(data[i]['count']);
    }
    return sum;
};