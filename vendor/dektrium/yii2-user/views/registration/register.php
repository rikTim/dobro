<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Присоединиться к нам';
?>


<!-- Sign in block -->
<div class="section">
    <div class="container user-info">
        <h1><?= $this->title; ?></h1>
        <div class="row">
            <!-- Form col -->
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <?php $form = ActiveForm::begin([
                    'id'                     => 'registration-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                ]); ?>

                <?= $form->field($model, 'email') ?>

                <?php if ($module->enableGeneratingPassword == false): ?>
                    <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
                <?php endif ?>

                <?= Html::submitButton('Присоедениться к нам', ['class' => 'btn btn-success btn-block']) ?>

                <?php ActiveForm::end(); ?>


                <br>
                <?= Connect::widget([
                    'baseAuthUrl' => ['/user/security/auth'],
                ]) ?>
            </div>
            <!-- /Form col -->

            <!-- Info col -->
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <p>Присоединяйся к Миру добрых людей! Благодаря тебе мы становимся сильнее. Вместе еще легче делать добрые дела, а счастливых людей и моментов станет еще больше!</p>
                <p>Информационный портал для добрых дел dobroua.com открыт и ты можешь быть ангелом или волонтером, попросить о помощи или помочь нам с организацией работы. </p>
                <p>Мы стремимся к тому, чтобы Мир добрых людей становился шире, поэтому, нам всегда нужны люди, которые помогут в организации работы портала. Неважно какие у тебя способности и профессия - дела найдутся для всех. В нашей стране ужасающая статистика инвалидности, онко-заболеваний, детские приюты и дома престарелых переполнены людьми, которые нуждаются в тебе. Вместе мы реализуем свое желание быть полезными миру без страха остаться обманутым! А еще, dobroua.com - место, где ценят помощь и мы обязательно поблагодарим тебя за все, что ты сделаешь! Спасибо, что ты с нами!</p>
            </div>

        </div>
    </div>
</div>
<!-- /Sign in block -->