<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;


$this->title = 'DobroUA.com События';
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="site-index">

    <div class="container">
        <div class="page-header">
            <h1>События</h1>
        </div>
    </div>

    <!-- types = { 1: новость, 2: задание, 3: сообщение, 4: новый нуждающийся, 5: сообщение на стене } -->

    <div class="section feeds">
        <div class="container">
            <?php if(!empty($feeds)): ?>
                <?php foreach($feeds as $feed): ?>
                    <?php if($feed['type'] == 1): ?>
                        <!-- Вывод новости -->
                        <div class="media">
                            <div class="media-left">
                                <a href="<?= Url::toRoute(['news/more', 'id' => $feed['id']]) ?>">
                                    <img class="media-object img-rounded"
                                         src="<?= Yii::getAlias('@web/images/news.png') ?>"
                                         data-holder-rendered="true"
                                         style="width: 128px; height: 128px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    <b><?=  Yii::$app->DLL->getfeedType($feed['type']) ?>: </b>
                                    <a href="<? Url::toRoute(['more', 'id' => $model->id])?>"><?= $model->title ?></a>
                                </h4>
                                <small><?= Yii::$app->formatter->asDatetime($feed['dateTime'], "php:d.m.Y") ?></small>
                                <p><?= Yii::$app->DLL->subStr($feed['text']) ?></p>
                            </div>
                        </div>
                        <!-- /Вывод новости -->

                    <?php elseif($feed['type'] == 2): ?>
                        <!-- Вывод задания -->
                        <div class="media">
                            <div class="media-left">
                                <a href="<?= Url::toRoute(['tasks/more', 'id' => $feed['id']]) ?>">
                                    <?php if(!empty($feed['img']) && file_exists(Yii::getAlias('@app/web/images/tasks/' . $feed['img']))): ?>
                                        <img class="media-object img-rounded"
                                             src="<?= Yii::getAlias('@web/images/tasks/m2_' . $feed['img']) ?>"
                                             data-holder-rendered="true"
                                             style="width: 128px;">
                                    <?php else: ?>
                                        <img class="media-object img-rounded"
                                             src="<?= Yii::getAlias('@web/images/no-image.jpeg') ?>"
                                             data-holder-rendered="true"
                                             style="width: 128px; height: 128px;">
                                    <?php endif; ?>
                                </a>
                            </div>


                            <div class="media-body">
                                <h4 class="media-heading img-rounded">
                                    <b><?=  Yii::$app->DLL->getfeedType($feed['type']) ?>: </b>
                                    <a href="<?= Url::toRoute(['tasks/more', 'id' => $feed['id']]) ?>"><?= $feed['title'] ?></a>
                                </h4>

                                <small><?= Yii::$app->formatter->asDatetime($feed['dateTime'], "php:d.m.Y") ?></small>
                                <p><?= strip_tags(Yii::$app->DLL->subStr($feed['text'])) ?></p>


                                <?php /** Если не заполнен профайл не могу подписаться на задания */ ?>
                                <?php $profile = Yii::$app->DLL->getProfile(); ?>
                                <?php if(!empty($profile)): ?>
                                    <?php if(empty($feed['volTask'])): ?>
                                        <?= Html::a("Могу помочь <span class='badge'>" . $feed['count'] . "</span>", ['tasks/apply', 'id' => $feed['id'], 'action' => 'subscribe'], ['class' => 'btn btn-primary', 'style' => 'float: right' ]);?>
                                    <?php else: ?>
                                        <?= Html::a("Не могу помочь<span class='badge'></span>", ['tasks/apply', 'id' => $feed['id'], 'action' => 'unsubscribe'], ['class' => 'btn btn-primary', 'style' => 'float: right' ]);?>
                                    <?php endif; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                        <!-- /Вывод задания -->

                    <?php elseif($feed['type'] == 3): ?>
                        <!-- Вывод сообщения -->
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object img-rounded"
                                     src="<?= Yii::getAlias('@web/images/envelope.png') ?>"
                                     data-holder-rendered="true"
                                     style="width: 128px; height: 128px;">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    <b><?=  Yii::$app->DLL->getfeedType($feed['type']) ?>: </b>
                                    <?= $feed['title'] ?>
                                </h4>
                                <small><?= Yii::$app->formatter->asDatetime($feed['dateTime'], "php:d.m.Y") ?></small>
                                <p><?= $feed['text'] ?></p>
                                <span class="btn btn-primary" data-toggle="modal" data-target="#contactModal">Ответить</span>
                            </div>
                        </div>
                        <!-- /Вывод сообщения -->

                    <? elseif($feed['type'] == 4): ?>
                        <!-- Вывод нуждающегося -->
                        <div class="media">
                            <div class="media-left">
                                <a href="<?= Url::toRoute(['needies/more', 'id' => $feed['id']]) ?>">
                                    <?php if(!empty($feed['img']) && file_exists(Yii::getAlias('@app/web/images/' . $feed['userId'] . '/' . $feed['img']))): ?>
                                        <img class="media-object img-rounded img-centered"
                                             src="<?= Yii::getAlias('@web/images/' . $feed['userId']  . '/m_' . $feed['img']) ?>"
                                             data-holder-rendered="true"
                                             style="width: 128px; height: 128px;">
                                    <?php else: ?>
                                        <img class="media-object img-rounded"
                                             src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>"
                                             data-holder-rendered="true"
                                             style="width: 128px; height: 128px;">
                                    <?php endif; ?>
                                </a>
                            </div>


                            <div class="media-body">
                                <h4 class="media-heading">
                                    <b><?=  Yii::$app->DLL->getfeedType($feed['type']) ?>: </b>
                                    <a href="<?= Url::toRoute(['needies/more', 'id' => $feed['id']]) ?>"><?= $feed['title'] ?></a>
                                </h4>
                                <p><?= $feed['text'] ?></p>
                            </div>
                        </div>
                        <!-- /Вывод нуждающегося -->

                    <?php else: ?>
                        <!-- Вывод Вывод сообщения о новой записи на стене -->
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object img-rounded"
                                     src="<?= Yii::getAlias('@web/images/wall.png') ?>"
                                     data-holder-rendered="true"
                                     style="width: 128px; height: 128px;">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    <b><?= Yii::$app->DLL->getfeedType($feed['type']) ?>: </b>
                                    <?= $feed['title'] ?>
                                </h4>
                                <small><?= Yii::$app->formatter->asDatetime($feed['dateTime'], "php:d.m.Y") ?></small>
                                <p><?= $feed['text'] ?></p>
                            </div>
                        </div>
                        <!-- /Вывод сообщения о новой записи на стене -->

                    <?php endif; ?>
                <?php endforeach; ?>


                <div style="height: 50px"></div>

                <!-- Pagination block -->
                <div class="container">
                    <div class="row">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>
                <!-- /Pagination block -->
            <?php else: ?>
                <h3>Список событий пуст</h3>
            <?php endif; ?>
        </div>
    </div>

</div>
