<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HelpTypesSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Help Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content static-pages-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-medkit"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Create Help Types', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <div class="box-body">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
//                        'filterModel' => $searchModel,
                        'columns' => [

                            'id',
                            'title',

                            [
                                'attribute'=>'isNew',
                                'format'=> 'raw',
                                'value'=>function($data)
                                {
                                    switch($data->isNew) {
                                        case 0:
                                            $isNew = 'old';
                                            break;
                                        case 1:
                                            $isNew = ' <span class="label bg-green ">new</span>';
                                            break;
                                    }
                                    return $isNew;
                                },
                            ],

                            ['class' => 'yii\grid\ActionColumn',
                                'template' => '{update}{delete}',
                                'options' => ['width' => '50'],
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>
