<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HelpTypes */

$this->title = 'Update Help Types: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Help Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content static-pages-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</section>
