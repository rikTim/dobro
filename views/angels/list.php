<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

$this->title = 'Ангелы проекта - Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'На этой страничке вы можете познакомиться и получить информацию про ангелов проекта, оказать помощь и стать ангелом']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="container b_fixed">
    <h1>Ангелы
        <div class="b_help_text">
            <?php if (Yii::$app->user->isGuest): ?>
                <p class="angel">Ангел - человек, у которого есть желание и возможность помочь морально или материально тем, кто
                    нуждается. <a href="<?= Url::toRoute(['user/register', 'type' => 2]) ?>">Став Ангелом</a>, ты
                    сможешь спасти жизнь человека или сделать кого-то счастливее.</p>
            <?php else: ?>
                <p class="angel">Ангел - человек, у которого есть желание и возможность помочь морально или материально тем, кто
                    нуждается. Став Ангелом, ты сможешь спасти жизнь человека или сделать кого-то счастливее.</p>
            <?php endif; ?></div>
    </h1>
    <div class="b_persons_list">


        <?= ListView::widget([
            'dataProvider' => $angels,
            'itemView' => '_view_list',
            'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
            'emptyText' => 'Список пуст',
        ]) ?>
    </div>
    <div class="clearboth"></div>
</div>



