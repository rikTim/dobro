<?php

use yii\helpers\Url;

?>



<?php $userRole = Yii::$app->DLL->getRoleByUserId($model->toUserId);?>
    <?php $user =  Yii::$app->DLL->getModelByUserId($model->toUserId); $user= $user::find()->where(['userId'=>$model->toUserId])->one()?>

<?php
        switch ($userRole) {
            case 'volunteer':
                $id = $model->id;
                $url = '/volunteers/more';
                $imgPath = $model->toVolunteerUser->avatar->path;
                break;
            case 'angel':
                $id = $model->id;
                $url = '/angels/more';
                $imgPath = $model->toAngelUser->avatar->path;
                break;
            case 'needy':
                $id = $model->id;
                $url = '/needies/more';
                $imgPath = $model->toNeedyUser->file->path;
                break;
        } ?>

<div class="b_pic"><a href="<?= Url::toRoute([$url, 'id' => $user->id])?>">
<?php if(!empty($imgPath) && file_exists(Yii::getAlias('@app/web/images/' . $model->toUserId . '/' . $imgPath))): ?>
        <img style="width:106px;" src="<?= Yii::getAlias('@web') . '/images/' . $model->toUserId . '/m_' . $imgPath ?>">
<?php else: ?>
    <img  style="width:106px;" src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>">
<?php endif; ?>
    </a></div>
<div class="b_desc_content">
    <div class="b_title"><a href="<?= Url::toRoute([$url, 'id' => $user->id])?>"><?= Yii::$app->DLL->subStr($user->name, 25)  . ' ' . $user->lastname?></a><span class="b_date"><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></span></div>
    <div class="b_desc"><span class="b_paid"><?= $model->text ?></span></div>
</div>

