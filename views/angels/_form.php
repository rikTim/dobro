<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="angels-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?php $form->field($model , 'userId')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'mainCountryId')->dropDownList($countries)->label('Страна') ?>

    <?= $form->field($model, 'city')->textInput()->label('Город') ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time')->textInput(['maxlength' => true])->label('Удобное время для звонка') ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'help')->textInput(['maxlength' => true]) ?>

    <?php $file = $model->avatar->path; ?>
    <?php if (!empty($file)): ?>
        <div class="form-active">
            <p><label class="control-label">Аватар</label></p>

            <div class="image-user">
                <div class="icons">
                    <i data-id="<?= $model->avatarId ?>" class="delete-photo fa fa-trash-o"></i>
                </div>
                <span class="photo">
                    <a class="profile-gallery-item"
                       href="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/' . $file ?>"
                       data-fancybox-group="gallery" title="">
                        <img
                            src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $file ?>"
                            alt="" id="img-<?= $model->avatarId ?>">
                    </a>
                </span>
            </div>
            <div class="clr"></div>
            <?= \yii\bootstrap\Html::button('Повернуть влево', ['class' => 'img-rotate btn btn-primary', 'data-side' => 'left', 'data-id' => $model->avatarId]) ?>
            <?= \yii\bootstrap\Html::button('Повернуть вправо', ['class' => 'img-rotate btn btn-primary', 'data-side' => 'right', 'data-id' => $model->avatarId]) ?>
        </div>

    <?php else: ?>
        <p><label class="control-label">Аватар</label></p>
        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
    <?php endif; ?>
    <?= \cyneek\yii2\widget\upload\crop\UploadCrop::widget([
        'form' => $form,
        'model' => $upload,
        'attribute' => 'imageFile',
        'jcropOptions' => [
            'aspectRatio' => 1,
        ]
    ]) ?>


    <div class="clr"></div>



    <?= $form->field($model, 'statusId')->dropDownList($statuses) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
