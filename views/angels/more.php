<?php

use app\components\WallWidget;
use app\components\RatingPopupWidget;
use app\components\HelpToHistoryWidget;

$this->title =  $model->name . ' ' . $model->lastname .' - ангел благотворительной организации Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' =>'Ангел '.$model->name . ' ' . $model->lastname .' принимает активное участие в помощи детям и взрослым и вносит весомый вклад в наш проект']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>


    <div class="container b_fixed">
        <div class="b_left-side b_left_side_qustionare col-md-4 col-sm-4 col-xs-12">
            <div class="b_left_slider_person">
                <div class="slider-pro" id="b_person-slider">
                    <div class="sp-slides">
                        <!-- Slide 1 -->
                        <div class="sp-slide">
                            <?php if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
                                <img style="width:280px;" class="sp-image"
                                     src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>"/>

                            <?php else: ?>
                                <img style="width:280px;" class="sp-image"
                                     src="<?= Yii::getAlias('@web') . '/images/noavatar.png' ?>"/>

                            <?php endif; ?>

                        </div>

                    </div>
                </div>

            </div>
            <center>
                <button type="button"
                        style="color: #fff; background: #006cff;border-radius: 20px;border-color: #006cff;box-shadow: none;"
                        class="btn btn-success" data-toggle="modal" data-target="#ratingModal">
                    Рейтинг <?= $model->ratingCount ?></button>
            </center>
        </div>

        <div class="b_person_desc col-md-8 col-sm-8 col-xs-12 pull-right">
            <div class="b_name"><h1><?= $model->name . ' ' . $model->lastname ?></h1><span
                    class="b_locate"><?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
            </div>
            <p><b>Возраст:</b>
                <?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) ?></p>
            <?php if (!empty($model->email) && $model->showEmail == 0): ?>
                <p><b>Email :</b> <?= $model->email ?></p>
            <?php endif ?>

            <?php if (!empty($model->phone) && $model->showPhone == 0): ?>
                <p><b>Телефон: </b><?= $model->phone ?></p>
            <?php endif ?>
            <?php if (!empty($model->help)): ?>
                <p><b>Могу помочь: </b><?= $model->help ?></p>
            <?php endif ?>
            <?php if (!empty($model->vk)): ?>
                <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><i class="fa social fa-vk"></i></a>
            <?php endif; ?>
            <?php if (!empty($model->fb)): ?>
                <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><i
                        class="fa social fa-facebook-official"></i></a>
            <?php endif; ?>
            <?= WallWidget::widget(['userId' => $model->userId]); ?>
        </div>
        <div class="b_left-side b_left_side_qustionare col-md-4 col-sm-4 col-xs-12">

            <div class="b_helpers">
                <h2>Уже помог</h2>
                <div class="b_helpers_wrapper">
                    <div class="b_persons_list">
                        <?= \yii\widgets\ListView::widget([
                            'dataProvider' => $helps,
                            'itemView' => '_view_more',
                            'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
                            'emptyText' => 'Список пуст',
                            'itemOptions' => [
                                'tag' => 'div',
                                'class' => 'b_persons_item',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!--    <div class="section wall more helper angel">-->
    <!--        <div class="container">-->
    <!--            <div class="row">-->
    <!--              -->
    <!--                <div class="col-xs-12 col-sm-4 online">-->
    <!--                    <div class="row">-->
    <!--                        <div class="img">-->
    <!--                            <center>-->
    <!--                                --><?php //if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
    <!--                                    <img-->
    <!--                                        src="--><? //= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?><!--"-->
    <!--                                        alt="avatar" class="img-rounded img-responsive img-centered">-->
    <!--                                --><?php //else: ?>
    <!--                                    <img src="--><? //= Yii::getAlias('@web') . '/images/noavatar.png' ?><!--" alt="avatar"-->
    <!--                                         class="img-rounded img-responsive img-centered">-->
    <!--                                --><?php //endif; ?>
    <!--                            </center>-->
    <!---->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    --><?php //$online = Yii::$app->DLL->getOnline($model->userId);
//                    if ($online == 1):
//                        ?>
    <!--                        <span class="label label-success label-online">Online</span>-->
    <!--                    --><?php //endif; ?>
    <!--                    <div class="row btns">-->
    <!--                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ratingModal">-->
    <!--                            Рейтинг --><? //= $model->ratingCount ?><!--</button>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--          -->
    <!--                <div class="col-xs-12 col-sm-8">-->
    <!--                    <div class="info">-->
    <!--                        --><? //= $this->render('_info', [
//                            'model' => $model
//                        ]) ?>
    <!--                    </div>-->
    <!---->
    <!--                    <div class="info">-->
    <!--                        --><? //= HelpToHistoryWidget::widget(['userId' => $model->userId]); ?>
    <!--                    </div>-->
    <!--                </div>-->
    <!--              -->
    <!--            </div>-->
    <!--        </div>-->
    <!---->
    <!--        -->
    <!--        -->
    <!---->
    <!--    </div>-->


<?= RatingPopupWidget::widget(['userId' => $model->userId]); ?>