<?php

use yii\helpers\Html;

$this->title = 'Изменить Ангела: ' . ' ' . $model->name . ' ' . $model->lastname;
?>

<section class="content angels-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'countries'=>$countries,
                        'cities'=>$cities,
                        'statuses' => $statuses,
                        'images' => $images,
                        'upload'=>$upload
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>