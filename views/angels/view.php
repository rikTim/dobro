<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Ангел : ' . $model->name . ' ' . $model->lastname;
?>

<section class="content angels-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                        <?php if ($model->statusId != 2): ?>
                            <?= Html::a('Подтвердить', ['approved', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?php endif; ?>
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-bordered detail-view">

                        <tbody>
                        <tr>
                            <th>Имя</th>
                            <td><?= $model->name ?></td>
                        </tr>

                        <tr>
                            <th>Фамилия</th>
                            <td><?= $model->lastname ?></td>
                        </tr>

                        <tr>
                            <th>Возраст</th>
                            <td><?= $model->age ?></td>
                        </tr>

                        <tr>
                            <th>Страна</th>
                            <td><?= $model->countryTitle?></td>
                        </tr>
                        <tr>
                            <th>Город</th>
                            <td><?= $model->city?></td>
                        </tr>

                        <tr>
                            <th>Телефон</th>
                            <td><?= $model->phone ?></td>
                        </tr>
                        <tr>
                            <th>Удобное время для звонка</th>
                            <td><?= $model->time ?></td>
                        </tr>


                        <tr>
                            <th>Email</th>
                            <td><?= $model->email ?></td>
                        </tr>

                        <tr>
                            <th>Ссылка на страницу в FB</th>
                            <td>
                                <?php if(!empty($model->fb)):?>
                                <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><?= 'facebook.com/' . $model->fb ?></a>
                                <?php endif;?>
                            </td>
                        </tr>

                        <tr>
                            <th>Ссылка на страницу в VK</th>
                            <td>
                                <?php if(!empty($model->vk)):?>
                                <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><?= 'www.vk.com/' . $model->vk ?></a>
                                <?php endif;?>
                            </td>
                        </tr>

                        <tr>
                            <th>Могу помочь</th>
                            <td><?= $model->help ?></td>
                        </tr>

                        <tr>
                            <th>Аватар</th>
                            <td> <?php $file = $model->avatar->path; ?>
                                <?php if (!empty($file)): ?>
                                    <a href="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/' . $file ?>"><img
                                            src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $file ?>"></a>
                                <?php else: ?>
                                    <div class="image-user">
                                        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <th>Статус</th>
                            <td><?= $model->status->title ?></td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</section>