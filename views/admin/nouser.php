<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Нету профайла"
?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">
                    <?php if(!empty($error)):?>
                        Есть такой пользователь с ролью <h3><?= $error?></h3>
                    <?php endif;?>
                    <table class="table table-striped table-bordered detail-view">
                        <thead>
                        <tr>

                            <td width="50%"><b>Email</b></td>
                            <td width="50%"><b></b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($noUsers)) :?>
                        <?php foreach($noUsers as $noUser):?>
                        <tr >
                            <td><?= $noUser ?></td>
                            <td> <?= Html::a('Удалить', ['admin/delete-user', 'email' => $noUser], ['class' => 'btn btn-danger','data' => [
                                    'confirm' => 'Удалить этот Email?',
                                    'method' => 'post',
                                ]]) ?></td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>