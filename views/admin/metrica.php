<?php $this->title = 'DobroUA Metrica'; ?>

    <section class="content-header">
        <h1>Metrica</h1>
    </section>

    <section class="content charts">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Traffic Summary</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="traffic-summary" class="metrica-chart" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Traffic Deepness</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="traffic-deepness" class="metrica-chart" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Traffic Hourly</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="traffic-hourly" class="metrica-chart" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Traffic Load</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="traffic-load" class="metrica-chart" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Geo Visits</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="geo-visits-chart" class="metrica-chart" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Geo Page Views</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="geo-views-chart" class="metrica-chart" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">
    var trafficSummary = '<?= $trafficSummary ?>';
    var trafficDeepness = '<?= $trafficDeepness ?>';
    var trafficHourly = '<?= $trafficHourly ?>';
    var trafficLoad = '<?= $trafficLoad ?>';
    var geo = '<?= $geo ?>';
</script>
