<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Рейтинг : ' . $user->name . ' ' . $user->lastname;
?>
<style type="text/css">
    tr th {
        width: 20%;
    }
</style>
<section class="content rating-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">

                    <?php if (!empty($newHelps)): $i = 0; ?>
                        <?php foreach ($newHelps as $newHelp): ?>
                            <?php $form = ActiveForm::begin(['action' => ['update', 'id' => $id, 'type' => $type], 'id' => $newHelp->id]); ?>
                            <table class="table table-striped table-bordered detail-view">
                                <tbody>
                                <tr>
                                    <th>Кому</th>
                                    <td>
                                        <a href='<?= Url::toRoute(['needies/view', 'id' => $newNeedies[$i]->id]); ?>'><?= $newNeedies[$i]->name . ' ' . $newNeedies[$i]->lastname ?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Телефон</th>
                                    <td><?= $newNeedies[$i]->phone ?></td>
                                </tr>
                                <tr>
                                    <th> Текст</th>
                                    <td><?= $newHelp->text ?></td>
                                </tr>

                                <?php if (!empty($newHelp->comment)): ?>
                                    <tr>
                                        <th> Коментарий</th>
                                        <td><?= $newHelp->comment ?></td>
                                    </tr>
                                <?php endif; ?>

                                    <tr>
                                        <th>Фото</th>
                                        <td>
                                                <?php foreach($newHelp->attaches as $attach): ?>
                                                    <?php if($attach->type == 2):?>
                                                    <a href="<?= Yii::getAlias('@web') . '/images/helps/' . $newHelp->id . '/' . $attach->path ?>">
                                                        <img  src="<?= Yii::getAlias('@web') . '/images/helps/' . $newHelp->id . '/' . $attach->path ?>">
                                                    </a>
                                                        <?php endif;?>
                                                <?php endforeach; ?>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                            <?= $form->field($newHelp, 'id')->hiddenInput(['value' => $newHelp->id, 'readonly' => true])->label(false) ?>
                            <?= $form->field($model, 'userId')->hiddenInput(['value' => $id])->label(false); ?>
                            <?= $form->field($model, 'rating')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'descr')->textInput(['value' => $newHelp->text]) ?>

                            <?= $form->field($model, 'dateTime')->hiddenInput(['value' => time()])->label(false) ?>
                            <div class="form-group">
                                <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-success']) ?>

                                <?= Html::a('Удалить', ['admin/delete-help', 'id' => $newHelp->id, 'type' => $type], ['class' => 'btn btn-danger ']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                            <?php $i++; endforeach; ?>

                    <?php else: ?>

                        <?php $form = ActiveForm::begin(['action' => ['update', 'id' => $id, 'type' => $type]]); ?>
                        <?= $form->field($model, 'userId')->hiddenInput(['value' => $id])->label(false); ?>
                        <?= $form->field($model, 'rating')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'descr')->textInput() ?>
                        <?= $form->field($model, 'dateTime')->hiddenInput(['value' => time()])->label(false) ?>
                        <div class="form-group">
                            <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    <?php endif; ?>

                    <?= Html::a('Отмена', ['admin/rating-' . $url], ['class' => 'btn btn-warning ']) ?>
                </div>
            </div>

            <?php if (!empty($helps)):
                $i = 0; ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>

                        <h3 class="box-title">Добрые Дела:</h3>
                    </div>
                    <div class="box-body">
                        <?php foreach ($helps as $help): ?>
                            <table class="table table-striped table-bordered detail-view">

                                <tbody>
                                <tr>
                                    <h3><?= $help->text ?></h3>
                                </tr>
                                <tr>
                                    <th>Кому</th>
                                    <td>
                                        <a href='<?= Url::toRoute(['needies/view', 'id' => $needies[$i]->id]); ?>'><?= $needies[$i]->name . ' ' . $needies[$i]->lastname ?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Когда</th>
                                    <td><?= Yii::$app->formatter->asDatetime($help->dateTime, "php:d/m/Y "); ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <?php $i++; endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!empty($ratings)): ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>

                        <h3 class="box-title">История начисления рейтинга:</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered detail-view">
                            <tbody>
                            <tr>
                                <th>Начислено рейтинга</th>
                                <th>Описание</th>
                                <th>Дата</th>
                                <th></th>
                            </tr>
                            <?php foreach ($ratings as $rating): ?>
                                <tr>
                                    <td><?= $rating->rating ?></td>
                                    <td><?= $rating->descr ?></td>
                                    <td><?= Yii::$app->formatter->asDatetime($rating->dateTime, "php:d.m.Y h:m ") ?></td>
                                    <td>
                                        <a href="<?= Url::toRoute(['admin/remove', 'id' => $rating->id]) ?>"><i
                                                class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    </div>
</section>