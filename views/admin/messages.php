<?

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Message';
?>

<!--direct-chat-contacts-open-->
<div class="box  direct-chat direct-chat-primary ">
    <div class="box-header with-border">

        <?php if (empty($id)): ?>
        <h3 class="box-title">Нет Сообщений</h3>

        <div class="box-tools pull-right">
            <?php else: ?>
            <h3 class="box-title"><?= $title . $user->name . " " . $user->lastname ?></h3>

            <div class="box-tools pull-right">
                <?php if (!empty($newMessage)): ?>
                    <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="New Messages">Новых сообщений :<?= $newMessage ?></span>
                <?php endif; ?>
                <button class="btn btn-box-tool" data-widget="chat-pane-toggle"><i
                        class="fa fa-comments label bg-blue view-message ">Сообщения</i></button>
            </div>

        </div>
        <div class="box-body">

            <div class="direct-chat-messages">
                <?php foreach ($messagesToAdmin as $key => $dialog): ?>
                    <?php if ($dialog->fromUserId == 1): ?>
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">Администратор</span>
                                                    <span
                                                        class="direct-chat-timestamp pull-left"><?= Yii::$app->formatter->asDatetime($dialog->dateTime, "php:d.m.Y H:i"); ?></span>
                            </div>
                            <img class="direct-chat-img"
                                 src="http://meliaagency.com/web/images/5e3affe0.png"
                                 alt="message user image">

                            <div class="direct-chat-text">
                                <?= $dialog->text ?>
                            </div>
                        </div>

                    <?php else: ?>
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                                    <span
                                                        class="direct-chat-name pull-left"><?= $user->name . " " . $user->lastname ?></span>
                                                    <span
                                                        class="direct-chat-timestamp pull-right"><?= Yii::$app->formatter->asDatetime($dialog->dateTime, "php:d.m.Y H:i"); ?></span>
                            </div>
                            <?php ?>
                            <?php if (!empty($avatar)): ?>
                                <img class="direct-chat-img"
                                     src="<?= Yii::getAlias('@web') . '/images/' . $user->userId . '/m_' . $avatar ?>"
                                     alt="message user image">
                            <?php else: ?>
                                <img class="direct-chat-img"
                                     src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                            <?php endif; ?>
                            <div class="direct-chat-text">
                                <?= $dialog->text ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <!-- Contacts are loaded here -->
            <div class="direct-chat-contacts">
                <ul class="contacts-list">
                    <?php for ($i = 0; $i < count($users); $i++): ?>
                        <li>
                            <form method="post">

                                <a class="view-dialog"
                                   href="<?= Url::toRoute(['admin/messages','type'=>$type,'id'=>$users[$i]->userId]); ?>">
                                    <input type="hidden" value="<?= $users[$i]->id ?>">
                                    <?php $file = $users[$i]->avatar->path ?>
                                    <?php if (!empty($file)): ?>
                                        <img class="contacts-list-img"
                                             src="<?= Yii::getAlias('@web') . '/images/' . $users[$i]->userId . '/m_' . $file ?> ">
                                    <?php else: ?>
                                        <img class="contacts-list-img"
                                             src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                                    <?php endif; ?>
                                    <div class="contacts-list-info">
                                                        <span class="contacts-list-name">
                                                          <?= $users[$i]->name . " " . $users[$i]->lastname ?>
                                                            <small
                                                                class="contacts-list-date pull-right"> <?= Yii::$app->formatter->asDatetime($texts[$i]->dateTime, "php:d.m.Y H:i"); ?></small>
                                                        </span>
                                                            <span
                                                                class="contacts-list-msg"><?= substr($texts[$i]->text, 0, 275) . ' ' ?>
                                                                <?php if ($texts[$i]->isReaded == 0 && $texts[$i]->toUserId == 1): ?>
                                                                <span class="badge bg-green">new</span></span>
                                                                <?php endif ?>
                                    </div>
                                </a>
                            </form>
                        </li>
                    <?php endfor; ?>
                </ul>
            </div>
            <!-- /.direct-chat-pane -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['admin/addmessage'])]); ?>


            <?= $form->field($model, 'toUserId')->hiddenInput(['value' => $user->userId])->label(false) ?>

            <?= $form->field($model, 'toUserType')->hiddenInput(['value' => $type])->label(false) ?>

            <?= $form->field($model, 'text')->textInput(['placeholder' => 'Введите текст ...'])->label(false) ?>


            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-flat send-message'], $options = ['onsubmit' => 'return false']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <?php endif; ?>

        </div>

    </div>
    <!-- /.box-footer-->
</div>