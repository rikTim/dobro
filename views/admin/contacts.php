<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactsSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content contacts-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-envelope"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,

                        'columns' => [

                            'id',
                            'name',
                            [
                                'attribute'=>'text',
                                'format'=>'ntext',
                                'value' => function ($data) {

                                    $count = iconv_strlen($data->text);
                                    if ($count > 140) {
                                        return Yii::$app->DLL->subStr($data->text);
                                    } else return $data->text;
                                },
                                'options'=>['width'=>'500'],
                            ],
                            'email:email',

                            'dateTime:datetime',

                            ['class' => 'yii\grid\ActionColumn',
                                'template' => '{delete}{view}',
                                'options' => ['width' => '50'],
                                'urlCreator' => function ($action, $model, $key, $index) {

                                    switch($action){

                                        case 'view':
                                            $url = Url::toRoute(['admin/view-contacts','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['admin/delete','id'=>$model->id]);
                                            return $url;

                                    }
                                }
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>