<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Рейтинг ' . $title;
?>

<section class="content angels-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Rating Recount', ['recount','type'=>$type], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'value' => 'id',
                                'filter' => false
                            ],
                            'name',
                            'lastname',
                            'phone',
                            'email:email',
                            [
                                'attribute' => 'id',
                                'format' => 'raw',
                                'label' => 'Добрые Дела',
                                'value' => function ($data) {
                                    $count = count(\app\models\UserHelp::getHelpForUser($data->userId));
                                    if ($count > 0) {
                                        return '<span class="label bg-green "> ' . $count . '</span>';
                                    } else {
                                        return 'Нет';
                                    }
                                },
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'id',
                                'format' => 'raw',
                                'label' => 'Рейтинг',
                                'value' => 'rating',
                                'filter' => false,
                            ],

                            [
                                'attribute' => 'id',
                                'label' => '',
                                'format' => 'raw',
                                'filter' => false,
                                'value' => function ($data) {
                                    return Html::a('Изменить', ['view', 'id' => $data->userId, 'type' => $data->type], ['class' => 'btn btn-primary']);
                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
