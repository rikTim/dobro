<?php


use yii\widgets\ListView;

$this->title = 'Партнеры проекта - Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'На этой страничке вы можете получить информацию о партнерах оказавших весомый вклад в наш проект']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>
<div class="site-index board">
    <div class="container">
        <div class="page-header">
            <h1>Партнеры</h1>
        </div>
        <div class="row">

                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
                    'itemView' => '_list',
                    'emptyText' => 'Список пуст',

                ]) ?>


        </div>
        </div>
        <div class="clr"></div>

    </div>