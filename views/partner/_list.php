<?php

use yii\helpers\Url;

?>
<div class="col-md-3">
    <a href="<?= Url::toRoute(['/partner/more', 'id' => $model->id]) ?>">
    <div class="partner">
        <div class="col-md-12" style="height: 50px"><h3 ><?= $model->name ?></h3></div>
        <img style="width: 200px;height:200px "
        src="<?= Yii::getAlias('@web/images/partner/' . $model->id . '/cropped/' . $model->avatar->path) ?>" alt="">

    </div>
    </a>
</div>