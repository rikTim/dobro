<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">

<!--                    --><?//= $model->avatar->path?>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [

                            'name',
                            'phone',
                            'url:url',
                            'avatar.path:image',
                            'text:ntext',
                        ],
                    ]) ?>
                    <h3>Фото</h3>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{pager}\n{summary}\n{items}",
                        'itemView' => '_admin_view',
                        'emptyText' => 'Список пуст',
                        'pager'=>[
                            'firstPageLabel' => 'Первая',
                            'lastPageLabel' => 'Последняя',
                            'nextPageLabel' => 'Следующая',
                            'prevPageLabel' => 'Предыдущая',
                            'maxButtonCount' => 5,
                        ],
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</section>

