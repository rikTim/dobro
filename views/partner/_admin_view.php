<?php

use yii\helpers\Html;

?>
<div class="col-md-3">
    <?= newerton\fancybox\FancyBox::widget([
        'target' => 'a[rel=fancybox]',
        'helpers' => true,
        'mouse' => true,
        'config' => [
            'maxWidth' => '90%',
            'maxHeight' => '90%',
            'playSpeed' => 7000,
            'padding' => 0,
            'fitToView' => false,
            'width' => '70%',
            'height' => '70%',
            'autoSize' => false,
            'closeClick' => false,
            'openEffect' => 'elastic',
            'closeEffect' => 'elastic',
            'prevEffect' => 'elastic',
            'nextEffect' => 'elastic',
            'closeBtn' => false,
            'openOpacity' => true,
            'helpers' => [
                'title' => ['type' => 'float'],
                'buttons' => [],
                'thumbs' => ['width' => 68, 'height' => 50],
                'overlay' => [
                    'css' => [
                        'background' => 'rgba(0, 0, 0, 0.8)'
                    ]
                ]
            ],
        ]
    ]); ?>

    <div class="form-active">
        <div class="image-user">
            <div class="icons">
                <i data-id="<?= $model->id ?>" class="delete-photo fa fa-trash-o"></i>
            </div>
                <span class="photo">
            <?= Html::a(Html::img(Yii::getAlias('@web/images/partner/' . $model->partnerId . '/cropped/' . $model->path)), Yii::getAlias('@web/images/partner/' . $model->partnerId . '/' . $model->path), ['rel' => 'fancybox']); ?>
                    <div class="clr"></div>
        </span>
        </div>
        <div class="clr"></div>
    </div>
</div>