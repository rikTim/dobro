<?php

use yii\widgets\ListView;

$this->title = 'DobroUA.com Партнер '.$model->name ;
$this->registerMetaTag(['name' => 'description', 'content' => 'DobroUA.com Партнер '.$model->name]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>
<div class="section wall more partner">
    <div class="container">
        <div class="row">
            <!-- Photo col -->
            <div class="col-xs-12 col-sm-4">
                <div class="row">
                    <div class="img">
                        <center>
                            <?php if(!empty($model->avatar->path) && file_exists(Yii::getAlias('@app/web/images/partner/' . $model->id . '/' . $model->avatar->path))): ?>
                                <img src="<?= Yii::getAlias('@web') . '/images/partner/' . $model->id . '/m_' . $model->avatar->path ?>" alt="avatar" class="img-rounded img-responsive img-centered">
                            <?php else: ?>
                                <img src="<?= Yii::getAlias('@web') . '/images/noavatar.png' ?>" alt="avatar" class="img-rounded img-responsive img-centered">
                            <?php endif; ?>
                        </center>

                    </div>
                    <script type="text/javascript">(function(w,doc) {
                            if (!w.__utlWdgt ) {
                                w.__utlWdgt = true;
                                var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                                s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
                                var h=d[g]('body')[0];
                                h.appendChild(s);
                            }})(window,document);
                    </script>
                    <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="false" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="rectangle" data-sn-ids="fb.vk.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1486815" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>
                </div>
            </div>
            <!-- /Photo col -->

            <!-- Info col -->
            <div class="col-xs-12 col-sm-8">
                <div class="info">
                    <?= $this->render('_info', [
                        'model' => $model
                    ]) ?>
                </div>


            </div>

        </div>
        <div class="clr"></div>
        <div class="info">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{pager}\n{summary}\n{items}",
                'itemView' => '_photo',
                'emptyText' => 'Список пуст',
                'pager'=>[
                    'firstPageLabel' => 'Первая',
                    'lastPageLabel' => 'Последняя',
                    'nextPageLabel' => 'Следующая',
                    'prevPageLabel' => 'Предыдущая',
                    'maxButtonCount' => 5,
                ],
            ]) ?>
        </div>
    </div>

</div>
