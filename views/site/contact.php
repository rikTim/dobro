<?php

use app\components\ContactWidget;

$this->title = 'Связь с администратором';
?>

<div class="site-index">

    <div class="container">
        <h1><?= $this->title; ?></h1>
    </div>

    <div class="section contact">
        <div class="container">
            <p><?= $model->text; ?></p>
        </div>
    </div>

    <?php if(!Yii::$app->user->isGuest): ?>
        <?= ContactWidget::widget(); ?>
    <?php endif; ?>
</div>
