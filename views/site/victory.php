<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Люди которым уже помогли';
?>


    <div class="container b_fixed">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box-header with-border">

                    <h1 class="box-title text-center"><?= Html::encode($this->title) ?></h1><br>

                    <div class="box-tools">
                        <? //= Html::a('Create Volunteer', ['create'], ['class' => 'btn btn-success']) ?><!--</p>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-sm-4">
                    <img class="thumbnail" alt="foto" src="/img/victory.jpg">
                </div>
                <div class="col-sm-8">
                    <h2 class="text-center text-uppercase"><br><br>
                        Самая добрая страничка нашего сайта.</h2>
                    <p class="text-left">Здесь люди которым каким- либо способом была оказана помощь и теперь они здоровы и с ними все в порядке.
                        Возможно, кто-то на реабилитации, но им уже ничего не страшно.
                        Скорее всего очень много людей приложило не мало усилий чтобы помочь этим людям. И теперь все случилось - лучик света заглянул и в их жизнь тоже.
                        Желаем всем больным - выздороветь. Грустным - улыбнуться. Уставшим - отдохнуть.
                        Верьте в чудеса - они сбываются!</p>
                </div>
            </div>
        </div>
        <div class="b_persons_list">

            <?= ListView::widget([
                'dataProvider' => $needies,
                'itemView' => '_list_victory',
                'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
                'emptyText' => 'Список пуст',
            ]) ?>
        </div>
        <div class="clearboth"></div>
    </div>

