<?php

use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Благотворительный проект - Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'Мы оказываем помощь детям и взрослым, волонтерская организация добрых людей поможет людям оказавшимся в трудной ситуации.']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);

?>
<div style='display:none'><h1>Благотворительный проект - Мир добрых людей</h1></div>
<div class="b_help_list_main">
    <div class="container b_fixed">
        <div class="row">
            <h2>
                <div class="pull-left"><a href="<?= Url::toRoute('/needies/list') ?>">Помоги им</a>
                    <div class="b_help_text">Чтобы быть счастливым, Миру нужно отдавать больше, чем берешь.</div>
                </div>
                <?php if (Yii::$app->user->isGuest): ?>
                    <div class="pull-plus pull-right">
                    <a class="btn btn_apply_for_help" href="<?= Url::toRoute('user/register') ?>" role="button">Мне нужна помощь</a>
                    </div>
                <?php endif; ?>
            </h2>
        </div>
    </div>

    <div class="b_pict b_needed_help gradient hidden-xs">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <?= $this->render('_needy_view', ['needy' => $needyMain]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="b_about_desc">
    <div class="container b_fixed">
        <div class="b_about_desc_content"><a id="about"></a>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="desc">
                    <h3>О проекте</h3>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="desc">
                    <p>Чтобы быть счастливым, Миру нужно отдавать больше, чем берешь. Это правило знакомо всем успешным
                        людям, но когда приходит осознание, первое время сложно понять, кому именно нужна помощь.
                        Просьбы на улице, в метро или в интернете часто оправданно не вызывают доверия. О системе работы
                        благотворительных фондов мало что известно. И тем не менее, тысячи людей в эту минуту страдают
                        от боли, голодают, им негде спрятаться от непогоды. Многие продали последнее имущество ради
                        спасения близкого человека.
                    </p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-md-offset-2 col-sm-push-0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe allowfullscreen="" class="embed-responsive-item" src="https://www.youtube.com/embed/2RwEXPeYqWM"></iframe>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="desc"><br>
                        <p>Этот сайт создан людьми, которые давно помогают тем, кто попал в беду и знают как сложно не
                            ошибиться в выборе реципиента. Вся работа над созданием проекта ведется добровольцами бесплатно.
                            Объявления, которые ты здесь видишь, проверены волонтерами. Они лично убедились в том, что людям
                            нужна помощь. Средства, которые ты передаешь, попадают напрямую на счет тех, кому они
                            предназначаются. Через этот сайт деньги не проходят. Здесь ты можешь только оставить просьбу или
                            найти того, кому можно помочь. Мы хотим, чтобы люди знали тех, кто делает этот мир лучше.
                            Поэтому, если ты позволишь, мы напишем благодарность тебе на сайте.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <img alt="Мы уже помогли" class="img-responsive" src="<?= Yii::getAlias('@web') . '/img/vbig.jpg' ?>"/>
            </div>
            <div class="victory-info">
                <h2 class="vbtn text-uppercase text-center"><a class="btn btn_apply_for_victory" href="<?= Url::toRoute('site/victory') ?>" role="button" style="color:#FFA500">Мы уже помогли</a></h2></div>
        </div>
    </div>
</div>

<div class="container-fluid good-dress">
    <div class="row">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <img alt="Добрые платья" class="img-responsive" src="<?= Yii::getAlias('@web') . '/img/dress.png' ?>"/>
                </div>
                <div class="good-dress-info">
                    <h2 class="text-uppercase text-center"><a class="btn btn_apply_for_dress" href="<?= Url::toRoute('shop/shop') ?>" role="button" style="color:#00baff">Добрые платья</a></h2></div>
            </div>
        </div>
    </div>
</div>

<div class="b_rating_main gradient">
    <div class="container b_fixed">
        <i class="wave"></i>
        <h2><span>Рейтинг</span>
            <div class="b_help_text">Рейтинг на DobroUa - это возможность показать свою активность в Мире добрых
                людей. Мы хотим знать своих героев! Повысить свой рейтинг можно оказывая материальную помощь людям,
                проверяя заявки, проверяя целевое расходование перечисленных средств, оставляя комментарии, сообщая
                о состоянии здоровья тех, кто оставлял заявку и получил помощь, сообщая про успехи и разочарования,
                делая свои предложения по улучшению портала, делая реальные шаги для расширения границ Мира добрый
                людей. С помощью рейтинга ты сможешь видеть размер своего позитивного вклада в этот мир!
            </div>
        </h2>
        <div class="b_rating_slider">
            <ul class="b_top_rating_title">
                <li class="b_volunteers">
                    <div>Топ</div>
                    <a href="<?= Url::toRoute('volunteers/list') ?>" style="color: #00baff;">
                        <span>Волонтеров</span></a></li>
                <li class="b_angels">
                    <div>Топ</div>
                    <a href="<?= Url::toRoute('angels/list') ?>">
                        <span>Ангелов</span></a></li>
            </ul>
            <div class="clearboth"></div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="b_volunteers_list">

                            <?= ListView::widget([
                                'dataProvider' => $topVolunteer,
                                'itemView' => '_top_volunteer',
                                'layout' => "{items}",
                                'emptyText' => 'Список пуст',
                            ]) ?>


                        </div>
                        <div class="b_angels_list">
                            <?= ListView::widget([
                                'dataProvider' => $topAngels,
                                'itemView' => '_top_angel',
                                'layout' => "{items}",
                                'emptyText' => 'Список пуст',
                            ]) ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="b_news_list_main">
    <div class="container b_fixed">
        <h2 class="text-uppercase head-news">новости</h2>

        <!--        <div class="b_news_item">-->
        <?= ListView::widget([
            'dataProvider' => $news,
            'itemView' => '_news',
            'layout' => "{items}",
            'emptyText' => 'Список пуст',
            'options' => ['style' => 'margin-bottom: 40px;'],

        ]) ?>
        <!--        </div>-->

    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 news-container">

                <p class="text-center"><a class="news_btn" href="<?= Url::toRoute('news/list') ?>">Больше новостей</a></p>

                <div class="clear"></div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bloc-memory">
                <div class="row">
                    <img alt="Светлая память" class="img-responsive" src="<?= Yii::getAlias('@web') . '/img/memory_big2.jpg' ?>"/>
                </div>
                <div class="memory-info">
                    <h2 class="text-uppercase text-center"><a class="btn btn_apply_for_memory" href="<?= Url::toRoute('site/memory') ?>" role="button">Светлая память</a></h2></div>
            </div>
        </div>
    </div><br><br>
</div>
</div>
</div>
<main class="content">
</main>


