<?php
use yii\widgets\ActiveForm;

$this->title = 'DobroUA | мир добрых людей';

?>

<!-- Services Section -->
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">О нас</h2>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-12">
                <p class="text-muted">Самый простой способ стать счастливым - протянуть руку помощи тому, кто в этом действительно нуждается.</p>
                <p class="text-muted">Пока ты читаешь эти строки, мы строим новый проект, призванный помогать тем, кому это необходимо.</p>
                <p class="text-muted">17 февраля - День спонтанного проявления доброты. Мы решили, что стартуем именно в этот день. НО чтобы все успеть нам очень нужна твоя помощь. Если ты активный человек с добрым сердцем, хочешь помогать людям, но не знаешь с чего начать, тогда присоединяйся к нам. Мы ищем волонтеров. Задач много, а с каждой потерянной минутой мы упускаем возможность помочь кому-то, кто в этом очень нуждается.</p>
            </div>
        </div>
    </div>
</section>


<!-- Contact Section -->
<section id="subscribe" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">ПОДПИСКА НА НОВОСТИ</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => true
                ]); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= $form->field($model, 'name')->textInput(['value' => '', 'placeholder'=>'Имя', 'required' => 'required'])->label(false) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'email')->input('email', ['value' =>'', 'placeholder'=>'Email', 'required' => 'required'])->label(false) ?>
                                <?= $form->field($model, 'type')->hiddenInput(['value'=> 1])->label(false);?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl">Send Message</button>

                            <p style="color: #fed136;; font-size: 20px;"><?= Yii::$app->session->getFlash('subscribe') ?></p>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">СВЯЖИСЬ С НАМИ</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => true
                ]); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= $form->field($model, 'name')->textInput(['value' => '', 'placeholder'=>'Имя', 'required' => 'required'])->label(false) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'email')->input('email', ['value' =>'', 'placeholder'=>'Email', 'required' => 'required'])->label(false) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'message')->textarea(['value' =>'', 'placeholder'=>'Сообщение', 'required' => 'required'])->label(false) ?>
                                <?= $form->field($model, 'type')->hiddenInput(['value'=> 2])->label(false);?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl">Send Message</button>
                            <p style="color: #fed136; font-size: 20px;"><?= Yii::$app->session->getFlash('contact') ?></p>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>