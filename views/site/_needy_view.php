<?php

use yii\helpers\Url;

?>
<?php foreach($needy as $key => $need ) :?>
    <?php $pos = $key+1;?>

<div class="b_posit_<?=$pos?>"><a href="<?= Url::toRoute(['needies/more', 'id' => $need->id]) ?>">
        <?php if(!empty($need->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $need->userId . '/' . $need->file->path))): ?>
            <img src="<?= Yii::getAlias('@web') . '/images/' . $need->userId . '/m_' . $need->file->path ?>"  >
        <?php else: ?>
            <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>"  >
        <?php endif; ?>

        <div class="b_title"><?= $need->name . ' ' . $need->lastname ?><br><?= $need->age;?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $need->age) ?>, <?= $need->city?> </div>
        <div class="b_fon"></div>
    </a>
</div>

<?php endforeach;?>
