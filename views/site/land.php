<?php
use yii\widgets\ActiveForm;

$this->title = 'DobroUA | мир добрых людей';

?>

<!-- Wrapper -->
<div class="wrapper style1">
    <!-- Banner -->
    <div id="banner" class="container">
        <h1 style="font-size: 50px"><a href="http://dobroua.com/">Dobro<i style="color: #00a2ff">U</i><i style="color: #fff000">A</i></a></h1>
        <br>
        <section>
            <p>Проект скоро стартует. Подпишись, если хочешь быть в курсе всех новостей</p>
            <p>До старта осталось</p>
            <div id="CDT"></div>
        </section>
    </div>
</div>


<div id="footer" class="wrapper style4">
    <div class="container">
        <section>
            <p>Самый простой способ стать счастливым - протянуть руку помощи тому, кто в этом действительно нуждается.</p>
            <p>Пока ты читаешь эти строки, мы строим новый проект, призванный помогать тем, кому это необходимо.</p>
            <p>17 февраля - День спонтанного проявления доброты. Мы решили, что стартуем именно в этот день. НО чтобы все успеть нам очень нужна твоя помощь. Если ты активный человек с добрым сердцем, хочешь помогать людям, но не знаешь с чего начать, тогда присоединяйся к нам. Мы ищем волонтеров. Задач много, а с каждой потерянной минутой мы упускаем возможность помочь кому-то, кто в этом очень нуждается. </p>
        </section>
    </div>
</div>

<!-- subscribe -->
<div id="footer" class="wrapper style2">
    <div class="container">
        <section>
            <header class="major">
                <h2>Подписаться на обновления</h2>
            </header>
            <?php $form = ActiveForm::begin([
                'enableClientValidation' => true
            ]); ?>
            <div class="row half">
                <div class="12u">
                    <?= $form->field($model, 'name')->textInput(['value' => '', 'placeholder'=>'Имя'])->label(false) ?>
                </div>
            </div>
            <div class="row half">
                <div class="12u">
                    <?= $form->field($model, 'email')->textInput(['value' =>'', 'placeholder'=>'Email'])->label(false) ?>
                    <?= $form->field($model, 'type')->hiddenInput(['value'=> 1])->label(false);?>
                </div>
            </div>
            <div class="row half">
                <div class="12u">
                    <ul class="actions">
                        <li>
                            <input type="submit" value="Подписаться" class="button alt" />
                        </li>
                    </ul>
                    <p style="color: #fff; font-size: 20px;"><?= Yii::$app->session->getFlash('subscribe') ?></p>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </section>
    </div>
</div>


<!-- contacts -->
<div id="footer" class="wrapper style3">
    <div class="container">
        <section>
            <header class="major">
                <h2 style="color: #337ab7">Свяжись с нами</h2>
            </header>
            <?php $form = ActiveForm::begin([
                'enableClientValidation' => true
            ]); ?>
            <div class="row half">
                <div class="12u">
                    <?= $form->field($model, 'name')->textInput(['value' => '', 'placeholder'=>'Имя'])->label(false) ?>
                </div>
            </div>
            <div class="row half">
                <div class="12u">
                    <?= $form->field($model, 'email')->textInput(['value' =>'', 'placeholder'=>'Email'])->label(false) ?>
                </div>
            </div>
            <div class="row half">
                <div class="12u">
                    <?= $form->field($model, 'message')->textarea(['value' =>'', 'placeholder'=>'Сообщение'])->label(false) ?>
                    <?= $form->field($model, 'type')->hiddenInput(['value'=> 2])->label(false);?>
                </div>
            </div>
            <div class="row half">
                <div class="12u">
                    <ul class="actions">
                        <li>
                            <input type="submit" value="Отправить сообщение" class="button alt" />
                        </li>
                    </ul>
                    <p style="color: #337ab7; font-size: 20px;"><?= Yii::$app->session->getFlash('contact') ?></p>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </section>
    </div>
</div>
