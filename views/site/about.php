<?php

$this->title = Yii::$app->params['siteName'] . ' ' . $model->title;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="site-index">

    <div class="jumbotron">
        <div class="container">
            <h1><?= $this->title; ?></h1>
        </div>
    </div>

    <div class="container body-content about">
        <div class="row">
            <p><?= $model->text; ?></p>
        </div>
    </div>
</div>
