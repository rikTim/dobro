<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;


$this->title = 'Светлая память';
?>

    <div class="container b_fixed">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box-header with-border">

                    <h1 class="box-title text-center"><?= Html::encode($this->title) ?></h1><br>

                    <div class="box-tools">
                        <? //= Html::a('Create Volunteer', ['create'], ['class' => 'btn btn-success']) ?><!--</p>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-sm-4">
                    <img class="thumbnail" alt="foto" src="/img/svecha.png">
                </div>
                <div class="col-sm-8">
                    <h2 class="text-center text-uppercase"><br><br>Светлая память.</h2>
                    <p class="text-left">Слова здесь не уместны. Люди, которым не успели помочь.
                        Вернуть бы тех… кого забрали небеса… Хоть на минутку — лишь увидеть лица… Чтоб посмотреть в давно забытые глаза… Сказать три слова… и отпустить их… к птицам…
                        Терпения близким.</p>
                </div>
            </div>
        </div>
        <div class="b_persons_list">

            <?= ListView::widget([
                'dataProvider' => $needies,
                'itemView' => '_list_memory',
                'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
                'emptyText' => 'Список пуст',
            ]) ?>
        </div>
        <div class="clearboth"></div>
    </div>