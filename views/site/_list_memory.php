
<?php
use yii\helpers\Url;
?>

<div class="col-md-6 col-sm-6 col-xs-12 b_persons">
    <div class="b_persons_item">
        <div class="b_pic"><a href="<?= Url::to(['needies/more', 'id' => $model->id])?>">
                <?php if(!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/m_' . $model->file->path))): ?>
                <img src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>" style="width: 106px" ></a></div>
        <?php else: ?>
        <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>" style="width: 106px" class="img-responsive"></a></div>
    <?php endif; ?>
    <div class="b_desc_content">
        <div class="b_title"><a href="<?= Url::to(['needies/more', 'id' => $model->id])?>" class="text-center"><?= $model->name . ' ' . $model->lastname ?></a>
            <span class="b_locate" style="font-size:13px"><?= $model->age . ' ' . Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
            <p class="text-center"><img src="<?= Yii::getAlias('@web') . '/img/rose.png'?>"><br><small class="text-center">Светлая память</small></p>
        </div>
    </div>
</div>
</div>
