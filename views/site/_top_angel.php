<?php
use yii\helpers\Url;

?>

<div class="b_persons_item">
    <div class="b_pic"><a href="<?= Url::toRoute(['angels/more', 'id' => $model->id]) ?>">
            <?php if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
                <img style="width: 106px" src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>"></a></div>
            <?php else: ?>
                <img  style="width: 106px" src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>"></a></div>
            <?php endif; ?>
            <div class="b_desc_content">
                <a href="<?= Url::toRoute(['angels/more', 'id' => $model->id]) ?>">
                <div class="b_title"><?= $model->name . ' ' . $model->lastname ?></div></a>
                <div class="b_desc"><span class="b_rating">
                    <?php if(!empty($model->rating)): ?>
                    Рейтинг: <?= $model->rating ?>
                    <?php endif;?>

                            </span></div>
            </div>
    </div>

