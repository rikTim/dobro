<?php
use yii\helpers\Url;
?>

<div class="col-md-6 b_news_item" style="height: 190px!important;">
    <div class="b_date"><?=Yii::$app->formatter->asDatetime($model->dateTime, "php:M d ")?></div>
    <div class="b_title"><a href="<?= Url::toRoute(['news/more', 'id' => $model->id]) ?>"><?= $model->title ?></a></div>
    <div class="b_text"><?= strip_tags(Yii::$app->DLL->subStr($model->text, 300)) ?>
    </div>

</div>