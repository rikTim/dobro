<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descr')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]);
    ?>


    <div class="form-group field-tasks-datetime">
        <label class="control-label" for="tasks-datetime">Дата</label>
        <?php
        if(!empty( $model->dateTime)){
            $model->dateTime = Yii::$app->formatter->asDatetime($model->dateTime, "php:m/d/Y ");}
        ?>
        <?echo MaskedInput::widget([
            'model' => $model,
            'attribute' => 'dateTime',
            'name' => 'dateTime',
            'mask' => '99/99/9999',
        ]);
        ?>
        <div class="help-block"></div>
    </div>


    <?php $file = $model->filePath; ?>
    <?php if (!empty($file)): ?>
        <div class="form-active">
            <p><label class="control-label">Фото</label></p>

            <div class="image-user">
                <div class="icons">

                </div>
                <span class="photo">
                    <a class="profile-gallery-item"
                       href="<?= Yii::getAlias('@web') . '/images/tasks/' . $file ?>"
                       data-fancybox-group="gallery" title="">
                        <img
                            src="<?= Yii::getAlias('@web') . '/images/tasks/m_' . $file ?>"
                            alt="">
                    </a>
                </span>
            </div>
        </div>
    <?php else: ?>
        <p><label class="control-label">Аватар</label></p>
        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
    <?php endif; ?>
    <div class="clr"></div>

    <?= $form->field($upload, 'imageFile')->fileInput()->label(false) ?>

    <?= $form->field($model, 'isActive')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
