<?php

use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'DobroUA.com Задания для волонтеров';
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>


<div class="site-index">
    <!-- Tasks block -->
    <div class="container">
        <div class="page-header">
            <h1>Задания для волонтеров</h1>
        </div>
    </div>

    <div class="section">
        <div class="container task list">
            <?php if(!empty($models)):  ?>
                <?php foreach($models as $model): ?>

                    <div class="media">
                        <div class="media-left">
                            <a href="<?= Url::toRoute(['tasks/more', 'id' => $model->id]) ?>">
                                <?php if(!empty($model->filePath) && file_exists(Yii::getAlias('@app/web/images/tasks/' . $model->filePath))): ?>
                                    <img class="media-object img-rounded"
                                         src="<?= Yii::getAlias('@web/images/tasks/m2_' . $model->filePath) ?>"
                                         data-holder-rendered="true"
                                         style="width: 128px;">
                                <?php else: ?>
                                    <img class="media-object img-rounded"
                                         src="<?= Yii::getAlias('@web/images/no-image.jpeg') ?>" alt="avatar"
                                         data-holder-rendered="true"
                                         style="width: 128px; height: 128px;">
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?= Url::toRoute(['tasks/more', 'id' => $model->id]) ?>"><?= $model->title ?></a></h4>

                            <?php if(!empty($model->dateTime)): ?>
                                <small><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></small>
                            <?php endif; ?>

                            <p><?= Yii::$app->DLL->subStr($model->descr) ?></p>

                            <?php /** Если не заполнен профайл не могу подписаться на задания */ ?>
                            <?php $profile = Yii::$app->DLL->getProfile(); ?>
                            <?php if(!empty($profile)): ?>
                                <?php if(!in_array($userId, ArrayHelper::map($model->userTask, 'id', 'userId'))): ?>
                                    <?= Html::a("Могу помочь <span class='badge'>" . count($model->userTask) . "</span>", ['tasks/apply', 'id' => $model->id, 'action' => 'subscribe'], ['class' => 'btn btn-primary text-right']);?>
                                <?php else: ?>
                                    <?= Html::a("Не могу помочь<span class='badge'></span>", ['tasks/apply', 'id' => $model->id, 'action' => 'unsubscribe'], ['class' => 'btn btn-primary text-right']);?>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                <?php endforeach; ?>

                <div style="height: 50px"></div>
            <?php else: ?>
                <h3>Нет активных заданий</h3>
            <?php endif; ?>
        </div>
    </div>

    <!-- Pagination block -->
    <div class="container">
        <div class="row">
            <?= LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        </div>
    </div>
    <!-- /Pagination block -->

    <!-- /Tasks block -->
</div>
