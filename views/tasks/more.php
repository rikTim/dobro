<?php

use yii\widgets\Pjax;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use app\components\SubscribersWidget;
use \app\components\TaskCommentWidget;

$this->title = 'DobroUA.com Задание ' . $model->title;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>


<div class="site-index">
    <div class="container">
        <div class="page-header">
            <h1><?= 'Задание ' . $model->title ?></h1>
        </div>
    </div>

    <!-- Tasks block -->
    <div class="container task more">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div id="gallery">
                <?php if(!empty($model->filePath) && file_exists(Yii::getAlias('@app/web/images/tasks/' . $model->filePath))): ?>
                    <a href="<?= Yii::getAlias('@web/images/tasks/m_' . $model->filePath) ?>">
                        <img src="<?= Yii::getAlias('@web/images/tasks/' . $model->filePath) ?>" alt="" >
                    </a>
                <?php else: ?>
                    <img src="<?= Yii::getAlias('@web/images/no-image.jpeg') ?>" alt="avatar" class="img-rounded">
                <?php endif; ?>
                    </div>
                <script type="text/javascript">(function (w, doc) {
                        if (!w.__utlWdgt) {
                            w.__utlWdgt = true;
                            var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                            s.type = 'text/javascript';
                            s.charset = 'UTF-8';
                            s.async = true;
                            s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                            var h = d[g]('body')[0];
                            h.appendChild(s);
                        }
                    })(window, document);
                </script>
                <div data-background-alpha="0.0" data-buttons-color="#FFFFFF"
                     data-counter-background-color="#ffffff" data-share-counter-size="12"
                     data-top-button="false" data-share-counter-type="separate" data-share-style="1"
                     data-mode="share" data-like-text-enable="false" data-mobile-view="false"
                     data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000"
                     data-share-shape="rectangle" data-sn-ids="fb.vk.ok." data-share-size="20"
                     data-background-color="#ffffff" data-preview-mobile="false"
                     data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1486815"
                     data-counter-background-alpha="1.0" data-following-enable="false"
                     data-exclude-show-more="true" data-selection-enable="true"
                     class="uptolike-buttons"></div>
            </div>

            <div class="col-xs-12 col-sm-8">
                <p><?= $model->descr ?></p>
                <p>
                    <?php if(!empty($model->dateTime)): ?>
                        <span class="date">Дата события: <?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></span>
                    <?php endif; ?>


                    <?php /** Если не заполнен профайл не могу подписаться на задания */ ?>
                    <?php $profile = Yii::$app->DLL->getProfile(); ?>
                    <?php if(!empty($profile)): ?>
                        <?php if(!in_array($userId, ArrayHelper::map($model->userTask, 'id', 'userId'))): ?>
                            <?= Html::a("Могу помочь <span class='badge'>" . count($model->userTask) . "</span>", ['tasks/apply', 'id' => $model->id, 'action' => 'subscribe'], ['class' => 'btn btn-primary text-right' ,'style'=>'color:#fff']);?>
                        <?php else: ?>
                            <?= Html::a("Не могу помочь<span class='badge'></span>", ['tasks/apply', 'id' => $model->id, 'action' => 'unsubscribe'], ['class' => 'btn btn-primary text-right']);?>
                        <?php endif; ?>

                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>
    <?php if(!empty($profile)): ?>
    <?= TaskCommentWidget::widget(['taskId' => $model->id]); ?>
    <?php endif; ?>

    <?= SubscribersWidget::widget(['model' => $model->userTask]); ?>

</div>
