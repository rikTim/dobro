<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TasksSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Задания';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content static-pages-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-medkit"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Создать задание', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <div class="box-body">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [

                            [
                                'attribute' => 'id',
                                'filter' => false,
                            ],
                            'title',
                            [
                                'attribute' => 'descr',
                                'format' => 'ntext',
                                'value' => function ($data) {
                                    return Yii::$app->DLL->subStr($data->descr);
                                },
                                'options' => ['width' => '400'],
                            ],
                            [
                                'attribute' => 'dateTime',

                                'value' => function ($data) {
                                    if (!empty($data->dateTime)) {
                                        return Yii::$app->formatter->asDatetime($data->dateTime, "php:m/d/Y ");
                                    } else {
                                        return ' ';
                                    }
                                },
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'dateTime',
                                    'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'd/mm/yyyy'
                                    ]
                                ]),
                                'options' => ['width' => '300'],

                            ],
                            [
                                'attribute' => 'id',
                                'format' => 'raw',
                                'label'=> 'Волонтеры',
                                'value' => function ($data) {
                                    $count = count($data->userTask);
                                    if ($count > 0) {
                                        return '<span class="label bg-green "> ' . $count . '</span>';
                                    } else {
                                        return 'Нет';
                                    }
                                },
                                'filter' => false,

                            ],
                            [
                                'attribute' => 'isActive',
                                'format' => 'raw',
                                'label' => 'Активна',
                                'value' => function ($data) {
                                    switch ($data->isActive) {
                                        case 0:
                                            $isNew = 'Не активно';
                                            break;
                                        case 1:
                                            $isNew = ' <span class="label bg-green ">Активно</span>';
                                            break;
                                    }
                                    return $isNew;
                                },
                            ],



                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '120'],
                                'template' => ' {comment} {view} {update}  {delete}',
                                'buttons' => [
                                    'comment' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, [
                                            'title' => Yii::t('app', 'Комментарий'),
                                        ]);
                                    },

                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch($action){

                                        case 'view':
                                            $url = Url::toRoute(['tasks/view','id'=>$model->id]);
                                            return $url;
                                        case 'update':
                                            $url = Url::toRoute(['tasks/update','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['tasks/delete','id'=>$model->id]);
                                            return $url;
                                        case 'comment':
                                            $url = Url::toRoute(['taskcomment/create','id'=> $model->id]);
                                            return $url;
                                    }
                                }
                            ],

                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>
