<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content static-pages-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-medkit"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-bordered detail-view">
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <td><?= $model->id ?></td>
                        </tr>
                        <tr>
                            <th>Заглавие</th>
                            <td><?= $model->title ?></td>
                        </tr>
                        <tr>
                            <th>Текст</th>
                            <td><?= $model->descr ?></td>
                        </tr>
                        <tr>
                            <th>Время</th>
                            <td>
                                <?php if (!empty($model->dateTime)): ?>
                                    <?= Yii::$app->formatter->asDatetime($model->dateTime, "php:m/d/Y "); ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Фото</th>
                            <td><?php $file = $model->filePath ?>
                                <?php if (!empty($file)): ?>
                                    <a href="<?= Yii::getAlias('@web') . '/images/tasks/' . $file ?> "><img
                                            src="<?= Yii::getAlias('@web') . '/images/tasks/m_' . $file ?>"></a>
                                <?php else: ?>
                                    <div class="image-user">
                                        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>


                        <tr>
                            <th>Активна</th>
                            <td><?= $isActive ?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
            <?php if (!empty($users)): ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Откликнувшиеся волонтеры</h3>
                    </div>
                    <div class="box-body">
                        <div class="all-image">
                            <?php foreach ($users as $key =>$user): ?>
                                <span class="photo">
                                        <a class="profile-gallery-item"

                                           href=" <?= Yii::getAlias('@web') . '/' . $url[$key] .'?id=' . $user->id ?>"
                                           data-fancybox-group="gallery" title="">
                                            <h4><?= $user->name . ' ' . $user->lastname ?></h4>

                                            <?php if ($user->avatarId == 0): ?>
                                                <div class="image-user">
                                                    <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                                                </div>
                                            <?php else: ?>
                                                <div class="image-user">
                                                    <img
                                                        src=" <?= Yii::getAlias('@web') . '/images/' . $user->userId . '/m_' . $user->avatar->path ?>"
                                                        alt="">
                                                </div>
                                            <?php endif ?>
                                        </a>
                                    </span>

                            <?php endforeach; ?>

                        </div>


                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
