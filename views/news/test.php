<?php

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="section wall">
    <div class="container user-register test">

        <?php $form = ActiveForm::begin([
                'id' => 'registration-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true, /** должно быть true */
                'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>


        <?= $form->field($model, 'cityId')->dropDownList(['' => ''], ['prompt' => 'Город', 'class' => 'chosenSelect_2', 'id' => 'citiesList'])->label('Город', ['class' => 'required']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>