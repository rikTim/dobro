<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content angels-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-newspaper-o"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                        <?= Html::a('Создать Новость', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>


                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [


                        'id',
                        'title',
                        [
                            'attribute' => 'text',
                            'format' => 'ntext',
                            'value' => function ($data) {
                                    return Yii::$app->DLL->subStr($data->text);
                            },
                            'options' => ['width' => '400'],
                        ],

                        [
                            'attribute' => 'dateTime',
                            'format' => ['datetime', 'php:d/m/Y '],
                        ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
    </div>
</section>

