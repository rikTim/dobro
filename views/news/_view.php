<?php

use yii\helpers\Url;

?>

<div class="col-xs-2 col-sm-1 col-md-1 col-lg-1 date">
    <?php $date = Yii::$app->DLL->getMonth(Yii::$app->formatter->asDatetime($model->dateTime, "php:d.M")) ?>
    <span><?= $date[0] ?> <br><?= $date[1] ?></span>
</div>
<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
    <h3><a href="<?= Url::toRoute(['news/more', 'id' => $model->id]) ?>"><?= $model->title ?></a></h3>
    <p><?= strip_tags(Yii::$app->DLL->subStr($model->text, 300)) ?></p>
</div>

