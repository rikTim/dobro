<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-bordered detail-view">

                        <tbody>
                        <tr>
                            <th>Id</th>
                            <td><?= $model->id ?></td>
                        </tr>

                        <tr>
                            <th>Заглавие</th>
                            <td><?= $model->title ?></td>
                        </tr>

                        <tr>
                            <th>Текст</th>
                            <td><?= $model->text ?></td>
                        </tr>

                        <tr>
                            <th>Время</th>
                            <td><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d/m/Y "); ?></td>
                        </tr>
                        <tr>
                            <th>Фото</th>
                            <td><?php $file=$model->filePath?>
                                <?php if (!empty($file)): ?>
                                    <a href="<?= Yii::getAlias('@web') . '/images/news/' . $file ?> "><img src="<?= Yii::getAlias('@web') . '/images/news/m_' . $file ?>"></a>
                                <?php else: ?>
                                    <div class="image-user">
                                        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg'?>">
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
