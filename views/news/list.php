<?php

use yii\helpers\Url;

$this->title = 'Новости -  благотворительной организации Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'Здесь мы делимся своими новостями и что происходит с проектом Мир добрых людей']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="site-index">
    <div class="container">
        <div class="page-header">
            <h1>Новости</h1>
        </div>
    </div>
</div>
<!-- News block -->
<div class="section news">
    <div class="container">

        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $news,
            'itemView' => '_view',
            'options' => ['class' => 'news-items'],
                    'layout' => "{items}<div class=\"clear\"></div>{pager}",
            'emptyText' => 'Список пуст',
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'row',
            ],
        ]) ?>
    </div>
</div>
<!-- /News block -->