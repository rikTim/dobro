<?php

$this->title = 'DobroUA.com ' . $model->title;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);

?>

<div class="site-index">
    <div class="container">
        <div class="page-header">
            <h1><?= $model->title; ?></h1>
        </div>
    </div>

    <div class="section news">
        <div class="container">
            <div id="gallery">
            <?php $file=$model->filePath?>
            <?php if (!empty($file)): ?>
                <a  href="<?= Yii::getAlias('@web') . '/images/news/' . $file ?> "><img style="width: 240px" class="news-img" src="<?= Yii::getAlias('@web') . '/images/news/' . $file ?>"></a>
<!--                <a href="--><?//= Yii::getAlias('@web') . '/images/' . $photo->userId . '/' . $photo->path ?><!--">-->
<!--                    <img src="--><?//= Yii::getAlias('@web') . '/images/' . $photo->userId . '/' . $photo->path ?><!--" alt="--><?//= $photo->descr ?><!--" style="height: 50px; max-width: 50px;">-->
<!--                </a>-->
            <?php endif; ?>
                </div>
            <p><?= $model->text; ?></p>

            <script type="text/javascript">(function(w,doc) {
                    if (!w.__utlWdgt ) {
                        w.__utlWdgt = true;
                        var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                    }})(window,document);
            </script>
            <div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="1" data-mode="share" data-like-text-enable="false" data-mobile-view="false" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="rectangle" data-sn-ids="fb.vk.ok." data-share-size="20" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1486815" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>
        </div>
    </div>
</div>
