<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use vova07\imperavi\Widget;

$this->title = ' Изменить счетчик  : ' . $model->name . ' ' . $model->lastname;

/* @var $this yii\web\View */
/* @var $model app\models\Needies */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">

                    <?php $form = ActiveForm::begin(); ?>


                    <?= $form->field($model, 'name')->textInput(['disabled'=>'disabled']) ?>

                    <?= $form->field($model, 'lastname')->textInput(['disabled'=>'disabled']) ?>

                    <?= $form->field($model, 'need')->textInput() ?>

                    <?= $form->field($model, 'got')->textInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>

                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
