<?php

use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Нужна помощь, помочь нуждающимся людям - Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'В этом разделе сайта вы можете узнать каким людям нужна помощь и сделать свой вклад в благотворительный проект.']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="container b_fixed">
    <h1>Люди, которым нужна помощь
        <div class="b_help_text"> <?php if (Yii::$app->user->isGuest): ?>
                <p><a href="<?= Url::toRoute(['user/register', 'type' => 1]) ?>">Нужна помощь</a> - если ты или твой
                    близкий человек попал в беду и вы не можете справиться своими силами, регистрируйтесь, пишите нам
                    свою просьбу и мы сделаем все, чтобы вас услышали и нашелся Ангел, который сможет вам помочь. Из-за
                    частых случаев обманов, мы будем вынуждены проверить предоставленную вами информацию.</p>
            <?php else: ?>
                <p>Нужна помощь - если ты или твой близкий человек попал в беду и вы не можете справиться своими силами,
                    регистрируйтесь, пишите нам свою просьбу и мы сделаем все, чтобы вас услышали и нашелся Ангел,
                    который сможет вам помочь. Из-за частых случаев обманов, мы будем вынуждены проверить
                    предоставленную вами информацию.</p>
            <?php endif; ?>
        </div>
    </h1>
    <div class="row b_people_list">
            <?= ListView::widget([
                'dataProvider' => $needies,
                'itemView' => '_view_list',
                'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
                'emptyText' => 'Список пуст',
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => 'col-md-4 col-sm-6 col-xs-12 b_people_item',
                ],
            ]) ?>
    </div>
</div>

