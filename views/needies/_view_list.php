
<?php
use yii\helpers\Url;
?>

<div class="b_pic"><a href="<?= Url::toRoute(['more', 'id' => $model->id]) ?>">
        <?php if(!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/m_' . $model->file->path))): ?>
            <img src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>" alt="avatar" class="img-rounded img-responsive img-centered">
        <?php else: ?>
            <img src="<?= Yii::getAlias('@web') . '/images/noavatar.png' ?>" style="width: 249px;height: 249px" alt="avatar">
        <?php endif; ?>
        <div class="b_to_more"><span class="btn to_more">подробнее</span>
        </div>
    </a>
</div>
<div class="b_desc_content">
    <div class="b_title"><a href="<?= Url::toRoute(['more', 'id' => $model->id]) ?>"><?= $model->name . ' ' . $model->lastname ?></a></div>
    <div class="b_desc"><?= $model->shortTrouble ?></div>
</div>

