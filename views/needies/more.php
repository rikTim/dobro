<?php

use app\components\WallWidget;
use app\components\GalleryWidget;
use app\components\HelpWidget;
use app\components\VideoWidget;
use app\components\HelpFromHistoryWidget;

$this->title = 'DobroUA.com Нужна помощь ' . $model->name . ' ' . $model->lastname;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>
<section class="more-needes">
    <?php if($victory!=null):?>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

                                <?php if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
                                    <p class="text-center">
                                        <img class="img-responsive" src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>"/>
                                        <span class="tetx-primary"><b><?= $model->name . ' ' . $model->lastname ?></b></span><br>
                                        <span class="tetx-primary"><?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
                                    </p>
                                <?php else: ?>
                                    <p class="text-center">
                                        <img class="img-responsive" src="<?= Yii::getAlias('@web') . '/images/noavatar.png' ?>"/>
                                        <span class="text-primary"><b><?= $model->name . ' ' . $model->lastname ?></b></span><br>
                                        <span class="tetx-primary"><?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
                                    </p>

                                <?php endif; ?>

                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <img class="img-responsive pull-right" src="<?= Yii::getAlias('@web') . '/img/thank.jpg' ?>"/>
                                <blockquote class="pull-left"><br>
                                    Спасибо за то, что меня поддержали<br>
                                    И мне руку помощи добрую дали!<br>
                                    Моя благодарность — она безгранична.<br>
                                    У вас, я надеюсь, все будет отлично!<br>
                                    <br>
                                    И я поддержу вас, когда будет нужно.<br>
                                    Зачем еще честь нам и верная дружба?<br>
                                    Еще раз огромное просто спасибо.<br>
                                    Живите спокойно, легко и красиво!<br>
                                </blockquote>
                            </div>
                        </div>
                        <?= WallWidget::widget(['userId' => $model->userId]); ?>
                    </div>
                </div>
            </div>
        </div>

    <?php elseif ($memory!=null): ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                                <?php if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
                                    <p class="text-center">
                                        <img class="img-responsive" src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>"/>
                                        <span class="tetx-primary"><b><?= $model->name . ' ' . $model->lastname ?></b></span><br>
                                        <span class="tetx-primary"><?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
                                    </p>
                                <?php else: ?>
                                    <p class="text-center">
                                        <img class="img-responsive" src="<?= Yii::getAlias('@web') . '/images/noavatar.png' ?>"/>
                                        <span class="text-primary"><b><?= $model->name . ' ' . $model->lastname ?></b></span><br>
                                        <span class="tetx-primary"><?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
                                    </p>

                                <?php endif; ?>

                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <img class="img-responsive pull-right" src="<?= Yii::getAlias('@web') . '/img/forever.jpg' ?>"/>
                                    <blockquote><br>
                                        Зачем ты мне снишься, скажи, ну зачем?<br>
                                        Зачем будоражишь те раны,<br>
                                        Которые время залечит совсем,<br>
                                        А может останутся шрамы…<br>
                                        Тебя не забыть мне за тысячу лет.<br>
                                        Глаза и улыбку твою…<br>
                                        Зачем ты оставил в душе моей след,<br>
                                        Который запомню навечно!
                                    </blockquote>
                            </div>
                        </div>
                        <?= WallWidget::widget(['userId' => $model->userId]); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else:?>
        <div class="container b_fixed">
            <div class="b_person_desc col-md-8 col-sm-8 col-xs-12 pull-right">
                <div class="b_name"><?= $model->name . ' ' . $model->lastname ?><span
                        class="b_locate"><?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
                </div>
                <p><b>Возраст:</b>
                    <?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) ?></p>

                <p><b>Номер телефона: </b> <?= !empty($model->phone) ? $model->phone : 'не указан' ?> </p>
                <?= sizeof($model->address) > 0 ? '' : '<p><b>Адрес: </b>' . $model->address . '</p>' ?>
                <?= sizeof($model->email) > 0 ? '' : '<p><b>Email: </b>' . $model->email . '</p>' ?>

                <?php if (!empty($model->cardNum) && $model->cardNum != ' '): ?>
                    <p><b>Реквизиты:</b> <?= $model->cardNum ?> </p>
                <?php endif; ?>

                <?php if (!empty($model->trouble)): ?>
                    <p><b>Проблема: </b> <?= $model->trouble ?> </p>
                <?php endif; ?>

                <?php if (!empty($model->specificNeed)): ?>
                    <p><b>Необходимо: </b><?= $model->specificNeed ?> </p>
                <?php endif; ?>
                <?php if (!empty($model->vk)): ?>
                    <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><i class="fa social fa-vk"></i></a>
                <?php endif; ?>
                <?php if (!empty($model->fb)): ?>
                    <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><i
                            class="fa social fa-facebook-official"></i></a>
                <?php endif; ?>

                <div class="info" style="margin-top: 20px">
                    <!-- Help modal btn -->
                    <?= HelpWidget::widget(['userId' => $model->userId]) ?>
                    <!-- /Help modal btn -->
                </div>
                <?php if (!empty($video)): ?>
                    <?php parse_str(parse_url($video->path, PHP_URL_QUERY), $my_array_of_vars); ?>
                    <p class="b_video_bar">
                        <iframe src="https://www.youtube.com/embed/<?= $my_array_of_vars['v'] ?>" frameborder="0"
                                allowfullscreen></iframe>
                    </p>
                <?php endif; ?>
                <?= WallWidget::widget(['userId' => $model->userId]); ?>
            </div>
            <div class="b_left-side b_left_side_qustionare col-md-4 col-sm-4 col-xs-12">
                <div class="b_left_slider_person">
                    <div class="slider-pro" id="b_person-slider">
                        <div class="sp-slides">
                            <!-- Slide 1 -->
                            <div class="sp-slide">
                                <?php if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
                                    <img class="sp-image" src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>"/>

                                <?php else: ?>
                                    <img class="sp-image" src="<?= Yii::getAlias('@web') . '/images/noavatar.png' ?>"/>

                                <?php endif; ?>

                            </div>

                        </div>
                    </div>

                </div>
                <?php if (!empty($photos)): ?>
                    <?= GalleryWidget::widget(['photos' => $photos]); ?>
                <?php endif; ?>
                <?php if (!empty($model->need) && !empty($model->got)): ?>
                    <div class="b_progress_amount">
                        <label><b>Собрано:</b> <?= $model->got . ' грн' ?> из <?= $model->need . ' грн' ?> </label>
                        <?php $percent = $model->got / $model->need * 100 ?>
                        <div class="b_progress_place">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0"
                                     aria-valuemax="<?= $model->need ?>" style="width: <?= $percent; ?>%;">

                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="b_helpers">
                    <h2>Перечисленные средства:</h2>
                    <div class="b_helpers_wrapper">
                        <div class="b_persons_list">
                            <?= \yii\widgets\ListView::widget([
                                'dataProvider' => $helps,
                                'itemView' => '_view_more',
                                'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
                                'emptyText' => 'Этот человек нуждается  помощи',
                                'itemOptions' => [
                                    'tag' => 'div',
                                    'class' => 'b_persons_item',
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    <?php endif;?>
</section>
<!--<section class="wall needies">-->
<!--    <div class="container user-info">-->
<!--        <div class="row">-->
<!--       -->
<!--            <div class="col-xs-12 col-sm-4 online">-->
<!--                <div class="img">-->
<!--                    <center>-->
<!--                        --><?php //if (!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/' . $model->file->path))): ?>
<!--                            <img-->
<!--                                src="--><?//= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?><!--"-->
<!--                                alt="avatar" class="img-rounded img-responsive img-centered">-->
<!--                        --><?php //else: ?>
<!--                            <img src="--><?//= Yii::getAlias('@web') . '/images/noavatar.png' ?><!--" alt="avatar"-->
<!--                                 class="img-rounded img-responsive img-centered">-->
<!--                        --><?php //endif; ?>
<!---->
<!--                        --><?php //if (!empty($photos)): ?>
<!--                            --><?//= GalleryWidget::widget(['photos' => $photos]); ?>
<!--                        --><?php //endif; ?>
<!---->
<!---->
<!--                        <script type="text/javascript">(function (w, doc) {-->
<!--                                if (!w.__utlWdgt) {-->
<!--                                    w.__utlWdgt = true;-->
<!--                                    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';-->
<!--                                    s.type = 'text/javascript';-->
<!--                                    s.charset = 'UTF-8';-->
<!--                                    s.async = true;-->
<!--                                    s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';-->
<!--                                    var h = d[g]('body')[0];-->
<!--                                    h.appendChild(s);-->
<!--                                }-->
<!--                            })(window, document);-->
<!--                        </script>-->
<!--                        <div data-background-alpha="0.0" data-buttons-color="#FFFFFF"-->
<!--                             data-counter-background-color="#ffffff" data-share-counter-size="12"-->
<!--                             data-top-button="false" data-share-counter-type="separate" data-share-style="1"-->
<!--                             data-mode="share" data-like-text-enable="false" data-mobile-view="false"-->
<!--                             data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000"-->
<!--                             data-share-shape="rectangle" data-sn-ids="fb.vk.ok." data-share-size="20"-->
<!--                             data-background-color="#ffffff" data-preview-mobile="false"-->
<!--                             data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1486815"-->
<!--                             data-counter-background-alpha="1.0" data-following-enable="false"-->
<!--                             data-exclude-show-more="true" data-selection-enable="true"-->
<!--                             class="uptolike-buttons"></div>-->
<!--                    </center>-->
<!--                </div>-->
<!--                --><?php //$online = Yii::$app->DLL->getOnline($model->userId);
//                if ($online == 1):
//                    ?>
<!--                    <span class="label label-success label-online-needy">Online</span>-->
<!--                --><?php //endif; ?>
<!---->
<!--            </div>-->
<!--         -->
<!--            <div class="col-xs-12 col-sm-8">-->
<!--                <div class="info">-->
<!--                    --><?//= $this->render('_info', [
//                        'model' => $model
//                    ]) ?>
<!--                </div>-->
<!---->
<!--                <div class="info">-->
<!--                   -->
<!--                    --><?//= HelpWidget::widget(['userId' => $model->userId]) ?>
<!--                   -->
<!--                </div>-->
<!---->
<!---->
<!--                --><?php //if (!empty($video)): ?>
<!--                    <div class="info">-->
<!--                        --><?//= VideoWidget::widget(['video' => $video]) ?>
<!--                    </div>-->
<!--                --><?php //endif; ?>
<!--            </div>-->
<!--            -->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="container">-->
<!--        <div class="col-xs-12 col-sm-4">-->
<!--        </div>-->
<!--        <div class="col-xs-12 col-sm-8 gallery-block">-->
<!--            <div class="info">-->
<!--                --><?//= HelpFromHistoryWidget::widget(['userId' => $model->userId]); ?>
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    -->
<!--    --><?//= WallWidget::widget(['userId' => $model->userId]) ?>
<!-- -->
<!---->
<!--</section>-->
