<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use vova07\imperavi\Widget;


/* @var $this yii\web\View */
/* @var $model app\models\Needies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="needies-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mainCountryId')->dropDownList($countries)->label('Страна') ?>

    <?= $form->field($model, 'city')->textInput()->label('Город') ?>

<!--    --><?//= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time')->textInput(['maxlength' => true])->label('Удобное время для звонка') ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shortTrouble')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'trouble')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'trouble')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]);
    ?>
    <?= $form->field($model, 'specificNeed')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]);
    ?>
    <?= $form->field($model, 'cardNum')->textInput(['maxlength' => true]) ?>
    <div class="form-group field-needies-registerDate">
        <label class="control-label" for="needies-registerDate">Дата Регистрации</label>
        <?php $model->registerDate = Yii::$app->formatter->asDatetime($model->registerDate, "php:m/d/Y ") ?>
        <?php
        echo MaskedInput::widget([
            'model' => $model,
            'attribute' => 'registerDate',
            'name' => 'registerDate',
            'mask' => '99/99/9999'
        ]);
        ?>
        <div class="help-block"></div>
    </div>


    <div class="form-group field-needies-updateDate">
        <label class="control-label" for="needies-updateDate">Дата Подтверждения</label>
        <?php if (!empty($model->updateDate)): ?>
            <?php $model->updateDate = Yii::$app->formatter->asDatetime($model->updateDate, "php:m/d/Y ") ?>
        <?php endif; ?>
        <? echo MaskedInput::widget([
            'model' => $model,
            'attribute' => 'updateDate',
            'name' => 'updateDate',
            'mask' => '99/99/9999'
        ]);
        ?>
        <div class="help-block"></div>
    </div>

    <?php $file = $model->avatar->path; ?>
    <?php if (!empty($file)): ?>
        <div class="form-active">
            <p><label class="control-label">Аватар</label></p>

            <div class="image-user">
                <div class="icons">
                    <i data-id="<?= $model->avatarId ?>" class="delete-photo fa fa-trash-o"></i>
                </div>
                <span class="photo">
                    <a class="profile-gallery-item"
                       href="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/' . $file ?>"
                       data-fancybox-group="gallery" title="">
                        <img
                            src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $file ?>"
                            alt="" id="img-<?= $model->avatarId ?>">
                    </a>
                </span>
            </div>
            <div class="clr"></div>
            <?= \yii\bootstrap\Html::button('Повернуть влево', ['class' => 'img-rotate btn btn-primary', 'data-side' => 'left', 'data-id' => $model->avatarId]) ?>
            <?= \yii\bootstrap\Html::button('Повернуть вправо', ['class' => 'img-rotate btn btn-primary', 'data-side' => 'right', 'data-id' => $model->avatarId]) ?>
        </div>
    <?php else: ?>
        <p><label class="control-label">Аватар</label></p>
        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
    <?php endif; ?>
    <?= \cyneek\yii2\widget\upload\crop\UploadCrop::widget([
        'form' => $form,
        'model' => $upload,
        'attribute' => 'imageFile',
        'jcropOptions' => [
            'aspectRatio' => 1,
        ]
    ]) ?>
    <div class="clr"></div>


    <div class="all-image">
        <?php if (!empty($images)): ?>
            <div class="form-active">
                <p><label class="control-label">Фото</label></p>
                <?php foreach ($images as $key => $image): ?>
                    <div class="image-user">
                        <div class="icons">
                            <i data-id="<?= $image->id ?>" class="delete-photo fa fa-trash-o"></i>
                        </div>
                                    <span class="photo">
                                        <a class="profile-gallery-item"

                                           href=" <?= Yii::getAlias('@web') . '/images/' . $model->userId . '/' . $image->path ?>"
                                           data-fancybox-group="gallery" title="">
                                            <img
                                                src=" <?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $image->path ?>"
                                                alt="">
                                        </a>
                                    </span>
                    </div>

                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="clr"></div>

    <?php if (!empty($videos)): ?>

        <p><label class="control-label">Видео</label></p>

        <?php foreach ($videos as $video): ?>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= substr($video->path, 32) ?>"
                    frameborder="0" allowfullscreen></iframe>
        <?php endforeach; ?>

    <?php endif; ?>
    <div class="clr"></div>

    <?= $form->field($model, 'statusId')->dropDownList($statuses) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if (!empty($images)): ?>
            <?= Html::a('Просмотр Фото', ['descriptions','id'=> $model->userId, 'user'=>$model->id], ['class' => 'btn btn-primary ']) ?>
        <?php endif; ?>
        <?= Html::a('Добавить Фото', ['upload' ,'id' =>$model->userId,'user'=>$model->id], ['class' => 'btn btn-success ']) ?>
        <?php if (!empty($videos)): ?>
            <?= Html::a('Просмотр Видео', ['video?id=' . $model->userId], ['class' => 'btn btn-primary ']) ?>
        <?php else : ?>
            <?= Html::a('Добавить Видео', ['video?id=' . $model->userId], ['class' => 'btn btn-success ']) ?>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
