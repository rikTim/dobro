<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$this->title = 'Кому помочь';
?>

<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">
                        <h4>Вы можете загружать не больше 30 фото  и  размером 500х100 </h4>
                        <?php $form = ActiveForm::begin([
                            'action' => ['upload', 'id'=> $userId , 'user'=> $user],
                            'options' => ['enctype' => 'multipart/form-data']
                        ]);?>

                        <label class="control-label">Добавить Фото</label>
                        <?= FileInput::widget([
                            'model' => $upload,
                            'attribute' => 'file[]',
                            'language' => 'ru',
                            'options' => ['multiple' => true,'accept' => 'image/*'],
                            'pluginOptions' => [
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => false,
                                'maxFileCount'=> 30,
                            ]
                        ]);?>
                        <br>
                    <?php if(!empty($error)):?>
                        <div style="color:red" class="help-block "><?=$error?></div>
                    <?php endif;?>

                    <?= Html::submitButton(  'Загрузить', ['class' =>  'btn btn-primary']) ?>
                        <?= Html::a('Отмена', ['descriptions','id'=> $userId, 'user'=>$user], ['class' => 'btn btn-warning ']) ?>

                        <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
