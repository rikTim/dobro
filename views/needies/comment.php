<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Statuses;
use dosamigos\datepicker\DatePicker;

$this->title = 'Создать Комментарий Кому помочь:' . $needies->name . ' ' . $needies->lastname;
?>

<section class="content comments-create">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <!--                    --><?//= $form->field($model, 'toUserId')->textInput(['value'=>$angel->userId]) ?>
                    <?= $form->field($model, 'toUserId')->hiddenInput(['value'=>$needies->userId])->label(false) ?>


                    <?= $form->field($model, 'fromUserId')->hiddenInput(['value' => '1'])->label(false) ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'dateTime')->hiddenInput(['value' => time()])->label(false) ?>


                    <?= $form->field($model, 'statusId')->hiddenInput(['value' => 2])->label(false) ?>




                    <!--                    --><?//= $form->field($model, 'statusId')->dropDownList(ArrayHelper::map(Statuses::find()->all(), 'id', 'title')) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Создать' , ['class'=> 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>