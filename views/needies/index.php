<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use app\models\Statuses;
use yii\helpers\Url;

$this->title = 'Кому помочь';
?>

<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'value' => 'id',
                                'filter' => false
                            ],

                            [
                                'attribute'=>'name',

                                'options' => ['width' => '200'],
                            ],


                            [
                                'attribute'=>'lastname',

                                'options' => ['width' => '200'],
                            ],
                            [
                                'attribute'=>'mainCountryId',
                                'value'=>'countryTitle',
                                'filter'=>ArrayHelper::map(\app\models\Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                                'options' => ['width' => '200'],
                            ],
                            [
                                'attribute'=>'city',
                                'value'=>'city',
                            ],
                            'email:email',
                            'phone',
                            [
                                'attribute' => 'registerDate',

                                'format' => ['datetime', 'php:d/m/Y '],
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'registerDate',
                                    'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'd/mm/yyyy'
                                    ],
                                ]),


                                'options' => ['width' => '300'],
                            ],
                            [
                                'attribute' => 'updateDate',

                                'value' => function ($data) {
                                    if (!empty($data->updateDate)) {
                                        return Yii::$app->formatter->asDatetime($data->updateDate, "php:m/d/Y ");
                                    } else {
                                        return ' ';
                                    }
                                },
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'updateDate',
                                    'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'd/mm/yyyy'
                                    ],
                                ]),
                                'options' => ['width' => '300'],
                            ],

                            [
                                'attribute'=>'statusPosition',
                                'label'=>'Статус',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $status = $data->status->title;
                                    switch ($status) {
                                        case 'новый':
                                            return '<span class="label bg-green "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'измененный':
                                            return '<span class="label bg-blue "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'отклоненный':
                                            return '<span class="label bg-yellow "> ' . $data->status->title . '</span>';
                                            break;
                                        default:
                                            return $data->status->title;
                                            break;
                                    }
                                },

                                'filter' => ArrayHelper::map(Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '120'],
                                'template' => '{message} {comment} {view} {update}  {delete}',
                                'buttons' => [
                                    'message' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, [
                                            'title' => Yii::t('app', 'Сообщение'),
                                        ]);
                                    },
                                    'comment' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, [
                                            'title' => Yii::t('app', 'Комментарий'),
                                        ]);
                                    },

                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch($action){
                                        case 'message':
                                            $url = Url::toRoute(['admin/messages','type'=>'4','id'=>$model->userId]);
                                            return $url;
                                        case 'view':
                                            $url = Url::toRoute(['needies/view','id'=>$model->id]);
                                            return $url;
                                        case 'update':
                                            $url = Url::toRoute(['needies/update','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['needies/delete','id'=>$model->id]);
                                            return $url;
                                        case 'comment':
                                            $url = Url::toRoute(['needies/comment','id'=> $model->id]);
                                            return $url;
                                    }
                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>