<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Кому помочь : ' . $model->name . ' ' . $model->lastname;
?>

<section class="content needies-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                        <?php if($model->statusId != 2):?>
                            <?= Html::a('Подтвердить', ['approved', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                        <?php endif;?>
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered detail-view">
                        <thead>
                            <tr>
                                <td width="20%"></td>
                                <td width="40%"><b>Новыен данные</b></td>
                                <td width="40%"><b>Старые данные</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr <?= !empty($diff->name) ? 'class="diff"' : ''; ?>>
                                <th>Имя</th>
                                <td><?= $model->name ?></td>
                                <td><?= $modelHistory->name ?></td>
                            </tr>
                            <tr <?= !empty($diff->lastname) ? 'class="diff"' : '' ?>>
                                <th>Фамилия</th>
                                <td><?= $model->lastname ?></td>
                                <td><?= $modelHistory->lastname ?></td>
                            </tr>
                            <tr <?= !empty($diff->age) ? 'class="diff"' : '' ?>>
                                <th>Возраст</th>
                                <td><?= $model->age ?></td>
                                <td><?= $modelHistory->age ?></td>
                            </tr>
                            <tr>
                                <th>Страна</th>
                                <td><?= $model->countryTitle?></td>
                            </tr>
                            <tr <?= !empty($diff->city) ? 'class="diff"' : '' ?>>
                                <th>Город</th>
                                <td><?= $model->city ?></td>
                                <td><?= $modelHistory->city ?></td>
                            </tr>
                            <tr <?= !empty($modelHistory->phone) &&  $model->phone != $modelHistory->phone ? 'class="diff"' : '' ?>>
                                <th>Телефон</th>
                                <td><?= $model->phone ?></td>
                                <td><?= $modelHistory->phone ?></td>
                            </tr>
                            <tr <?= !empty($diff->time) ? 'class="diff"' : '' ?>>
                                <th>Удобное время для звонка</th>
                                <td><?= $model->time ?></td>
                                <td><?= $modelHistory->time ?></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td colspan="2"><?= $model->email ?></td>
                            </tr>
                            <tr <?= !empty($diff->fb) ? 'class="diff"' : '' ?>>
                                <th>Ссылка на страницу в FB</th>
                                <td>
                                    <?php if(!empty($model->fb)) :?>
                                    <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><?= 'facebook.com/' . $model->fb ?></a>
                                    <?php endif;?>
                                </td>
                                <td>
                                    <?php if(!empty($modelHistory->fb)) :?>
                                        <a target="_blank" href="https://facebook.com/<?= $modelHistory->fb ?>"><?= 'facebook.com/' . $modelHistory->fb ?></a>
                                    <?php endif;?>
                                </td>
                            </tr>
                            <tr <?= !empty($diff->vk) ? 'class="diff"' : '' ?>>
                                <th>Ссылка на страницу в VK</th>
                                <td>
                                    <?php if(!empty($model->vk)) :?>
                                        <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><?= 'www.vk.com/' . $model->vk ?></a>
                                    <?php endif;?>
                                </td>
                                <td>
                                    <?php if(!empty($modelHistory->vk)) :?>
                                        <a target="_blank" href="https://www.vk.com/<?= $modelHistory->vk ?>"><?= 'www.vk.com/' . $modelHistory->vk ?></a>
                                    <?php endif;?>
                                </td>
                            </tr>
                            <tr <?= !empty($diff->shortTrouble) ? 'class="diff"' : '' ?>>
                                <th>Проблема(коротко)</th>
                                <td><?= $model->shortTrouble ?></td>
                                <td><?= $modelHistory->shortTrouble ?></td>
                            </tr>
                            <tr <?= !empty($diff->trouble) ? 'class="diff"' : '' ?>>
                                <th>Проблема</th>
                                <td><?= $model->trouble ?></td>
                                <td><?= $modelHistory->trouble ?></td>
                            </tr>
                            <tr <?= !empty($diff->specificNeed) ? 'class="diff"' : '' ?>>
                                <th>Необходимо</th>
                                <td><?= $model->specificNeed ?></td>
                                <td><?= $modelHistory->specificNeed ?></td>
                            </tr>
                            <tr <?= !empty($modelHistory->cardNum) && $model->cardNum != $modelHistory->cardNum ? 'class="diff"' : '' ?>>
                                <th>Реквизиты</th>
                                <td><?= $model->cardNum ?></td>
                                <td><?= $modelHistory->cardNum ?></td>
                            </tr>

                            <tr>
                                <th>Дата регистрации</th>
                                <td colspan="2"><?= Yii::$app->formatter->asDatetime($model->registerDate, "php:d/m/Y "); ?></td>
                            </tr>

                            <tr>
                                <th>Дата проверки</th>
                                <?php if(!empty($model->updateDate)):?>
                                <td colspan="2"><?= Yii::$app->formatter->asDatetime($model->updateDate, "php:d/m/Y "); ?></td>
                                <?php else:?>
                                <td colspan="2"></td>
                                <?php endif?>
                            </tr>

                            <tr <?= !empty($modelHistory->avatarId) && $model->avatarId != $modelHistory->avatarId ? 'class="diff"' : '' ?>>
                                <th>Аватар</th>
                                <td><?php $file = $model->avatar->path?>
                                    <?php if (!empty($file)): ?>
                                        <a href="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $file ?> "><img src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $file ?>"></a>
                                    <?php else: ?>
                                        <div class="image-user">
                                            <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg'?>">
                                        </div>
                                    <?php endif; ?>
                                </td>
                                <td><?php $file = $modelHistory->avatar->path?>
                                    <?php if (!empty($file)): ?>
                                        <a href="<?= Yii::getAlias('@web') . '/images/' . $modelHistory->userId . '/m_' . $file ?> "><img src="<?= Yii::getAlias('@web') . '/images/' . $modelHistory->userId . '/m_' . $file ?>"></a>
                                    <?php endif; ?>
                                </td>
                            </tr>

                            <?php if (!empty($images)): ?>
                                <tr>
                                    <th>Фото</th>
                                    <td>
                                        <?php foreach ($images as $image): ?>
                                            <span class="photo">
                                            <a class="profile-gallery-item"
                                               href="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/' . $image->path?>"
                                               data-fancybox-group="gallery" title="">
                                                <img src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $image->path ?>"
                                                     alt="">
                                                <p><?= $image->descr ?></p>
                                            </a>
                                        </span>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>

                            <?php if (!empty($videos)): ?>
                                <tr>
                                    <th>Видео</th>
                                    <td>
                                        <?php foreach ($videos as $video): ?>
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= substr($video->path, 32) ?>" frameborder="0" allowfullscreen></iframe>
                                        <?php endforeach; ?>

                                    </td>
                                </tr>
                            <?php endif; ?>

                            <tr>
                                <th>Статус</th>
                                <td colspan="2"><?= $model->status->title ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section>