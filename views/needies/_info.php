<h3><?= $model->name . ' ' . $model->lastname ?></h3>
<span class="city"><?= $model->countryTitle . ', ' . $model->city ?></span>
<?php if ($model->status->id == 2): ?>
    <h4 class="status-label"><span class="label label-success">Анкета проверена</span></h4>
<?php endif; ?>

<p><b>Возраст: </b> <?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) ?></p>
<p><b>Номер телефона: </b> <?= !empty($model->phone) ? $model->phone : 'не указан' ?> </p>
<?= sizeof($model->address) > 0 ? '' : '<p><b>Адрес: </b>' . $model->address . '</p>' ?>
<?= sizeof($model->email) > 0 ? '' : '<p><b>Email: </b>' . $model->email . '</p>' ?>

<?php if (!empty($model->cardNum) && $model->cardNum != ' '): ?>
    <p><b>Реквизиты:</b> <?= $model->cardNum ?> </p>
<?php endif; ?>

<?php if(!empty($model->trouble)):?>
    <p><b>Проблема: </b> <?= $model->trouble ?> </p>
<?php endif;?>

<?php if(!empty($model->specificNeed)):?>
    <p><b>Необходимо: </b><?= $model->specificNeed ?> </p>
<?php endif;?>

<?php if(!empty($model->need) && !empty($model->got)): ?>
    <p><b>Собрано:</b> <?= $model->got . ' грн' ?> из <?= $model->need . ' грн' ?></p>

    <?php $percent = $model->got / $model->need * 100 ?>

    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="<?= $model->need ?>" style="width: <?= $percent; ?>%;">
            <?= $percent . '%' ?>
        </div>
    </div>

<?php endif; ?>



<?php if(!empty($model->vk)): ?>
    <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><i class="fa social fa-vk"></i></a>
<?php endif; ?>
<?php if(!empty($model->fb)): ?>
    <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><i class="fa social fa-facebook-official"></i></a>
<?php endif; ?>