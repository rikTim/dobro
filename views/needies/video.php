<?php

use yii\helpers\Html;

//use yii\widgets\ActiveForm;
//use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;

$this->title = 'Needies';
?>

<section class="content video-add">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?php if(!empty($video)):?>
                        <p>
                            <?= Html::a('Удалить', ['delete-video','id'=>$video->id], ['class' => 'btn btn-danger']) ?></p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="box-body">

                    <?php if (!empty($video)): ?>

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'userId')->hiddenInput(['value' => $id])->label(false) ?>

                        <?= $form->field($model, 'fileType')->hiddenInput(['value' => '2'])->label(false) ?>

                        <p><label class="control-label">Видео</label></p>

                        <iframe width="560" height="315"
                                src="https://www.youtube.com/embed/<?= substr($video->path, 32) ?>" frameborder="0"
                                allowfullscreen></iframe>
                        <div class="clr"></div>

                        <?= $form->field($model, 'descr')->textInput(['maxlength' => true])->label('Комментарий') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-warning ']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    <?php else: ?>
                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'userId')->hiddenInput(['value' => $id])->label(false) ?>

                        <?= $form->field($model, 'fileType')->hiddenInput(['value' => '2'])->label(false) ?>

                        <?= $form->field($model, 'path')->textInput(['maxlength' => true])->label('Ссылка') ?>

                        <?= $form->field($model, 'descr')->textInput(['maxlength' => true])->label('Комментарий') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-warning ']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    <? endif; ?>


                </div>
            </div>
        </div>
    </div>
</section>
