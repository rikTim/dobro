<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Фото ';
?>

<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Добавить Фото', ['upload','id'=> $id,'user'=>$user], ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
                <div class="box-body">

                    <?php if (!empty($files)): ?>

                        <?php $form = ActiveForm::begin(); ?>
                        <?php $count = count($files) ?>
                        <?php foreach ($files as $file): ?>
                            <?= $form->field($file, 'id[]')->hiddenInput(['value' => $file->id])->label(false) ?>
                            <?= $form->field($file, 'userId[]')->hiddenInput(['value' => $file->userId])->label(false) ?>

                            <?php if (!empty($file->path)): ?>
                                <p><label class="control-label">Фото</label></p>
                                <img style="height: 160px;"
                                     src="<?= Yii::getAlias('@web') . '/images/' . $file->userId . '/m_' . $file->path ?>"
                                     alt="" id="img-<?= $file->id ?>">
                                <br><br>
                                <?= \yii\bootstrap\Html::button('Повернуть влево', ['class' => 'img-rotate btn btn-primary', 'data-side' => 'left', 'data-id' => $file->id]) ?>
                                <?= \yii\bootstrap\Html::button('Повернуть вправо', ['class' => 'img-rotate btn btn-primary', 'data-side' => 'right', 'data-id' => $file->id]) ?>
                                <?= \yii\bootstrap\Html::button('Установить водяной знак', ['class' => 'img-set-wm btn btn-primary', 'data-id' => $file->id]) ?>
                                <br><br>
                            <?php else: ?>
                                <p><label class="control-label">Фото</label></p>
                                <img src="http://dobroua.com/web/images/no-image.jpeg">
                            <?php endif; ?>
                            <?= $form->field($file, 'descr[]')->textarea(['value' => $file->descr])->label('Комментарий') ?>
                        <?php endforeach ?>

                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Отмена', ['update','id'=>$user], ['class' => 'btn btn-warning ']) ?>
                        <?php ActiveForm::end(); ?>

                    <?php endif ?>


                </div>
            </div>
        </div>
    </div>
</section>
