<?php

use yii\helpers\Url;

?>

<?php //print_r($model);
//die();?>

<?php $userRole = Yii::$app->DLL->getRoleByUserId($model->fromUserId);?>
<?php  if($model->fromUserId !== 1):?>

    <?php $user =  Yii::$app->DLL->getModelByUserId($model->fromUserId); $user= $user::find()->where(['userId'=>$model->fromUserId])->one()?>
    <?php
    switch ($userRole) {
        case 'volunteer':
            $url = '/volunteer/more';
            $imgPath = $user->avatar->path;
            break;
        case 'angel':
            $url = '/angels/more';
            $imgPath = $user->avatar->path;
            break;
        case 'needy':
            $url = '/needies/more';
            $imgPath = $model->fromVolunteerUser->file->path;
            break;
    } ?>
<?php else:?>
    <?php $userName = "Администрация DobroUA.com"; ?>
<?php endif;?>

<?php if ($model->fromUserId == 1): ?>

    <div class="b_pic"><img style="width:106px;" src="<?= Yii::getAlias('@web') . '/images/admin.jpg' ?>">
        </div>
    <div class="b_desc_content">
        <div class="b_title"><?= $userName ?><span class="b_date"><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></span></div>
        <div class="b_desc"><span class="b_paid"><?= $model->text ?></span> <div class="clear"></div></div>

    </div>

<?php else: ?>
<!--    --><?php //print_r($imgPath);die();?>
<div class="b_pic"><a href="<?= Url::toRoute([$url, 'id' => $user->id])?>">
<?php if(!empty($imgPath) && file_exists(Yii::getAlias('@app/web/images/' . $model->fromUserId . '/' . $imgPath))): ?>
        <img style="width:106px;" src="<?= Yii::getAlias('@web') . '/images/' . $model->fromUserId . '/m_' . $imgPath ?>">
<?php else: ?>
    <img  style="width:106px;" src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>">
<?php endif; ?>
    </a></div>
<div class="b_desc_content">
    <div class="b_title"><a href="<?= Url::toRoute([$url, 'id' => $user->id])?>"><?= Yii::$app->DLL->subStr($user->name, 25)  . ' ' . $user->lastname?></a><span class="b_date"><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></span></div>
    <div class="b_desc"><span class="b_paid"><?= $model->text ?></span></div>
    <div class="clear"></div>
</div>


<?php endif;?>