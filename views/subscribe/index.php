<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubscribeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content angels-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,

                        'columns' => [


                            'id',
                            'name',
                            'email:email',
                            ['attribute' => 'message',
                                'format' => 'ntext',
                                'options' => ['width' => '700'],
                            ],

                            'time:datetime',

                            'ip',


                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>
