<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use app\models\Statuses;
use yii\helpers\Url;

$this->title = 'Счетчик Кому помочь';
?>

<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'value' => 'id',
                                'filter' => false
                            ],

                            [
                                'attribute'=>'name',

                                'options' => ['width' => '200'],
                            ],


                            [
                                'attribute'=>'lastname',

                                'options' => ['width' => '200'],
                            ],
                            [
                                'attribute'=> 'need',
                            ],

                            [
                                'attribute'=>'got',
                            ],




                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '120'],
                                'template' => '  {update}  ',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch($action){
                                        case 'update':
                                            $url = Url::toRoute(['shop/change','id'=>$model->id]);
                                            return $url;

                                    }
                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>