<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShopViewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Магазин';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-star"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?= Html::a('Создать лот', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <div class="box-body">


                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'number',
                            'size',
                            'color',
                            'consist',
                            'cost',
                            'avatarId',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '120'],
                                'template' => '{message} {comment} {view} {update}  {delete}',
                                'buttons' => [
                                    'message' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-shopping-cart"></span>', $url, [
                                            'title' => Yii::t('app', 'Сообщение'),
                                        ]);
                                    },



                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch ($action) {
                                        case 'message':
                                            $url = Url::toRoute(['shop/watermark','id'=>$model->avatarId]);
                                            return $url;
                                        case 'view':
                                            $url = Url::toRoute(['shop/view','id'=>$model->id]);
                                            return $url;
                                        case 'update':
                                            $url = Url::toRoute(['shop/update','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['shop/delete','id'=>  $model->id]);
                                            return $url;
                                    }


                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
