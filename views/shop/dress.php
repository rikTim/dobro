<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

//$this->title = '"Добрые платья" дает возможность за небольшую сумму приобрести красивое платье, пожертвованное во имя спасения чьей-то жизни.';
$this->registerMetaTag(['name' => 'description', 'content' => '"Добрые платья" дает возможность за небольшую сумму приобрести красивое платье, пожертвованное во имя спасения чьей-то жизни.']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="container b_fixed">
    <div class="b_adds_list">
        <center><h1>Добрые платья</h1></center>
        <div style="display:inline-block; float: left;margin-bottom: 20px;width: 20%;margin-right: 20px"><a href="<?= Url::toRoute(['needies/more','id'=>106])?>"><h3>AЛЕКСА ДАРИЯ</h3><img style="width:100%;" class="sp-image" src="<?= Yii::getAlias('@web/images/484/m_6deb154a84a0da007633ee1389e971f5.jpg')?>"></a></div>
        <div style="margin-bottom: 20px; margin-top: 60px "><p>Проект "Добрые платья" дает возможность за небольшую сумму приобрести красивое платье, пожертвованное во имя спасения чьей-то жизни. Приобретая себе обновку, Вы фактически покупаете жизненно-необходимые препараты для больного человека или помогаете в проведении операции. Мы все знаем, что кроме мира, который мы видим, есть и другой тонкий мир, где все наши материальные блага ровным счётом ничего не стоят. То душевное богатство, которое мы накапливаем, имеет гараздо большее значение.

                У каждой женщины в шкафу есть платье, которое пылится там уже долгое время.
                Это платье может спасти чью—то жизнь! Присоединяйтесь к нашему проекту, дарите и приобретайте платья—дарите надежду людям, оказавшимся в тяжелой ситуации.
                Мы подаем руку помощи сейчас, а Вселенная обязательно отблагодарит нас позже!
                Оттдавая, мы всегда получаем вдвое больше.</p>
        <br>
        <p>
            Если Вам понравилось платье и Вы хотите его приобрести или примерять - обращайтесь по телефону <b>+380730763097 - Дария.</b> Оплата за платье только на карту приват банка. Таким образом, мы каждый раз будем готовы ответить на вопрос: сколько же собрано Дарии на лекарства?
           <br><b> Номер карты 4149 4978 5139 2763 - Приват Банк Алекса Дария</b>
        </p></div>

        <?php if (!empty($needies->need) && !empty($needies->got)): ?>
            <div class="b_progress_amount">
                <label><b>Собрано:</b> <?= $needies->got . ' грн' ?> из <?= $needies->need . ' грн' ?> </label>
                <?php $percent = $needies->got / $needies->need * 100 ?>
                <div class="b_progress_place">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0"
                             aria-valuemax="<?= $needies->need ?>" style="width: <?= $percent; ?>%;">

                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div style="margin-bottom: 100px;"></div>
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_view_dress',
            'layout' => "{summary}\n{items}<div class=\"clr\"></div>{pager}",
            'emptyText' => 'На данный момент наш магазин пуст',
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'col-md-3 shop',
            ],
        ]) ?>

    </div>

</div>
