<?php
$this->title = 'DobroUA.com Добрые платья ';

?>

<div class="container-fluid">
    <center><h3>Если Вам понравилось платье и Вы хотите его приобрести или примерять - обращайтесь по телефону <b>+380730763097 - Дария.</b></h3></center>
    <div class="col-md-6 dress-main">
        <img class="my-foto-container"
             src="<?= Yii::getAlias('@web/images/shop/') . $model->id . '/big/' . $model->avatar->path; ?>"
             data-large="<?= Yii::getAlias('@web/images/shop/'). $model->id .'/'.$model->avatar->path; ?>"
             title="Фото">
        <script type="text/javascript">(function (w, doc) {
                if (!w.__utlWdgt) {
                    w.__utlWdgt = true;
                    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })(window, document);
        </script>
        <div data-background-alpha="0.0" data-buttons-color="#FFFFFF"
             data-counter-background-color="#ffffff" data-share-counter-size="12"
             data-top-button="false" data-share-counter-type="separate" data-share-style="1"
             data-mode="share" data-like-text-enable="false" data-mobile-view="false"
             data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000"
             data-share-shape="rectangle" data-sn-ids="fb.vk.ok." data-share-size="20"
             data-background-color="#ffffff" data-preview-mobile="false"
             data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1486815"
             data-counter-background-alpha="1.0" data-following-enable="false"
             data-exclude-show-more="true" data-selection-enable="true"
             class="uptolike-buttons"></div>

    </div>

    <div class="col-md-6">
        <?php if(!empty($model->cost)):?>
        <h2><b> Цена :<?= $model->cost ?></b></h2>
        <?php endif;?>
        <?php if(!empty($model->color)):?>
        <h2><b> Цвет :<?= $model->color ?></b></h2>
        <?php endif;?>
        <?php if(!empty($model->consist)):?>
        <h2><b> Cостав :<?= $model->consist ?></b></h2>
        <?php endif;?>
        <?php if(!empty($model->size)):?>
        <h2><b> Размер :<?= $model->size ?></b></h2>
        <?php endif;?>
        <?php if(!empty($model->state)):?>
            <h2><b> Состояние:<?= $model->state ?></b></h2>
        <?php endif;?>
        <?php if(!empty($model->text)):?>
            <h2><b> Описание:<?= $model->text ?></b></h2>
        <?php endif;?>

        <center><div class="my-container"></div></center>
    </div>
    <div class="clear"></div>
    <?php if (!empty($files)): ?>
        <div class="col-md-6">
            <?php foreach ($files as $file): ?>


                <img class="my-foto" src="<?= Yii::getAlias('@web/images/shop/'). $model->id . '/' . $file->path; ?>"
                     data-large="<?= Yii::getAlias('@web/images/shop/'). $model->id .'/'.$file->path; ?>">

            <?php endforeach; ?>

        </div>

        <div class="col-md-6"></div>
        <div class="clear"></div>
    <?php endif; ?>
    <div class="col-md-12">
<!--        --><?php //if (!Yii::$app->user->isGuest): ?>
            <?= \app\components\ShopCommentWidget::widget(['shopId' => $model->id]); ?>
<!--        --><?php //endif; ?>
    </div>
</div>

<script>
    jQuery(function () {

        //вешаем плагин на контейнер-картинку
        $(".my-foto-container").imagezoomsl({
            descarea: ".my-container",
            zoomrange: [1, 12],
            magnifiereffectanimate: "fadeIn",
            magnifierborder: "none"
        });

        //клик по превью-картинке
        $(".my-foto").click(function () {

            var that = this;

            //копируем атрибуты из превью-картинки в контейнер-картинку
            $(".my-foto-container").fadeOut(600, function () {

                $(this).attr("src", $(that).attr("src"))              // путь до small картинки
                    .attr("data-large", $(that).attr("data-large"))       // путь до big картинки

                    //дополнительные атрибуты, если есть
                    //.attr("data-title",       $(that).attr("data-title"))       // заголовок подсказки
                    //.attr("data-help",        $(that).attr("data-help"))        // текст подсказки
                    //.attr("data-text-bottom", $(that).attr("data-text-bottom")) // текст снизу картинки

                    .fadeIn(1000);
            });
        });
    });
</script>

<style>

</style>
<!--<style>-->
<!--    .my-photo{-->
<!--        border: 1px solid #F0F0F0;-->
<!--        width: 250px;-->
<!--        height: 250px;-->
<!--    }-->
<!--</style>-->

