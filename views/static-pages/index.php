<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Статические страницы';
?>

<section class="content static-pages-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-paperclip"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
<!--                        <p>--><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?><!--</p>-->
                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
//                        'filterModel' => $searchModel,
                        'columns' => [


                            'id',
                            'title',
                            'text:ntext',

                            ['class' => 'yii\grid\ActionColumn',
                                'template' => '{view}{update}',
                                'options' => ['width' => '50'],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>