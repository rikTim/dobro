<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->title;
?>

<section class="content static-pages-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-paperclip"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'title',
                            'text:ntext',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>