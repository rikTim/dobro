<h3><?= $model->name . ' ' . $model->lastname ?></h3>
<span class="city"><?= $model->countryTitle . ', ' . $model->city ?></span> <br>
<p>

</p>

<p><b>Возраст: </b> <?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) ?></p>

<?php if(!empty($model->phone) && $model->showPhone == 0): ?>
    <p><b>Телефон: </b><?= $model->phone ?></p>
<?php endif ?>

<?php if(!empty($model->email) && $model->showEmail == 0): ?>
    <p><b>Email: </b><?= $model->email ?></p>
<?php endif ?>

<?php if(!empty($model->help)): ?>
    <p><b>Могу помочь: </b><?= $model->help ?></p>
<?php endif ?>

<?php if(!empty($model->vk)): ?>
    <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><i class="fa social fa-vk"></i></a>
<?php endif ?>

<?php if(!empty($model->fb)): ?>
    <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><i class="fa social fa-facebook-official"></i></a>
<?php endif ?>