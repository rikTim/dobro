<?php

use yii\helpers\Url;
?>

<div class="col-md-6 col-sm-6 col-xs-12 b_persons">
    <div class="b_persons_item">
        <div class="b_pic"><a href="<?= Url::toRoute(['more', 'id' => $model->id]) ?>">
                <?php if(!empty($model->file->path) && file_exists(Yii::getAlias('@app/web/images/' . $model->userId . '/m_' . $model->file->path))): ?>
                    <img src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>" style="width: 106px" ></a></div>
                <?php else: ?>
                    <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>" style="width: 106px"></a></div>
                <?php endif; ?>
<!--                <img src="gfx/pic1.jpg"></a></div>-->
        <div class="b_desc_content" style="height: 116px;">
            <div class="b_title"><a href="<?= Url::toRoute(['more', 'id' => $model->id]) ?>"><?= $model->name . ' ' . $model->lastname ?></a><span class="b_locate"><?= $model->age . ' ' . Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $model->age) . ' ' . $model->countryTitle . ', ' . $model->city ?></span>
            </div>
            <div class="b_desc"><span class="b_rating"><?= !empty($model->rating) ? 'Рейтинг: ' . $model->rating : ' ' ?></span></div>
        </div>
    </div>
</div>