<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\Statuses;

$this->title = 'Волонтеры';
?>

<section class="content volunteers-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <? //= Html::a('Create Volunteer', ['create'], ['class' => 'btn btn-success']) ?><!--</p>-->
                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'value' => 'id',
                                'filter' => false
                            ],
//                            'userId',
                            'name',
                            'lastname',
                            'age',
                            [
                                'attribute'=>'mainCountryId',
                                'value'=>'countryTitle',
                                'filter'=>ArrayHelper::map(\app\models\Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                            ],
                            [
                                'attribute'=>'city',
                                'value'=>'city',
                            ],
                            'email:email',
                            'phone',
                            [
                                'attribute' => 'statusPosition',
                                'label' => 'Статус',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $status = $data->status->title;
                                    switch ($status) {
                                        case 'новый':
                                            return '<span class="label bg-green "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'измененный':
                                            return '<span class="label bg-blue "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'отклоненный':
                                            return '<span class="label bg-yellow "> ' . $data->status->title . '</span>';
                                            break;
                                        default:
                                            return $data->status->title;
                                            break;
                                    }
                                },
                                'filter' => ArrayHelper::map(Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),
                            ],

                            [
                                'attribute' => 'createTime',
                                'format' => 'raw',
                                'label' => 'Пользователь',
                                'filter' => false,
                                'value' => function($data)
                                {
                                    $time = time()-86400;
                                    if($data->createTime > $time){
                                        return '<span class="label bg-green "> ' . новый . '</span>';
                                    }else{
                                        return 'просмотреный';
                                    }
                                }
                            ],


                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '120'],
                                'template' => '{message} {comment} {view} {update}  {delete}',
                                'buttons' => [
                                    'message' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, [
                                            'title' => Yii::t('app', 'Сообщение'),
                                        ]);
                                    },
                                    'comment' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, [
                                            'title' => Yii::t('app', 'Комментарий'),
                                        ]);
                                    },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch ($action) {
                                        case 'message':
                                            $url = Url::toRoute(['admin/messages','type'=>3,'id'=>$model->userId]);
                                            return $url;
                                        case 'view':
                                            $url = Url::toRoute(['volunteers/view','id'=>$model->id]);
                                            return $url;
                                        case 'update':
                                            $url = Url::toRoute(['volunteers/update','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['volunteers/delete','id'=>$model->id]);
                                            return $url;
                                        case 'comment':
                                            $url = Url::toRoute(['volunteers/comment','id'=>$model->id]);
                                            return $url;

                                    }
                                }
                            ],],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>