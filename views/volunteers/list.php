<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

$this->title = 'Волонтеры проекта - Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'На этой страничке вы можете познакомиться и получить информацию о волонтерах фонда, оказать помощь и стать волонтером']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="container b_fixed">
    <h1>Волонтеры
        <div class="b_help_text">
            <?php if (Yii::$app->user->isGuest): ?>
                <p>Волонтер - призвание для тех, кто готов проверять информацию из просьбы о помощи в своем городе или вблизи него и не боится столкнуться с бедой. <a href="<?= Url::toRoute(['user/register', 'type' => 3]) ?>">Став волонтером</a> у тебя будет возможность сделать свой вклад в жизни многих людей.</p>
            <?php else: ?>
                <p>Волонтер - призвание для тех, кто готов проверять информацию из просьбы о помощи в своем городе или вблизи него и не боится столкнуться с бедой. Став волонтером у тебя будет возможность сделать свой вклад в жизни многих людей.</p>
            <?php endif; ?></div>
    </h1>
    <div class="b_persons_list">

        <?= ListView::widget([
            'dataProvider' => $volunteer,
            'itemView' => '_view_list',
            'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
            'emptyText' => 'Список пуст',
        ]) ?>
    </div>
    <div class="clearboth"></div>
</div>



