<?php

use yii\helpers\Html;

$this->title = 'Изменить Волонтера: ' . ' ' . $model->name . ' ' . $model->lastname;
?>

<section class="content volunteers-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'statuses' => $statuses,
                        'countries' => $countries,
                        'cities' => $cities,
                        'images' => $images,
                        'upload' => $upload,
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
</section>