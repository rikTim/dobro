<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskComment */

$this->title = 'Создать коментарий к заданию';
$this->params['breadcrumbs'][] = ['label' => 'Task Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <?= Html::encode($this->title) ?>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'taskId')->hiddenInput(['value'=>$taskId])->label(false) ?>

                    <?= $form->field($model, 'userId')->hiddenInput(['value'=>1])->label(false) ?>

                    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'dateTime')->hiddenInput(['value'=>time()])->label(false) ?>

                    <?= $form->field($model, 'statusId')->dropDownList($status) ?>

                    <div class="form-group">
                        <?= Html::submitButton( 'Create' , ['class' =>  'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>


