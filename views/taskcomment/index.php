<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use \app\models\Statuses;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskCommentSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комментарии к заданиям';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'taskId',
                                'format'=>'raw',
                                'value' => function($data){
                                    return '<a href=' . \yii\helpers\Url::toRoute(["tasks/view", "id"=>$data->taskId]) . '>' . $data->task->title . '</a>';
                                }
                            ],
                            [
                                'attribute' => 'userId',
                                'format'=>'raw',
                                'value' => function($data){
                                    return $data->getFullName($data->userId);
                                }
                            ],
                            'text',
                            [
                                'attribute' => 'dateTime',
                                'format' => ['datetime', 'php:d/m/Y '],

                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'dateTime',
                                    'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'd/mm/yyyy'
                                    ]
                                ]),
                                'options' => ['width' => '300'],
                            ],
                            [
                                'attribute' => 'statusId',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $status = $data->status->title;
                                    switch ($status) {
                                        case 'новый':
                                            return '<span class="label bg-green "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'измененный':
                                            return '<span class="label bg-blue "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'отклоненный':
                                            return '<span class="label bg-yellow "> ' . $data->status->title . '</span>';
                                            break;
                                        default:
                                            return $data->status->title;
                                            break;
                                    }
                                },
                                'filter' => ArrayHelper::map(Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),
                                'options' => ['width' => '200'],
                            ],

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>
