<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'taskId')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'statusId')->dropDownList($status) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
