<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskComment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Task Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <?= Html::encode($this->title) ?>

                    <div class="box-tools">
                        <p>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>
                    </div>
                </div>
                <div class="box-body">


                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'taskId:raw',
                            'userId:raw',
                            'text',
                            'dateTime:datetime',
                            'status.title',
                        ],
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</section>
