<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskCommentSeach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-comment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'taskId') ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'text') ?>

    <?= $form->field($model, 'dateTime') ?>

    <?php // echo $form->field($model, 'statusId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
