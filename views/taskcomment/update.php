<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskComment */

$this->title = 'Изменить Коментарий к заданию: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Task Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section class="content ">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <?= Html::encode($this->title) ?>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'status' => $status,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</section>

