<?php

use yii\helpers\Url;
use app\components\WallWidget;
use app\components\RatingPopupWidget;
use app\components\GalleryWidget;
use app\components\VideoWidget;

$this->title = 'DobroUA.com Профиль ' . $model->name . ' ' . $model->lastname;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<!-- Profile block -->
<div class="section wall profile">
    <div class="container user-info">
        <div class="row">
            <!-- Photo col -->
            <div class="col-xs-12 col-md-4 ">
                <div class="row">
                    <div class="img">
                        <center>
                            <?php if(!empty($model->file->path)): ?>
                                <img src="<?= Yii::getAlias('@web') . '/images/' . $model->userId . '/m_' . $model->file->path ?>" alt="avatar" class="img-rounded">
                            <?php else: ?>
                                <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>" alt="avatar" class="img-rounded">
                            <?php endif; ?>
                        </center>
                    </div>
                </div>
                <div class="row btns">
                    <?php if(Yii::$app->DLL->getRoleByUserId(Yii::$app->user->id) != 'needy' && $model->rating->rating): ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ratingModal">Рейтинг: <?= $model->ratingCount ?></button>
                        <br><br>
                    <?php endif; ?>
                    <a href="<?= Url::toRoute(['edit']) ?>" style="color: #fff; background: #006cff;border-radius: 20px;border-color: #006cff;box-shadow: none;" class="btn btn-primary">Редактировать профиль</a>
<!--                    <button type="button"-->
<!--                            style="color: #fff; background: #006cff;border-radius: 20px;border-color: #006cff;box-shadow: none;"-->
<!--                            class="btn btn-success" data-toggle="modal" data-target="#ratingModal">-->
<!--                        Рейтинг --><?//= $model->ratingCount ?><!--</button>-->
                </div>
            </div>
            <!-- /Photo col -->

            <!-- Info col -->
            <?= $this->render('_info', [
                'model' => $model
            ]) ?>
            <!-- /Info col -->
        </div>

        <div class="container">
            <div class="row">
                <?php if(!empty($photos)): ?>
                    <?= GalleryWidget::widget(['photos' => $photos]); ?>
                <?php endif; ?>

                <?php if(!empty($video)): ?>
                    <?= VideoWidget::widget(['video' => $video]) ?>
                <?php endif; ?>
                <?= WallWidget::widget(['userId' => $model->userId]); ?>
            </div>
        </div>
    </div>

    <!-- User wall block -->

    <!-- /User wall block -->

</div>
<!-- /Volunteer more block -->

<!-- Rating history popup -->
<?= RatingPopupWidget::widget(['userId' => $model->userId]); ?>
<!-- /Rating history popup -->
