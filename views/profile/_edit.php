<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php if ($type == 1): ?>
    <!-- Needy form edit -->
    <div class="row form needy-form">
        <h1>Редактирование информации</h1>
<!--        <p>Нужна помощь - если ты или твой близкий человек попал в беду и вы не можете справиться своими силами,-->
<!--            регистрируйтесь, пишите нам свою просьбу и мы сделаем все, чтобы вас услышали и нашелся Ангел, который-->
<!--            сможет вам помочь. Из-за частых случаев обманов, мы будем вынуждены проверить предоставленную вами-->
<!--            информацию.</p>-->
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-inline b_duty_form row'],
            'enableClientValidation' => true,

        ]); ?>

        <?php $cardNum = explode(' - ', $model->cardNum); ?>
        <?php $phone = explode(' - ', $model->phone) ?>

        <?= $form->field($model, 'name', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Имя*'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'lastname', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Фамилия*'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'phone', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Телефон*', 'class' => 'form-control phoneMask'])->label(false) ?>

        <?= $form->field($model, 'email', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Email'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'phoneOwner', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Контактное лицо', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'time', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Укажите удобное время для звонка', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'phoneOwner', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Контактное лицо', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'mainCountryId', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->dropDownList($countries, ['class' => 'form-control', 'id' => 'countriesList'])->label(false) ?>

        <?= $form->field($model, 'city', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['id' => 'citiesList', 'placeholder' => 'Город'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'bank', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Банк', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'cardNum', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Реквизиты', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'age', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Возраст'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'vk', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в VK'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'fb', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в FB'], ['class' => 'form-control'])->label(false) ?>

        <div class="form-group b_load_pict col-xs-12 col-sm-12">
            <label>Аватар*</label> <?= $form->field($uploadModel, 'needyFile')->fileInput()->label(false) ?>
        </div>


        <?= $form->field($model, 'shortTrouble', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textInput(['placeholder' => 'Краткое описание проблемы', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'trouble', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Описание проблемы', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'specificNeed', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Необходимо', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'need', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Необходимо (денежная сумма)', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить профайл', ['class' => 'btn btn-join_us']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Needy edit block -->

<?php elseif ($type == 3): ?>
    <!-- Volunteer form edit -->
    <div class="row form volunteer-form">
        <h1>Редактирование информации</h1>
<!--        <p>Волонтер - призвание для тех, кто готов проверять информацию из просьбы о помощи в своем городе или-->
<!--            вблизи него и не боится столкнуться с бедой. Это возможность сделать свой вклад в жизни многих людей.</p>-->
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-inline b_duty_form row'],
            'enableClientValidation' => true
        ]); ?>
        <?= $form->field($model, 'name', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Имя*'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'lastname', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Фамилия*'], ['class' => 'form-control'])->label(false) ?>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'phone', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Телефон*', 'class' => 'form-control phoneMask'])->label(false) ?>

            <?= $form->field($model, 'showPhone', ['options' => ['class' => 'form-group']])->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'], ['data-type' => '4', 'class' => 'radioList'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'email', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Email'], ['class' => 'form-control'])->label(false) ?>

            <?= $form->field($model, 'showEmail')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'], ['data-type' => '4', 'class' => 'radioList'])->label(false) ?>
        </div>


        <?= $form->field($model, 'mainCountryId', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->dropDownList($countries, ['class' => 'form-control', 'id' => 'countriesList'])->label(false) ?>

        <?= $form->field($model, 'city', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['id' => 'citiesList', 'placeholder' => 'Город'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'age', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Возраст'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'vk', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в VK'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'fb', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в FB'], ['class' => 'form-control'])->label(false) ?>
        <div class="form-group b_load_pict col-xs-12 col-sm-12">
            <label>Аватар*</label> <?= $form->field($uploadModel, 'volunteerFile')->fileInput()->label(false) ?>
        </div>


        <?= $form->field($model, 'help', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Могу помочь', 'data-type' => '2'], ['class' => 'form-control'])->label(false) ?>

        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить профайл', ['class' => 'btn btn-join_us']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Volunteer edit block -->

<?php elseif ($type == 2): ?>
    <!-- Angel form edit -->
    <div class="row form angel-form">
        <h1>Редактирование информации</h1>
<!--        <p>Ангел - если ты имеешь желание и возможность помочь морально или материально тем, кто нуждается,-->
<!--            то этот раздел для тебя. На этой платформе размещаются только проверенные просьбы с реальными контактами-->
<!--            людей.-->
<!--            Став Ангелом, ты сможешь спасти жизнь человека или сделать кого-то счастливее.</p>-->
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-inline b_duty_form row'],
            'enableClientValidation' => true
        ]); ?>
        <?= $form->field($model, 'name', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Имя*'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'lastname', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Фамилия*'], ['class' => 'form-control'])->label(false) ?>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'phone', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Телефон*', 'class' => 'form-control phoneMask'])->label(false) ?>

            <?= $form->field($model, 'showPhone', ['options' => ['class' => 'form-group']])->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'], ['data-type' => '4', 'class' => 'radioList'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'email', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Email'], ['class' => 'form-control'])->label(false) ?>

            <?= $form->field($model, 'showEmail')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'], ['data-type' => '4', 'class' => 'radioList'])->label(false) ?>
        </div>


        <?= $form->field($model, 'mainCountryId', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->dropDownList($countries, ['class' => 'form-control', 'id' => 'countriesList'])->label(false) ?>

        <?= $form->field($model, 'city', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['id' => 'citiesList', 'placeholder' => 'Город'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'age', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Возраст'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'vk', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в VK'], ['class' => 'form-control'])->label(false) ?>

        <?= $form->field($model, 'fb', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в FB'], ['class' => 'form-control'])->label(false) ?>

        <div class="form-group b_load_pict col-xs-12 col-sm-12">
            <label>Аватар*</label> <?= $form->field($uploadModel, 'angelFile')->fileInput()->label(false) ?>
        </div>


        <?= $form->field($model, 'help', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Могу помочь', 'data-type' => '2'], ['class' => 'form-control'])->label(false) ?>

        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить профайл', ['class' => 'btn btn-join_us']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Angel edit block -->
<?php endif; ?>

<div style="height: 50px"></div>
