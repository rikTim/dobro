<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'DobroUA.com Редактирование профиля ' . $model->name . ' ' . $model->lastname;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<!-- Profile edit block -->
<div class="section profile">
    <div class="container">

        <?php if(!$isEdit): ?>
            <div class="edit">
                <!-- New profile from socials -->
                <? if($isSocial): ?>
                    <?= $this->render('_social', [
                        'needyModel' => $needyModel,
                        'angelModel' => $angelModel,
                        'volunteerModel' => $volunteerModel,
                        'needyUploadModel' => $needyUploadModel,
                        'angelUploadModel' => $angelUploadModel,
                        'volunteerUploadModel' => $volunteerUploadModel,
                    ]) ?>
                <? endif; ?>
                <!-- /New profile from socials -->
            </div>
            <!-- /Forms block -->
        <?php else: ?>

            <!-- Edit profile -->
            <?= $this->render('_edit', [
                'type' => $type,
                'model' => $model,
                'uploadModel' => $uploadModel,
                'cities' => $cities,
                'countries' => $countries,
                'regions'=> $regions

            ]) ?>
            <!-- /Edit profile -->

        <?php endif; ?>

    </div>

</div>
<!-- /Profile edit block -->