<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php if($type == 1): ?>
    <!-- Forms needy block -->
    <div class="row form needy-form">
        <p>Нужна помощь - если ты или твой близкий человек попал в беду и вы не можете справиться своими силами, регистрируйтесь, пишите нам свою просьбу и мы сделаем все, чтобы вас услышали и нашелся Ангел, который сможет вам помочь. Из-за частых случаев обманов, мы будем вынуждены проверить предоставленную вами информацию.</p>
        <?php $form = ActiveForm::begin([
            'id' => 'needy-registration-form',
            'options' => ['enctype' => 'multipart/form-data'],
            'enableClientValidation' => true
        ]); ?>

        <div class="col-xs-12 col-sm-6">
            <?= $form->field($needyModel, 'name')->textInput(['placeholder' => 'Имя'])->label('Имя', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'age')->textInput(['placeholder' => 'Возраст'])->label('Возраст', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'mainCountryId')->dropDownList($countries, ['class' => 'citySelect'])->label('Страна', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'phone')->textInput(['placeholder' => 'Телефон', 'class' => 'phoneMask form-control'])->label('Телефон', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'phoneOwner')->textInput(['placeholder' => 'Контактное лицо'])->label('Контактное лицо') ?>
            <?= $form->field($needyModel, 'cardNum')->textInput(['placeholder' => 'Реквизиты'])->label('Реквизиты') ?>
            <?= $form->field($needyModel, 'bank')->textInput(['placeholder' => 'Банк'])->label('Банк') ?>
        </div>

        <div class="col-xs-12 col-sm-6">
            <?= $form->field($needyModel, 'lastname')->textInput(['placeholder' => 'Фамилия'])->label('Фамилия', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'email')->textInput(['placeholder' => 'Email', 'value' => Yii::$app->user->identity->email, 'disabled' => true])->label('Email', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'cityId')->dropDownList($cities, ['class' => 'citySelect'])->label('Город', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'vk')->textInput(['placeholder' => 'Ссылка на страницу в VK'])->label('Ссылка на страницу в VK') ?>
            <?= $form->field($needyModel, 'fb')->textInput(['placeholder' => 'Ссылка на страницу в FB'])->label('Ссылка на страницу в FB') ?>
            <?= $form->field($needyModel, 'time')->textInput(['placeholder' => 'Укажите удобное время для звонка'])->label('Укажите удобное время для звонка') ?>
            <?= \cyneek\yii2\widget\upload\crop\UploadCrop::widget([
                'form' => $form,
                'model' => $uploadModel,
                'attribute' => 'needyFile',
                'jcropOptions' => [
                    'aspectRatio' => 1,
                ]
            ]) ?>
        </div>

        <div class="col-xs-12">
            <?= $form->field($needyModel, 'shortTrouble')->textInput(['placeholder' => 'Краткое описание проблемы'])->label('Краткое описание проблемы', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'trouble')->textarea(['placeholder' => 'Описание проблемы'])->label('Проблема', ['class' => 'required']) ?>
            <?= $form->field($needyModel, 'specificNeed')->textarea(['placeholder' => 'Необходимо'])->label('Необходимо',['class' => 'required']) ?>
        </div>

        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить профайл', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Forms block -->

<? elseif($type == 2): ?>
    <!-- Forms angels block -->
    <div class="row form angel-form">
        <p>Ангел - если ты имеешь желание и возможность помочь морально или материально тем, кто нуждается,
            то этот раздел для тебя. На этой платформе размещаются только проверенные просьбы с реальными контактами людей.
            Став Ангелом, ты сможешь спасти жизнь человека или сделать кого-то счастливее.</p>
        <?php $form = ActiveForm::begin([
            'id' => 'angel-registration-form',
            'options' => ['enctype' => 'multipart/form-data'],
            'enableClientValidation' => true
        ]); ?>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($angelModel, 'name')->textInput(['placeholder' => 'Имя'])->label('Имя', ['class' => 'required']) ?>
            <?= $form->field($angelModel, 'age')->textInput(['placeholder' => 'Возраст'])->label('Возраст', ['class' => 'required']) ?>
            <?= $form->field($angelModel, 'city')->textInput(['placeholder' => 'Город'])->label('Город', ['class' => 'required']) ?>
            <?= $form->field($angelModel, 'vk')->textInput(['placeholder' => 'Ссылка на страницу в VK'])->label('Ссылка на страницу в VK') ?>
            <?= $form->field($angelModel, 'fb')->textInput(['placeholder' => 'Ссылка на страницу в FB'])->label('Ссылка на страницу в FB') ?>
            <?= \cyneek\yii2\widget\upload\crop\UploadCrop::widget([
                'form' => $form,
                'model' => $uploadModel,
                'attribute' => 'angelFile',
                'jcropOptions' => [
                    'aspectRatio' => 1,
                ]
            ]) ?>
        </div>

        <div class="col-xs-12 col-sm-6">
            <?= $form->field($angelModel, 'lastname')->textInput(['placeholder' => 'Фамилия'])->label('Фамилия', ['class' => 'required']) ?>
            <?= $form->field($angelModel, 'email')->textInput(['placeholder' => 'Email', 'value' => Yii::$app->user->identity->email, 'disabled' => true])->label('Email', ['class' => 'required']) ?>
            <?php $angelModel->showEmail = 0; ?>
            <?= $form->field($angelModel, 'showEmail')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'])->label(false) ?>

            <?= $form->field($angelModel, 'phone')->textInput(['placeholder' => 'Телефон', 'class' => 'phoneMask form-control'])->label('Телефон') ?>
            <?php $angelModel->showPhone = 0; ?>
            <?= $form->field($angelModel, 'showPhone')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'])->label(false) ?>
            <?= $form->field($angelModel, 'time')->textInput(['placeholder' => 'Укажите удобное время для звонка'])->label('Укажите удобное время для звонка') ?>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?= Html::submitButton('Сохранить профайл', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <!-- /Forms block -->

<?php elseif($type == 3): ?>
    <!-- Forms volunteer block -->
    <div class="row form volunteer-form">
        <p>Волонтер - призвание для тех, кто готов проверять информацию из просьбы о помощи в своем городе или
            вблизи него и не боится столкнуться с бедой. Это возможность сделать свой вклад в жизни многих людей.</p>
        <?php $form = ActiveForm::begin([
            'id' => 'volunteer-registration-form',
            'options' => ['enctype' => 'multipart/form-data'],
            'enableClientValidation' => true
        ]); ?>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($volunteerModel, 'name')->textInput(['placeholder' => 'Имя'])->label('Имя', ['class' => 'required']) ?>
            <?= $form->field($volunteerModel, 'age')->textInput(['placeholder' => 'Возраст'])->label('Возраст', ['class' => 'required']) ?>
            <?= $form->field($volunteerModel, 'city')->textInput(['placeholder' => 'Город'])->label('Город', ['class' => 'required']) ?>
            <?= $form->field($volunteerModel, 'vk')->textInput(['placeholder' => 'Ссылка на страницу в VK'])->label('Ссылка на страницу в VK') ?>
            <?= $form->field($volunteerModel, 'fb')->textInput(['placeholder' => 'Ссылка на страницу в FB'])->label('Ссылка на страницу в FB') ?>
            <?= \cyneek\yii2\widget\upload\crop\UploadCrop::widget([
                'form' => $form,
                'model' => $uploadModel,
                'attribute' => 'volunteerFile',
                'jcropOptions' => [
                    'aspectRatio' => 1,
                ]
            ]) ?>
        </div>

        <div class="col-xs-12 col-sm-6">
            <?= $form->field($volunteerModel, 'lastname')->textInput(['placeholder' => 'Фамилия'])->label('Фамилия', ['class' => 'required']) ?>
            <?= $form->field($volunteerModel, 'email')->textInput(['placeholder' => 'Email', 'value' => Yii::$app->user->identity->email, 'disabled' => true])->label('Email', ['class' => 'required']) ?>
            <?php $volunteerModel->showEmail = 0; ?>
            <?= $form->field($volunteerModel, 'showEmail')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'])->label(false) ?>
            <?= $form->field($volunteerModel, 'phone')->textInput(['placeholder' => 'Телефон', 'class' => 'phoneMask form-control'])->label('Телефон', ['class' => 'required']) ?>
            <?php $volunteerModel->showPhone = 0; ?>
            <?= $form->field($volunteerModel, 'showPhone')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'])->label(false) ?>
            <?= $form->field($volunteerModel, 'time')->textInput(['placeholder' => 'Укажите удобное время для звонка'])->label('Укажите удобное время для звонка') ?>

        </div>

        <div class="col-xs-12">
            <?= $form->field($volunteerModel, 'help')->textarea(['Могу помочь'])->label('Могу помочь', ['class' => 'required']) ?>
        </div>

        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить профайл', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php endif; ?>

<div style="height: 50px"></div>