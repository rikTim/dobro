<div class="col-xs-12 col-sm-8 info">
    <h3><?= $model->name . ' ' . $model->lastname ?></h3>
    <span class="city"><?= $model->countryTitle . ', ' . $model->city ?></span>

    <?php
        if($model->status->id == 1 || $model->status->id == 4):
            $btnType = 'label-info';
        elseif($model->status->id == 2):
            $btnType = 'label-success';
        else:
            $btnType = 'label-danger';
        endif;
    ?>

    <h4 style="display: inline-block; float: right">
        <span class="label <?= $btnType ?>"><?= $model->status->title ?></span>
    </h4>

    <br>
    <p><b>Возраст: </b> <?= $model->age ?> <?= Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $volunteersCount) ?></p>

    <?php if(!empty($model->phone)): ?>
        <p><b>Номер телефона: </b> <?= $model->phone ?> </p>
    <?php endif; ?>

    <?php if(!empty($model->address)): ?>
        <p><b>Адрес:</b> <?= $model->address ?> </p>
    <?php endif; ?>

    <p><b>Email:</b> <?= Yii::$app->user->identity->email ?> </p>

    <?php if(!empty($model->help)): ?>
        <p><b>Могу помочь:</b> <?= $model->help ?> </p>
    <?php endif; ?>

    <?php if(!empty($model->specificNeed)):?>
        <p><b>Необходимо: </b><?= $model->specificNeed ?> </p>
    <?php endif;?>

    <?php if(!empty($model->shortTrouble)): ?>
        <p><b>Короткое описание проблемы:</b> <?= $model->shortTrouble ?> </p>
    <?php endif; ?>

    <?php if(!empty($model->trouble)): ?>
        <p><b>Проблема:</b> <?= $model->trouble ?> </p>
    <?php endif; ?>

    <?php if(!empty($model->cardNum) && $model->cardNum != ' '): ?>
        <p><b>Реквизиты:</b> <?= $model->cardNum ?> </p>
    <?php endif; ?>

    <?php if(!empty($model->vk)): ?>
        <a target="_blank" href="https://www.vk.com/<?= $model->vk ?>"><i class="fa social fa-vk"></i></a>
    <?php endif; ?>

    <?php if(!empty($model->fb)): ?>
        <a target="_blank" href="https://facebook.com/<?= $model->fb ?>"><i class="fa social fa-facebook-official"></i></a>
    <?php endif; ?>
</div>