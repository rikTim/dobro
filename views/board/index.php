<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BoardSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Boards';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-envelope"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            [
                                'attribute'=>'userId',
                                'label'=>'Кто Создал',
                                'value'=> function($data){
                                    $userModel = Yii::$app->DLL->getModelByUserId($data->userId);

                                    $user = $userModel->find()->where(['userId'=>$data->userId])->one();
                                     return  $user->name . ' ' . $user->lastname;

                                }
                            ],

                            [
                                'attribute'=>'countryId',
                                'value'=>'countryTitle',
                                'filter'=>ArrayHelper::map(\app\models\Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                            ],
                            [
                                'attribute'=>'cityId',
                                'value'=>'cityTitle',
                                'filter'=>ArrayHelper::map(\app\models\Cities::find()->where(['countryId' => 2])->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
                            ],
                            [
                                'attribute'=>'helpId',
                                'value'=>'helpTitle',
                                'filter'=>ArrayHelper::map(\app\models\NeedType::find()->where('id != 1')->asArray()->all(), 'id', 'title'),
                            ],
                            [
                                'attribute' => 'statusPosition',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $status = $data->status->title;
                                    switch ($status) {
                                        case 'новый':
                                            return '<span class="label bg-green "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'измененный':
                                            return '<span class="label bg-blue "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'отклоненный':
                                            return '<span class="label bg-yellow "> ' . $data->status->title . '</span>';
                                            break;
                                        default:
                                            return $data->status->title;
                                            break;
                                    }
                                },
                                'label' => 'Статус',
                                'filter' => ArrayHelper::map(\app\models\Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),
                            ],
                            // 'text:ntext',
                            // 'contacts',
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>
