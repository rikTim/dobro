<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Board */

$this->title = $user->name . ' ' . $user->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Boards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">

                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered detail-view">

                        <tbody>
                        <tr>
                            <th>Кто создал</th>
                            <td>
                                <a href="<?= Url::toRoute([$url . '/view', 'id' => $user->id]); ?>"><?= $user->name . ' ' . $user->lastname ?></a>
                            </td>
                        </tr>

                        <tr>
                            <th>Страна</th>
                            <td><?= $board->countryTitle ?></td>
                        </tr>

                        <tr>
                            <th>Город</th>
                            <td><?= $board->cityTitle ?></td>
                        </tr>

                        <tr>
                            <th>Категория</th>
                            <td><?= $board->helpTitle ?></td>
                        </tr>

                        <tr>
                            <th>Может помочь</th>
                            <td><?= $board->text ?></td>
                        </tr>

                        <tr>
                            <th>Контакты</th>
                            <td><?= $board->contacts ?></td>
                        </tr>
                        <tr>
                            <th>Подписчики</th>
                            <td>
                                <?php if (!empty($avatars)): ?>
                                    <?php foreach ($avatars as $key => $avatar): ?>

                                        <span class="photo">
                                        <a class="profile-gallery-item"

                                           href="<?= Url::toRoute([$url[$key], 'id' => $needy[$key]->id]); ?>"
                                           data-fancybox-group="gallery" title="">
                                            <h4><?= $needy[$key]->name . ' ' . $needy[$key]->lastname ?></h4>

                                            <?php if (empty($avatar)): ?>
                                                <div class="image-user">
                                                    <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                                                </div>
                                            <?php else: ?>
                                                <div class="image-user">
                                                    <img
                                                        src=" <?= Yii::getAlias('@web') . '/images/' . $needy[$key]->userId . '/m_' . $avatar ?>"
                                                        alt="">
                                                </div>
                                            <?php endif ?>
                                        </a>
                                    </span>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    Нету подписчиков
                                <?php endif; ?>

                            </td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>