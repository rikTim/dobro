<?php

use yii\helpers\Url;

?>

<div class="b_pic"><a href="<?=Url::toRoute(['/board/more', 'id' => $model->id])?>">
        <?php if (!empty($model->image)):?>
        <img style="width:106px" src="<?= Yii::getAlias('@web/images/board/m_' . $model->image) ?>">
        <?php else:?>
        <img style="width:106px" src="<?= Yii::getAlias('@web/images/help-image.jpeg') ?>">
        <?php endif;?>

        <?php $userRole = Yii::$app->DLL->getRoleByUserId($model->userId);
        switch ($userRole) {
            case 'volunteer':
                $name = $model->fromVolunteerUser->name;
                $lastname = $model->fromVolunteerUser->lastname;
                $id = $model->fromVolunteerUser->id;
                $url = '/volunteers/more';
                break;
            case 'angel':
                $name = $model->fromAngelUser->name;
                $lastname = $model->fromAngelUser->lastname;
                $id = $model->fromAngelUser->id;
                $url = '/angels/more';
                break;
            case 'needy':
                $name = $model->fromNeedyUser->name;
                $lastname = $model->fromNeedyUser->lastname;
                $id = $model->fromNeedyUser->id;
                $url = '/needies/more';
                break;
        } ?>
    </a></div>
    <div class="b_desc_content">
        <div class="b_title"><a href="<?= Url::toRoute([$url, 'id' => $id])?>"><?= $name . ' ' . $lastname ?></a><span class="b_locate"><?= $model->countryTitle . ', ' . $model->city?></span></div>
        <div class="b_desc"><a href="<?= Url::toRoute(['/board/more', 'id' => $model->id]) ?>"><?= $model->help->title ?></a>
            <p><?= Yii::$app->DLL->subStr($model->text, 250) ?> </p>
        </div>
        <div class="b_more"><a href="<?= Url::toRoute(['/board/more', 'id' => $model->id]) ?>">подробнее</a></div>
    </div>

<?php //if (!empty($modelBoard)): ?>
<!--    --><?php //$userRole = Yii::$app->DLL->getRoleByUserId($model->userId);
//    switch ($userRole) {
//        case 'volunteer':
//            $name = $model->fromVolunteerUser->name;
//            $lastname = $model->fromVolunteerUser->lastname;
//            break;
//        case 'angel':
//            $name = $model->fromAngelUser->name;
//            $lastname = $model->fromAngelUser->lastname;
//            break;
//        case 'needy':
//            $name = $model->fromNeedyUser->name;
//            $lastname = $model->fromNeedyUser->lastname;
//            break;
//    } ?>
<!--    -->
<!--    <a href="--><?//= Url::toRoute(['/board/more', 'id' => $model->id]) ?><!--">--><?//= $name . ' ' . $lastname ?><!--</a>-->
<!--    <span class="city">--><?//= $model->countryTitle
//        . ', ' . $model->city ?><!--</span>-->
<!--    <h4><p>--><?//= $model->help->title ?><!--</p></h4>-->
<?php //else: ?>
<!--    <h4><p>--><?//= $model->help->title ?><!--</p></h4>-->
<!--    Cтатус: --><?php //switch ($model->statusId) {
//        case 1:
//            $status = '<span class="label label-success">Новый</span>';
//            break;
//        case 4:
//            $status = '<span class="label label-primary">Измененый</span>';
//            break;
//        case 3:
//            $status = '<span class="label label-warning">Отклоненный</span>';
//            break;
//        case 2:
//            $status = '<span class="label label-primary">Подтвержденный</span>';
//            break;
//    } ?>
<!--    --><?//= $status ?>
<?php //endif; ?>