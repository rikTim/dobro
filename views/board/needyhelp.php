<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BoardSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Boards';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-envelope"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            [
                                'attribute'=>'userId',
                                'label'=>'Кто Создал',
                                'value'=> function($data){
                                    $userModel = Yii::$app->DLL->getModelByUserId($data->userId);

                                    $user = $userModel->find()->where(['userId'=>$data->userId])->one();
                                    return  $user->name . ' ' . $user->lastname;

                                }
                            ],

                            [
                                'attribute'=>'countryId',
                                'value'=>'countryTitle',
                                'filter'=>ArrayHelper::map(\app\models\Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                            ],
                            [
                                'attribute'=>'cityId',
                                'value'=>'cityTitle',
                                'filter'=>ArrayHelper::map(\app\models\Cities::find()->where(['countryId' => 2])->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
                            ],
                            [
                                'attribute'=>'helpId',
                                'value'=>'helpTitle',
                                'filter'=>ArrayHelper::map(\app\models\NeedType::find()->where('id != 1')->asArray()->all(), 'id', 'title'),
                            ],
                            [
                                'attribute'=>'statusId',
                                'label'=> 'Мне нужно',
                                'format'=>'raw',
                                'value'=>function($data){
                                    $count =$data->getNeedyCount();
                                    if($count > 0){
                                        $count ='<span class="label bg-green "> ' . $count . '</span>';
                                    }
                                    return $count ;
                                }
                            ],

                            // 'text:ntext',
                            // 'contacts',
                            ['class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '40'],
                                'template' => '{view}',
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch ($action) {

                                        case 'view':
                                            $url = Url::toRoute(['board/needy-view','id'=>$model->id]);
                                            return $url;

                                    }


                                }
                            ],

                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</section>
