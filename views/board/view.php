<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Board */

$this->title = $user->name . ' ' . $user->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Boards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content comments-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?php if ($model->statusId != 2): ?>
                            <?= Html::a('Подтвердить', ['approved', 'id' => $model->id,], ['class' => 'btn btn-success']) ?>
                        <?php endif; ?>
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id, 'type' => $type], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered detail-view">

                        <tbody>
                        <tr>
                            <th>Кто создал</th>
                            <td>
                                <a href="<?= Url::toRoute([$url . '/view', 'id' => $user->id]); ?>"><?= $user->name . ' ' . $user->lastname ?></a>
                            </td>
                        </tr>

                        <tr>
                            <th>Страна</th>
                            <td><?= $model->countryTitle ?></td>
                        </tr>

                        <tr>
                            <th>Город</th>
                            <td><?= $model->cityTitle ?></td>
                        </tr>

                        <tr>
                            <th>Категория</th>
                            <td><?= $model->helpTitle ?></td>
                        </tr>

                        <tr>
                            <th>Может помочь</th>
                            <td><?= $model->text ?></td>
                        </tr>

                        <tr>
                            <th>Контакты</th>
                            <td><?= $model->contacts ?></td>
                        </tr>
                        <tr>
                            <th>Контакты</th>
                            <td><?= $model->image?></td>
                        </tr>

                        <tr>
                            <th>Фото</th>
                            <td><?php $file = $model->image ?>
                                <?php if (!empty($file)): ?>
                                    <a href="<?= Yii::getAlias('@web') . '/images/board/' . $file ?> "><img
                                            src="<?= Yii::getAlias('@web') . '/images/board/m_' . $file ?>"></a>
                                <?php else: ?>
                                    <div class="image-user">
                                        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>
