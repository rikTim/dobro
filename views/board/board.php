<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Доска объявлений проекта - Мир добрых людей';
$this->registerMetaTag(['name' => 'description', 'content' => 'В этом разделе сайта вы можете присоедениться к миру добрых людей и разместить объявление с предложением помощи']);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>

<div class="container b_fixed">
    <h1><?php if (!empty($modelBoard)) : ?>
            <h1>Доска объявлений</h1>
        <?php else: ?>
            <h1>Мои объявления</h1>
        <?php endif; ?>
        <div class="b_help_text">Присоединяйся к Миру добрых людей! Благодаря тебе мы становимся сильнее. Вместе еще легче делать добрые дела, а счастливых людей и моментов станет еще больше! </div></h1>
    <?php $form = ActiveForm::begin(
        ['options'=>['class'=>'form-inline b_adds_form']]
    ); ?>

    <?= $form->field($modelBoard, 'countryId',['options'=>['class'=>'form-group col-md-4 col-sm-4 col-xs-12']])->dropDownList($countries,['prompt' => 'Страна','class' => 'form-control', 'id' => 'countriesList'])->label(false) ?>

    <?= $form->field($modelBoard, 'city',['options'=>['class'=>'form-group col-md-4 col-sm-4 col-xs-12']])->textInput(['id' => 'citiesList', 'placeholder' => 'Город','class' =>'form-control'])->label(false) ?>

    <?= $form->field($modelBoard, 'helpId',['options'=>['class'=>'form-group col-md-4 col-sm-4 col-xs-12']])->dropDownList($help,['prompt' => 'Категория объявления'], ['class' => 'form-control'])->label(false) ?>


        <div class="form-group b_actions">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn_search']) ?>
            <?= Html::a('Очистить', ['board'], ['class' => 'btn btn_reset']) ?>
            <?php if (!Yii::$app->user->isGuest): ?>

                <?php
                $model = Yii::$app->DLL->getModelByUserId(Yii::$app->user->id);
                $model = $model::find()->where(['userId' => Yii::$app->user->id])->one();
                ?>

                <?php if($model->statusId  == 1): ?>
                    <?= Html::button('Создать объявление', ['class' => 'btn btn_search']) ?>
                <?php else: ?>
                    <?= Html::a('Создать объявление', ['create'], ['class' => 'btn btn_search','style'=>'width: 200px;']) ?>
                <?php endif; ?>

                <?php if (!empty($userBoard)) : ?>
                    <?= Html::a('Мои объявления', ['user-board'], ['class' => 'btn btn_reset']) ?>
                <?php endif; ?>
            <?php endif; ?>
<!--            <p class="noApprovedMsg no-approved-msg col-md-12">Ваш профиль не подтвержден. Вы сможете создавать обьявления после подтверждения администратором.</p>-->

        </div>
    <?php ActiveForm::end(); ?>
    <div class="b_adds_list">

        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $board,
            'itemView' => '_view_board',
            'layout' => "{summary}\n{items}<div class=\"clear\"></div>{pager}",
            'emptyText' => 'На сайте еще нет обьявлений',
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'b_adds_item',
            ],
        ]) ?>

    </div>

</div>


