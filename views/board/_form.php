<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Board */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="board-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userId')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'countryId')->dropDownList($countries)->label('Страна') ?>

    <?= $form->field($model, 'cityId')->dropDownList($cities)->label('Город') ?>

    <?= $form->field($model, 'helpId')->dropDownList($help) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'contacts')->textInput(['maxlength' => true]) ?>



    <?php $file = $model->image; ?>
    <?php if (!empty($file)): ?>
        <div class="form-active">
            <p><label class="control-label">Фото</label></p>

            <div class="image-user">
                <div class="icons">

                </div>
                <span class="photo">
                    <a class="profile-gallery-item"
                       href="<?= Yii::getAlias('@web') . '/images/board/' . $file ?>"
                       data-fancybox-group="gallery" title="">
                        <img
                            src="<?= Yii::getAlias('@web') . '/images/board/m_' . $file ?>"
                            alt="">
                    </a>
                </span>
            </div>
        </div>
    <?php else: ?>
        <p><label class="control-label">Фото</label></p>
        <img src="<?= Yii::getAlias('@web') . '/images/no-image.jpeg' ?>">
    <?php endif; ?>

    <div class="clr"></div>
    <?= $form->field($model, 'statusId')->dropDownList($statuses) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
