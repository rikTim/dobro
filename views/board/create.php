<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


$this->title = $model->isNewRecord ? 'Создать' : 'Изменить' . ' объявление';
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>
<div class="site-index">

    <div class="container">
        <div class="page-header">
            <h1><?= $model->isNewRecord ? 'Создать' : 'Изменить' ?> объявление</h1>

        </div>
    </div>
    <div class="section list board">
        <div class="container">
            <?php if (!Yii::$app->user->isGuest): ?>
            <?php $form = ActiveForm::begin([
                'enableClientValidation' => true,
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($model, 'userId')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>

            <?= $form->field($model, 'statusId')->hiddenInput(['value' => 1])->label(false) ?>

            <?= $form->field($model, 'countryId')->dropDownList($countries, ['prompt' => 'Выберите страну', 'class' => 'chosenSelect', 'id' => 'countriesList'])->label('Страна') ?>

            <?= $form->field($model, 'city')->textInput(['id' => 'citiesList', 'placeholder' => 'Город'])->label('Город') ?>

            <?= $form->field($model, 'helpId')->dropDownList($help, ['prompt' => 'Выберите категорию', 'class' => 'chosenSelect']) ?>

            <div class="form-group b_load_pict col-xs-12 col-sm-12" >
                <label>Фото*</label> <?= $form->field($upload, 'boardFile')->fileInput()->label(false) ?>
            </div>
            <div class="clear"></div>


            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'contacts')->textarea(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','style'=>'color: #fff;background: #006cff;border-radius: 20px;border-color: #006cff;box-shadow: none;']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <?php else: ?>
            <div class="alert alert-warning">
                <a href="<?= Url::toRoute(['/user/register']) ?>">Заполните свой профиль</a> Вы сможете общаться с
                другими пользователями, получать уведомления о всех новых событиях и полноценно пользоваться всеми
                функциями сайта только после одобрения Вашей персональной информации администратором
            </div>
        <?php endif; ?>
    </div>
</div>
</div>
