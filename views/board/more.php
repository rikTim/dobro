<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'DobroUA.com Объявление  ' . $model->help->title;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['keywords']]);
?>


<!-- Volunteer more block -->
<div class="section more helper board">
    <div class="container">
        <div class="page-header">
            <h1>Объявление </h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col">
                <h3>
                    <a href="<?= Url::toRoute([$url . '/more', 'id' => $user->id]) ?>"><?= $user->name . ' ' . $user->lastname ?></a>
                </h3>
                <span class="city"><?= $model->country->title . ', ' . $model->city ?></span> <br>
                <p><b>Категория объявления: </b><?= $model->help->title ?></p>
                <p><b>Контакты: </b><?= $model->contacts ?></p>
                <p><b>Я могу помочь: </b> <?= $model->text ?></p>
            </div>
                <?php if (!Yii::$app->user->isGuest && $model->userId != Yii::$app->user->id): ?>
                    <?php if (empty($isNeedyHelp)): ?>
                        <?= Html::a("Необходимо <span class='badge'></span>", ['board/apply', 'id' => $model->id, 'action' => 'subscribe'], ['class' => 'btn btn-primary', 'style' => 'float: right']); ?>
                    <?php else: ?>
                        <?= Html::a("Не нужно<span class='badge'></span>", ['board/apply', 'id' => $model->id , 'action' => 'unsubscribe'], ['class' => 'btn btn-primary', 'style' => 'float: right']); ?>
                    <?php endif; ?>
                <?php endif; ?>
        </div>
    </div>
</div>