<?php
use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Присоединиться к нам';
?>

<!-- Sign in block -->
<div class="section">
    <div class="container user-register">
        <h1><?= $this->title; ?>

            <div class="row">
                <!-- Form col -->
                <?php
                $type = Yii::$app->request->get('type');
                if (!empty($type)) : $model->role = $type;
                else: $model->role = 1;
                endif;
                ?>

                <!-- Info col -->
                <div class="b_help_text">Присоединяйся к Миру добрых людей! Благодаря тебе мы становимся сильнее. Вместе
                    еще легче делать добрые дела, а счастливых людей и моментов станет еще больше! Информационный портал
                    для добрых дел dobroua.com открыт и ты можешь быть ангелом или волонтером, попросить о помощи или
                    помочь нам с организацией работы. Мы стремимся к тому, чтобы Мир добрых людей становился шире,
                    поэтому, нам всегда нужны люди, которые помогут в организации работы портала. Не важно какие у тебя
                    способности и профессия - дела найдутся для всех. В нашей стране ужасающая статистика инвалидности,
                    онко-заболеваний, детские приюты и дома престарелых переполнены людьми, которые нуждаются в тебе.
                    Вместе мы реализуем свое желание быть полезными миру без страха остаться обманутым! А еще,
                    dobroua.com - место, где ценят помощь и мы обязательно поблагодарим тебя за все, что ты сделаешь!
                    Спасибо, что ты с нами!
                </div>
        </h1>
        <!-- /Info col -->
        <div class="b_duty">

            <?php $form = ActiveForm::begin([
                'id' => 'registration-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true, /** должно быть true */
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-inline b_duty_form row'],
            ]); ?>
            <div class="formInputs">
                <div class="form-group b_duty_group">
                    <label class="b_duty_title" for="exampleInputEmail3">Роль <i class="b_icon-arr"></i></label>

                    <?= $form->field($model, 'role')->radioList(['1' => 'Нужна помощь', '2' => 'Ангел', '3' => 'Волонтер'], ['class' => 'roleTypes'])->label(false) ?>

                    <span class="roleInfo">

                        <div class="b_help_text">Нужна помощь - если ты или твой близкий человек попал в беду и вы не
                            можете справиться своими
                            силами, регистрируйтесь, пишите нам свою просьбу и мы сделаем все, чтобы вас услышали и
                            нашелся Ангел, который сможет вам помочь. Из-за частых случаев обманов, мы будем вынуждены
                            проверить предоставленную вами информацию.
                        </div>
                       <div class="b_help_text" style="display: none">Ангел - если ты имеешь желание и возможность
                           помочь морально или материально тем, кто
                           нуждается, то этот раздел для тебя. На этой платформе размещаются только проверенные просьбы
                           с реальными контактами людей. Став Ангелом, ты сможешь спасти жизнь человека или сделать
                           кого-то счастливее.
                       </div>
                        <div class="b_help_text" style="display: none">Волонтер - призвание для тех, кто готов проверять
                            информацию из просьбы о помощи в своем
                            городе или вблизи него и не боится столкнуться с бедой. Это возможность сделать свой вклад в
                            жизни многих людей.
                        </div>
                    </span>

                    <?php $model->showEmail = 0; ?>
                    <?php $model->showPhone = 0; ?>
                    <div class="clearboth"></div>


                    <?= $form->field($model, 'name', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Имя*'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'lastname', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Фамилия*'], ['class' => 'form-control'])->label(false) ?>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'phone', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Телефон*', 'class' => 'form-control phoneMask'])->label(false) ?>

                        <?= $form->field($model, 'showPhone', ['options' => ['class' => 'form-group']])->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'], ['data-type' => '4', 'class' => 'radioList'])->label(false) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'email', ['options' => ['class' => 'form-group', 'style' => 'width:100%']])->textInput(['placeholder' => 'Email'], ['class' => 'form-control'])->label(false) ?>

                        <?= $form->field($model, 'showEmail')->radioList(['0' => 'Виден всем', '1' => 'Виден только администратору'], ['data-type' => '4', 'class' => 'radioList'])->label(false) ?>
                    </div>
                    <?= $form->field($model, 'time', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Укажите удобное время для звонка', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'phoneOwner', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Контактное лицо', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>



                    <?= $form->field($model, 'countryId', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->dropDownList($countries, ['class' => 'form-control', 'id' => 'countriesList'])->label(false) ?>

                    <?= $form->field($model, 'city', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['id' => 'citiesList', 'placeholder' => 'Город'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'bank', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Банк', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'cardNum', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Реквизиты', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'age', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Возраст'], ['class' => 'form-control'])->label(false) ?>

                    <?php if ($module->enableGeneratingPassword == false): ?>
                        <?= $form->field($model, 'password', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->passwordInput(['placeholder' => 'Пароль'], ['class' => 'form-control'])->label(false) ?>
                    <?php endif ?>

                    <?= $form->field($model, 'vk', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в VK'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'fb', ['options' => ['class' => 'form-group col-xs-12 col-sm-6']])->textInput(['placeholder' => 'Ссылка на страницу в FB'], ['class' => 'form-control'])->label(false) ?>


                    <div class="form-group b_load_pict col-xs-12 col-sm-12" >
                        <label>Аватар*</label> <?= $form->field($uploadModel, 'imageFile')->fileInput()->label(false) ?>
                    </div>


                    <?= $form->field($model, 'shortTrouble', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textInput(['placeholder' => 'Краткое описание проблемы', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'trouble', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Описание проблемы', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'specificNeed', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Необходимо', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'help', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Могу помочь', 'data-type' => '3'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'help', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Могу помочь', 'data-type' => '2'], ['class' => 'form-control'])->label(false) ?>

                    <?= $form->field($model, 'need', ['options' => ['class' => 'form-group col-xs-12 col-sm-12']])->textarea(['placeholder' => 'Необходимо (денежная сумма)', 'data-type' => '1'], ['class' => 'form-control'])->label(false) ?>

                    <div class="clear"></div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <?= Html::submitButton('Присоединиться к нам', ['class' => 'btn btn-join_us']) ?>
                </div>


                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <!-- /Form col -->
    </div>
</div>
</div>
<!--<div style="height: 50px"></div>-->
<!-- /Sign in block -->