<?php

use yii\helpers\Html;

$this->title = 'Изменить Комментарий на стене: ' . $toUser->name . ' ' . $toUser->lastname;
?>
<section class="content static-pages-update">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'toUser' => $toUser,
                        'fromUser' => $fromUser,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>