<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Statuses;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;

$this->title = 'Комментарии ' . $title;
?>

<section class="content comments-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-envelope"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
<!--                        --><?//= Html::a('Создать Коментарий ' . $title, ['create','type' => $type], ['class' => 'btn btn-success']) ?>

                    </div>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,

                        'columns' => [

                            [
                                'attribute' => 'id',
                                'filter' => false,
                                'options' => ['width' => '50'],
                            ],

                            [
                                'attribute' => 'toUserId',
                                'label' => 'Кому ',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return $data->getFullName($data->toUserId);
                                },
                                'filter' => '',

                            ],

                            [
                                'attribute' => 'fromUserId',
                                'label' => 'От кого ',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return $data->getFullName($data->fromUserId);
                                },
                                'filter' => '',

                            ],

                            [
                                'attribute' => 'text',
                                'format' => 'ntext',
                                'value' => function ($data) {

                                    $count = iconv_strlen($data->text);
                                    if ($count > 140) {
                                        return Yii::$app->DLL->subStr($data->text);
                                    } else return $data->text;
                                },
                                'options' => ['width' => '400'],
                            ],


                            [
                                'attribute' => 'statusId',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $status = $data->status->title;
                                    switch ($status) {
                                        case 'новый':
                                            return '<span class="label bg-green "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'измененный':
                                            return '<span class="label bg-blue "> ' . $data->status->title . '</span>';
                                            break;
                                        case 'отклоненный':
                                            return '<span class="label bg-yellow "> ' . $data->status->title . '</span>';
                                            break;
                                        default:
                                            return $data->status->title;
                                            break;
                                    }
                                },
                                'filter' => ArrayHelper::map(Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),
                                'options' => ['width' => '200'],
                            ],

                            [
                                'attribute' => 'dateTime',
                                'format' => ['datetime', 'php:d/m/Y '],

                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'dateTime',
                                    'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'd/mm/yyyy'
                                    ]
                                ]),
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '110'],

                                'template' => '{approved} {rejected} {view} {update}  {delete}',
                                'buttons' => [
                                    'approved' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                                            'title' => Yii::t('app', 'Подтвердить'),
                                            ]);
                                    },
                                    'rejected' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                                            'title' => Yii::t('app', 'Отклонить'),
                                        ]);
                                    }

                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {

                                    switch($action){
                                        case 'approved':
                                            $url = Url::toRoute(['comments/approved','id'=> $model->id ]);
                                            return $url;
                                        case 'view':
                                            $url = Url::toRoute(['comments/view','id'=>$model->id]);
                                            return $url;
                                        case 'update':
                                            $url = Url::toRoute(['comments/update','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['comments/delete','id'=>$model->id]);
                                            return $url;
                                        case 'rejected':
                                            $url =  Url::toRoute(['rejected' ,'id'=> $model->id]);
                                            return $url;

                                    }
                                }
                            ],

                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>