<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Коментарий на стене : ' . $toUser->name . ' ' . $toUser->lastname;
?>

<section class="content comments-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                    <div class="box-tools">
                        <?php if ($model->statusId != 2): ?>
                            <?= Html::a('Подтвердить', ['approved', 'id' => $model->id, 'toUserId' => $model->toUserId], ['class' => 'btn btn-success']) ?>
                        <?php endif; ?>
                        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $model->id, 'type' => $type], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered detail-view">

                        <tbody>
                        <tr>
                            <th>Кому</th>
                            <td>
                                <a href="<?= Url::toRoute([$toUrl . '/view', 'id' => $toUser->id]); ?>"><?= $toUser->name . ' ' . $toUser->lastname ?></a>
                            </td>
                        </tr>
                        <?php if ($model->fromUserId == 1): ?>
                            <tr>
                                <th>От кого</th>
                                <td>Администратор</td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <th>От кого</th>
                                <td>
                                    <a href="<?= Url::toRoute([$fromUrl . '/view', 'id' => $fromUser->id]); ?>"><?= $fromUser->name . ' ' . $fromUser->lastname ?></a>
                                </td>
                            </tr>
                        <?php endif ?>
                        <tr>
                            <th>Текст</th>
                            <td><?= $model->text ?></td>
                        </tr>

                        <tr>
                            <th>Время</th>
                            <td><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d/m/Y "); ?></td>
                        </tr>

                        <tr>
                            <th>Статус</th>
                            <td><?= $model->status->title ?></td>
                        </tr>
                        <?php if (!empty($model->attaches)): ?>
                            <tr>
                                <th>Фото</th>
                                <td>
                                    <?php foreach ($model->attaches as $attach): ?>
                                        <a href="<?= Yii::getAlias('@web') . '/images/attachments/' . $model->id . '/' . $attach->path ?>">
                                            <img
                                                src="<?= Yii::getAlias('@web') . '/images/attachments/' . $model->id . '/m_' . $attach->path ?>"></a>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>