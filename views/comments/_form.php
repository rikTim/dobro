<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
use app\models\Statuses;
use vova07\imperavi\Widget;


/* @var $this yii\web\View */
/* @var $model app\models\Comments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comments-form">

    <?php $form = ActiveForm::begin(['action' => ['update', 'id'=> $model->id ]]); ?>

    <?= $form->field($model, 'toUserId')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fromUserId')->hiddenInput()->label(false) ?>
    <div class="form-group field-comments-fromuserid required">
        <label class="control-label" for="comments-fromuserid">Кому</label>
        <input type="text" class="form-control" name="" value="<?= $toUser->name . ' ' . $toUser->lastname ?>">

        <div class="help-block"></div>
    </div>

    <div class="form-group field-comments-fromuserid required">
        <label class="control-label" for="comments-fromuserid">От кого</label>
        <?php if ($model->fromUserId == 1): ?>
            <input type="text" class="form-control" name="" value="Администратор">

        <?php else: ?>

            <input type="text" class="form-control" name="" value="<?= $fromUser->name . ' ' . $fromUser->lastname ?>">
        <?php endif; ?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]);
    ?>
    <div class="form-group field-comments-updateDate">
        <label class="control-label" for="comments-dateTime">Дата</label>
        <?php $model->dateTime = Yii::$app->formatter->asDatetime($model->dateTime, "php:m/d/Y ") ?>
        <? echo MaskedInput::widget([
            'model' => $model,
            'attribute' => 'dateTime',
            'name' => 'dateTime',
            'mask' => '99/99/9999'
        ]);
        ?>
        <div class="help-block"></div>
    </div>

   <?php if(!empty($model->attaches)): ?>
        <div class="form-active">
            <p><label class="control-label">Фото</label></p>

            <?php foreach($model->attaches as $attach): ?>
            <div class="image-user">
                <div class="icons">
                    <i data-id="<?= $attach->id ?>" class="delete-photo fa fa-trash-o"></i>
                </div>
                <span class="photo">
                    <a class="profile-gallery-item"
                       href="<?= Yii::getAlias('@web') . '/images/attachments/' . $model->id . '/' . $attach->path ?>"
                       data-fancybox-group="gallery" title="">
                        <img
                            src="<?= Yii::getAlias('@web') . '/images/attachments/' . $model->id . '/m_' . $attach->path ?>"
                            alt="" >
                    </a>
                </span>
            </div>
            <?php endforeach;?>
        </div>

    <?php endif; ?>
    <div class="clr"></div>

    <?= $form->field($model, 'statusId')->dropDownList(ArrayHelper::map(Statuses::find()->all(), 'id', 'title')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
