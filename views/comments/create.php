<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Statuses;
use dosamigos\datepicker\DatePicker;

$this->title = 'Создать Комментарий';
?>

<section class="content comments-create">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>

                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'toUserId')->textInput() ?>


                    <?= $form->field($model, 'fromUserId')->hiddenInput(['value' => '18'])->label(false) ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                    <!--                    --><? //= $form->field($model, 'dateTime')->textInput(['maxlength' => true]) ?>
                    <?php $model->dateTime = Yii::$app->formatter->asDatetime(time(), "php:m/d/Y ") ?>
                    <?= $form->field($model, 'dateTime')->widget(
                        DatePicker::className(), [
                        // inline too, not bad
                        'inline' => true,
                        // modify template for custom rendering
                        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'mm/d/yyyy'
                        ]
                    ]); ?>

                    <?= $form->field($model, 'statusId')->dropDownList(ArrayHelper::map(Statuses::find()->all(), 'id', 'title')) ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>