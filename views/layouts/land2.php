<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DobroUA Мир добрых людей</title>


    <!-- Bootstrap Core CSS -->
    <link href="<?= Yii::getAlias('@web/css/land2/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= Yii::getAlias('@web/css/land2/agency.css'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= Yii::getAlias('@web/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

<!-- Header -->
<header>
    <div class="container">
        <div class="intro-text">
            <div class="intro-heading">Dobro<i style="color:#00a2ff">U</i><i style="color:#fff000">A</i></div>
            <div class="intro-lead-in">Мир добрых людей</div>
            <div class="intro-lead-in">До старта осталось</div>
            <div id="CDT"></div>
        </div>
    </div>
</header>

<?= $content ?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="copyright">Copyright &copy; DobroUA 2016</span>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="<?= Yii::getAlias('@web/js/land2/jquery.js'); ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= Yii::getAlias('@web/js/land2/bootstrap.min.js'); ?>"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<script src="<?= Yii::getAlias('@web/js/land2/classie.js'); ?>"></script>
<script src="<?= Yii::getAlias('@web/js/land2/cbpAnimatedHeader.js'); ?>"></script>

<!-- Contact Form JavaScript -->

<script src="<?= Yii::getAlias('@web/js/land2/jqBootstrapValidation.js'); ?>"></script>
<script src="<?= Yii::getAlias('@web/js/land2/contact_me.js'); ?>"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= Yii::getAlias('@web/js/land2/agency.js'); ?>"></script>


<script src="<?= Yii::getAlias('@web/js/land2/countdown.js'); ?>"></script>



<script type="text/javascript">(function(w,doc) {
if (!w.__utlWdgt ) {
    w.__utlWdgt = true;
    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
}})(window,document);
</script>
<div data-background-alpha="0.0" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="13" data-mode="share" data-like-text-enable="false" data-mobile-view="false" data-icon-color="#ffffff" data-orientation="fixed-left" data-text-color="#000000" data-share-shape="rectangle" data-sn-ids="fb.vk." data-share-size="40" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1471392" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>
</body>

</html>
