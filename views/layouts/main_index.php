<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\components\AuthWidget;
use app\components\ContactPopupWidget;
use app\components\NotificationMessageWidget;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use app\components\CountersWidget;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link
        href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic'
        rel='stylesheet' type='text/css'>
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
    <link href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/style.css" rel="stylesheet" type="text/css">
    <!--    <link href="-->
    <? //= Yii::$app->getUrlManager()->getBaseUrl() ?><!--/css/page.css" rel="stylesheet" type="text/css">-->
    <!--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>-->
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/bootstrap/js/bootstrap.min.js"
            type="text/javascript"></script>
    <!--    <script src="-->
    <? //= Yii::$app->getUrlManager()->getBaseUrl() ?><!--/js/js.js" type="text/javascript"></script>-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
    <!--    <script type="text/javascript">-->
    <!--        $(document).ready(function () {-->
    <!---->
    <!--        });-->
    <!--    </script>-->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">


    <?php

    $menuItems = [];

    $menuItems = [


        ['label' => 'Пользователи', 'url' => ['#'], 'options' => ['class' => 'item'], 'items' => [
            ['label' => 'Волонтеры', 'url' => ['/volunteers/list'], 'options' => ['class' => 'item']],
            ['label' => 'Ангелы', 'url' => ['/angels/list',], 'options' => ['class' => 'item']],
            ['label' => 'Кому помочь', 'url' => ['/needies/list',], 'options' => ['class' => 'item']],
        ]],

//        ['label' => 'Доска объявлений', 'url' => ['/board/board'], 'options' => ['class' => 'item']],

        ['label' => 'Партнеры', 'url' => ['/partner/partner'], 'options' => ['class' => 'item']],

    ];
    if (!Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'События', 'url' => ['/profile/index'], 'options' => ['class' => 'item'], 'visible' => (Yii::$app->user->can('angel')) && (Yii::$app->user->can('volunteer'))];
        $menuItems[] = ['label' => 'Задания', 'url' => ['/feed/index'], 'options' => ['class' => 'item'], 'visible' => (Yii::$app->user->can('angel')) && (Yii::$app->user->can('volunteer'))];

    }

    ?>

    <!-- Меню -->

    <?php


    NavBar::begin([

        'options' => [
            'class' => 'navbar b_menu_navbar',
            'style' => 'background-color: #103082',
        ],
        'innerContainerOptions' => ['class' => 'container-fluid'],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav b_menu  '],

        'items' => $menuItems,
    ]);

    $menuItems = [];
    $menuItems = [

        '<li style="margin-top: 8px;color:#fff;">Мы на:&nbsp;&nbsp;&nbsp;</li>',
        ['label' => '', 'url' => 'https://www.facebook.com/groups/1079732442078250/', 'options' => ['class' => 'b_fb']],

        ['label' => '', 'url' => 'https://vk.com/dobrouacom', 'options' => ['class' => 'b_vk']],

    ];?>

    <?php echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-right b_registrate'],

        'items' => $menuItems,
    ]);
    ?>
    <div id = 'w10' class="nav navbar-nav navbar-right b_registrate">
    <?php
   if (Yii::$app->user->isGuest): ?>
        <button type="button" class="btn btn-enter" data-toggle="modal"
                data-target="#authMadal">Войти
            <i class="b_icon-enter"></i></button>
    <?php else: ?>
        <a href="<?= Url::toRoute(['/site/logout']) ?>">Выход
            (<?= Yii::$app->user->identity->email ?>)</a>
    <?php endif;?>

    </div>

        <?

    NavBar::end();
    echo '<div class="clearfix"></div>';

    ?>
    <!--    <nav class="navbar b_menu_navbar" style="background-color: #103082">-->
    <!--        <div class="navbar-header">-->
    <!--            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"-->
    <!--                    class="navbar-toggle collapsed" type="button">-->
    <!--                <span class="sr-only">Toggle navigation</span>-->
    <!--                <span class="icon-bar"></span>-->
    <!--                <span class="icon-bar"></span>-->
    <!--                <span class="icon-bar"></span>-->
    <!--            </button>-->
    <!--        </div>-->
    <!--        <div class="navbar-collapse collapse" id="navbar" aria-expanded="false">-->
    <!--            <ul class="nav navbar-nav b_menu">-->
    <!--                <li class="item dropdown"><a class="dropdown-toggle"-->
    <!--                                             data-toggle="dropdown">Пользователи <b class="caret"></b></a>-->
    <!--                    <ul id="w4" class="dropdown-menu">-->
    <!--                        <li class="item"><a href="-->
    <? //= Url::toRoute(['/volunteers/list']) ?><!--">Волонтеры</a></li>-->
    <!--                        <li class="item"><a href="-->
    <? //= Url::toRoute(['/angels/list']) ?><!--">Ангелы</a></li>-->
    <!--                        <li class="item"><a href="-->
    <? //= Url::toRoute(['/needies/list']) ?><!--">Кому помочь</a></li>-->
    <!---->
    <!--                    </ul>-->
    <!--                </li>-->
    <!---->
    <!---->
    <!--                <li><a href="--><? //= Url::toRoute(['/board/board']) ?><!--">Доска объявлений</a></li>-->
    <!--                --><?php //if (!Yii::$app->user->isGuest): ?>
    <!--                    <li><a href="--><? //= Url::toRoute(['/profile/index']) ?><!--">Мой профиль</a></li>-->
    <!--                    --><?php //if (key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id)) !== 'needy'): ?>
    <!--                        <li><a href="--><? //= Url::toRoute(['/feed/index']) ?><!--">События</a></li>-->
    <!--                        <li><a href="--><? //= Url::toRoute(['/tasks/list']) ?><!--">Задания</a></li>-->
    <!--                    --><?php //endif; ?>
    <!--                --><?php //endif; ?>
    <!--                <li><a href="--><? //= Url::toRoute(['/partner/partner']) ?><!--">Партнеры</a></li>-->
    <!--            </ul>-->
<!--                <ul style="margin-top: -7px;" class="nav navbar-nav navbar-right b_registrate">-->
<!--                    <li><a href="https://www.facebook.com/groups/1079732442078250/" class="b_fb"></a><a-->
<!--                            href="https://vk.com/dobrouacom" class="b_vk"></a></li>-->
    <!--                <li>-->

    <!--                </li>-->
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </nav>-->
    <div class="header">
        <div class="container ">


            <div class="b_logo_header">
                <a href="<?= Url::toRoute(['/site/index']) ?>" class="b_logo"><img
                        src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/gfx/logo.png"></a>
                <?php if (Yii::$app->user->isGuest): ?>
                    <div class="pull-right"><a role="button" href="<?= Url::toRoute(['/user/register']) ?>"
                                               class="btn btn_join">Присоединиться</a></div>
                <?php endif; ?>
            </div>
            <div class="b_about text-center">
                <h2><a href="#about"><i class="b_icon-play pull-right"></i></a>О проекте
                    <div class="help-block"><a href="#about">Подробнее</a></div>
                </h2>
                <div class="b_stat_bar">
                    <ul>
                        <li class="b_volunteers"><a
                                href="<?= Url::toRoute(['/volunteers/list']) ?>"><span><?= \app\models\Volunteers::find()->where(['statusId' => 2])->count() ?></span>
                                волонтеров</a></li>
                        <li class="b_angels"><a
                                href="<?= Url::toRoute(['/angels/list']) ?>"><span><?= \app\models\Angels::find()->where(['statusId' => 2])->count() ?></span>
                                ангелов</a></li>
                    </ul>
                </div>
            </div>
            <div class="b_good">
                <a href="https://www.instagram.com/dobrouacom/" target="_blank">
                    <div class="pull-right" style="color: #fff000!important;">
                        <span>#делай</span>добро
                    </div>
                </a>
            </div>
        </div>
    </div>

    <?= NotificationMessageWidget::widget(); ?>

    <?= $content ?>
</div>

<div class="footer">
    <div class="container b_fixed" style="padding-top: 0px;">

            <nav class="navbar b_menu_navbar pull-right">
                <div class="navbar-collapse collapse" id="navbar" aria-expanded="false">
                    <ul class="nav navbar-nav b_menu">
                        <li><a href="<?= Url::toRoute(['/volunteers/list']) ?>">Волонтеры</a></li>
                        <li><a href="<?= Url::toRoute(['/angels/list']) ?>">Ангелы</a></li>
                        <li><a href="<?= Url::toRoute(['/needies/list']) ?>">Кому помочь</a></li>
<!--                        <li><a href="--><?//= Url::toRoute(['/board/board']) ?><!--">Доска объявлений</a></li>-->
                    </ul>
                </div><!--/.nav-collapse -->
            </nav>
            <div class="b_logo b_footer_item"><a href="<?= Url::toRoute(['/site/index']) ?>"><img
                        src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/gfx/logo_bt.png"></a></div>
            <div class="b_copy b_footer_item">© Все права защищены 2016</div>


            <? //= ContactPopupWidget::widget() ?>
            <?= AuthWidget::widget() ?>







            <?php $this->endBody() ?>

    </div>


</body>
</html>
<?php $this->endPage() ?>


<script id="cities-template" type="text/x-handlebars-template">
    {{#each cities}}
    <option value="{{id}}">{{title}}</option>
    {{/each}}
</script>

<script id="regions-template" type="text/x-handlebars-template">
    {{#each regions}}
    <option value="{{mainId}}">{{title}}</option>
    {{/each}}
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter35408725 = new Ya.Metrika({
                    id: 35408725,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks"); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/35408725" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-76055147-1', 'auto');
    ga('send', 'pageview');

</script>