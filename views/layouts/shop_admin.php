<?php

use yii\helpers\Html;
use app\components\AsideShopWidget;
use yii\helpers\Url;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/favicon.ico" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/admin/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/admin/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/admin/_all-skins.min.css') ?>">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['subscribe/index?page=1&sort=-id']); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>D</b>UA</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Dobro</b>UA</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!--                 Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a class="label-danger" data-method="post" href="<?= Url::toRoute(['admin/logout']); ?>">  <i class="fa fa-sign-out" ></i>Выход</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>


    <?= AsideShopWidget::widget(); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?= $content ?>
    </div><!-- /.content-wrapper -->


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; <?= date('Y') ?>
    </footer>
</div><!-- ./wrapper -->


<script src="<?= Yii::getAlias('@web/js/admin/jQuery-2.1.4.min.js') ?>"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="<?= Yii::getAlias('@web/js/admin/bootstrap.min.js') ?>"></script>
<?php $this->endBody() ?>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="<?= Yii::getAlias('@web/js/admin/bootstrap.min.js') ?>"></script>
<script src="<?= Yii::getAlias('@web/js/admin/app.min.js') ?>"></script>
<script src="<?= Yii::getAlias('@web/js/admin/admin.js') ?>"></script>
<script src="<?= Yii::getAlias('@web/js/admin/demo.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>


<script src="https://oss.maxcdn.com/underscorejs/1.8.3/underscore-min.js"></script>
<script src="<?= Yii::getAlias('@web/js/admin/metrica_charts.js') ?>"></script>

<script src="<?= Yii::getAlias('@web/js/admin/scripts/datetimepiker/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="<?= Yii::getAlias('@web/js/admin/scripts/yii/yii.gridView.js') ?>"></script>
<script src="<?= Yii::getAlias('@web/js/admin/scripts/yii/yii.js') ?>"></script>


</body>
</html>
<?php $this->endPage() ?>

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35408725 = new Ya.Metrika({ id:35408725, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/35408725" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
