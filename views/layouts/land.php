<!DOCTYPE HTML>
<html>
<head>
    <title>DobroUA</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="<?= Yii::getAlias('@web/css/land/ie/html5shiv.js'); ?>"></script><![endif]-->
    <script src="<?= Yii::getAlias('@web/js/land/jquery.min.js'); ?>"></script>
    <script src="<?= Yii::getAlias('@web/js/land/jquery.dropotron.min.js'); ?>"></script>
    <script src="<?= Yii::getAlias('@web/js/land/skel.min.js'); ?>"></script>
    <script src="<?= Yii::getAlias('@web/js/land/skel-layers.min.js'); ?>"></script>
    <script src="<?= Yii::getAlias('@web/js/land/init.js'); ?>"></script>
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/land/skel.css') ?>" />
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/land/style.css') ?>" />
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/land/style-wide.css') ?>" />
    <!--[if lte IE 8]><link rel="stylesheet" href="<?= Yii::getAlias('@web/css/land/ie/v8.css'); ?>" /><![endif]-->

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/land/normalize.css') ?>">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/land/style-countdown.css') ?>">
    <script src="<?= Yii::getAlias('@web/js/land/countdown.js'); ?>"></script>

    <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
</head>
<body>


<script type="text/javascript">(function(w,doc) {
        if (!w.__utlWdgt ) {
            w.__utlWdgt = true;
            var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
        }})(window,document);
</script>
<div data-background-alpha="0.0" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="separate" data-share-style="13" data-mode="share" data-like-text-enable="false" data-mobile-view="false" data-icon-color="#ffffff" data-orientation="fixed-left" data-text-color="#000000" data-share-shape="rectangle" data-sn-ids="fb.vk." data-share-size="40" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1471392" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="true" data-selection-enable="true" class="uptolike-buttons" ></div>

<?= $content ?>

<!-- Copyright -->
<div id="copyright">
    <div class="container">
        <div class="copyright">
            <p><a href="http://dobroua.com">DobroUA.com</a> мир добрых людей</p>
        </div>
    </div>
</div>

</body>
</html>
