<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShopComment */

$this->title = 'Создать коментарий к лоту';
$this->params['breadcrumbs'][] = ['label' => 'Shop Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <?= Html::encode($this->title) ?>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">

                    <?php $form = \yii\widgets\ActiveForm::begin(); ?>

                    <?= $form->field($model, 'taskId')->hiddenInput(['value' => $shopId])->label(false) ?>

                    <?= $form->field($model, 'userId')->hiddenInput(['value' => 1])->label(false) ?>

                    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

                    <?= $form->field($model, 'dateTime')->hiddenInput(['value' => time()])->label(false) ?>

                    <?= $form->field($model, 'statusId')->dropDownList($status) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php \yii\widgets\ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</section>

