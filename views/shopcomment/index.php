<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShopCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Коментарии';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content needies-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [


                            'shopId',
//                            'userId',
                            'text:ntext',



                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '120'],
                                'template' => '{message} {comment} {view} {update}  {delete}',
                                'buttons' => [

                                    'comment' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', $url, [
                                            'title' => Yii::t('app', 'Комментарий'),
                                        ]);
                                    },


                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    switch ($action) {

                                        case 'view':
                                            $url = Url::toRoute(['shopcomment/view','id'=>$model->id]);
                                            return $url;
                                        case 'update':
                                            $url = Url::toRoute(['shopcomment/update','id'=>$model->id]);
                                            return $url;
                                        case 'delete':
                                            $url = Url::toRoute(['shopcomment/delete','id'=>  $model->id]);
                                            return $url;
                                        case 'comment':
                                            $url = Url::toRoute(['shopcomment/create','id'=> $model->id]);
                                            return $url;
                                    }


                                }
                            ],

                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>

