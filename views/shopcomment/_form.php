<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ShopComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shopId')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'userId')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'statusId')->dropDownList($status) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
