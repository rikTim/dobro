<?php

namespace app\commands;

use Yii;
use app\models\Needies;


class DateController extends \yii\console\Controller
{
    public function actionRun()
    {
        $lang = 0;
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=$lang\r\n"
            )
        );


        $country = [
            '1',
//            '2',
//            '3',
//            '4',
//            '11',
//            '15',
//            '39',

        ];

        foreach ($country as $countryId) {

            $methodUrl = 'http://api.vk.com/method/database.getRegions?v=5.5&need_all=1&offset=0&count=1000&country_id=' . $countryId;
            $streamContext = stream_context_create($headerOptions);
            $json = file_get_contents($methodUrl, false, $streamContext);
            $arr = json_decode($json, true);
//            print_r($arr);
//            die();


            foreach ($arr['response']['items'] as $regionId) {


                $lang = 0;
                $headerOptions = array(
                    'http' => array(
                        'method' => "GET",
                        'header' => "Accept-language: en\r\n" .
                            "Cookie: remixlang=$lang\r\n"
                    )
                );


                $methodUrl = 'http://api.vk.com/method/database.getCities?v=5.5&country_id=' . $countryId . '&region_id=' . $regionId['id'] . '&offset=0&need_all=1&count=1000';
//               echo $methodUrl;
//                die();
                $streamContext = stream_context_create($headerOptions);
                $json = file_get_contents($methodUrl, false, $streamContext);
                $arr = json_decode($json, true);
//                print_r($arr['response']['items']);
//                die();

                $count = $arr['response']['count'];


                if ($count > 1000) {


                    $count = ceil($count / 1000);
                    for ($i = 0; $i < $count; $i++) {

                        $lang = 0;
                        $headerOptions = array(
                            'http' => array(
                                'method' => "GET",
                                'header' => "Accept-language: en\r\n" .
                                    "Cookie: remixlang=$lang\r\n"
                            )
                        );
                        $offset = $i * 1000;
                        $methodUrl = 'http://api.vk.com/method/database.getCities?v=5.5&country_id=' . $countryId . '&region_id=' . $regionId['id'] . '&offset=' . $offset . '&need_all=1&count=1000';
                        $streamContext = stream_context_create($headerOptions);
                        $json = file_get_contents($methodUrl, false, $streamContext);
                        $arr = json_decode($json, true);

                        foreach ($arr['response']['items'] as $title) {
                            if (!empty($title['title']) && !empty($regionId['id'] && !empty($countryId))) {
                                $value [] = '(' . $regionId['id'] . ', ' . $countryId . ' ,"' . $title['title'] . '" )';
                            }

                        }
                        if (!empty($value)) {
//                           echo implode(', ' . "", $value) ;



                            Yii::$app->db->createCommand('INSERT INTO `cities` ( `regionId` ,`countryId` , `title`) VALUES ' . implode(', ' . "", $value))->query();
                        }
                        $value = '';
                    }
                } else {
                    foreach ($arr['response']['items'] as $title) {
                        if (!empty($title['title']) && !empty($regionId['id'] && !empty($countryId))) {
                            $value [] = '(' . $regionId['id'] . ', ' . $countryId . ' ,"' . $title['title'] . '" )';
                        }

                    }
                    if (!empty($value)) {
//                        echo implode(', ' . "", $value) ;

//                        echo 'INSERT INTO `cities` ( `regionId` ,`countryId` , `title`) VALUES ' . implode(', ' , $value); die();


                        Yii::$app->db->createCommand('INSERT INTO `cities` ( `regionId` ,`countryId` , `title`) VALUES ' . implode(', ', $value))->query();


//                        die();
                    }
                    $value = '';
                }

            }
        }
    }

}