<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;


        // admin index page
        $adminIndex = $auth->createPermission('adminIndex');
        $adminIndex->description = 'adminIndex';
        $auth->add($adminIndex);


        // volunteer tasks
        $tasks = $auth->createPermission('task');
        $tasks->description = 'volunteer tasks';
        $auth->add($tasks);


        // create admin role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $adminIndex);


        // create volunteer role
        $volunteer = $auth->createRole('volunteer');
        $auth->add($volunteer);
        $auth->addChild($volunteer, $tasks);


        // create needy role
        $needy = $auth->createRole('needy');
        $auth->add($needy);


        // create angel role
        $angel = $auth->createRole('angel');
        $auth->add($angel);


        $auth->assign($admin, 1);
    }
}