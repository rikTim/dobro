'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    csso = require('gulp-csso');

// JS tasks
//gulp.task('js', function() {
//  gulp.src('web/js/*.js')
//    .pipe(concat('app.min.js'))
//    .pipe(gulp.dest('web/js/'))
//});

// SASS tasks
gulp.task('sass', function () {
  gulp.src('web/css/sass/**/*')
      .pipe(sass())
      .pipe(concat('style.min.css'))
      .pipe(csso())
      .pipe(gulp.dest('web/css/'));
});

// WATCH tasks
gulp.task('watch', function() {
  //gulp.watch('web/js/*.js', ['js']);
  gulp.watch('web/css/sass/*.scss', ['sass']);
});

gulp.task('default', [
  //'js',
  'sass',
  'watch'
]);