<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Angels[] $angels
 * @property Needies[] $needies
 * @property Volunteers[] $volunteers
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'countryId'], 'string', 'max' => 120],
            [['regionId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'countryId' => 'Country',
            'title' => 'Title',
            'regionId'=> 'Region',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAngels()
    {
        return $this->hasMany(Angels::className(), ['cityId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeedies()
    {
        return $this->hasMany(Needies::className(), ['cityId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteers()
    {
        return $this->hasMany(Volunteers::className(), ['cityId' => 'id']);
    }
}
