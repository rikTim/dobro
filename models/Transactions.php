<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property integer $fromUserId
 * @property integer $toUserId
 * @property integer $trancastion
 * @property string $dateTime
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromUserId', 'toUserId', 'trancastion', 'dateTime'], 'required'],
            [['fromUserId', 'toUserId', 'trancastion'], 'integer'],
            [['dateTime'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fromUserId' => 'From User ID',
            'toUserId' => 'To User ID',
            'trancastion' => 'Trancastion',
            'dateTime' => 'Date Time',
        ];
    }
}
