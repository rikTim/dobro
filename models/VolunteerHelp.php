<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "volunteerHelp".
 *
 * @property integer $id
 * @property integer $volunteerId
 * @property integer $helpTypeId
 *
 * @property Volunteers $volunteer
 * @property HelpTypes $helpType
 */
class VolunteerHelp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'volunteerHelp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['volunteerId', 'helpTypeId'], 'required'],
            [['volunteerId', 'helpTypeId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'volunteerId' => 'Volunteer ID',
            'helpTypeId' => 'Help Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteer()
    {
        return $this->hasOne(Volunteers::className(), ['id' => 'volunteerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpType()
    {
        return $this->hasOne(HelpTypes::className(), ['id' => 'helpTypeId']);
    }

    public function addVolunteerHelp($helpId, $volunteerId)
    {
        $this->helpTypeId = $helpId;
        $this->volunteerId = $volunteerId;
        $this->save();
    }
}
