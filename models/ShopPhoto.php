<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shopPhoto".
 *
 * @property integer $id
 * @property integer $shopId
 * @property string $path
 */
class ShopPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopPhoto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shopId', 'path'], 'required'],
            [['shopId'], 'integer'],
            [['path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shopId' => 'Shop ID',
            'path' => 'Path',
        ];
    }
}
