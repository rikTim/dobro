<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "volunteers".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property string $lastname
 * @property integer $age
 * @property integer $cityId
 * @property string $phone
 * @property string $email
 * @property string $vk
 * @property string $fb
 * @property integer $avatarId
 * @property integer $statusId
 *
 * @property VolunteerHelp[] $volunteerHelps
 * @property VolunteerTask[] $volunteerTasks
 * @property Users $user
 * @property Cities $city
 * @property Statuses $status
 */
class Volunteers extends \yii\db\ActiveRecord
{
    public $type = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'volunteers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'name', 'lastname', 'age','mainCountryId', 'city', 'phone', 'email', 'showPhone', 'showEmail', 'help'], 'required'],
            [['userId', 'cityId', 'mainCountryId', 'showPhone', 'avatarId', 'statusId'], 'integer'],
            [['email'], 'email'],
            [['age'], 'integer', 'max' => 99],
            [['city', 'phone', 'email', 'vk', 'fb','time'], 'string', 'max' => 120],
            [['name','lastname'],'string','max'=>20],
            [['createTime'],'string','max'=>255],
            [['help'], 'string'],
            [['type'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'ID Пользователя',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'age' => 'Возраст',
            'city' => 'Город',
            'cityId' => 'Город',
            'phone' => 'Телефон',
            'email' => 'Email',
            'vk' => 'Ссылка на страницу в VK',
            'fb' => 'Ссылка на страницу в FB',
            'showPhone' => 'Показывать телефон',
            'showEmail' => 'Показывать  email',
            'help' => 'Могу помочь',
            'avatarId' => 'Aватар',
            'statusId' => 'Статус',
            'time' => 'Укажите удобное время для звонка',
            'mainCountryId'=>'Страна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteerHelps()
    {
        return $this->hasMany(VolunteerHelp::className(), ['volunteerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteerTasks()
    {
        return $this->hasMany(UserTask::className(), ['volunteerId' => 'id']);
    }

    /**
     *
     */
    public function getAvatar()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId']);
    }

    public function getCityTitle()
    {
        return $this->cities->title;
    }

    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['mainId' => 'mainCountryId']);
    }

    public function getCountryTitle()
    {
        return $this->country->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRating()
    {
        return $this->hasOne(Ratings::className(), ['userId' => 'userId']);
    }

    /**
     * @return mixed
     */
        public function getRatingCount() {
        return $this->hasMany(Ratings::className(), ['userId' => 'userId'])
            ->sum('rating');
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        $this->setAttribute('vk', Yii::$app->DLL->getLinkId($this->vk));
        $this->setAttribute('fb', Yii::$app->DLL->getLinkId($this->fb));
        return parent::beforeSave($insert);
    }

    /** @inheritdoc */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $role = key(Yii::$app->authManager->getRolesByUser($this->userId));
            if (empty($role)) {
                $auth = Yii::$app->authManager;
                $authorRole = $auth->getRole('volunteer');
                $auth->assign($authorRole, $this->userId);
            }
        }
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNewVolunteers()
    {
        $time = time()-86400;
        return $this->find()
            ->where('createTime >' . $time)
            ->asArray()
            ->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getReversedVolunteers()
    {
        return $this->find()
            ->where(['statusId' => 4])
            ->asArray()
            ->all();
    }
}
