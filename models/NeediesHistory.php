<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "neediesHistory".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property string $lastName
 * @property integer $age
 * @property string $city
 * @property string $phone
 * @property string $vk
 * @property string $fb
 * @property string $shortTrouble
 * @property string $trouble
 * @property string $specificNeed
 * @property string $cardNum
 * @property integer $avatarId
 * @property string $diff
 */
class NeediesHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neediesHistory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'name', 'age', 'city', 'phone', 'vk', 'fb', 'shortTrouble'], 'required'],
            [['userId', 'age', 'avatarId'], 'integer'],
            [['trouble', 'specificNeed', 'diff'], 'string'],
            [['name', 'lastname', 'time', 'city', 'phone', 'vk', 'fb', 'shortTrouble', 'cardNum'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'name' => 'Name',
            'lastname' => 'Last Name',
            'age' => 'Age',
            'city' => 'City',
            'phone' => 'Phone',
            'time' => 'Time',
            'vk' => 'Vk',
            'fb' => 'Fb',
            'shortTrouble' => 'Short Trouble',
            'trouble' => 'Trouble',
            'specificNeed' => 'Specific Need',
            'cardNum' => 'Card Num',
            'avatarId' => 'Avatar ID',
            'diff' => 'Diff',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }
}
