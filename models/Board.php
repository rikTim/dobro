<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "board".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $countryId
 * @property integer $cityId
 * @property integer $helpId
 * @property string $text
 * @property string $contacts
 */
class Board extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'countryId', 'city', 'helpId', 'text', 'contacts', 'statusId'], 'required'],
            [['userId', 'regionId', 'countryId', 'cityId', 'helpId', 'statusId'], 'integer'],
            [['text'], 'string'],
            [['image'],'string', 'max' => 250],
            [['city'], 'string', 'max' => 120],
            [['contacts'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'countryId' => 'Страна',
            'cityId' => 'Город',
            'city' => 'Город',
            'helpId' => 'Я могу помочь',
            'text' => 'Опишите детальнее, чем вы можете помочь ',
            'contacts' => 'Контактные данные',
            'statusId' => 'Статус'
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('statusId', 1);
        }
        return parent::beforeSave($insert);
    }

    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId']);
    }

    public function getCityTitle()
    {
        return $this->cities->title;
    }

    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'countryId']);
    }

    public function getCountryTitle()
    {
        return $this->country->title;
    }

    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusId']);
    }

    public function getHelp()
    {
        return $this->hasOne(NeedType::className(), ['id' => 'helpId']);
    }

    public function getHelpTitle()
    {
        return $this->help->title;
    }
    //-----------------------------------
    public function getFromNeedyUser()
    {
        return $this->hasOne(Needies::className(), ['userId' => 'userId']);
    }

    public function getFromVolunteerUser()
    {
        return $this->hasOne(Volunteers::className(), ['userId' => 'userId']);
    }

    public function getFromAngelUser()
    {
        return $this->hasOne(Angels::className(), ['userId' => 'userId']);
    }
    //-----------------------------------

    public function newBoard()
    {
        return $this->find()
            ->where(['statusId'=>1])
            ->asArray()
            ->all();
    }
    public function getReversedBoard()
    {
        return $this->find()
            ->where(['statusId' => 4])
            ->asArray()
            ->all();
    }
    public function getNeedyCount() {
        return $this->hasMany(NeedyHelp::className(), ['boardId' => 'id'])
            ->count('needyId');
    }

}
