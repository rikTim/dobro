<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shop".
 *
 * @property integer $id
 * @property integer $size
 * @property string $color
 * @property string $consist
 * @property integer $cost
 * @property integer $avatarId
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['size', 'color', 'consist', 'cost', 'avatarId'], 'required'],
            [[ 'avatarId'], 'integer'],
            [['color', 'cost','consist'], 'string', 'max' => 255],
            [['size','number','state'], 'string', 'max' => 45],
            [['text'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'size' => 'Размер',
            'color' => 'Цвет',
            'consist' => 'Состав',
            'cost' => 'Цена',
            'avatarId' => 'Фото',


            'number' => 'Номер',
            'state' => 'Состояние',
            'text' => 'Описание',
        ];
    }

    public function getAvatar()
    {
        return $this->hasOne(ShopPhoto::className(), ['id' => 'avatarId']);
    }
}
