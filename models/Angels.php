<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "angels".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property string $lastname
 * @property integer $age
 * @property integer $cityId
 * @property string $phone
 * @property string $email
 * @property string $vk
 * @property string $fb
 * @property integer $avatarId
 * @property integer $statusId
 * @property integer $isPlus
 *
 * @property Users $user
 * @property Cities $city
 * @property Statuses $status
 */
class Angels extends \yii\db\ActiveRecord
{
    public $type = 2;
    public $ratingOrder;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'angels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'name', 'lastname', 'age', 'cityId','mainCountryId', 'email', 'showPhone', 'showEmail', 'avatarId', 'statusId', 'isPlus'], 'required'],
            [['userId', 'age', 'cityId', 'mainCountryId', 'avatarId', 'showPhone', 'statusId', 'isPlus'], 'integer'],
            [['email'], 'email'],
            [['age'], 'integer', 'max' => 99],
            [['city', 'phone', 'email', 'vk', 'fb','time'], 'string', 'max' => 120],
            [['name','lastname'],'string','max'=>20],
            [['createTime','help'],'string','max'=>255],
            [['type'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'age' => 'Возраст',
            'city' => 'Город',
            'cityId' => 'Город',
            'phone' => 'Телефон',
            'email' => 'Email',
            'showPhone' => 'Показывать телефон',
            'showEmail' => 'Показывать  email',
            'vk' => 'Ссылка на страницу в VK',
            'fb' => 'Ссылка на страницу в FB',
            'avatarId' => 'Ваше фото',
            'statusId' => 'Статус',
            'isPlus' => 'Элитный',
            'time' => 'Укажите удобное время для звонка',
            'mainCountryId'=>'Страна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId']);
    }

    public function getCityTitle()
    {
        return $this->cities->title;
    }

    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['mainId' => 'mainCountryId']);
    }

    public function getCountryTitle()
    {
        return $this->country->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusId']);
    }

    /**
     *
     */
    public function getRating()
    {
        return $this->hasOne(Ratings::className(), ['userId' => 'userId']);
    }

    /**
     * @return mixed
     */
    public function getRatingCount() {
        return $this->hasMany(Ratings::className(), ['userId' => 'userId'])
            ->sum('rating');
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNewAngels()
    {
        $a = time()-86400;

        return $this->find()
            ->where('createTime > ' .$a)
            ->asArray()
            ->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getReversedAngels()
    {
        return $this->find()
            ->where(['statusId' => 4])
            ->asArray()
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }

    /**
     *
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('vk', Yii::$app->DLL->getLinkId($this->vk));
            $this->setAttribute('fb', Yii::$app->DLL->getLinkId($this->fb));
        }
        return parent::beforeSave($insert);
    }

    /** @inheritdoc */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $role = key(Yii::$app->authManager->getRolesByUser($this->userId));
            if (empty($role)) {
                $auth = Yii::$app->authManager;
                $authorRole = $auth->getRole('angel');
                $auth->assign($authorRole, $this->userId);
            }
        }
    }
}
