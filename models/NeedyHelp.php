<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "needyHelp".
 *
 * @property integer $id
 * @property integer $needyId
 * @property integer $boardId
 */
class NeedyHelp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'needyHelp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['needyId', 'boardId'], 'required'],
            [['needyId', 'boardId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'needyId' => 'Needy ID',
            'boardId' => 'Board ID',
        ];
    }
}
