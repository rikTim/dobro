<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attachment".
 *
 * @property integer $id
 * @property integer $commentId
 * @property string $path
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commentId', 'path'], 'required'],
            [['commentId', 'type'], 'integer'],
            [['path'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'commentId' => 'Comment ID',
            'path' => 'Path',
            'type' => 'Type'
        ];
    }
}
