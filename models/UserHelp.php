<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userHelp".
 *
 * @property integer $id
 * @property integer $fromUserId
 * @property integer $toUserId
 * @property string $text
 * @property integer $dateTime
 */
class UserHelp extends \yii\db\ActiveRecord
{
    public $money;
    public $things;

    public $userName;
    public $realId;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userHelp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromUserId', 'toUserId', 'text', 'dateTime'], 'required'],
            [['fromUserId', 'toUserId', 'dateTime','isVerify'], 'integer'],
            [['text' , 'comment'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fromUserId' => 'From User ID',
            'toUserId' => 'To User ID',
            'text' => 'Text',
            'comment' => 'Comment',
            'dateTime' => 'Date Time',
        ];
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord && $insert) {
            $this->setAttributes([
                'fromUserId' => Yii::$app->user->id,
                'dateTime' => time(),
                'isVerify' => 0
            ]);
        }
        return parent::beforeSave($insert);
    }

    public static function getHelpForUser($userId)
    {
       return  UserHelp::find()->where(['fromUserId'=>$userId , 'isVerify'=> 0  ])->asArray()->all();
    }

    public function getHelp($type)
    {
        $new = [];
        $helps = $this->find()
            ->where(['isVerify' => 0])
            ->asArray()
            ->all();
        foreach ($helps as $help)
        {
            $fromUserId = $help['fromUserId'];
            $role = Yii::$app->DLL->getRoleByUserId($fromUserId);
            if($role == $type){
                $new[] = $help;
            }
        }
        return $new;
    }

    public function getNewHelp()
    {
        return $this->find()
            ->where(['isVerify'=>0])
            ->asArray()
            ->all();
    }

    //-----------------------------------
    public function getFromNeedyUser()
    {
        return $this->hasOne(Needies::className(), ['userId' => 'fromUserId']);
    }

    public function getFromVolunteerUser()
    {
        return $this->hasOne(Volunteers::className(), ['userId' => 'fromUserId']);
    }

    public function getFromAngelUser()
    {
        return $this->hasOne(Angels::className(), ['userId' => 'fromUserId']);
    }
    //-----------------------------------

    //-----------------------------------
    public function getToNeedyUser()
    {
        return $this->hasOne(Needies::className(), ['userId' => 'toUserId']);
    }

    public function getToVolunteerUser()
    {
        return $this->hasOne(Volunteers::className(), ['userId' => 'toUserId']);
    }

    public function getToAngelUser()
    {
        return $this->hasOne(Angels::className(), ['userId' => 'toUserId']);
    }
    //-----------------------------------

    public function getAttaches()
    {
        return $this->hasMany(Attachment::className(), ['commentId' => 'id']);
    }

}
