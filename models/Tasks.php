<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property string $title
 * @property string $descr
 * @property string $dateTime
 *
 * @property VolunteerTask[] $volunteerTasks
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'descr'], 'required'],
            [['descr'], 'string','max' => 1000],
            [['title','filePath'], 'string', 'max' => 120],
            [['dateTime', 'dateCreate'], 'string', 'max' => 11],
            [['isActive'],'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заглавие',
            'descr' => 'Текст',
            'dateTime' => 'Дата',
            'dateCreate' => 'Дата создания',
            'filePath'=> 'Фото',
            'isActive'=>'Активно'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getUserTask()
    {
        return $this->hasMany(UserTask::className(), ['taskId' => 'id']);
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('dateCreate', time());
        }
        return parent::beforeSave($insert);
    }
}
