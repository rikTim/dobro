<?php
namespace app\models;

use dektrium\user\models\RegistrationForm as BaseRegistrationForm;

class RegistrationForm extends BaseRegistrationForm
{
    public $role;

    /**   */
    public $name;
    public $lastname;
    public $age;
    public $phone;
    public $vk;
    public $fb;
    public $time;
    public $cityId;
    public $city;
    public $countryId;
    public $avatarId;
    public $regionId;

    /** Needies fields  */
    public $phoneOwner;
    public $trouble;
    public $shortTrouble;
    public $specificNeed;
    public $cardNum;
    public $bank;
    public $need;

    /** Volunteers/Angels fields  */
    public $showPhone;
    public $showEmail;
    public $help;




    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'age' => 'Возраст',
            'phone' => 'Телефон',
            'phoneOwner' => 'Владелец номера',
            'vk' => 'Ссылка на страницу в VK',
            'fb' => 'Ссылка на страницу в FB',
            'trouble' => 'Проблема',
            'shortTrouble' => 'Краткое описание проблемы',
            'cardNum' => 'Реквизиты',
            'bank' => 'Банк',
            'time' => 'Укажите удобное время для звонка',
            'cityId' => 'Город',
            'city' => 'Город',
            'countryId' => 'Страна',
            'specificNeed' => 'Необходимо',
            'avatarId' => 'Аватар',
            'need' => 'Необходимо (денежная сумма)',
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {

        $rules = parent::rules();
        $rules['roleRequired'] = ['role', 'required'];
        $rules['nameRequired'] = ['name', 'required'];
        $rules['lastnameRequired'] = ['lastname', 'required'];
        $rules['nameLen'] = ['name', 'string', 'max' => 20];
        $rules['lastnameLen'] = ['lastname', 'string', 'max' => 20];
        $rules['ageRequired'] = ['age', 'required'];
        $rules['ageInteger'] = ['age', 'integer', 'max' => 99];
        $rules['phoneRequired'] = ['phone', 'required'];
        $rules['vkRequired'] = ['vk', 'safe'];
        $rules['fbRequired'] = ['fb', 'safe'];
        $rules['timeSafe'] = ['time', 'safe'];
        $rules['cityRequired'] = ['city', 'safe'];
        $rules['countryRequired'] = ['countryId', 'required'];
        $rules['showPhoneRequired'] = ['showPhone', 'safe'];
        $rules['showEmailRequired'] = ['showEmail', 'safe'];
        $rules['helpRequired'] = ['help', 'safe'];
        $rules['avatarRequired'] = ['avatarId', 'safe'];
        $rules['phoneOwnerRequired'] = ['phoneOwner', 'safe'];
        $rules['shortTroubleRequired'] = ['shortTrouble', 'safe'];
        $rules['shortTroubleRequired'] = ['shortTrouble', 'string', 'max' => 80];
        $rules['troubleRequired'] = ['trouble', 'safe'];
        $rules['specificNeedRequired'] = ['specificNeed', 'safe'];
        $rules['cardNumRequired'] = ['cardNum', 'safe'];
        $rules['bankSafe'] = ['bank', 'safe'];
        $rules['needLen'] = ['need', 'integer', 'max' => 10000000000];
        $rules['needSafe'] = ['need', 'safe'];
        return $rules;
    }
}
