<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Board;

/**
 * BoardSeach represents the model behind the search form about `app\models\Board`.
 */
class BoardSeach extends Board
{
    public $statusPosition;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'countryId', 'cityId', 'helpId','statusId'], 'integer'],
            [['text', 'contacts','statusPosition'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Board::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'userId',
                'countryId',

                'helpId',
                'contacts',
                'text',
                'statusPosition'=>[
                    'asc' => ['statuses.position' => SORT_ASC],
                    'desc' => ['statuses.position' => SORT_DESC],
                ],
                'cityId',

            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['statuses']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'userId' => $this->userId,
            'countryId' => $this->countryId,
            'cityId' => $this->cityId,
            'helpId' => $this->helpId,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'contacts', $this->contacts]);
        $query->joinWith(['status' => function ($q) {
            $q->where('statuses.position LIKE "%' . $this->statusPosition . '%"');
        }]);

        return $dataProvider;
    }
}
