<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $path
 * @property integer $userId
 * @property integer $fileType
 * @property string $descr
 *
 * @property Users $user
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'userId', 'fileType', ], 'required'],
            [['userId', 'fileType'], 'integer'],
            [['path', 'descr'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'userId' => 'User ID',
            'fileType' => 'File Type',
            'descr' => 'Descr',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userId']);
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getFile($id)
    {
        return $this->find()->where(['id'=>$id])->one();
    }

    /**
     * @param $userId
     * @param $type
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getFileType($userId,$type)
    {
        return $this->find()->where(['userId'=>$userId,'fileType'=>$type])->all();
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getAllFile($id)
    {
        return $this->find()->where(['userId'=>$id,'fileType'=>3])->all();
    }
}
