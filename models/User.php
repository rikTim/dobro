<?php
namespace app\models;

use dektrium\user\models\User as BaseUser;
use yii\web\UploadedFile;

class User extends BaseUser
{
    public $role;

    /**   */
    public $name;
    public $lastname;
    public $age;
    public $phone;
    public $vk;
    public $fb;
    public $time;
    public $cityId;
    public $city;
    public $countryId;
    public $avatarId;

    /** Needies fields  */
    public $phoneOwner;
    public $trouble;
    public $shortTrouble;
    public $specificNeed;
    public $cardNum;
    public $bank;
    public $need;

    /** Volunteers/Angels fields  */
    public $showPhone;
    public $showEmail;
    public $help;



    public function scenarios()
    {
        $attributes = [
            'role',
            'name', 'lastname', 'age', 'phone', 'vk', 'fb', 'time', 'cityId', 'city', 'countryId', 'avatarId',
            'phoneOwner', 'trouble', 'shortTrouble', 'cardNum', 'bank', 'specificNeed', 'need',
            'showPhone', 'showEmail','createTime',
            'help',
        ];

        $scenarios = parent::scenarios();
        foreach ($attributes as $attribute) {
            $scenarios['create'][] = $attribute;
            $scenarios['update'][] = $attribute;
            $scenarios['register'][] = $attribute;
        }

        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        return $rules;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $message = var_export($_POST, true);
            \Yii::$app->mailer->compose()
                ->setFrom('admin@dobroua.com')
                ->setTo(['programek@mail.ua'])
                ->setSubject('Запрос на регистрацию dobroua.com')
                ->setHtmlBody($message)
                ->send();


            /** add role to new user */
            $auth = \Yii::$app->authManager;
            switch ($this->role) {
                case 1: $authorRole = $auth->getRole('needy'); break;
                case 2: $authorRole = $auth->getRole('angel'); break;
                case 3: $authorRole = $auth->getRole('volunteer'); break;
            }

            if (!empty($authorRole)) {
                $auth->assign($authorRole, $this->id);
            }


            $avatarId = null;
            $uploadModel = new UploadForm();
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            if (!empty($uploadModel->imageFile)) {
                $avatarId = $uploadModel->upload($this->id);
            }


            /** save user profile data */
            switch ($this->role) {
                case 1:
                    $needyModel = new Needies();
                    $needyModel->setAttributes([
                        'userId' => $this->id,
                        'name' => $this->name,
                        'lastname' => $this->lastname,
                        'email' => $this->email,
                        'age' => $this->age,
                        'city' => $this->city,
                        'mainCountryId' => $this->countryId,
                        'phone' => $this->phone,
                        'phoneOwner' => $this->phoneOwner,
                        'vk' => $this->vk,
                        'fb' => $this->fb,
                        'trouble' => $this->trouble,
                        'shortTrouble' => $this->shortTrouble,
                        'cardNum' => $this->cardNum,
                        'bank' => $this->bank,
                        'specificNeed' => $this->specificNeed,
                        'time' => $this->time,
                        'need' => $this->need,
                        'statusId' => 1,
                        'avatarId' => $avatarId,
                    ]);
                    $needyModel->save(false);
                    \Yii::$app->DLL->newUser('needies/view/' . $needyModel->id);
                    break;

                case 2:
                    $angelModel = new Angels();
                    $angelModel->setAttributes([
                        'userId' => $this->id,
                        'name' => $this->name,
                        'lastname' => $this->lastname,
                        'email' => $this->email,
                        'age' => $this->age,
                        'city' => $this->city,
                        'mainCountryId' => $this->countryId,
                        'phone' => $this->phone,
                        'vk' => $this->vk,
                        'fb' => $this->fb,
                        'time' => $this->time,
                        'statusId' => 2,
                        'help' => $this->help,
                        'avatarId' => $avatarId,
                        'createTime' => time()
                    ]);
                    $angelModel->save(false);
                    \Yii::$app->DLL->newUser('angels/view/' . $angelModel->id);
                    break;

                case 3:
                    $volunteerModel = new Volunteers();
                    $volunteerModel->setAttributes([
                        'userId' => $this->id,
                        'name' => $this->name,
                        'lastname' => $this->lastname,
                        'email' => $this->email,
                        'age' => $this->age,
                        'city' => $this->city,
                        'mainCountryId' => $this->countryId,
                        'phone' => $this->phone,
                        'vk' => $this->vk,
                        'fb' => $this->fb,
                        'time' => $this->time,
                        'statusId' => 2,
                        'help' => $this->help,
                        'avatarId' => $avatarId,
                        'createTime' => time()
                    ]);
                    $volunteerModel->save(false);
                    \Yii::$app->DLL->newUser('volunteers/view/' . $volunteerModel->id);
                    break;
            }
            /** login after registration */
            \Yii::$app->user->login($this, $this->module->rememberFor);

        }
    }
}
