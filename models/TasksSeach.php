<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSeach represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSeach extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'descr', 'dateTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tasks::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->dateTime)) {
            // date to search
            $date = \DateTime::createFromFormat('d/m/Y', $this->dateTime);
            $date->setTime(0, 0, 0);
            $unixDateStart = $date->getTimeStamp();
            $date->add(new \DateInterval('P1D'));
            $date->sub(new \DateInterval('PT1S'));
            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'dateTime', $unixDateStart, $unixDateEnd]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'descr', $this->descr]);


        return $dataProvider;
    }
}
