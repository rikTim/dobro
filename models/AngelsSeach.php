<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Angels;

/**
 * AngelsSeach represents the model behind the search form about `app\models\Angels`.
 */
class AngelsSeach extends Angels
{
    public $cityTitle;
    public $statusPosition;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','cityId','mainCountryId', 'userId', 'age',  'avatarId', 'statusId', 'isPlus'], 'integer'],
            [['name', 'lastname', 'phone', 'email', 'vk', 'fb','cityTitle','statusPosition','createTime','city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Angels::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'lastname',
                'age',
                'phone',
                'statusPosition'=>[
                    'asc' => ['statuses.position' => SORT_ASC],
                    'desc' => ['statuses.position' => SORT_DESC],
                ],
                'cityId',
                'city',
                'mainCountryId',
                'email',
                'createTime'
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
//            $query->joinWith(['city']);
            $query->joinWith(['statuses']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'userId' => $this->userId,
            'age' => $this->age,
            'cityId' => $this->cityId,
            'city' => $this->city,
            'mainCountryId' => $this->mainCountryId,
            'avatarId' => $this->avatarId,
            'statusId' => $this->statusId,
            'isPlus' => $this->isPlus,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'vk', $this->vk])
            ->andFilterWhere(['like', 'cityId', $this->cityId])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'mainCountryId', $this->mainCountryId])
            ->andFilterWhere(['like', 'fb', $this->fb]);

//        $query->joinWith(['city' => function ($q) {
//            $q->where('cities.title LIKE "%' . $this->cityTitle . '%"');
//        }]);
        $query->joinWith(['status' => function ($q) {
            $q->where('statuses.position LIKE "%' . $this->statusPosition . '%"');
        }]);

        return $dataProvider;
    }
}
