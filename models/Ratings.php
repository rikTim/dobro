<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ratings".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $rating
 */
class Ratings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ratings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'rating', 'descr'], 'required'],
            [['userId', 'rating', 'dateTime'], 'integer'],
            [['descr'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'rating' => 'Рейтинг',
            'descr' => 'Описание',
            'dateTime' => 'Дата'
        ];
    }
}
