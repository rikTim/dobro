<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $toUserId
 * @property integer $fromUserId
 * @property string $text
 * @property string $dateTime
 * @property integer $isReaded
 */
class Messages extends \yii\db\ActiveRecord
{
    public $userName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['toUserId', 'toUserType', 'text', 'dateTime', 'fromUserType', 'toUserType'], 'required'],
            [['toUserId', 'fromUserId', 'isReaded', 'fromUserType', 'toUserType'], 'integer'],
            [['text'], 'string','max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'toUserId' => 'To User ID',
            'fromUserId' => 'From User ID',
            'text' => 'Текст',
            'dateTime' => 'Date Time',
            'isReaded' => 'Is Readed',
            'fromUserType' => 'From User Type',
            'toUserType' => 'To User Type',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttributes([
                'fromUserId' => Yii::$app->user->id,
                'dateTime' => time(),
                'isReaded' => 0
            ]);
        }
        return parent::beforeSave($insert);
    }


    //-----------------------------------
    public function getFromNeedyUser()
    {
        return $this->hasOne(Needies::className(), ['userId' => 'fromUserId']);
    }

    public function getFromVolunteerUser()
    {
        return $this->hasOne(Volunteers::className(), ['userId' => 'fromUserId']);
    }

    public function getFromAngelUser()
    {
        return $this->hasOne(Angels::className(), ['userId' => 'fromUserId']);
    }
    //-----------------------------------

    /**
     * @param $type
     * @return mixed
     */
    public function getOneMessage($type)
    {
        $messages = $this->find()
            ->select('fromUserId, toUserId')
            ->where(" ((toUserId = 1 AND toUserType = 1) AND (fromUserType = " . $type . ")) OR ((fromUserId = 1  AND fromUserType = 1) AND (toUserType = " . $type . ") )")
            ->orderBy('dateTime DESC')
            ->one();
        if ($messages->fromUserId == 1) {
            $id = $messages->toUserId;
        } else {
            $id = $messages->fromUserId;
        }
        return $id;
    }

    /**
     * @param $userid
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getMessagesToAdmin($userid, $type)
    {
        return $messages = $this->find()
            ->where("((fromUserId = " . $userid . " AND fromUserType = " . $type . " ) AND (toUserId = 1 AND toUserType = 1)) OR ((fromUserId = 1  AND fromUserType = 1) AND ( toUserId =" . $userid . " AND toUserType = " . $type . ") )")
            ->orderBy('dateTime DESC')
            ->all();
    }

    /**
     * @param $toId
     * @param $fromId
     * @return int
     */
    public function readMessage($toId, $fromId)
    {
        return $this->updateAll(['isReaded' => 1], ['toUserId' => $toId, 'fromUserId' => $fromId]);
    }

    /**
     * @param $type
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getAllMessage($type)
    {
        return $messages = $this->find()
            ->select('fromUserId , toUserId, text , dateTime ')
            ->where(" ((toUserId = 1 AND toUserType = 1) AND(fromUserType = " . $type . ")) OR ((fromUserId = 1 AND fromUserType = 1) AND (toUserType = " . $type . ") )")
            ->groupBy(['toUserId', 'fromUserId'])
            ->orderBy('dateTime DESC')
            ->all();
    }

    /**
     * @param $type
     * @param $id
     * @return array|null|string|\yii\db\ActiveRecord
     *
     */
    public function getMessageUserType($type, $id)
    {

        $user = '';
        switch ($type) {
            case 2:
                $user = Angels::find()->where(['userId' => $id])->one();
                break;
            case 3:
                $user = Volunteers::find()->where(['userId' => $id])->one();
                break;
            case 4:
                $user = Needies::find()->where(['userId' => $id])->one();
                break;
        }

        return $user;
    }

    /**
     * @param $allMessage
     * @return array
     */
    public function getAllUseridInMessages($allMessage)
    {
        $fromuseridArr = [];

        if (!empty($allMessage)) {
            foreach ($allMessage as $key => $message) {
                if (!in_array($message->fromUserId, $fromuseridArr)) {
                    if ($message->fromUserId == 1) {
                        if (!in_array($message->toUserId, $fromuseridArr)) {
                            $fromuseridArr[] = $message->toUserId;
                        }
                    } else {
                        if (!in_array($message->fromUserId, $fromuseridArr)) {
                            $fromuseridArr[] = $message->fromUserId;
                        }
                    }
                } else {
                    unset($allMessage[$key]);
                }
            }
        }
        return $fromuseridArr;
    }

    /**
     * @param $userid
     * @param $type
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getTextMessage($userid, $type)
    {
        return $message = $this->find()
            ->select("text,dateTime,isReaded,toUserId")
            ->where("((fromUserId = " . $userid . " AND fromUserType = " . $type . " ) AND (toUserId = 1 AND toUserType = 1)) OR ((fromUserId = 1  AND fromUserType = 1) AND ( toUserId =" . $userid . " AND toUserType = " . $type . ") )")
            ->orderBy('dateTime DESC')
            ->one();
    }

    /**
     * @param $fromuseridArr
     * @param $type
     * @return mixed
     */
    public function getAllInform($fromuseridArr, $type)
    {
        $pathArr = [];
        $user = [];
        $texts = [];

        if (!empty($fromuseridArr)) {
            $count = count($fromuseridArr);
            for ($i = 0; $i < $count; $i++) {
                $user[$i] = $this->getMessageUserType( $type,$fromuseridArr[$i]);
                $path = $user[$i]->avatar->path;
                $pathArr[$i] = $path;
                $texts[$i] = $this->getTextMessage($fromuseridArr[$i], $type);
            }
        }

        $AllInform[0] = $pathArr;
        $AllInform[1] = $texts;
        $AllInform[2] = $user;
        return $AllInform;
    }


    public function writeMessage($fromUserId, $fromUserType, $toUserId, $toUserType, $text, $isReaded = 0)
    {
        $this->fromUserId = $fromUserId;
        $this->fromUserType = $fromUserType;
        $this->toUserId = $toUserId;
        $this->toUserType = $toUserType;
        $this->text = $text;
        $this->isReaded = $isReaded;
        $this->dateTime = time();
        $this->save();
    }

    public function getCountMessageToAdmin($type)
    {
        $count = $message = $this->find()
            ->select(['COUNT(*) AS count'])
            ->where('toUserId = 1 AND toUserType = 1 AND fromUserType = ' . $type . ' AND isReaded = 0')
            ->asArray()
            ->all();

        return $count[0]['count'];
    }

    public function newMessage($type)
    {
        $count = $this->getCountMessageToAdmin($type);
        return $count > 0 ? $count : '';
    }

    
    /** Рассылка сообщений всем волонтерам о новом задании */
    public function taskNotification($text)
    {
        $volunteersArray = AuthAssignment::find()->where(['item_name' => 'volunteer'])->all();

        foreach ($volunteersArray as $volunteer) {
            $messages = new Messages();
            $messages->writeMessage(1, 1, $volunteer->user_id, 3, 'Появилось новое задание ' . $text, 0);
            unset($messages);

            $volunteerEmail = $volunteer->volunteer->email;
            if (!empty($volunteerEmail)) {
                $message = 'На сайте DobroUA.com добавлено новое задание "' . $text . '"';
                Yii::$app->mail->compose()
                    ->setTo($volunteerEmail)
                    ->setFrom('admin@dobroua.com')
                    ->setSubject('Новое задание для волонтеров на сайте dobroua.com')
                    ->setTextBody($message, 'text/html')
                    ->send();
            }

            Yii::$app->DLL->addNotification($volunteer->user_id);
        }
    }






















































}
