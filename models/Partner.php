<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property integer $id
 * @property string $name
 * @property integer $phone
 * @property string $url
 * @property integer $avatarId
 * @property string $text
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'url', 'avatarId', 'text'], 'required'],
            [['phone', 'avatarId'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 120],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'url' => 'Ссылка',
            'avatarId' => 'Аватар',
            'text' => 'Описание',
        ];
    }

    public function getAvatar()
    {
        return $this->hasOne(PartnerPhoto::className(), ['id' => 'avatarId']);
    }
}
