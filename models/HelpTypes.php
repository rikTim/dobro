<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "helpTypes".
 *
 * @property integer $id
 * @property string $title
 * @property integer $isNew
 *
 * @property VolunteerHelp[] $volunteerHelps
 */
class HelpTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'helpTypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'isNew'], 'required'],
            [['isNew'], 'integer'],
//            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'isNew' => 'Is New',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteerHelps()
    {
        return $this->hasMany(VolunteerHelp::className(), ['helpTypeId' => 'id']);
    }

    public function getNew()
    {
        return $this->find()
            ->where(['isNew' => 1])
            ->asArray()
            ->all();
    }
}
