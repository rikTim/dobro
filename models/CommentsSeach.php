<?php

namespace app\models;

use app\components\DLL;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Comments;

/**
 * CommentsSeach represents the model behind the search form about `app\models\Comments`.
 */
class CommentsSeach extends Comments
{
    public $auth;
    public $fullName;
    public $statusPosition;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'toUserId',  'fromUserId',  'statusId'], 'integer'],
            [['text', 'dateTime','auth','fullName','statusPosition'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type = '')
    {
        $query = Comments::find();
        $query->leftJoin(['auth_assignment'], 'auth_assignment.user_id = toUserId');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'toUserId',
                'fromUserId',
                'statusId',
                'text',
                'dateTime',
                'statusPosition'=>[
                    'asc' => ['statuses.position' => SORT_ASC],
                    'desc' => ['statuses.position' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['statuses']);
            return $dataProvider;
        }
        if (!empty($this->dateTime)) {
            // date to search
            $date = \DateTime::createFromFormat('d/m/Y', $this->dateTime);
            $date->setTime(0, 0, 0);
            $unixDateStart = $date->getTimeStamp();
            $date->add(new \DateInterval('P1D'));
            $date->sub(new \DateInterval('PT1S'));
            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'dateTime', $unixDateStart, $unixDateEnd]);
        }
//        if(!empty($this->fullName)){
//            $query->
//        }


        $query->andFilterWhere([
            'id' => $this->id,
            'auth_assignment.item_name'=>$type,
            'fromUserId' => $this->fromUserId,
            'statusId' => $this->statusId,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->joinWith(['status' => function ($q) {
            $q->where('statuses.position LIKE "%' . $this->statusPosition . '%"');
        }]);

        return $dataProvider;
    }
}
