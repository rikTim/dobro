<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskComment;

/**
 * TaskCommentSeach represents the model behind the search form about `app\models\TaskComment`.
 */
class TaskCommentSeach extends TaskComment
{

    public $statusPosition;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'taskId', 'userId', 'statusId'], 'integer'],
            [['text', 'dateTime','statusPosition'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskComment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->setSort([
            'attributes' => [
                'id',
                'taskId',
                'userId',
                'statusId',
                'text',
                'dateTime',
                'statusPosition'=>[
                    'asc' => ['statuses.position' => SORT_ASC],
                    'desc' => ['statuses.position' => SORT_DESC],
                ],
            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['statuses']);
            return $dataProvider;
        }

        if (!empty($this->dateTime)) {
            // date to search
            $date = \DateTime::createFromFormat('d/m/Y', $this->dateTime);
            $date->setTime(0, 0, 0);
            $unixDateStart = $date->getTimeStamp();
            $date->add(new \DateInterval('P1D'));
            $date->sub(new \DateInterval('PT1S'));
            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'dateTime', $unixDateStart, $unixDateEnd]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'taskId' => $this->taskId,
            'userId' => $this->userId,
            'statusId' => $this->statusId,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->joinWith(['status' => function ($q) {
            $q->where('statuses.position LIKE "%' . $this->statusPosition . '%"');
        }]);

        return $dataProvider;
    }
}
