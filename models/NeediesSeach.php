<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Needies;

/**
 * NeediesSeach represents the model behind the search form about `app\models\Needies`.
 */
class NeediesSeach extends Needies
{
    public $cityTitle;
    public $statusPosition;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userId',  'cityId', 'mainCountryId', 'avatarId','need','got' ], 'integer'],
            [['name', 'lastname', 'age', 'address', 'phone', 'email', 'vk', 'fb', 'trouble', 'cardNum', 'registerDate','updateDate','statusId','cityTitle','statusPosition','city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id = '' )
    {
        $query = Needies::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'lastname',
                'age',
                'phone',
                'statusId',
                'registerDate',
                'updateDate',
                'statusPosition'=>[
                    'asc' => ['statuses.position' => SORT_ASC],
                    'desc' => ['statuses.position' => SORT_DESC],
                ],
//                'cityTitle' => [
//                    'asc' => ['cities.title' => SORT_ASC],
//                    'desc' => ['cities.title' => SORT_DESC],
//                ],
                'cityId',
                'city',
                'mainCountryId',
                'email',
                'need',
                'got'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            $query->joinWith(['statuses']);
            return $dataProvider;
        }


        if(!empty($this->updateDate)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->updateDate);
            $date->setTime(0, 0, 0);
            $unixDateStart = $date->getTimeStamp();
            $date->add(new \DateInterval('P1D'));
            $date->sub(new \DateInterval('PT1S'));
            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'updateDate', $unixDateStart, $unixDateEnd]);
        }


        if(!empty($this->registerDate)) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->registerDate);
            $date->setTime(0, 0, 0);
            $unixDateStart = $date->getTimeStamp();
            $date->add(new \DateInterval('P1D'));
            $date->sub(new \DateInterval('PT1S'));
            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'registerDate', $unixDateStart, $unixDateEnd]);
        }

        if(!empty($id)){
            $this->userId = $id;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'userId' => $this->userId,
            'cityId' => $this->cityId,
            'city' => $this->city,
            'mainCountryId'=>$this->mainCountryId,
            'avatarId' => $this->avatarId,
            'statusId' => $this->statusId,
            'need'=> $this->need,
            'got' => $this->got

        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'vk', $this->vk])
            ->andFilterWhere(['like', 'fb', $this->fb])
            ->andFilterWhere(['like', 'trouble', $this->trouble])
            ->andFilterWhere(['like','cityId',$this->cityId])
            ->andFilterWhere(['like','city',$this->city])
            ->andFilterWhere(['like','need',$this->need])
            ->andFilterWhere(['like','got',$this->got])
            ->andFilterWhere(['like','mainCountryId',$this->mainCountryId])
            ->andFilterWhere(['like', 'cardNum', $this->cardNum]);

//        $query->joinWith(['city' => function ($q) {
//            $q->where('cities.title LIKE "%' . $this->cityTitle . '%"');
//        }]);
        $query->joinWith(['status' => function ($q) {
            $q->where('statuses.position LIKE "%' . $this->statusPosition . '%"');
        }]);


        return $dataProvider;
    }
}
