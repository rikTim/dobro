<?php

namespace app\models;

use Yii;

class Countries extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mainId' => 'mainId',
            'title' => 'Title',
            'order' => 'Order',
        ];
    }
}