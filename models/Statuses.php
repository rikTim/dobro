<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statuses".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Angels[] $angels
 * @property Comments[] $comments
 * @property Needies[] $needies
 * @property Volunteers[] $volunteers
 */
class Statuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 120],
            [['position'],'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAngels()
    {
        return $this->hasMany(Angels::className(), ['statusId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['statusId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeedies()
    {
        return $this->hasMany(Needies::className(), ['statusId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteers()
    {
        return $this->hasMany(Volunteers::className(), ['statusId' => 'id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getAllStatuses()
    {
        return $this->find()->all();
    }

}
