<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "volunteerTask".
 *
 * @property integer $id
 * @property integer $volunteerId
 * @property integer $taskId
 */
class UserTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userTask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'taskId'], 'required'],
            [['userId', 'taskId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'Volunteer ID',
            'taskId' => 'Task ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getVolunteer()
//    {
//        return $this->hasOne(Volunteers::className(), ['id' => 'volunteerId']);
//    }

    public function getNew()
    {
        return $this->find()->all();
    }

}