<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "needies".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property string $lastname
 * @property string $age
 * @property integer $cityId
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $vk
 * @property string $fb
 * @property string $trouble
 * @property string $cardNum
 * @property string $registerDate
 * @property integer $avatarId
 * @property integer $statusId
 *
 * @property Users $user
 * @property Cities $city
 * @property Statuses $status
 */
class Needies extends \yii\db\ActiveRecord
{

    public $bank;
    public $phoneOwner;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'needies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'name', 'lastname', 'age', 'city', 'phone', 'email', 'shortTrouble', 'trouble', 'statusId', 'specificNeed'], 'required'],
            [['userId', 'cityId', 'mainCountryId', 'avatarId', 'statusId', 'need', 'got'], 'integer'],
            [['shortTrouble'], 'string', 'max' => 80],
            [['email'], 'email'],
            [['trouble'], 'string'],
            [['specificNeed'], 'string', 'max' => 1000],
            [['address', 'city', 'phone', 'email', 'vk', 'fb', 'cardNum', 'bank', 'phoneOwner', 'time'], 'string', 'max' => 120],
            [['name','lastname'],'string','max'=>20],
            [['age'], 'integer', 'max' => 99],
            [['registerDate', 'updateDate', 'needTypeId'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'ID Пользователя',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'age' => 'Возраст',
            'city' => 'Город',
            'cityId' => 'Город',
            'mainCountryId' => 'Страна',
            'address' => 'Адресс',
            'phone' => 'Телефон',
            'phoneOwner' => 'Владелец номера',
            'email' => 'Email',
            'vk' => 'Ссылка на страницу в VK',
            'fb' => 'Ссылка на страницу в FB',
            'trouble' => 'Проблема',
            'shortTrouble' => 'Краткое описание проблемы',
            'cardNum' => 'Реквизиты',
            'needTypeId' => 'Короткое описание проблемы',
            'bank' => 'Банк',
            'registerDate' => 'Дата Регистрации',
            'updateDate' => 'Дата Проверки',
            'avatarId' => 'Аватарка',
            'statusId' => 'Статус',
            'time' => 'Укажите удобное время для звонка',
            'specificNeed' => 'Необходимо',
            'need' => 'Необходимо',
            'got' => 'Уже собрали',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId']);
    }

    public function getCityTitle()
    {
        return $this->cities->title;
    }

    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['mainId' => 'mainCountryId']);
    }

    public function getCountryTitle()
    {
        return $this->country->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatarId']);
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('registerDate', time());
        }


        if (!empty($this->bank)) {
            $this->setAttribute('cardNum', $this->cardNum . ' - ' . $this->bank);
        }
        if (!empty($this->phoneOwner)) {
            $this->setAttribute('phone', $this->phone . ' - ' . $this->phoneOwner);
        }

        $this->setAttribute('vk', Yii::$app->DLL->getLinkId($this->vk));
        $this->setAttribute('fb', Yii::$app->DLL->getLinkId($this->fb));

        return parent::beforeSave($insert);
    }

    /** @inheritdoc */

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $role = key(Yii::$app->authManager->getRolesByUser($this->userId));
            if (empty($role)) {
                $auth = Yii::$app->authManager;
                $authorRole = $auth->getRole('needy');
                $auth->assign($authorRole, $this->userId);
            }
        }
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNewNeedies()
    {
        return $this->find()
            ->where(['statusId' => 1])
            ->asArray()
            ->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getReversedNeedies()
    {
        return $this->find()
            ->where(['statusId' => 4])
            ->asArray()
            ->all();
    }

    /**
     *
     */
    public function UpdateRecords()
    {
        $this->updateAll(['statusId' => '5'], 'updateDate < ' . time());
    }
}
