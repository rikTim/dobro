<?php

namespace app\models;

use Imagine\Image\ManipulatorInterface;
use yii\base\Model;
use yii\helpers\BaseFileHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $volunteerFile;
    public $needyFile;
    public $angelFile;
    public $file;
    public $boardFile;

    public function rules()
    {
        return [
            [['imageFile', 'volunteerFile', 'needyFile', 'angelFile'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => 'avatarUpload'],
            [['imageFile'], 'required', 'on' => 'imageUpload'],
            [['imageFile'], 'image',  'extensions' => 'png, jpg, jpeg', 'on' => 'imageUpload'],
            [['boardFile'], 'required', 'on' => 'boardUpload'],
            [['boardFile'], 'image',  'extensions' => 'png, jpg, jpeg', 'on' => 'boardUpload'],
            [['volunteerFile'], 'required', 'on' => 'volunteerUpload'],
            [['volunteerFile'], 'image',  'extensions' => 'png, jpg, jpeg', 'on' => 'volunteerUpload'],
            [['needyFile'], 'required', 'on' => 'needyUpload'],
            [['needyFile'], 'image',  'extensions' => 'png, jpg, jpeg', 'on' => 'needyUpload'],
            [['angelFile'], 'required', 'on' => 'angelUpload'],
            [['angelFile'], 'image',  'extensions' => 'png, jpg, jpeg', 'on' => 'angelUpload'],
            [['file'], 'image', 'skipOnEmpty' => false, 'minHeight' => 100, 'minWidth' => 500, 'maxFiles' => 30, 'message' => '', 'on' => 'files'],
            [['file'], 'image', 'skipOnEmpty' => false, 'maxFiles' => 30, 'message' => '', 'on' => 'partner'],
            [['imageFile'], 'required', 'on' => 'partnerAvatar'],
            [['imageFile'], 'image', 'minHeight' => 200, 'minWidth' => 200, 'extensions' => 'png, jpg, jpeg', 'on' => 'partnerAvatar'],

            [['imageFile'], 'required', 'on' => 'shopAvatar'],
            [['imageFile'], 'image', 'minHeight' => 600, 'minWidth' => 600, 'extensions' => 'png, jpg, jpeg', 'on' => 'shopAvatar'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFile' => 'Аватар',
            'volunteerFile' => 'Аватар',
            'needyFile' => 'Аватар',
            'angelFile' => 'Аватар',
            'boardFile' => 'Фото'
        ];
    }

    public function upload($id)
    {


        if ($this->validate()) {
            $dir = 'images/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            Image::thumbnail($dir . '/' . $imgName, 300, 300, ManipulatorInterface::THUMBNAIL_INSET)
                ->save($dir . '/m_' . $imgName);

            // save image
            $filesModel = new Files();
            $filesModel->setAttributes([
                'path' => $imgName,
                'userId' => $id,
                'fileType' => 1
            ]);
            $filesModel->save();

            return $filesModel->id;
        } else {
            return false;
        }
    }


    public function filesUpload($id)
    {
        $this->scenario = 'files';


        if ($this->file && $this->validate()) {


            foreach ($this->file as $file) {
                $dir = 'images/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                // crop image
                \Yii::$app->DLL->imgResize($dir . '/' . $imgName, 160, $dir . '/m_' . $imgName);

                $filesModel = new Files();
                $filesModel->setAttributes([
                    'path' => $imgName,
                    'userId' => $id,
                    'fileType' => 3
                ]);
                $filesModel->save();
            }
            return true;
        } else {
            return $this->getErrors();
        }
    }

    /**
     * Прикрепление файлов на стене
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function wallFileUpload($id)
    {

        if ($this->file && $this->validate()) {

            foreach ($this->file as $file) {
                $dir = 'images/attachments/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                $attachmentModel = new Attachment();
                $attachmentModel->setAttributes([
                    'commentId' => $id,
                    'path' => $imgName,
                    'type' => 1 // 1 - тип, что это прикрепление на стене
                ]);

                $attachmentModel->save();
            }
            return true;
        } else {
            echo 'error';
            die();
        }


    }

    public function helpFileUpload($id)
    {
        if ($this->file && $this->validate()) {
            foreach ($this->file as $file) {
                $dir = 'images/helps/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                $attachmentModel = new Attachment();
                $attachmentModel->setAttributes([
                    'commentId' => $id,
                    'path' => $imgName,
                    'type' => 2 // 2 - тип, что это прикрепление к помощи
                ]);

                $attachmentModel->save();
            }
            return true;
        } else {
            echo 'error';
            die();
        }
    }

    public function tasksFile()
    {
        $this->scenario = 'avatarUpload';

        if ($this->imageFile && $this->validate()) {

            $dir = 'images/tasks';
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            // crop image
            \Yii::$app->DLL->imgResizeTask($dir . '/' . $imgName, 360, $dir . '/m_' . $imgName);


            \Yii::$app->DLL->imgResizeTask($dir . '/' . $imgName, 128, $dir . '/m2_' . $imgName);

            return $imgName;
        } else {
            return false;
        }
    }

    public function newsFile()
    {
        $this->scenario = 'avatarUpload';
        if ($this->imageFile && $this->validate()) {
            $dir = 'images/news';
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            // crop image
            \Yii::$app->DLL->imgResize($dir . '/' . $imgName, 160, $dir . '/m_' . $imgName);

            return $imgName;
        } else {
            return false;
        }
    }

    public function boardFile($id)
    {
        $this->scenario = 'boardUpload';

        if ($this->validate()) {
            $dir = 'images/board';
            $imgName = md5(time() . $this->boardFile->baseName) . '.' . $this->boardFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->boardFile->saveAs($dir . '/' . $imgName);


            Image::thumbnail($dir . '/' . $imgName, 300, 300, ManipulatorInterface::THUMBNAIL_INSET)
                ->save($dir . '/m_' . $imgName);


            $boardModel = new Board();
            $boardModel->updateAll(['image' => $imgName], 'id  =' . $id);

            return true;
        } else {
            return false;
        }
    }

    public function partnerAvatar($id)
    {
        $this->scenario = 'partnerAvatar';
        if ($this->imageFile && $this->validate()) {
            $dir = 'images/partner/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);

            // crop image
            $this->autoResize($dir, $imgName, 1);

            $partnerPhotoModel = new PartnerPhoto();
            $partnerPhotoModel->setAttributes([
                'path' => $imgName,
                'partnerId' => $id,
            ]);
            $partnerPhotoModel->save();

            $partnerModel = new Partner();
            $partnerModel->updateAll(['avatarId' => $partnerPhotoModel->id], 'id  =' . $id);
            return $partnerPhotoModel->id;
        } else {
            return false;
        }
    }

    public function partnerPhotos($id)
    {
        $this->scenario = 'partner';
        if ($this->validate()) {
            foreach ($this->file as $file) {
                $dir = 'images/partner/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);

                $this->autoResize($dir, $imgName, 1);

                $partnerPhotoModel = new PartnerPhoto();
                $partnerPhotoModel->setAttributes([
                    'path' => $imgName,
                    'partnerId' => $id,
                ]);
                $partnerPhotoModel->save();
            }
            return true;
        } else {
            return false;
        }
    }


    protected function autoResize($dir, $imgName)
    {
        $dirCropped = $dir . '/cropped/';
        BaseFileHelper::createDirectory($dirCropped);
//        list($weight, $height, $type) = getimagesize($dir . '/' . $imgName);

//        $n = $weight / $height;
//        switch ($n) {
//            case $n > $k:
//                $newWeight = $height * $k;
//                $newHeight = $height;
//                $x = ($weight-$newWeight)/2;
//                $y = 0;
//                break;
//            case $n < $k:
//                $newHeight = $weight / $k;
//                $newWeight = $weight;
//                $x = 0;
//                $y = ($height-$newHeight)/2;
//                break;
//            default :
//                $newHeight = $height;
//                $newWeight = $weight;
//                $x = 0;
//                $y = 0;
//                break;
//        }
//
//        Image::crop($dir . '/' . $imgName, $newWeight, $newHeight, [$x, $y])
//            ->save($dir . '/m_' . $imgName);
        Image::thumbnail($dir . '/' . $imgName, 200, 200, ManipulatorInterface::THUMBNAIL_INSET)
            ->save($dirCropped . $imgName);

    }


    public function shopAvatar($id)
    {

        if ($this->imageFile && $this->validate()) {
            $dir = 'images/shop/' . $id;
            $imgName = md5(time() . $this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($dir);
            $this->imageFile->saveAs($dir . '/' . $imgName);
            $dirSave = $dir.'/small/';
            BaseFileHelper::createDirectory($dirSave);
            Image::thumbnail($dir . '/' . $imgName, 200, 200, ManipulatorInterface::THUMBNAIL_INSET)
                ->save($dirSave . $imgName);

            $dirSave = $dir.'/big/';
            BaseFileHelper::createDirectory($dirSave);
            Image::thumbnail($dir . '/' . $imgName, 600, 600, ManipulatorInterface::THUMBNAIL_INSET)
                ->save($dirSave . $imgName);


            $photoModel = new ShopPhoto();
            $photoModel->setAttributes([
                'path' => $imgName,
                'shopId' => $id,
            ]);
            $photoModel->save();

            $model = new Shop();
            $model->updateAll(['avatarId' => $photoModel->id], 'id  =' . $id);
            return $photoModel->id;
        } else {
            return false;
        }
    }

    public function shopPhotos($id)
    {
        $this->scenario = 'partner';
        if ($this->validate()) {
            foreach ($this->file as $file) {
                $dir = 'images/shop/' . $id;
                $imgName = md5(time() . $file->baseName) . '.' . $file->extension;

                BaseFileHelper::createDirectory($dir);
                $file->saveAs($dir . '/' . $imgName);
                $dirSave = $dir.'/small/';
                BaseFileHelper::createDirectory($dirSave);
                Image::thumbnail($dir . '/' . $imgName, 200, 200, ManipulatorInterface::THUMBNAIL_INSET)
                    ->save($dirSave . $imgName);

                $dirSave = 'images/shop/large/' . $id;
                BaseFileHelper::createDirectory($dirSave);
                Image::thumbnail($dir . '/' . $imgName, 600, 600, ManipulatorInterface::THUMBNAIL_INSET)
                    ->save($dirSave . $imgName);



                $photoModel = new ShopPhoto();
                $photoModel->setAttributes([
                    'path' => $imgName,
                    'shopId' => $id,
                ]);
                $photoModel->save();
            }
            return true;
        } else {
            return false;
        }
    }


}
