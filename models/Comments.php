<?php

namespace app\models;

use app\components\DLL;
use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $toUserId
 * @property integer $fromUserId
 * @property string $text
 * @property string $dateTime
 * @property integer $statusId
 *
 * @property Statuses $status
 */
class Comments extends \yii\db\ActiveRecord
{
    public $userName;
    public $realId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['toUserId', 'fromUserId', 'text', 'dateTime', 'statusId'], 'required'],
            [['toUserId', 'fromUserId', 'statusId'], 'integer'],
            [['text'], 'string','max' => 1000],
            [['dateTime'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'toUserId' => 'Кому',
            'fromUserId' => 'От кого',
            'text' => 'Текст',
            'dateTime' => 'Время',
            'statusId' => 'Статус',
        ];
    }


    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('fromUserId', Yii::$app->user->id);
            $this->setAttribute('statusId', 1);
            $this->setAttribute('dateTime', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuth()
    {
        return $this->hasOne(AuthAssignment::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'toUserId']);
    }


    //-----------------------------------
    public function getFromNeedyUser()
    {
        return $this->hasOne(Needies::className(), ['userId' => 'fromUserId']);
    }

    public function getFromVolunteerUser()
    {
        return $this->hasOne(Volunteers::className(), ['userId' => 'fromUserId']);
    }

    public function getFromAngelUser()
    {
        return $this->hasOne(Angels::className(), ['userId' => 'fromUserId']);
    }
    //-----------------------------------

    public function getAttaches()
    {
        return $this->hasMany(Attachment::className(), ['commentId' => 'id']);
    }

    /**
     * @param $userId
     * @return string
     */
    public function getFullName($userId)
    {
        if($userId == 1)
        {
            return '<span class="label bg-yellow "> ' . "Администратор" . '</span>';

        }

        else {
            $model = DLL::getModelByUserId($userId);
            if (!empty($model)) {
                $user = $model::find()->where(['userId' => $userId])->one();
            }
            if (!empty($user)) {
                return $user->name . ' ' . $user->lastname;
            } else {

                return '<span class="label bg-red "> ' . "Нету профайла" . '</span>';
            }
        }
    }

    /**
     * @param $type
     * @return array
     */
    public function getNewComment($type)
    {
        $new = [];
        $comments = $this->find()
            ->where(['statusId' => 1])
            ->asArray()
            ->all();
        foreach ($comments as $comment) {
            $toUserId = $comment['toUserId'];
            $role = Yii::$app->DLL->getRoleByUserId($toUserId);
            if ($role == $type) {
                $new[] = $comment;
            }
        }
        return $new;
    }
}
