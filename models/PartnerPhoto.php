<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partnerPhoto".
 *
 * @property integer $id
 * @property integer $partnerId
 * @property string $path
 */
class PartnerPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partnerPhoto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partnerId', 'path'], 'required'],
            [['partnerId'], 'integer'],
            [['path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partnerId' => 'Partner ID',
            'path' => 'Path',
        ];
    }
}
