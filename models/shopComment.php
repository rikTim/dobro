<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "shopComment".
 *
 * @property integer $id
 * @property integer $shopId
 * @property integer $userId
 * @property string $text
 */
class shopComment extends \yii\db\ActiveRecord
{

    public $userName;
    public $realId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopComment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shopId', 'userId', 'text','statusId','dateTime'], 'required'],
            [['shopId', 'userId','statusId'], 'integer'],
            [['text','dateTime'], 'string'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('userId', Yii::$app->user->id);
            $this->setAttribute('statusId', 1);
            $this->setAttribute('dateTime', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shopId' => 'Лот',
            'userId' => 'Пользователь',
            'text' => 'Текст',
            'statusId' => 'Статус',
            'dateTime' => 'Время'
        ];
    }

    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'taskId']);
    }


    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusId']);
    }

    public function getFullName($id)
    {
        $role = Yii::$app->DLL-> getRoleByUserId($id);
        switch ($role){
            case 'angel': $user =  Angels::find()->where(['userId'=>$id])->one(); $url = 'angels/view'; break;
            case 'volunteer': $user = Volunteers::find()->where(['userId'=>$id])->one(); $url = 'volunteers/view'; break;
            case 'needy' : $user = Needies::find()->where(['userId'=>$id])->one(); $url = 'needies/view'; break;
        }
        return '<a href=' . Url::toRoute([$url,"id"=>$user->id]) . '>' . $user->name . ' ' . $user->lastname . '</a>';
    }

    //-----------------------------------
    public function getFromNeedyUser()
    {
        return $this->hasOne(Needies::className(), ['userId' => 'userId']);
    }

    public function getFromVolunteerUser()
    {
        return $this->hasOne(Volunteers::className(), ['userId' => 'userId']);
    }

    public function getFromAngelUser()
    {
        return $this->hasOne(Angels::className(), ['userId' => 'userId']);
    }


    public function getNewComment()
    {


        return $this->find()
            ->where(['statusId'=>1])
            ->asArray()
            ->all();
    }

}
