<?php

namespace app\models;

use Yii;

class Contacts extends \yii\db\ActiveRecord
{
    public $captcha;

    public static function tableName()
    {
        return 'contacts';
    }

    public function rules()
    {
        return [
            [['name', 'email', 'text','isRead'], 'required'],
            ['captcha', 'required'],
            ['captcha', 'captcha'],
            [['text'], 'string','max' => 1000],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'text' => 'Текст',
            'dateTime' => 'Время'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->dateTime = time();
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param $data
     * @return bool
     */
    public function message($data)
    {
        $this->attributes = $data;

        if ($this->save(false)) {
            $text = 'На dobroua.com был оставлен вопрос от '.$data['name'];
            $text .= ' email: '.$data['email'];
            $text .= ' текст сообщения: '.$data['text'];


            $email = Yii::$app->params['email'];

            Yii::$app->mailer->compose()
                ->setFrom('admin@dobroua.com')
                ->setTo($email)
                ->setSubject('Вопрос с сайта dobroua.com')
                ->setTextBody($text)
                ->send();

            return true;
        } else {
            return false;
        }
    }

    public function readMessage()
    {
        return $this->updateAll(['isRead' => 1]);
    }
    public function newContacts()
    {
        return $this->find()
            ->where(['isRead'=>0])
            ->asArray()
            ->all();
    }
}
