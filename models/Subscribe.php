<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscribe".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property integer $time
 * @property integer $type
 */
class Subscribe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'type'], 'required'],
            [['ip'], 'string', 'max' => 120],
            [['isRead'], 'integer'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'message' => 'Сообщение',
            'time' => 'Время',
            'type' => 'Type',
            'ip'=>'IP Адрес'
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('time', time());
            $this->setAttribute('ip', $this->getUserHostAddress());
            $this->setAttribute('isRead', 0);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return mixed
     */
    public function getUserHostAddress()
    {
        if (!empty($_SERVER['HTTP_X_REAL_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * @return int
     */
    public function readMessage()
    {
        return $this->updateAll(['isRead' => 1]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNew()
    {
        return $this->find()
            ->where(['isRead' => 0])
            ->asArray()
            ->all();
    }

    public function getEmail()
    {
        return $this->find()
            ->select("email")
            ->groupBy(['email'])
            ->all();
    }
}
