-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:8889
-- Время создания: Янв 12 2016 г., 16:27
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `dobroua`
--

-- --------------------------------------------------------

--
-- Структура таблицы `angels`
--

CREATE TABLE `angels` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `age` int(2) NOT NULL,
  `cityId` int(11) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `vk` varchar(120) NOT NULL,
  `fb` varchar(120) NOT NULL,
  `avatarId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL,
  `isPlus` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `title`) VALUES
(1, 'Киев'),
(2, 'Днепропетровск'),
(3, 'Донецк'),
(4, 'Запорожье'),
(5, 'Кривой Рог'),
(6, 'Львов'),
(7, 'Луганск'),
(8, 'Мариуполь'),
(9, 'Николаев'),
(10, 'Одесса'),
(11, 'Севастополь'),
(12, 'Симферополь'),
(13, 'Харьков'),
(14, 'Винница'),
(15, 'Чернигов');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `text` text NOT NULL,
  `dateTime` varchar(11) NOT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `path` varchar(120) NOT NULL,
  `userId` int(11) NOT NULL,
  `fileType` int(1) NOT NULL,
  `descr` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `helpTypes`
--

CREATE TABLE `helpTypes` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `isNew` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `text` text NOT NULL,
  `dateTime` varchar(11) NOT NULL,
  `isReaded` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `toUserId`, `fromUserId`, `text`, `dateTime`, `isReaded`) VALUES
(1, 1, 1, 'qwqwqweqw', '1452608223', 0),
(2, 1, 1, 'qwqwqweqw', '1452608312', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1452596786),
('m140209_132017_init', 1452596789),
('m140403_174025_create_account_table', 1452596790),
('m140504_113157_update_tables', 1452596791),
('m140504_130429_create_token_table', 1452596792),
('m140830_171933_fix_ip_field', 1452596792),
('m140830_172703_change_account_table_name', 1452596792),
('m141222_110026_update_ip_field', 1452596792);

-- --------------------------------------------------------

--
-- Структура таблицы `needies`
--

CREATE TABLE `needies` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `age` varchar(2) NOT NULL,
  `cityId` int(11) NOT NULL,
  `address` varchar(120) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `vk` varchar(120) NOT NULL,
  `fb` varchar(120) NOT NULL,
  `trouble` text NOT NULL,
  `cardNum` varchar(120) NOT NULL,
  `registerDate` varchar(11) NOT NULL,
  `avatarId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `gravatar_id` varchar(32) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`) VALUES
(1, NULL, NULL, 'test@gmail.com', '1aedb8d9dc4751e229a335e371db8058', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `staticPages`
--

CREATE TABLE `staticPages` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `staticPages`
--

INSERT INTO `staticPages` (`id`, `title`, `text`) VALUES
(1, 'О нас ', 'Очевидно проверяется, что интеграл Фурье отнюдь не очевиден. Правда, некоторые специалисты отмечают, что интеграл от функции, обращающейся в бесконечность вдоль линии усиливает контрпример. Метод последовательных приближений привлекает интеграл по поверхности. Согласно последним исследованиям, первая производная искажает параллельный сходящийся ряд, откуда следует доказываемое равенство!'),
(2, 'Контакты', 'Из первых блюд распространены супы-пюре и бульоны, но подают их редко, тем не менее верховье представляет собой различный термальный источник. Здесь работали Карл Маркс и Владимир Ленин, но альбатрос последовательно оформляет крестьянский кит. Суша морей входит культурный расовый состав. Бурное развитие внутреннего туризма привело Томаса Кука к необходимости организовать поездки за границу, при этом пейзажный парк применяет широколиственный лес. Водохранилище начинает бесплатный органический мир. Тюлень, на первый взгляд, последовательно оформляет храмовый комплекс, посвященный дилмунскому богу Енки,.');

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`id`, `title`) VALUES
(1, 'new'),
(2, 'aproved'),
(3, 'rejected'),
(4, 'edited');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `descr` text NOT NULL,
  `dateTime` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `trancastion` int(11) NOT NULL,
  `dateTime` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`) VALUES
(1, 'test', 'test@gmail.com', '$2y$12$00vhCdCZeKivxS/jVbqAvedFVpvqpw.1sggaA2gFbKw14ivcVctqa', 'kCVa1al_rq8rPfmjJfoZU32nSkK2p-tt', NULL, NULL, NULL, '::1', 1452603104, 1452603104, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `userTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `userTypes`
--

CREATE TABLE `userTypes` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `userTypes`
--

INSERT INTO `userTypes` (`id`, `title`) VALUES
(1, 'admin'),
(2, 'angel'),
(3, 'volunteer'),
(4, 'needy');

-- --------------------------------------------------------

--
-- Структура таблицы `volunteerHelp`
--

CREATE TABLE `volunteerHelp` (
  `id` int(11) NOT NULL,
  `volunteerId` int(11) NOT NULL,
  `helpTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `volunteers`
--

CREATE TABLE `volunteers` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `lastanme` varchar(120) NOT NULL,
  `age` int(2) NOT NULL,
  `cityId` int(11) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `vk` varchar(120) NOT NULL,
  `fb` varchar(120) NOT NULL,
  `avatarId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `volunteerTask`
--

CREATE TABLE `volunteerTask` (
  `id` int(11) NOT NULL,
  `volunteerId` int(11) NOT NULL,
  `taskId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `angels`
--
ALTER TABLE `angels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `angels_fk0` (`userId`),
  ADD KEY `angels_fk1` (`cityId`),
  ADD KEY `angels_fk2` (`statusId`);

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_fk0` (`statusId`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_fk0` (`userId`);

--
-- Индексы таблицы `helpTypes`
--
ALTER TABLE `helpTypes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `needies`
--
ALTER TABLE `needies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `needies_fk0` (`userId`),
  ADD KEY `needies_fk1` (`cityId`),
  ADD KEY `needies_fk2` (`statusId`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_fk0` (`userId`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `staticPages`
--
ALTER TABLE `staticPages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_fk0` (`userTypeId`);

--
-- Индексы таблицы `userTypes`
--
ALTER TABLE `userTypes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `volunteerHelp`
--
ALTER TABLE `volunteerHelp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volunteerHelp_fk0` (`volunteerId`),
  ADD KEY `volunteerHelp_fk1` (`helpTypeId`);

--
-- Индексы таблицы `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volunteers_fk0` (`userId`),
  ADD KEY `volunteers_fk1` (`cityId`),
  ADD KEY `volunteers_fk2` (`statusId`);

--
-- Индексы таблицы `volunteerTask`
--
ALTER TABLE `volunteerTask`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volunteerTask_fk0` (`volunteerId`),
  ADD KEY `volunteerTask_fk1` (`taskId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `angels`
--
ALTER TABLE `angels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `helpTypes`
--
ALTER TABLE `helpTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `needies`
--
ALTER TABLE `needies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `staticPages`
--
ALTER TABLE `staticPages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `userTypes`
--
ALTER TABLE `userTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `volunteerHelp`
--
ALTER TABLE `volunteerHelp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `volunteerTask`
--
ALTER TABLE `volunteerTask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `angels`
--
ALTER TABLE `angels`
  ADD CONSTRAINT `angels_fk0` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `angels_fk1` FOREIGN KEY (`cityId`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `angels_fk2` FOREIGN KEY (`statusId`) REFERENCES `statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_fk0` FOREIGN KEY (`statusId`) REFERENCES `statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_fk0` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `needies`
--
ALTER TABLE `needies`
  ADD CONSTRAINT `needies_fk0` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `needies_fk1` FOREIGN KEY (`cityId`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `needies_fk2` FOREIGN KEY (`statusId`) REFERENCES `statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_fk0` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_fk0` FOREIGN KEY (`userTypeId`) REFERENCES `dorboua`.`userTypes` (`id`);

--
-- Ограничения внешнего ключа таблицы `volunteerHelp`
--
ALTER TABLE `volunteerHelp`
  ADD CONSTRAINT `volunteerHelp_fk0` FOREIGN KEY (`volunteerId`) REFERENCES `volunteers` (`id`),
  ADD CONSTRAINT `volunteerHelp_fk1` FOREIGN KEY (`helpTypeId`) REFERENCES `dorboua`.`helpTypes` (`id`);

--
-- Ограничения внешнего ключа таблицы `volunteers`
--
ALTER TABLE `volunteers`
  ADD CONSTRAINT `volunteers_fk0` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `volunteers_fk1` FOREIGN KEY (`cityId`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `volunteers_fk2` FOREIGN KEY (`statusId`) REFERENCES `statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `volunteerTask`
--
ALTER TABLE `volunteerTask`
  ADD CONSTRAINT `volunteerTask_fk0` FOREIGN KEY (`volunteerId`) REFERENCES `volunteers` (`id`),
  ADD CONSTRAINT `volunteerTask_fk1` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`);
