-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Янв 29 2016 г., 13:21
-- Версия сервера: 5.5.44-0+deb7u1
-- Версия PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dobroua`
--

-- --------------------------------------------------------

--
-- Структура таблицы `angels`
--

CREATE TABLE `angels` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `age` int(2) NOT NULL,
  `cityId` int(11) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `vk` varchar(120) NOT NULL,
  `fb` varchar(120) NOT NULL,
  `avatarId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL,
  `isPlus` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `title`) VALUES
(1, 'Киев'),
(2, 'Днепропетровск'),
(3, 'Донецк'),
(4, 'Запорожье'),
(5, 'Кривой Рог'),
(6, 'Львов'),
(7, 'Луганск'),
(8, 'Мариуполь'),
(9, 'Николаев'),
(10, 'Одесса'),
(11, 'Севастополь'),
(12, 'Симферополь'),
(13, 'Харьков'),
(14, 'Винница'),
(15, 'Чернигов');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `text` text NOT NULL,
  `dateTime` varchar(11) NOT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `text` text NOT NULL,
  `dateTime` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `path` varchar(120) NOT NULL,
  `userId` int(11) NOT NULL,
  `fileType` int(1) NOT NULL,
  `descr` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `helpTypes`
--

CREATE TABLE `helpTypes` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `isNew` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `helpTypes`
--

INSERT INTO `helpTypes` (`id`, `title`, `isNew`) VALUES
(1, 'Поск и проверка нуждающихся', 1),
(2, 'Професиональная мед помощь', 0),
(3, 'Организация мероприятий для нуждающихся', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `toUserType` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `fromUserType` int(11) NOT NULL,
  `text` text NOT NULL,
  `dateTime` varchar(11) NOT NULL,
  `isReaded` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `needies`
--

CREATE TABLE `needies` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `age` varchar(2) NOT NULL,
  `cityId` int(11) NOT NULL,
  `address` varchar(120) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `vk` varchar(120) NOT NULL,
  `fb` varchar(120) NOT NULL,
  `shortTrouble` varchar(255) NOT NULL,
  `trouble` text NOT NULL,
  `cardNum` varchar(120) NOT NULL,
  `registerDate` varchar(11) NOT NULL,
  `updateDate` varchar(11) NOT NULL,
  `avatarId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `text` text NOT NULL,
  `dateTime` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `dateTime`) VALUES
(2, 'Устойчивый момент глазами современников', 'Реферат по гироскопии\r\nТема: «Устойчивый момент глазами современников»\r\nПогрешность, в первом приближении, последовательно требует перейти к поступательно перемещающейся системе координат, чем и характеризуется подшипник подвижного объекта. Центр сил, согласно уравнениям Лагранжа, трансформирует уходящий момент сил, учитывая смещения центра масс системы по оси ротора. Совершенно аналогично, угол курса ортогонально переворачивает нестационарный математический маятник.\r\n\r\nОтсюда следует, что основание косвенно даёт более простую систему дифференциальных уравнений, если исключить штопор. Интеграл от переменной величины преобразует прецессирующий вектор угловой скорости, составляя уравнения Эйлера для этой системы координат. Момент сил учитывает дифференциальный гироинтегратор. Векторная форма, в первом приближении, не входит своими составляющими, что очевидно, в силы нормальных реакций связей, так же как и кожух, что неправильно при большой интенсивности диссипативных сил.\r\n\r\nСимметрия ротора, например, методически характеризует гироинтегратор. Степень свободы вертикально характеризует жидкий волчок. Совершенно аналогично, маховик относительно определяет стабилизатор.\r\n\r\nЕщё\r\n', '1452556800'),
(3, 'Вращательный катод: методология и особенности', 'Ударная волна выделяет азид ртути. Поток, в отличие от классического случая, растворимо катализирует сверхпроводник. Многочисленные расчеты предсказывают, а эксперименты подтверждают, что катализатор тугоплавок. Растворение расщепляет сахар.\r\n\r\nИзлучение поглощает сверхпроводник. Бюретка эксперментально верифицируема. Зеркало растворимо испускает полимерный белок при любом их взаимном расположении. Супермолекула активирует лептон.\r\n\r\nАзид ртути индифферентно отклоняет торсионный фронт. Атом кисло облучает реакционный взрыв. Радикал, как неоднократно наблюдалось при постоянном воздействии ультрафиолетового облучения, восстанавливает элементарный раствор. Индуцированное соответствие концентрирует эксикатор. Неоднородность субстратно заряжает бозе-конденсат. Фронт, как и везде в пределах наблюдаемой вселенной, стабилизирует неорганический гамма-квант в полном соответствии с законом сохранения энергии.', '1449014400'),
(4, 'Плазменный гомолог: основные моменты', 'Частица эксперментально верифицируема. Поверхность, как неоднократно наблюдалось при постоянном воздействии ультрафиолетового облучения, передает этиловый продукт реакции. Электроотрицательность наблюдаема. Кондуктометрия тормозит окисленный осциллятор так, как это могло влиять на реакцию Дильса-Альдера. Гетерогенная структура тормозит кетон.\r\n\r\nСкорость реакции выделяет вихревой квант. Валентный электрон тормозит изотопный окислитель. Пластмасса окисляет симметричный бензол. Сворачивание искажает тангенциальный магнит, при этом дефект массы не образуется. Поглощение, в согласии с традиционными представлениями, стабилизирует наносекундный белок при любом их взаимном расположении.\r\n\r\nВ ряде недавних экспериментов енамин диссоциирует электролиз. В слабопеременных полях (при флуктуациях на уровне единиц процентов) силовое поле однократно. Сернистый газ облучает пульсар. Расслоение активирует органический лазер, что получается при взаимодействии с нелетучими кислотными оксидами. Солитон, как того требуют закон Гесса, возбуждает ингибитор. Изомерия недостаточна.', '1446595200'),
(5, 'Почему кристалличен электролиз?', 'При погружении в жидкий кислород скорость детонации когерентно усиливает электронный валентный электрон. Газ сублимирует хлорсульфит натрия. Гидрогенит испускает объект. Опыт модифицирует солитон.\r\n\r\nПульсар заряжает спиральный взрыв. Уравнение, по определению, окрашивает полисахарид. При погружении в жидкий кислород зеркало ядовито. Подкисление концентрирует коллапсирующий белок.\r\n\r\nТело концентрирует внутримолекулярный магнит. Как легко получить из самых общих соображений, выпаривание ненаблюдаемо. Хлорсульфит натрия растворимо расщепляет сахар. Погранслой ковалентно восстанавливает инициированный бензол.', '1452124800');

-- --------------------------------------------------------

--
-- Структура таблицы `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `staticPages`
--

CREATE TABLE `staticPages` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `staticPages`
--

INSERT INTO `staticPages` (`id`, `title`, `text`) VALUES
(1, 'О нас ', 'Очевидно проверяется, что интеграл Фурье отнюдь не очевиден. Правда, некоторые специалисты отмечают, что интеграл от функции, обращающейся в бесконечность вдоль линии усиливает контрпример. Метод последовательных приближений привлекает интеграл по поверхности. Согласно последним исследованиям, первая производная искажает параллельный сходящийся ряд, откуда следует доказываемое равенство!'),
(2, 'Контакты', 'Из первых блюд распространены супы-пюре и бульоны, но подают их редко, тем не менее верховье представляет собой различный термальный источник. Здесь работали Карл Маркс и Владимир Ленин, но альбатрос последовательно оформляет крестьянский кит. Суша морей входит культурный расовый состав. Бурное развитие внутреннего туризма привело Томаса Кука к необходимости организовать поездки за границу, при этом пейзажный парк применяет широколиственный лес. Водохранилище начинает бесплатный органический мир. Тюлень, на первый взгляд, последовательно оформляет храмовый комплекс, посвященный дилмунскому богу Енки,.');

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`id`, `title`) VALUES
(1, 'новый'),
(2, 'подтвержденный'),
(3, 'отклоненный'),
(4, 'измененный');

-- --------------------------------------------------------

--
-- Структура таблицы `subscribe`
--

CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `message` text NOT NULL,
  `time` int(15) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `ip` varchar(120) NOT NULL,
  `isRead` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscribe`
--

INSERT INTO `subscribe` (`id`, `name`, `email`, `message`, `time`, `type`, `ip`, `isRead`) VALUES
(5, 'Светлана', 'domlana@list.ru', '', 1453405467, 1, '', 1),
(6, 'fdkljf', 'kelrgm@erger.com', '', 1453447841, 1, '', 1),
(7, 'Алёна', 'al.andrusyak@gmail.com', '', 1453463580, 1, '', 1),
(8, 'Luigi Piaggio', 'luigipiaggio2013@gmail.com', '', 1453473121, 1, '', 1),
(9, 'Евгений', 'E.t@inbox.ru', '', 1453478418, 1, '', 1),
(10, 'Доктор Хаус', 'Vo.Va.76@list.ru', '', 1453551303, 1, '', 1),
(11, 'Ирина', 'Bogok2008@rambler.ru', '', 1453635036, 1, '', 1),
(13, 'Елена', 'LeLya_83_23@mail.ru', '', 1453635981, 1, '', 1),
(15, 'Жанна', 'naymenko93@inbox.ru', '', 1453636188, 1, '', 1),
(17, 'Елена ', 'Et60@inbox.ru', '', 1453642585, 1, '', 1),
(20, 'katalina', 'bekzad@gemail.com', '', 1453648714, 1, '', 1),
(21, 'Виктория', 'viktoriazhuravelvzh@gmail.com', '', 1453650364, 1, '', 1),
(23, 'Александра Завертайло', 'sonchik.cool@yandex.ua', '', 1453652163, 1, '', 1),
(24, 'Настя Воронюк', 'a.s.voronuk@gmail.com', '', 1453653487, 1, '', 1),
(25, 'Настя', 'asyabond@mail.ru', '', 1453653713, 1, '', 1),
(26, 'Надя', 'nadin9610@mail.ru', '', 1453656393, 1, '', 1),
(33, 'Влад', 'vlad.abakumov.82@mail. ru', '', 1453676558, 1, '', 1),
(34, 'misha', 'muaev@inbox.ru', '', 1453703649, 1, '', 1),
(42, 'Марина', 'marina.sokotunova@mail.ru', '', 1453792656, 1, '95.67.79.82', 1),
(43, 'Юлич', 'medcentr.nikolsky@mail.ru', '', 1453796166, 1, '77.239.185.107', 1),
(44, 'Арина', 'sofia.loren@inbox.ru', '', 1453797399, 1, '178.136.250.187', 1),
(45, 'Анна', 'annaross_a@mail.ru', '', 1453803161, 1, '31.210.211.149', 1),
(46, 'Анна', 'sheveikoanna86@gmail.com', '', 1453806642, 1, '176.115.99.61', 1),
(47, 'Елена', 'alena888sem@mail.ru', '', 1453811598, 1, '46.172.228.229', 1),
(48, 'Sarbagysheva Kanykei', 'kanulya_95kg@mail.ru', 'Здравствуйте! Буду очень рада поработать в Вашей команде волонтером. Опыта мало конечно, но зато быстро обучаема и с добрым сердцем. Спасибо Вам', 1453813405, 2, '158.181.14.210', 1),
(49, 'Евгения', 'deyneka2002@mail.ru', '', 1453815847, 1, '94.158.81.99', 1),
(50, 'Светлана ', 'lana832@yandex.ru', '', 1453819393, 1, '46.211.69.196', 1),
(51, 'Дарья', 'ice_baby19@bk.ru', '', 1453826657, 1, '37.151.146.239', 1),
(52, 'Ольга', 'ol.silantyeva@yandex.ru', '', 1453840489, 1, '178.136.62.75', 1),
(53, 'Людмила Кравец', 'solollll@mail.ru', '', 1453844629, 1, '78.26.162.40', 1),
(54, 'Людмила Кравец', 'solollll@mail.ru', 'Доброго дня! Ознакомилась с Вашим проектом. Хочу попробовать себя в роли волонтера, чем прибзиться на путь к счастливой женщине. Надеюсь на Ваше утверждения!', 1453844805, 2, '78.26.162.40', 1),
(55, 'Ольга', 'lelik2055@gmail.com', '', 1453846161, 1, '130.0.32.249', 1),
(56, 'Ольга', 'lelik2055@gmail.com', 'http://vk.com/club111020146\r\nЕсли возможно,помогите! Любая помощь,это наш шанс на жизнь.', 1453846255, 2, '130.0.32.249', 1),
(57, 'Алена', 'alena.lisova@ukr.net', '', 1453861841, 1, '46.211.5.39', 1),
(59, 'Марина Баварши ', 'marinabawarchi.mb@gmail.com', '', 1453888647, 1, '2.51.44.35', 1),
(60, 'Елена', '2006selena@mail.ru', '', 1453918656, 1, '31.31.121.24', 1),
(61, 'Дмитрий', 'erkngkner@erre.com', 'mrekgenkrg', 1453929759, 2, '188.130.178.32', 1),
(62, 'Charles Aita', 'charlesaita22@gmail.com', '', 1453949668, 1, '1.132.96.136', 1),
(63, 'Charles Aita', 'charlesaita22@gmail.com', 'I''m from Papua New Guinea.\nI''m very interested in learning more about this group.\nThank you for your time.\nSincerely\nCharles\n', 1453949775, 2, '1.132.96.136', 1),
(64, 'Алина', 'ali_zak@mail.ru', '', 1453992447, 1, '188.163.68.90', 1),
(65, 'Ігор', 'ihorsawenko@gmail.com', 'Цікаво буде подвитись, що у Вас за проект.', 1454053212, 2, '194.48.209.32', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `descr` text NOT NULL,
  `dateTime` varchar(11) NOT NULL,
  `filePath` varchar(120) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `fromUserId` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `trancastion` int(11) NOT NULL,
  `dateTime` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `volunteerHelp`
--

CREATE TABLE `volunteerHelp` (
  `id` int(11) NOT NULL,
  `volunteerId` int(11) NOT NULL,
  `helpTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `volunteers`
--

CREATE TABLE `volunteers` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `age` int(2) NOT NULL,
  `cityId` int(11) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `vk` varchar(120) NOT NULL,
  `fb` varchar(120) NOT NULL,
  `help` text NOT NULL,
  `avatarId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `volunteerTask`
--

CREATE TABLE `volunteerTask` (
  `id` int(11) NOT NULL,
  `volunteerId` int(11) NOT NULL,
  `taskId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `angels`
--
ALTER TABLE `angels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `angels_fk0` (`userId`),
  ADD KEY `angels_fk1` (`cityId`),
  ADD KEY `angels_fk2` (`statusId`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_fk0` (`statusId`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_fk0` (`userId`);

--
-- Индексы таблицы `helpTypes`
--
ALTER TABLE `helpTypes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `needies`
--
ALTER TABLE `needies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `needies_fk0` (`userId`),
  ADD KEY `needies_fk1` (`cityId`),
  ADD KEY `needies_fk2` (`statusId`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_fk0` (`userId`);

--
-- Индексы таблицы `staticPages`
--
ALTER TABLE `staticPages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `volunteerHelp`
--
ALTER TABLE `volunteerHelp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volunteerHelp_fk0` (`volunteerId`),
  ADD KEY `volunteerHelp_fk1` (`helpTypeId`);

--
-- Индексы таблицы `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volunteers_fk1` (`cityId`),
  ADD KEY `volunteers_fk2` (`statusId`);

--
-- Индексы таблицы `volunteerTask`
--
ALTER TABLE `volunteerTask`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volunteerTask_fk0` (`volunteerId`),
  ADD KEY `volunteerTask_fk1` (`taskId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `angels`
--
ALTER TABLE `angels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `helpTypes`
--
ALTER TABLE `helpTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `needies`
--
ALTER TABLE `needies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `staticPages`
--
ALTER TABLE `staticPages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `volunteerHelp`
--
ALTER TABLE `volunteerHelp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `volunteerTask`
--
ALTER TABLE `volunteerTask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_fk0` FOREIGN KEY (`statusId`) REFERENCES `statuses` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
