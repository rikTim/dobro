<?php

namespace app\components;

use app\models\shopComment;
use Yii;
use yii\base\Widget;


class AuthWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('auth');
    }




}