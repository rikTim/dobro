<?php

namespace app\components;

use app\models\UserHelp;
use Yii;
use yii\base\Widget;


class HelpFromHistoryWidget extends Widget
{
    public $userId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $userHelps = UserHelp::find()->where(['toUserId' => $this->userId, 'isVerify' => 1])->orderBy(['dateTime' => SORT_DESC])->all();

        return $this->render('helpFromHistory', [
            'userId' => $this->userId,
            'models' => $this->getNameByUserId($userHelps),
        ]);
    }

    public function getNameByUserId($userHelps)
    {
        foreach ($userHelps as $userHelp) {
            if ($userHelp->fromUserId == 1) {
                $userHelp->userName = "Администрация DobroUA.com";
                $userHelp->realId = 'admin';
            } else {
                $model = Yii::$app->DLL->getModelByUserId($userHelp->fromUserId);
                $nl = $model::find()->where(['userId' => $userHelp->fromUserId])->one();
                $userHelp->userName = $nl->name . ' ' . $nl->lastname;
                $userHelp->realId = $nl->id;
            }
        }
        return $userHelps;
    }


}