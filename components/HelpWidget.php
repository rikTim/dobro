<?php

namespace app\components;

use app\models\UploadForm;
use app\models\UserHelp;
use Yii;
use yii\base\Widget;


class HelpWidget extends Widget
{
    public $userId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('helpPopup', [
            'model' => new UserHelp(),
            'toUserId' => $this->userId,
            'upload' => new UploadForm(),
        ]);
    }
}