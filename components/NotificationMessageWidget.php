<?php

namespace app\components;

use app\models\Notifications;
use Yii;
use yii\base\Widget;


class NotificationMessageWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('notificationMessages');
    }
}