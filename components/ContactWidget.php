<?php

namespace app\components;

use app\models\Messages;
use app\models\Needies;
use Yii;
use yii\base\Widget;


class ContactWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $userId = Yii::$app->user->id;
        $messages = Messages::find()->where('fromUserId = ' . $userId . ' or toUserId = ' . $userId)->orderBy(['dateTime' => SORT_DESC])->all();

        if (!Yii::$app->user->isGuest) {
            $currentModel = Yii::$app->DLL->getModelByUserId($userId);
            if (!empty($currentModel)) {
                $profile = $currentModel::find()->where(['userId' => $userId])->one();
            }
        }

        return $this->render('contact', [
            'messages' => $this->getNameByUserId($messages, $userId),
            'model' => new Messages(),

        ]);
    }


    public function getNameByUserId($messages, $userId)
    {
        $model = Yii::$app->DLL->getModelByUserId($userId);
        foreach ($messages as $message) {
            $nl = $model::find()->where(['userId' => $userId])->one();
            $message->userName = $nl->name . ' ' . $nl->lastname;

        }
        return $messages;
    }

}