<?php

namespace app\components;

use Yii;
use yii\base\Widget;


class VideoWidget extends Widget
{
    public $video;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('video', [
            'video' => $this->video
        ]);
    }

}