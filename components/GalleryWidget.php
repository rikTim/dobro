<?php

namespace app\components;

use Yii;
use yii\base\Widget;


class GalleryWidget extends Widget
{
    public $photos;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('gallery', [
            'photos' => $this->photos
        ]);
    }

}