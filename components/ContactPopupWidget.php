<?php

namespace app\components;

use app\models\Contacts;
use app\models\Messages;
use Yii;
use yii\base\Widget;

class ContactPopupWidget extends Widget
{

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('contactPopup', [
            'model' => new Contacts(),
            'message' => new Messages(),
        ]);
    }

}
