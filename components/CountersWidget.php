<?php

namespace app\components;

use app\models\Notifications;
use Yii;
use yii\base\Widget;


class CountersWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('counters', [
            'notification' => Notifications::find()->where(['userId' => Yii::$app->user->id])->one()->notification
        ]);
    }
}