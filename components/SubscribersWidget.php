<?php

namespace app\components;

use Yii;
use yii\base\Widget;


class SubscribersWidget extends Widget
{

    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('subscribers', [
            'models' => $this->model,
        ]);
    }
}