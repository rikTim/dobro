<?php

namespace app\components;

use app\models\Angels;
use app\models\Board;
use app\models\Comments;
use app\models\Contacts;
use app\models\HelpTypes;
use app\models\Messages;
use app\models\Needies;
use app\models\NeedyHelp;
use app\models\shopComment;
use app\models\Subscribe;
use app\models\TaskComment;
use app\models\Tasks;
use app\models\UserHelp;
use app\models\UserTask;
use app\models\VolunteerHelp;
use app\models\Volunteers;
use app\models\VolunteerTask;
use Yii;
use yii\base\Widget;


class AsideShopWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('aside_shop',[
            'newComment' => $this->newComment(),
        ]);
    }
    public function newComment()
    {

        $commentModels = new shopComment();
        $count = count($commentModels->getNewComment());
        return $count > 0 ? $count : '';

    }

}