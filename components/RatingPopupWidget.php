<?php

namespace app\components;

use app\models\Contacts;
use app\models\Ratings;
use Yii;
use yii\base\Widget;

class RatingPopupWidget extends Widget
{
    public $userId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('ratingpopup', [
            'models' => Ratings::find()->where(['userId' => $this->userId])->orderBy(['dateTime' => SORT_DESC])->all()
        ]);
    }

}
