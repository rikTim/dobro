<?php
namespace app\components;

use app\models\AuthAssignment;
use app\models\News;
use app\models\Notifications;
use app\models\Users;
use dektrium\user\models\User;
use Yii;
use yii\base\Component;
use app\models\Angels;
use app\models\Needies;
use app\models\Volunteers;
use yii\helpers\BaseVarDumper;
use yii\imagine\Image;
use Mailgun\Mailgun;

class DLL extends Component
{
    public $content;

    public function init()
    {
        parent::init();
    }

    // Yii::$app->DLL->methodName();

    /**
     * Получение модели пользователя по его userId
     * @param $id
     * @return Angels|Needies|Volunteers|null
     */
    public static function getModelByUserId($id)
    {
        $role = key(Yii::$app->authManager->getRolesByUser($id));

        if (!empty($role)) {
            switch ($role) {
                case 'needy':
                    $model = new Needies();
                    break;
                case 'volunteer':
                    $model = new Volunteers();
                    break;
                case 'angel':
                    $model = new Angels();
                    break;
                default:
                    $model = null;
            }
            return $model;
        } else {
            return null;
        }
    }

    /**
     * Получение роли пользователя по его userId
     * @param $id
     * @return mixed|null
     */
    public static function getRoleByUserId($id)
    {
        $role = key(Yii::$app->authManager->getRolesByUser($id));
        if (!empty($role)) {
            return $role;
        } else {
            return null;
        }
    }

    /**
     * Получение профиля пользователя
     * @return mixed
     */
    public function getProfile()
    {
        if (!Yii::$app->user->isGuest) {
            $currentModel = $this->getModelByUserId(Yii::$app->user->id);
            if (!empty($currentModel)) {
                return $currentModel::find()->where(['userId' => Yii::$app->user->id])->one();
            }
        }
    }

    /** как то обьеденить с верхней */
    public function getProfileById($id)
    {
        if (!Yii::$app->user->isGuest) {
            $currentModel = $this->getModelByUserId($id);
            if (!empty($currentModel)) {
                return $currentModel::find()->where(['userId' => $id])->one();
            }
        }
    }

    /**
     * Обрезка блинных текстов
     * @param $str
     * @param int $len
     * @return string
     */
    public static function subStr($str, $len = 300)
    {
        if (strlen($str) < $len) {
            return $str;
        }
        $subStr = substr($str, 0, $len);
        return substr($subStr, 0, strrpos($subStr, ' ')) . ' ...';
    }

    /**
     * Получение месяца
     * @param $date
     * @return array
     */
    public function getMonth($date)
    {
        $date = explode('.', $date);
        $day = $date[0];
        $month = $date[1];

        switch ($month) {
            case 'Jan':
                $month = 'ЯНВ';
                break;
            case 'Feb':
                $month = 'ФЕВ';
                break;
            case 'Mar':
                $month = 'МАР';
                break;
            case 'Apr':
                $month = 'АПР';
                break;
            case 'May':
                $month = 'МАЙ';
                break;
            case 'Jun':
                $month = 'ИЮН';
                break;
            case 'Jul':
                $month = 'ИЮЛ';
                break;
            case 'Aug':
                $month = 'АВГ';
                break;
            case 'Sep':
                $month = 'СЕН';
                break;
            case 'Oct':
                $month = 'ОКТ';
                break;
            case 'Nov':
                $month = 'НОЯ';
                break;
            case 'Dec':
                $month = 'ДЕК';
                break;
        }

        return [$day, $month];
    }

    /**
     * Получение типа события по его id
     * @param $type
     * @return string
     */
    public function getFeedType($type)
    {
        switch ($type) {
            case 1:
                $type = 'новость';
                break;
            case 2:
                $type = 'задание';
                break;
            case 3:
                $type = 'сообщение';
                break;
            case 4:
                $type = 'внимание! Нужна помощь';
                break;
            case 5:
                $type = 'новая запись на стене';
                break;
        }
        return $type;
    }

    /**
     * Рассылка email о новых нуждающихся
     * всем волонтерам и ангелам
     */
    public function newNeedyNotification($id, $name, $lastname)
    {
        $helpersArray = AuthAssignment::find()->where('item_name = "volunteer" OR item_name = "angel"')->all();
        foreach ($helpersArray as $helper) {
            if ($helper->item_name == 'volunteer') {
                $email = $helper->volunteer->email;
                $user = Volunteers::find()->where(['userId' => $helper->user_id])->one();
                $name1 = $user->name;
            } else {
                $email = $helper->angel->email;
                $user = Angels::find()->where(['userId' => $helper->user_id])->one();
                $name1 = $user->name;
            }
            if (!empty($email)) {
                $message = '
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <!-- If you delete this meta tag, Half Life 3 will never be released. -->
                        <meta name="viewport" content="width=device-width"/>

                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

                        <style type="text/css">

                        </style>

                    </head>

                    <body style="-webkit-font-smoothing:antialiased;
                        -webkit-text-size-adjust:none;
                        width: 100%!important;
                        height: 100%;" bgcolor="#FFFFFF">

                    <!-- HEADER -->
                    <table style="width: 100%;"  bgcolor="#00a2ff">
                        <tr>
                            <td></td>
                            <td style="display:block!important;
                        max-width:600px!important;
                        margin:0 auto!important; /* makes it centered */
                        clear:both!important;" class="header container">

                                <div style="padding:15px;
                        max-width:600px;
                        margin:0 auto;
                        display:block; " class="content">
                                    <table bgcolor="#00a2ff">
                                        <tr>
                                            <td><img src="http://dobroua.com/web/images/dobro_logo_cr.png"/></td>
                                            <!--<td align="right"><h6 class="collapse">Basic</h6></td>-->
                                        </tr>
                                    </table>
                                </div>

                            </td>
                            <td></td>
                        </tr>
                    </table><!-- /HEADER -->


                    <!-- BODY -->
                    <table style="width: 100%;" >
                        <tr>
                            <td></td>
                            <td style="display:block!important;
                        max-width:600px!important;
                        margin:0 auto!important; /* makes it centered */
                        clear:both!important;" class="container" bgcolor="#FFFFFF">

                                <div style="padding:15px;
                        max-width:600px;
                        margin:0 auto;
                        display:block; " class="content">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <table>
                                                    <td > <h3 style="font-weight:500; font-size: 23px;">Доброго времени суток, ' . $name1 . ' </h3></td>
                                                    <td style="width:30%;"><img style="width:100%" src="http://dobroua.com/web/images/makedobro.png" alt=""/> </td>
                                                </table>
                                                <img style="width:100%" src="http://dobroua.com/web/images/bg.jpg" alt=""/>
                                                <table>
                                                    <tr><h3 style="font-weight:500; font-size: 23px;">А у нас новости для Вас! </h3></tr>
                                                    <td>
                                                        <p style="padding:15px;background-color:#ECF8FF;margin-bottom: 15px;" > На сайте dobroua.com был добавлен человек, которому нужна помощь <a style="font-weight:bold;color: #2BA6CB;" href="http://' . Yii::$app->params['domain'] . '/needies/more?id=' . $id . '">' . $name . ' ' . $lastname . '  </a>
                                                        </p>
                                                    </td>
                                                </table>

                                                <table style="background-color: #ebebeb;" width="100%">
                                                    <tr>
                                                        <td>

                                                            <!-- column 1 -->
                                                            <table style="width: 300px;float:left;" align="left" class="column">
                                                                <tr style="padding-left: 15px;padding-right: 15px;">
                                                                    <td style="padding-left: 15px;padding-right: 15px;">

                                                                        <h5 style="margin: 15px; font-weight:900; font-size: 17px; text-transform: uppercase; color:#444;" class="">Следи за нами:</h5>
                                                                        <p class="">
                                                                                <a style="margin-right:10px;cursor:pointer;
                                                                                padding: 3px 7px;font-size:12px;margin-bottom:10px;
                                                                                text-decoration:none;color: #FFF;font-weight:bold;display:block;
                                                                                text-align:center;background-color: #3B5998!important;" href="https://www.facebook.com/groups/1079732442078250/" class="soc-btn fb">
                                                                                Facebook</a>
                                                                            <a style="margin-right:10px;cursor:pointer;
                                                                            padding: 3px 7px;font-size:12px;margin-bottom:10px;
                                                                            text-decoration:none;color: #FFF;font-weight:bold;display:block;
                                                                            text-align:center;background-color: #597DA3!important;" href="https://vk.com/dobrouacom" class="soc-btn vk">ВКонтакте</a>
                                                                        </p>


                                                                    </td>
                                                                </tr>
                                                            </table><!-- /column 1 -->

                                                            <!-- column 2 -->
                                                            <table  align="left" class="column">
                                                                <tr style="padding-left: 15px;padding-right: 15px;">
                                                                    <td style="padding-left: 15px;padding-right: 15px;">

                                                                        <h5 style="margin: 15px; font-weight:900; font-size: 17px; text-transform: uppercase; color:#444;" class="">Свяжись с нами:</h5>
                                                                        Email: <strong><a style="color: #2BA6CB;"
                                                                                          href="emailto:admin@dobroua.com">admin@dobroua.com</a></strong></p>

                                                                    </td>
                                                                </tr>
                                                            </table><!-- /column 2 -->

                                                            <span style="display: block; clear: both;" class="clear"></span>

                                                        </td>
                                                    </tr>
                                                </table><!-- /social & contact -->

                                            </td>
                                        </tr>
                                    </table>
                                </div><!-- /content -->

                            </td>
                            <td></td>
                        </tr>
                    </table><!-- /BODY -->
                    </body>
                    </html>';
                Yii::$app->mailer->compose()
                    ->setFrom('admin@dobroua.com')
                    ->setTo($email)
                    ->setSubject('На сайте dobroua.com был добавлен человек, которому нужна помощь')
                    ->setHtmlBody($message)
                    ->send();
            }

            $this->addNotification($helper->user_id);
        }
        return true;
    }

    /**
     * Добавление пометки в уведомлениях
     * @param $userId
     */
    public function addNotification($userId)
    {
        $notification = Notifications::find()->where(['userId' => $userId])->one();
        if (!empty($notification)) {
            Notifications::updateAll(['notification' => $notification->notification + 1], 'userId = ' . $userId);
        } else {
            $notificationModel = new Notifications();
            $notificationModel->setAttributes([
                'userId' => $userId,
                'notification' => 1
            ]);
            $notificationModel->save();
            unset($notificationModel);
        }
    }


    public function newWallCommentEmail($model)
    {
        if ($model->toUserId != $model->fromUserId) {
            $user = User::find()->where(['id' => $model->toUserId])->one();
            $profile = $this->getProfileById($model->fromUserId);

            $toUser = $this->getProfileById($model->toUserId);
            $name = $toUser->name;
            $class = $profile::className();
            $class = explode('\\', $class);

            $message = '
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <!-- If you delete this meta tag, Half Life 3 will never be released. -->
                        <meta name="viewport" content="width=device-width"/>

                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

                        <style type="text/css">

                        </style>

                    </head>

                    <body style="-webkit-font-smoothing:antialiased;
                        -webkit-text-size-adjust:none;
                        width: 100%!important;
                        height: 100%;" bgcolor="#FFFFFF">

                    <!-- HEADER -->
                    <table style="width: 100%;"  bgcolor="#00a2ff">
                        <tr>
                            <td></td>
                            <td style="display:block!important;
                        max-width:600px!important;
                        margin:0 auto!important; /* makes it centered */
                        clear:both!important;" class="header container">

                                <div style="padding:15px;
                        max-width:600px;
                        margin:0 auto;
                        display:block; " class="content">
                                    <table bgcolor="#00a2ff">
                                        <tr>
                                            <td><img src="http://dobroua.com/web/images/dobro_logo_cr.png"/></td>
                                            <!--<td align="right"><h6 class="collapse">Basic</h6></td>-->
                                        </tr>
                                    </table>
                                </div>

                            </td>
                            <td></td>
                        </tr>
                    </table><!-- /HEADER -->


                    <!-- BODY -->
                    <table style="width: 100%;" >
                        <tr>
                            <td></td>
                            <td style="display:block!important;
                        max-width:600px!important;
                        margin:0 auto!important; /* makes it centered */
                        clear:both!important;" class="container" bgcolor="#FFFFFF">

                                <div style="padding:15px;
                        max-width:600px;
                        margin:0 auto;
                        display:block; " class="content">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <table>
                                                    <td > <h3 style="font-weight:500; font-size: 23px;">Доброго времени суток, ' . $name . ' </h3></td>
                                                    <td style="width:30%;"><img style="width:100%" src="http://dobroua.com/web/images/makedobro.png" alt=""/> </td>
                                                </table>
                                                <img style="width:100%" src="http://dobroua.com/web/images/bg.jpg" alt=""/>
                                                <table>
                                                    <tr><h3 style="font-weight:500; font-size: 23px;">А у нас новости для Вас! </h3></tr>
                                                    <td>
                                                        <p style="padding:15px;background-color:#ECF8FF;margin-bottom: 15px;" > Вам оставлен новый комментарий на сайте <a style="font-weight:bold;color: #2BA6CB;" href="http://' . Yii::$app->params['domain'] . '">Dobroua.com от пользователя <a href="http://' . Yii::$app->params['domain'] . '/' . strtolower(end($class)) . '/more?id=' . $profile->id . '">' . $profile->name . ' ' . $profile->lastname . ' </a>
                                                        </p>
                                                    </td>
                                                </table>

                                                <table style="background-color: #ebebeb;" width="100%">
                                                    <tr>
                                                        <td>

                                                            <!-- column 1 -->
                                                            <table style="width: 300px;float:left;" align="left" class="column">
                                                                <tr style="padding-left: 15px;padding-right: 15px;">
                                                                    <td style="padding-left: 15px;padding-right: 15px;">

                                                                        <h5 style="margin: 15px; font-weight:900; font-size: 17px; text-transform: uppercase; color:#444;" class="">Следи за нами:</h5>
                                                                        <p class="">
                                                                                <a style="margin-right:10px;cursor:pointer;
                                                                                padding: 3px 7px;font-size:12px;margin-bottom:10px;
                                                                                text-decoration:none;color: #FFF;font-weight:bold;display:block;
                                                                                text-align:center;background-color: #3B5998!important;" href="https://www.facebook.com/groups/1079732442078250/" class="soc-btn fb">
                                                                                Facebook</a>
                                                                            <a style="margin-right:10px;cursor:pointer;
                                                                            padding: 3px 7px;font-size:12px;margin-bottom:10px;
                                                                            text-decoration:none;color: #FFF;font-weight:bold;display:block;
                                                                            text-align:center;background-color: #597DA3!important;" href="https://vk.com/dobrouacom" class="soc-btn vk">ВКонтакте</a>
                                                                        </p>


                                                                    </td>
                                                                </tr>
                                                            </table><!-- /column 1 -->

                                                            <!-- column 2 -->
                                                            <table  align="left" class="column">
                                                                <tr style="padding-left: 15px;padding-right: 15px;">
                                                                    <td style="padding-left: 15px;padding-right: 15px;">

                                                                        <h5 style="margin: 15px; font-weight:900; font-size: 17px; text-transform: uppercase; color:#444;" class="">Свяжись с нами:</h5>
                                                                        Email: <strong><a style="color: #2BA6CB;"
                                                                                          href="emailto:admin@dobroua.com">admin@dobroua.com</a></strong></p>

                                                                    </td>
                                                                </tr>
                                                            </table><!-- /column 2 -->

                                                            <span style="display: block; clear: both;" class="clear"></span>

                                                        </td>
                                                    </tr>
                                                </table><!-- /social & contact -->

                                            </td>
                                        </tr>
                                    </table>
                                </div><!-- /content -->

                            </td>
                            <td></td>
                        </tr>
                    </table><!-- /BODY -->
                    </body>
                    </html>';
            Yii::$app->mailer->compose()
                ->setFrom('admin@dobroua.com')
                ->setTo($user->email)
                ->setSubject('Вам новый комментарий на сайте dobroua.com')
                ->setHtmlBody($message)
                ->send();
        }
    }


    /**
     * @param $link
     * @return mixed
     */
    public function getLinkId($link)
    {
        $link = explode('/', $link);
        return end($link);
    }

    /**
     * Склонение слов по колличеству
     * @param $word
     * @param $zero
     * @param $one
     * @param $few
     * @param $many
     * @param $count
     * @return string
     */
    public function getWordEnd($word, $zero, $one, $few, $many, $count)
    {
        if ($count > 20) {
            $count = (string)$count;
            $count = $count[strlen($count) - 1];
        }

        if ($count == 0) {
            return $word . $zero;
        } elseif ($count == 1) {
            return $word . $one;
        } elseif ($count > 1 && $count < 5) {
            return $word . $few;
        } elseif ($count >= 5) {
            return $word . $many;
        }
    }

    /**
     * Наложение водяного знака на изображение
     * @param $filename string Путь к исходному изображению
     * @param $watermark string Путь к водяному знаку
     */
    public function watermark($filename, $watermark)
    {
        $imagine = Image::getImagine();
        $img = $imagine->open($filename);
        $size = $img->getSize();
        $wm = $imagine->open($watermark);
        $sizeWm = $wm->getSize();
        if ($size->getWidth() > $sizeWm->getWidth() && $size->getHeight() > $sizeWm->getHeight()) {
            $imgWm = [
                $size->getWidth() / 2 - $sizeWm->getWidth() / 2,
                $size->getHeight() / 2 - $sizeWm->getHeight() / 2
            ];
        } else {
            $imgWm = [0, 0];
        }


        Image::watermark($filename, $watermark, $imgWm)
            ->save($filename);
    }

    /**
     * @param $filename
     * @param $needHeight
     * @param $newFileName
     * @return bool
     */
    public static function imgResize($filename, $needHeight, $newFileName)
    {
        list($width, $height) = getimagesize($filename);
        $ratio = $width / $height;
        $newWidth = $needHeight * $ratio;
        $newHeight = $needHeight;

        $thumb = imagecreatetruecolor($newWidth, $newHeight);
        $info = pathinfo($filename);
        if ($info['extension'] == 'png') {
            $source = imagecreatefrompng($filename);
        } else {
            $source = imagecreatefromjpeg($filename);
        }

        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagejpeg($thumb, $newFileName);
        return true;
    }


    public static function imgResizeTask($filename, $needWidth, $newFileName)
    {
        list($width, $height) = getimagesize($filename);
        $ratio = $height / $width;
        $newWidth = $needWidth;
        $newHeight = $needWidth * $ratio;

        $thumb = imagecreatetruecolor($newWidth, $newHeight);
        $info = pathinfo($filename);
        if ($info['extension'] == 'png') {
            $source = imagecreatefrompng($filename);
        } else {
            $source = imagecreatefromjpeg($filename);
        }

        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagejpeg($thumb, $newFileName);
        return true;
    }


    public function resize($file_input, $file_output, $w_o, $h_o, $percent = false)
    {
        list($w_i, $h_i, $type) = getimagesize($file_input);
        if (!$w_i || !$h_i) {
            echo 'Невозможно получить длину и ширину изображения';
            return;
        }
        $types = array('', 'gif', 'jpeg', 'png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom' . $ext;
            $img = $func($file_input);
        } else {
            echo 'Некорректный формат файла';
            return;
        }
        if ($percent) {
            $w_o *= $w_i / 100;
            $h_o *= $h_i / 100;
        }
        if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
        if (!$w_o) $w_o = $h_o / ($h_i / $w_i);

        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
        if ($type == 2) {
            return imagejpeg($img_o, $file_output, 100);
        } else {
            $func = 'image' . $ext;
            return $func($img_o, $file_output);
        }
    }

    /**
     * Получение колличество лет по дате рождения
     * @param $date
     * @return string
     */
    public function getAge($date)
    {
        $dateTime = strtotime($date);
        $fullAge = time() - $dateTime;
        return Yii::$app->formatter->asDatetime($fullAge, "php:Y ") - 1970;
    }


    /**
     * Отправка админу сообщение о новом пользователе
     *
     * @param $url - ссылка на профиль зарешестрирующегося пользователя
     */
    public function newUser($url)
    {
        $text = 'На сайте добавился новый  <a href="http://' . Yii::$app->params['domain'] . '/web/' . $url . '">пользователь</a>';

        Yii::$app->mailer->compose()
            ->setFrom('admin@dobroua.com')
            ->setTo(Yii::$app->params['email'])
            ->setSubject('Новый пользователь dobroua.com')
            ->setHtmlBody($text)
            ->send();
    }

    /**
     * Получения статуса Online
     * сохраняется 15 мин после последнего действия
     * $id - id юзера
     */
    public function getOnline($id)
    {
        $online = Users::find()->where(['id' => $id])->one();
        $time = time() - 900;
        if ($online->visitTime > $time) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     */
    public function onliner($id)
    {
        $userModel = new Users();
        $user = $userModel->findOne($id);
        $user->visitTime = time();
        $user->save(false);
    }

    public function getSearch($count, $post, $model)
    {

        switch ($count) {
            case 0:
                $query = $model->find()->where(['statusId' => 2]);
                break;
            case 1:
                $query = $model->find()->where(['statusId' => 2, 'countryId' => $post['countryId']]);
                break;
            case 3:
                $query = $model->find()->where(['statusId' => 2, 'city' => $post['city']]);
                break;
            case 5:
                $query = $model->find()->where(['statusId' => 2, 'regionId' => $post['regionId']]);
                break;
            case 10:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId']]);
                break;

            case 4:
                $query = $model->find()->where(['statusId' => 2, 'countryId' => $post['countryId'], 'city' => $post['city']]);
                break;
            case 6:
                $query = $model->find()->where(['statusId' => 2, 'countryId' => $post['countryId'], 'regionId' => $post['regionId']]);
                break;
            case 8:
                $query = $model->find()->where(['statusId' => 2, 'city' => $post['city'], 'regionId' => $post['regionId']]);
                break;
            case 11:
                $query = $model->find()->where(['statusId' => 2, 'countryId' => $post['countryId'], 'helpId' => $post['helpId']]);
                break;
            case 13:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId'], 'city' => $post['city']]);
                break;
            case 15:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId'], 'regionId' => $post['regionId']]);
                break;

            case 9:
                $query = $model->find()->where(['statusId' => 2, 'countryId' => $post['countryId'], 'city' => $post['city'], 'regionId' => $post['regionId']]);
                break;
            case 14:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId'], 'countryId' => $post['countryId'], 'city' => $post['city']]);
                break;
            case 16:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId'], 'regionId' => $post['regionId'], 'countryId' => $post['countryId']]);
                break;
            case 18:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId'], 'city' => $post['city'], 'regionId' => $post['regionId']]);
                break;
            case 19:
                $query = $model->find()->where(['statusId' => 2, 'helpId' => $post['helpId'], 'city' => $post['city'], 'regionId' => $post['regionId'], 'countryId' => $post['countryId']]);
                break;
        }
        return $query;
    }

    public function getSearchCount($post)
    {
        $count = 0;
        if (!empty($post['countryId'])) {
            $count += 1;
        }
        if (!empty($post['city'])) {
            $count += 3;
        }
        if (!empty($post['regionId'])) {
            $count += 5;
        }
        if (!empty($post['helpId'])) {
            $count += 10;
        }
        return $count;
    }

    public function newNews($id)
    {
        $model = new News();
        $news = $model->findOne($id);
        $helpersArray = AuthAssignment::find()->where('item_name = "volunteer" OR item_name = "angel"')->all();


        foreach ($helpersArray as $helper) {

            if ($helper->item_name == 'volunteer') {
                $email = $helper->volunteer->email;
                $user = Volunteers::find()->where(['userId' => $helper->user_id])->one();
                $name = $user->name;
            } else {
                $email = $helper->angel->email;
                $user = Angels::find()->where(['userId' => $helper->user_id])->one();
                $name = $user->name;
            }


            if (!empty($email)) {


                $message = '
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <!-- If you delete this meta tag, Half Life 3 will never be released. -->
                        <meta name="viewport" content="width=device-width"/>

                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

                        <style type="text/css">

                        </style>

                    </head>

                    <body style="-webkit-font-smoothing:antialiased;
                        -webkit-text-size-adjust:none;
                        width: 100%!important;
                        height: 100%;" bgcolor="#FFFFFF">

                    <!-- HEADER -->
                    <table style="width: 100%;"  bgcolor="#00a2ff">
                        <tr>
                            <td></td>
                            <td style="display:block!important;
                        max-width:600px!important;
                        margin:0 auto!important; /* makes it centered */
                        clear:both!important;" class="header container">

                                <div style="padding:15px;
                        max-width:600px;
                        margin:0 auto;
                        display:block; " class="content">
                                    <table bgcolor="#00a2ff">
                                        <tr>
                                            <td><img src="http://dobroua.com/web/images/dobro_logo_cr.png"/></td>
                                            <!--<td align="right"><h6 class="collapse">Basic</h6></td>-->
                                        </tr>
                                    </table>
                                </div>

                            </td>
                            <td></td>
                        </tr>
                    </table><!-- /HEADER -->


                    <!-- BODY -->
                    <table style="width: 100%;" >
                        <tr>
                            <td></td>
                            <td style="display:block!important;
                        max-width:600px!important;
                        margin:0 auto!important; /* makes it centered */
                        clear:both!important;" class="container" bgcolor="#FFFFFF">

                                <div style="padding:15px;
                        max-width:600px;
                        margin:0 auto;
                        display:block; " class="content">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <table>
                                                    <td > <h3 style="font-weight:500; font-size: 23px;">Доброго времени суток, ' . $name . ' </h3></td>
                                                    <td style="width:30%;"><img style="width:100%" src="http://dobroua.com/web/images/makedobro.png" alt=""/> </td>
                                                </table>
                                                <img style="width:100%" src="http://dobroua.com/web/images/bg.jpg" alt=""/>
                                                <table>
                                                    <tr><h3 style="font-weight:500; font-size: 23px;">А у нас новости для Вас! </h3></tr>
                                                    <td>
                                                       <img style="width:100%" src="http://dobroua.com/web/images/news/' . $news->filePath . '">
                                                        <p style="padding:15px;background-color:#ECF8FF;margin-bottom: 15px;" >' . strip_tags($this->subStr($news->text, 500)) . '<a style="font-weight:bold;color: #2BA6CB;" href="http://' . Yii::$app->params['domain'] . '/news/more/' . $news->id . '">Click it! &raquo;</a>
                                                        </p>
                                                    </td>
                                                </table>

                                                <table style="background-color: #ebebeb;" width="100%">
                                                    <tr>
                                                        <td>

                                                            <!-- column 1 -->
                                                            <table style="width: 300px;float:left;" align="left" class="column">
                                                                <tr style="padding-left: 15px;padding-right: 15px;">
                                                                    <td style="padding-left: 15px;padding-right: 15px;">

                                                                        <h5 style="margin: 15px; font-weight:900; font-size: 17px; text-transform: uppercase; color:#444;" class="">Следи за нами:</h5>
                                                                        <p class="">
                                                                                <a style="margin-right:10px;cursor:pointer;
                                                                                padding: 3px 7px;font-size:12px;margin-bottom:10px;
                                                                                text-decoration:none;color: #FFF;font-weight:bold;display:block;
                                                                                text-align:center;background-color: #3B5998!important;" href="https://www.facebook.com/groups/1079732442078250/" class="soc-btn fb">
                                                                                Facebook</a>
                                                                            <a style="margin-right:10px;cursor:pointer;
                                                                            padding: 3px 7px;font-size:12px;margin-bottom:10px;
                                                                            text-decoration:none;color: #FFF;font-weight:bold;display:block;
                                                                            text-align:center;background-color: #597DA3!important;" href="https://vk.com/dobrouacom" class="soc-btn vk">ВКонтакте</a>
                                                                        </p>


                                                                    </td>
                                                                </tr>
                                                            </table><!-- /column 1 -->

                                                            <!-- column 2 -->
                                                            <table  align="left" class="column">
                                                                <tr style="padding-left: 15px;padding-right: 15px;">
                                                                    <td style="padding-left: 15px;padding-right: 15px;">

                                                                        <h5 style="margin: 15px; font-weight:900; font-size: 17px; text-transform: uppercase; color:#444;" class="">Свяжись с нами:</h5>
                                                                        Email: <strong><a style="color: #2BA6CB;"
                                                                                          href="emailto:admin@dobroua.com">admin@dobroua.com</a></strong></p>

                                                                    </td>
                                                                </tr>
                                                            </table><!-- /column 2 -->

                                                            <span style="display: block; clear: both;" class="clear"></span>

                                                        </td>
                                                    </tr>
                                                </table><!-- /social & contact -->

                                            </td>
                                        </tr>
                                    </table>
                                </div><!-- /content -->

                            </td>
                            <td></td>
                        </tr>
                    </table><!-- /BODY -->
                    </body>
                    </html>';
                Yii::$app->mailer->compose()
                    ->setFrom('admin@dobroua.com')
                    ->setTo($email)
                    ->setSubject('На сайте dobroua.com была добавлена Новость')
                    ->setHtmlBody($message)
                    ->send();
            }
        }

    }

}
