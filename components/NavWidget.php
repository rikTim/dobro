<?php

namespace app\components;

use Yii;
use yii\base\Widget;


class NavWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('navbar');
    }
}