<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>

<span class="contact-btn contactBtn" data-toggle="modal" data-target="#contactModal">
    <i class="fa fa-envelope-o"></i><br>
    <div class="text">Связаться с<br>нами</div>
</span>

<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="#contactModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Написать нам</h4>
            </div>

            <?php /** Можем писать администратору только с заполенным профилем */ ?>
            <?php $profile = Yii::$app->DLL->getProfile(); if(Yii::$app->user->isGuest || empty($profile)): ?>
                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute('/site/contacts'),
                ]); ?>
                <div class="modal-body">
                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label('Имя')?>
                    <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app','Email')])?>
                    <?= $form->field($model, 'text')->textArea(['placeholder'=> 'Сообщение'])->label('Сообщение')?>
                    <?= $form->field($model, 'captcha')->widget(Captcha::className(), ['captchaAction' => '/site/captcha']) ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            <?php else: ?>
                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute('/site/contacts-register'),
                ]); ?>
                <div class="modal-body">
                    <?= $form->field($message, 'text')->textArea(['placeholder'=> 'Сообщение'])->label('Сообщение')?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
