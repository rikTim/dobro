<?php

use yii\helpers\Url;
?>

<?php if(!empty($models)): ?>
    <div class="clearfix">
        <h4>Уже помог</h4>
        <div class="col-xs-12 history">
            <?php foreach($models as $model): ?>
                <?php
                $userRole = Yii::$app->DLL->getRoleByUserId($model->toUserId);
                switch($userRole) {
                    case 'volunteer': $link = 'volunteers'; $imgPath = $model->toVolunteerUser->file->path; break;
                    case 'angel': $link = 'angels'; $imgPath = $model->toAngelUser->file->path; break;
                    case 'needy': $link = 'needies'; $imgPath = $model->toNeedyUser->file->path; break;
                }
                ?>
                <div class="media">
                    <div class="media-left">
                        <a href="<?= Url::toRoute([$link.'/more', 'id' => $model->realId]) ?>">
                            <?php if(!empty($imgPath) && file_exists(Yii::getAlias('@app/web/images/' . $model->toUserId . '/' . $imgPath))): ?>
                                <img class="media-object img-rounded avatar"
                                     src="<?= Yii::getAlias('@web') . '/images/' . $model->toUserId . '/m_' . $imgPath ?>"
                                     data-holder-rendered="true">
                            <?php else: ?>
                                <img class="media-object img-rounded avatar"
                                     src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>"
                                     data-holder-rendered="true">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <a href="<?= Url::toRoute([$link.'/more', 'id' => $model->realId]) ?>" class="name"><?= $model->userName ?> </a>
                        </h4>
                        <small><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></small>
                        <p><?= $model->text ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif; ?>
