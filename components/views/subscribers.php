<?php

use yii\helpers\Url;
?>

<div class="container subscribers">
    <?php if(!empty($models)): ?>
        <h3 class="text-center">Уже подписались</h3>
        <div class="row">
            <?php foreach($models as $model): ?>
                <?php $profile = Yii::$app->DLL->getProfileById($model->userId); ?>
                <?php $role = Yii::$app->DLL->getRoleByUserId($model->userId); ?>
                <div class="col-xs-4 sub-item">
                    <a href="<?= Url::toRoute(['/' . $role . 's/more', 'id' => $profile->id]) ?>">
                        <?php if(!empty($profile->file->path)): ?>
                            <img src="<?= Yii::getAlias('@web') . '/images/' . $profile->userId . '/m_' . $profile->file->path ?>" alt="avatar" class="img-circle">
                        <?php else: ?>
                            <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>" alt="avatar" class="img-circle">
                        <?php endif; ?>
                    </a>

                    <a href="<?= Url::toRoute(['/' . $role . 's/more', 'id' => $profile->id]) ?>"><?= $profile->name . ' ' . $profile->lastname ?></a>

                    <p><?= !empty($profile->rating) ? 'Рейтинг: ' . $profile->rating : '' ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>