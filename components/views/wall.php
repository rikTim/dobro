<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;

?>

<div class="container user-wall">
        <!-- Form block -->
        <?php $profile = Yii::$app->DLL->getProfile(); ?>
        <?php if(!Yii::$app->user->isGuest && !empty($profile)  ): ?>
            <div>
                <?php $form = ActiveForm::begin([
                    'id' => 'wall-form',
                    'enableClientValidation' => true,
                    'action' => Url::toRoute('/site/add-wall-comment'),
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <div class="col-xs-8 col-sm-10">
                    <div class="form">
                        <div class="form-group">
                            <?= $form->field($comment, 'text')->textInput(['value' => '', 'placeholder' => 'Написать'])->label(false) ?>
                            <?= $form->field($comment, 'toUserId')->hiddenInput(['value' => $userId])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

                <div class="col-xs-12">
                    <?= Html::button('Прикрепить файл', ['class' => 'btn btn-info addAttaches']) ?>
                </div>

                <div class="col-xs-12 attach-form attachForm">
                    <label class="control-label">Добавить файл</label>
                    <?= FileInput::widget([
                        'model' => $upload,
                        'attribute' => 'file[]',
                        'language' => 'ru',
                        'options' => [
                            'multiple' => true,
                            'accept' => 'image/*'
                        ],
                        'pluginOptions' => [
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false,
                            'maxFileCount' => 30,
                        ]
                    ]);?>
                </div>

                <?php ActiveForm::end(); ?>

                <br><br><br>
                <?php
                    $wallMessage = Yii::$app->session->getFlash('wallMessage');
                    if(!empty($wallMessage)): ?>
                    <p class="bg-success flash-message"><?= Yii::$app->session->getFlash('wallMessage') ?></p>
                <?php endif; ?>
            </div>
        <?php else: ?>
            <div class="alert alert-warning">
                <a href="<?= Url::toRoute(['/user/register']) ?>">Зарегистрируйтесь</a> Вы сможете общаться с другими пользователями, получать уведомления о всех новых событиях и полноценно пользоваться всеми функциями сайта только после одобрения Вашей персональной информации администратором
            </div>
        <?php endif; ?>
        <!-- /Form block -->


        <?php if(!empty($comments)): ?>
            <!-- Comments block -->
            <div class="comments col-xs-12">
                <?php foreach($comments as $comment): ?>
                    <div class="wall-item clearfix">
                        <div class="img">
                            <?php if($comment->fromUserId != 1): ?>
                            <?php
                            $userRole = Yii::$app->DLL->getRoleByUserId($comment->fromUserId);
                            switch($userRole) {
                                case 'volunteer': $link = 'volunteers'; $imgPath = $comment->fromVolunteerUser->file->path; break;
                                case 'angel': $link = 'angels'; $imgPath = $comment->fromAngelUser->file->path; break;
                                case 'needy': $link = 'needies'; $imgPath = $comment->fromNeedyUser->file->path; break;
                            }
                            ?>

                            <a href="<?= Url::toRoute([$link.'/more', 'id' => $comment->realId]) ?>">
                                <?php if(!empty($imgPath) && file_exists(Yii::getAlias('@app/web/images/' . $comment->fromUserId . '/' . $imgPath))): ?>
                                    <img src="<?= Yii::getAlias('@web') . '/images/' . $comment->fromUserId . '/m_' . $imgPath ?>" alt="avatar" class="avatar img-rounded">
                                <?php else: ?>
                                    <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>" alt="avatar" class="avatar img-rounded">
                                <?php endif; ?>
                            </a>
                            <?php else: ?>
                                <img src="<?= Yii::getAlias('@web/images/admin.jpg') ?>" alt="avatar" class="avatar img-rounded">
                            <?php endif; ?>
                        </div>

                        <?php if($comment->fromUserId != 1): ?>
                            <a href="<?= Url::toRoute([$link.'/more', 'id' => $comment->realId]) ?>" class="name"><?= $comment->userName ?> </a>
                        <?php else: ?>
                            <p class="name"><b><?= $comment->userName ?></b></p>
                        <?php endif; ?>



                        <p class="date"><?= Yii::$app->formatter->asDatetime($comment->dateTime, "php:d.m.Y") ?></p>
                        <p class="text"><?= $comment->text ?></p>

                        <!-- Attaches -->
                        <?php if(!empty($comment->attaches)): ?>
                            <div id="gallery">
                                <?php foreach($comment->attaches as $attach): ?>
                                    <a href="<?= Yii::getAlias('@web') . '/images/attachments/' . $comment->id . '/' . $attach->path ?>">
                                        <img class="gallery" src="<?= Yii::getAlias('@web') . '/images/attachments/' . $comment->id . '/' . $attach->path ?>">
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <!-- /Attaches -->
                    </div>
                <?php endforeach; ?>
                <!-- /Comments block -->
            </div>
        <?php endif; ?>
</div>

<div style="height: 50px"></div>