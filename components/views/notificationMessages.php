<?php

use yii\helpers\Url;
?>

<?php $message = Yii::$app->session->getFlash('message'); if(!empty($message)): ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-info">
                        <?= $message ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php $error = Yii::$app->session->getFlash('error'); if(!empty($error)): ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-danger">
                        <?= $error ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php $profile = Yii::$app->DLL->getProfile(); if(!Yii::$app->user->isGuest && empty($profile)): ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-warning">
                        <a href="<?= Url::toRoute(['/profile/edit']) ?>">Заполните свой профиль</a> Вы сможете общаться с другими пользователями, получать уведомления о всех новых событиях и полноценно пользоваться всеми функциями сайта только после одобрения Вашей персональной информации администратором
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>