<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use kartik\file\FileInput;

?>

<?php $role = Yii::$app->DLL->getRoleByUserId(Yii::$app->user->id); ?>
<?php if($role != 'needy'): ?>
    <div class="clearfix">
        <div class="col-xs-3 col-sm-2">
            <button style="color: #fff; background: #006cff;border-radius: 20px;border-color: #006cff;box-shadow: none;" type="button" class="btn btn-success" data-toggle="modal" data-target="#helpModal">Я помог</button>
        </div>
        <div class="col-xs-9 col-sm-10">
            Нажимая кнопку "Я помог", Вы можете сообщить общественности о том, что хорошего Вы сделали для этого человека, тем самым увеличив свой рейтинг на проекте.
        </div>
    </div>
<?php endif; ?>


<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="#helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <?php $profile = Yii::$app->DLL->getProfile(); ?>
            <?php if(!Yii::$app->user->isGuest && !empty($profile)  ): ?>

                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute('/site/help'),
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <div class="modal-body">
                    <?= $form->field($model, 'money')->textInput(['value' => '', 'placeholder' => 'Укажите перечисленую сумму (например, 100 грн)'])->label('Я помог финансово') ?>
                    <?= $form->field($model, 'things')->textArea(['value' => '', 'placeholder' => 'Напишите, что вы сделали'])->label('Я помог нематериально') ?>
                    <?= $form->field($model, 'comment')->textarea(['value' => '', 'placeholder' => 'Комментарий'])->label('Комментарий') ?>

<!--                    <label class="control-label">Добавить Фото</label>-->
<!--                    --><?//= FileInput::widget([
//                        'model' => $upload,
//                        'attribute' => 'file[]',
//                        'language' => 'ru',
//                        'options' => ['multiple' => true,'accept' => 'image/*'],
//                        'pluginOptions' => [
//                            'showPreview' => true,
//                            'showCaption' => true,
//                            'showRemove' => true,
//                            'showUpload' => false,
//                            'maxFileCount' => 30,
//                        ]
//                    ]);?>

                    <?= $form->field($upload, 'file[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label(false) ?>
                    <?= $form->field($model, 'toUserId')->hiddenInput(['value' => $toUserId])->label('') ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>

            <?php else: ?>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        <a href="<?= Url::toRoute(['/user/register']) ?>">Зарегистрируйтесь</a> Вы сможете общаться с другими пользователями, получать уведомления о всех новых событиях и полноценно пользоваться всеми функциями сайта только после одобрения Вашей персональной информации администратором
                    </div>
                </div>
            <?php endif; ?>


        </div>
    </div>
</div>
