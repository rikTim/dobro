<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>

<div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="#ratingModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">История рейтинга</h4>
                <p>Рейтинг начисляется пользователю сайта dobroua за оказанную материальную помощь людям, проверку заявок, сообщения о состоянии тех, кто оставлял заявку о помощи, оставленные комментарии, но исключительно после проверки поступившей информации администратором.</p>
            </div>
            <div class="modal-body">
                <?php if(!empty($models)): ?>
                    <?php foreach($models as $model): ?>
                        <div class="item clear">
                            <div class="row">
                                <div class="col-xs-10">
                                    <span class="rating">+ <?= $model->rating ?></span>
                                    <?php if(!empty($model->descr)): ?>
                                        <span class="name"><?= $model->descr ?></span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-xs-2">
                                    <?php if(!empty($model->dateTime)): ?>
                                        <span class="date"><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h4>Рейтинг отсутствует</h4>
                <?php endif; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>