<nav id="w0" class="navbar-inverse navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#w0-collapse" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span></button>
        </div>
        <div id="w0-collapse" class="navbar-collapse collapse" aria-expanded="false" style="height: 1.11111px;">
            <ul id="w1" class="navbar-nav nav">
                <li class="active"><a href="/dobroua.com/web/">Главная</a></li>
                <li><a href="/dobroua.com/web/profile/index">Профиль</a></li>
                <li><a href="/dobroua.com/web/volunteers/list">Волонтеры</a></li>
                <li><a href="/dobroua.com/web/angels/list">Ангелы</a></li>
                <li><a href="/dobroua.com/web/needies/list">Нуждающиеся</a></li>
                <li><a href="/dobroua.com/web/about">О нас</a></li>
                <li><a href="/dobroua.com/web/contact">Контакты</a></li>
            </ul>
            <ul id="w1" class="navbar-nav navbar-right nav">
                <li data-toggle="modal" data-target="#authMadal"><a href="#"><i class="fa fa-sign-in"></i>Войти</a></li>
                <li><a href="/dobroua.com/web/user/register"><i class="fa fa-users"></i>Присоедениться</a></li>
            </ul>
        </div>
    </div>
</nav>