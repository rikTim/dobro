<?php

    use yii\helpers\Url;
?>

<?php if(!empty($models)): ?>
    <?php if($blockName === 'ангелов'): ?>
        <h2><a href="<?= Url::toRoute('angels/list') ?>" >Топ <?= $blockName ?></a></h2>
    <?php else: ?>
        <h2><a href="<?= Url::toRoute('volunteers/list') ?>" >Топ <?= $blockName ?></a></h2>
    <?php endif; ?>


    <?php foreach($models as $key => $val): ?>
        <div class="row">
            <div class="img">
                <a href="<?= Url::toRoute([ $blockName == 'ангелов' ? 'angels' . '/more' : 'volunteers' . '/more', 'id' => $val['id']]) ?>">
                    <?php if(!empty($val['img']) && file_exists(Yii::getAlias('@app/web/images/' . $val['userId'] . '/' . $val['img']))): ?>
                        <img src="<?= Yii::getAlias('@web') . '/images/' . $val['userId'] . '/m_' . $val['img'] ?>" alt="avatar" class="img-circle">
                    <?php else: ?>
                        <img src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>" alt="avatar" class="img-circle">
                    <?php endif; ?>
                </a>
            </div>



            <div class="name">
                <?php if($blockName === 'ангелов'): ?>
                    <a href="<?= Url::toRoute(['angels/more', 'id' => $val['id']]) ?>"><?= $val['name'] ?></a>
                <?php else: ?>
                    <a href="<?= Url::toRoute(['volunteers/more', 'id' => $val['id']]) ?>"><?= $val['name'] ?></a>
                <?php endif; ?>
                <?php if(!empty($val['rating'])): ?>
                    <p>Рейтинг: <?= $val['rating'] ?> </p>
                <?php endif; ?>
            </div>
        </div>
        <?php if($key < 2): ?>
            <div class="line"></div>
        <?php endif; ?>
    <?php endforeach; ?>

<?php else: ?>
    <h2>Еще нет зарегистрированных <?= $blockName ?></h2>
<?php endif; ?>