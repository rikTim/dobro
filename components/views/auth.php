<?php

use dektrium\user\widgets\Login;
?>

<div class="modal fade" id="authMadal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Вход</h4>
            </div>
            <div class="modal-body">
                <?= Login::widget(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary enterBtn">Продолжить</button>
            </div>
        </div>
    </div>
</div>