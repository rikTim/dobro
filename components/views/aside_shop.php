<?php

use yii\helpers\Url;
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">



            <li class="header"></li>

            <li>
                <a href="<?= Url::toRoute(['shop/index']); ?>">
                    <i class="fa fa-star text-orange"></i> <span>Магазин</span>
                </a>
            </li>


            <li>
                <a href="<?= Url::toRoute(['shopcomment/index']); ?>">
                    <i class="fa fa-newspaper-o"></i> <span>Коментарий</span>
                    <span class="label bg-green pull-right"><?= $newComment?></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['shop/counter']) ?>">
                    <i class="fa fa-bar-chart"></i><span>Счетчик</span>
                </a>
            </li>

<!--            <li>-->
<!--                <a href="--><?//= Url::toRoute(['admin/metrica']) ?><!--">-->
<!--                    <i class="fa fa-bar-chart"></i><span>Metrica</span>-->
<!--                </a>-->
<!--            </li>-->
<!---->
<!---->
<!--            <li>-->
<!--                <a href="--><?//= Url::toRoute(['needies/counter']) ?><!--">-->
<!--                    <i class="fa fa-bar-chart"></i><span>Счетчик</span>-->
<!--                </a>-->
<!--            </li>-->


        </ul>
    </section>
</aside>