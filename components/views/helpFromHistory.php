<?php

use yii\helpers\Url;

?>

<?php if (!empty($models)): ?>
    <div class="clearfix">
        <h4>Уже помогли</h4>
        <div class="col-xs-12 history">
            <?php foreach ($models as $model): ?>
                <?php
                $userRole = Yii::$app->DLL->getRoleByUserId($model->fromUserId);
                switch ($userRole) {
                    case 'volunteer':
                        $link = 'volunteers';
                        $imgPath = $model->fromVolunteerUser->file->path;
                        break;
                    case 'angel':
                        $link = 'angels';
                        $imgPath = $model->fromAngelUser->file->path;
                        break;
                    case 'needy':
                        $link = 'needies';
                        $imgPath = $model->fromNeedyUser->file->path;
                        break;
                }
                ?>
                <?php if ($model->realId == 'admin'): ?>
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object img-rounded avatar"
                                 src="<?= Yii::getAlias('@web') . '/images/admin.jpg' ?>"
                                 data-holder-rendered="true">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <?= $model->userName ?>
                            </h4>
                            <small><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></small>
                            <p><?= $model->text ?></p>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="media">
                        <div class="media-left">

                            <a href="<?= Url::toRoute([$link . '/more', 'id' => $model->realId]) ?>">
                                <?php if (!empty($imgPath) && file_exists(Yii::getAlias('@app/web/images/' . $model->fromUserId . '/' . $imgPath))): ?>
                                    <img class="media-object img-rounded avatar"
                                         src="<?= Yii::getAlias('@web') . '/images/' . $model->fromUserId . '/m_' . $imgPath ?>"
                                         data-holder-rendered="true">
                                <?php else: ?>
                                    <img class="media-object img-rounded avatar"
                                         src="<?= Yii::getAlias('@web') . '/images/no-image-man.png' ?>"
                                         data-holder-rendered="true">
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <a href="<?= Url::toRoute([$link . '/more', 'id' => $model->realId]) ?>"
                                   class="name"><?= $model->userName ?> </a>
                            </h4>
                            <small><?= Yii::$app->formatter->asDatetime($model->dateTime, "php:d.m.Y") ?></small>
                            <p><?= $model->text ?></p>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif; ?>
