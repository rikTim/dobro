<?php

use yii\helpers\Url;
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
<!--            <li class="active treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>-->
<!--                    <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="--><?//= Url::toRoute(['subscribe/index','page'=>1,'sort'=>'-id']); ?><!--">-->
<!--                    <i class="fa fa-envelope"></i> <span>Подписчики</span>-->
<!--                    <span class="label bg-green pull-right">--><?//= $newSub?><!--</span>-->
<!---->
<!--                </a>-->
<!--            </li>-->

            <li>
                <a href="<?= Url::toRoute(['admin/contacts']); ?>">
                    <i class="fa fa-envelope text-aqua"></i> <span>Вопросы</span>
                    <span class="label bg-green pull-right"><?= $newContacts?></span>
                </a>
            </li>

            <li class="header"> Пользователи </li>
            <li>
                <a href="<?= Url::toRoute(['angels/index','sort'=>'statusPosition']); ?>">
                    <i class="fa fa-user text-green"></i> <span>Ангелы</span>
                    <span class="label bg-green pull-right"><?= $newAngels?></span>
                    <span class="label bg-blue pull-right-two"><?= $reversedAngels?></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['volunteers/index','sort'=>'statusPosition']); ?>">
                    <i class="fa fa-user text-yellow"></i> <span>Волонтеры</span>
                    <span class="label bg-green pull-right"><?= $newVolunters?></span>
                    <span class="label bg-blue pull-right-two"><?= $reversedVolunters?></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['needies/index','sort'=>'statusPosition']); ?>">
                    <i class="fa fa-user text-red"></i> <span>Кому помочь</span>
                    <span class="label bg-green pull-right"><?= $newNeedies?></span>
                    <span class="label bg-blue pull-right-two"><?= $reversedNeedies?></span>
                </a>
            </li>


            <li class="header"> Комментарии </li>
<!---->
<!--            <li class="treeview">-->
<!--                <a href="--><?//= Url::toRoute(['comments/index','type'=>'needy']); ?><!--">-->
<!--                    <i class="fa fa-envelope"></i> <span>Comments</span>-->
<!--                    <i class="fa fa-angle-left pull-right"></i>-->
<!--                </a>-->
<!--                <ul class="treeview-menu" style="display: none;">-->
<!--                    <li class="active"><a href="--><?//= Url::toRoute(['comments/index','type'=>'angel']); ?><!--">Angels <span class="label bg-green pull-right"></span></a></li>-->
<!--                    <li><a href="--><?//= Url::toRoute(['comments/index','type'=>'angel']); ?><!--">Volunteers <span class="label bg-green pull-right"></span></a></li>-->
<!--                    <li><a href="--><?//= Url::toRoute(['comments/index','type'=>'needy']); ?><!--">Needies <span class="label bg-green pull-right"></span></a></li>-->
<!--                </ul>-->
<!--            </li>-->




            <li>
            <a href="<?= Url::toRoute(['comments/index','type'=>'angel','sort'=>'statusPosition']); ?>">
                <i class="fa fa-envelope text-green"></i> <span>Ангелы</span>
                <span class="label bg-green pull-right"><?= $newCommentAngels?></span>
            </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['comments/index','type'=>'volunteer','sort'=>'statusPosition']); ?>">
                    <i class="fa fa-envelope text-yellow"></i> <span>Волонтеры</span>
                    <span class="label bg-green pull-right"><?= $newCommentVolunteers?></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['comments/index','type'=>'needy','sort'=>'statusPosition']); ?>">
                    <i class="fa fa-envelope text-red"></i> <span>Кому помочь</span>
                    <span class="label bg-green pull-right"><?= $newCommentNeedies?></span>
                </a>
            </li>

            <li class="header"> Сообщения </li>
            <li>
                <a href="<?= Url::toRoute(['admin/messages','type'=>'2']); ?>">
                    <i class="fa fa-envelope text-green"></i> <span>Ангелы</span>
                    <span class="label bg-green pull-right"><?=$newMessageAngels?></span>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['admin/messages','type'=>'3']); ?>">
                    <i class="fa fa-envelope text-yellow"></i> <span>Волонтеры</span>
                    <span class="label bg-green pull-right"><?=$newMessageVolunteers?></span>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['admin/messages','type'=>'4']); ?>">
                    <i class="fa fa-envelope text-red"></i> <span>Кому помочь</span>
                    <span class="label bg-green pull-right"><?=$newMessageNeedies?></span>
                </a>
            </li>
            <li class="header">Рейтинг</li>
            <li>
                <a href="<?= Url::toRoute(['admin/rating-angels']); ?>">
                    <i class="fa fa-star text-green"></i> <span>Ангелы</span>
                    <span class="label bg-green pull-right"><?= $newAngelHelp?></span>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['admin/rating-volunteer']); ?>">
                    <i class="fa fa-star text-yellow"></i> <span>Волонтеры</span>
                    <span class="label bg-green pull-right"><?= $newVolunteerHelp?></span>
                </a>
            </li>
            <li class="header">Обьявления</li>


            <li>
                <a href="<?= Url::toRoute(['board/index']); ?>">
                    <i class="fa fa-external-link text-aqua "></i> <span>Доска обьявлений</span>
                    <span class="label bg-green pull-right"><?= $newBoard?></span>
                    <span class="label bg-blue pull-right-two"><?= $reversedBoard?></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['board/needy-help']); ?>">
                    <i class="fa fa-external-link text-orange"></i> <span>Мне нужно</span>
                    <span class="label bg-green pull-right"><?= $getNeedyBoard?></span>

                </a>
            </li>

            <li class="header">Задания</li>
            <li>
                <a href="<?= Url::toRoute(['tasks/index']); ?>">
                    <i class="fa fa-medkit text-aqua"></i> <span>Задания</span>
                    <span class="label bg-green pull-right"><?= $getVolunterHelp?></span>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['taskcomment/create']) ?>">
                    <i class="fa fa-medkit text-orange"></i><span>Коментарии</span>
                    <span class="label bg-green pull-right"><?= $getTaskComment?></span>
                </a>
            </li>
            <li class="header"></li>

            <li>
                <a href="<?= Url::toRoute(['partner/index']); ?>">
                    <i class="fa fa-star text-orange"></i> <span>Партнеры</span>
                </a>
            </li>


            <li>
                <a href="<?= Url::toRoute(['news/index']); ?>">
                    <i class="fa fa-newspaper-o"></i> <span>Новости</span>
                    <span class="label bg-green pull-right"></span>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['admin/metrica']) ?>">
                    <i class="fa fa-bar-chart"></i><span>Metrica</span>
                </a>
            </li>


            <li>
                <a href="<?= Url::toRoute(['needies/counter']) ?>">
                    <i class="fa fa-bar-chart"></i><span>Счетчик</span>
                </a>
            </li>


        </ul>
    </section>
</aside>