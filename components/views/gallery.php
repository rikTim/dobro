<div id="gallery">
    <?php foreach ($photos as $photo) : ?>
        <a href="<?= Yii::getAlias('@web') . '/images/' . $photo->userId . '/' . $photo->path ?>">
            <img src="<?= Yii::getAlias('@web') . '/images/' . $photo->userId . '/' . $photo->path ?>" alt="<?= $photo->descr ?>" style="height: 50px; max-width: 50px;">
        </a>
    <?php endforeach; ?>
</div>