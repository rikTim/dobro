<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<section class="container contact-widget">
    <?php if(!empty($messages)): ?>
        <?php foreach($messages as $message): ?>
            <div class="row">

                <div class="img">
                    <?php
                    $userRole = Yii::$app->DLL->getRoleByUserId($message->fromUserId);
                    switch($userRole) {
                        case 'volunteer': $imgPath = $message->fromVolunteerUser->file->path; break;
                        case 'angel': $imgPath = $message->fromAngelUser->file->path; break;
                        case 'needy': $imgPath = $message->fromNeedyUser->file->path; break;
                    }
                    ?>

                    <?php $img = $message->fromUserId == Yii::$app->user->id ? Yii::$app->user->id . '/' . $imgPath : 'admin.png' ?>
                    <img src="<?= Yii::getAlias('@web') . '/images/' . $img ?>" class="img-rounded">
                </div>

                <div class="message">
                    <p class="name"><?= $message->fromUserId == Yii::$app->user->id ? $message->userName : 'Администратор DobroUA' ?></p>
                    <p class="text"><?= $message->text ?></p>
                    <p class="date"><?= Yii::$app->formatter->asDatetime($message->dateTime, "php:d.m.Y H:i") ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <div class="row">
        <?php $form = ActiveForm::begin() ?>
            <div class="col-md-10">
                <?= $form->field($model, 'text')->textInput(['value' => '', 'placeholder'=>'Написать'])->label(false) ?>
            </div>
            <div class="col-md-2">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-block']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>