<?php

namespace app\components;

use app\models\Angels;
use app\models\Board;
use app\models\Comments;
use app\models\Contacts;
use app\models\HelpTypes;
use app\models\Messages;
use app\models\Needies;
use app\models\NeedyHelp;
use app\models\Subscribe;
use app\models\TaskComment;
use app\models\Tasks;
use app\models\UserHelp;
use app\models\UserTask;
use app\models\VolunteerHelp;
use app\models\Volunteers;
use app\models\VolunteerTask;
use Yii;
use yii\base\Widget;


class AsideWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('aside',[
            'newAngels' => $this->newAngels(),
            'newNeedies' => $this->newNeedies(),
            'newVolunters' => $this->newVolunteers(),
            'newSub'=>$this->newSub(),
            'newHelp'=>$this->newHelp(),
            'newMessageAngels'=>$this->newMessageAngels(),
            'newMessageVolunteers'=>$this->newMessageVolunteers(),
            'newMessageNeedies'=>$this->newMessageNeedies(),
            'reversedAngels' => $this->reversedAngels(),
            'reversedNeedies' => $this->reversedNeedies(),
            'reversedVolunters' => $this->reversedVolunteers(),
            'newCommentVolunteers'=>$this->newComment('volunteer'),
            'newCommentAngels'=>$this->newComment('angel'),
            'newCommentNeedies'=>$this->newComment('needy'),
            'newAngelHelp'=>$this->newHelps('angel'),
            'newVolunteerHelp'=>$this->newHelps('volunteer'),
            'newRating'=>$this->newRating(),
            'newContacts'=>$this->newContacts(),
            'newBoard' => $this->newBoard(),
            'reversedBoard'=> $this->reversedBoard(),
            'getVolunterHelp' => $this->getVolunterHelp(),
            'getNeedyBoard' => $this->getNeedyBoard(),
            'getTaskComment' => $this->getTaskComment(),
        ]);
    }

    public function newAngels()
    {
        $angelsModels = new Angels();
        $count = count($angelsModels->getNewAngels());
        return $count > 0 ? $count : '';
    }

    public function newNeedies()
    {
        $neediesModels = new Needies();
        $count = count($neediesModels->getNewNeedies());
        return $count > 0 ? $count : '';
    }

    public function newVolunteers()
    {
        $volunteersModels = new Volunteers();
        $count = count($volunteersModels->getNewVolunteers());
        return $count > 0 ? $count : '';
    }

    public function newSub()
    {
        $sub = new Subscribe();
        $count = count($sub->getNew());
        return $count > 0 ? $count : '';
    }

    public function newHelp()
    {
        $helpTypeModel = new HelpTypes();
        $count = count($helpTypeModel->getNew());
        return $count > 0 ? $count : '';
    }

    public function newMessageAngels()
    {
        $messageModel = new Messages();
        $count = $messageModel->getCountMessageToAdmin(2);
        return $count > 0 ? $count : '';
    }

    public function newMessageVolunteers()
    {
        $messageModel = new Messages();
        $count = $messageModel->getCountMessageToAdmin(3);
        return $count > 0 ? $count : '';
    }

    public function newMessageNeedies()
    {
        $messageModel = new Messages();
        $count = $messageModel->getCountMessageToAdmin(4);
        return $count > 0 ? $count : '';
    }
    public function reversedAngels()
    {
        $angelsModels = new Angels();
        $count = count($angelsModels->getReversedAngels());
        return $count > 0 ? $count : '';
    }

    public function reversedNeedies()
    {
        $neediesModels = new Needies();
        $count = count($neediesModels->getReversedNeedies());
        return $count > 0 ? $count : '';
    }

    public function reversedVolunteers()
    {
        $volunteersModels = new Volunteers();
        $count = count($volunteersModels->getReversedVolunteers());
        return $count > 0 ? $count : '';
    }

    public function newComment($type)
    {
        $commentsModels = new Comments();
        $count = count($commentsModels->getNewComment($type));
        return $count > 0 ? $count : '';
    }

    public function newHelps($type)
    {
        $model = new UserHelp();
        $count = count($model->getHelp($type));
        return $count > 0 ? $count : '';
    }

    public function newRating()
    {
        $model = new UserTask();
        $count = count($model->getNew());
        return $count > 0 ? $count : '';
    }

    public function newContacts()
    {
        $model = new Contacts();
        $count = count($model->newContacts());
        return $count > 0 ? $count : '';

    }

    public function newBoard()
    {
        $model = new Board();
        $count = count($model->newBoard());
        return $count > 0 ? $count : '';
    }

    public function reversedBoard()
    {
        $boardModels = new Board();
        $count = count($boardModels->getReversedBoard());
        return $count > 0 ? $count : '';
    }

    public function getVolunterHelp()
    {
        $volunteer = new UserTask();
        $count = count($volunteer->find()->all());
        return $count > 0 ? $count : '';
    }

    public function getNeedyBoard()
    {
        $needyModel = new NeedyHelp();
        $count = count($needyModel->find()->all());
        return $count > 0 ? $count : '';
    }

    public function getTaskComment()
    {
        $taskCommentBoard = new TaskComment();
        $count = count($taskCommentBoard->find()->where(['statusId'=>1])->one());
        return $count > 0 ? $count : '';
    }
}