<?php

namespace app\components;

use app\models\UserHelp;
use Yii;
use yii\base\Widget;


class HelpToHistoryWidget extends Widget
{
    public $userId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $userHelps = UserHelp::find()->where(['fromUserId' => $this->userId, 'isVerify' => 1])->orderBy(['dateTime' => SORT_DESC])->all();

        return $this->render('helpToHistory', [
            'userId' => $this->userId,
            'models' => $this->getNameByUserId($userHelps),
        ]);
    }

    public function getNameByUserId($userHelps)
    {
        foreach ($userHelps as $userHelp) {
            $model = Yii::$app->DLL->getModelByUserId($userHelp->toUserId);
            $nl = $model::find()->where(['userId' => $userHelp->toUserId])->one();
            $userHelp->userName = $nl->name . ' ' . $nl->lastname;
            $userHelp->realId = $nl->id;
        }
        return $userHelps;
    }
}