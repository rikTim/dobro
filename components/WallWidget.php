<?php

namespace app\components;

use app\models\Attachment;
use app\models\Comments;
use Yii;
use yii\base\Widget;
use app\models\UploadForm;


class WallWidget extends Widget
{
    public $userId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new Comments();
        $comments = $model::find()->where(['toUserId' => $this->userId, 'statusId' => 2])->orderBy(['dateTime' => SORT_DESC])->all();

        return $this->render('wall', [
            'userId' => $this->userId,
            'comment' => $model,
            'comments' => $this->getNameByUserId($comments),
            'upload' => new UploadForm(),
        ]);
    }

    public function getNameByUserId($comments)
    {
        foreach ($comments as $comment) {
            if ($comment->fromUserId != 1) {
                $model = Yii::$app->DLL->getModelByUserId($comment->fromUserId);
                $nl = $model::find()->where(['userId' => $comment->fromUserId])->one();
                $comment->userName = $nl->name . ' ' . $nl->lastname;
                $comment->realId = $nl->id;
            } else {
                $comment->userName = 'Администратор DobroUA.com';
            }
        }
        return $comments;
    }
}