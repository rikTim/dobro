<?php

namespace app\components;

use Yii;
use yii\base\Widget;


class TopWidget extends Widget
{

    public $models;
    public $blockName;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('top', [
            'models' => $this->models,
            'blockName' => $this->blockName
        ]);
    }
}