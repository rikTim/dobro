<?php

namespace app\components;


use app\models\TaskComment;
use Yii;
use yii\base\Widget;



class TaskCommentWidget extends Widget
{
    public $taskId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new TaskComment();
        $comments = $model::find()->where(['taskId' => $this->taskId, 'statusId' => 2])->orderBy(['dateTime' => SORT_DESC])->all();

        return $this->render('taskcomment', [
            'taskId' => $this->taskId,
            'comment' => $model,
            'comments' => $this->getNameByUserId($comments),
//            'upload' => new UploadForm(),
        ]);
    }

    public function getNameByUserId($comments)
    {
        foreach ($comments as $comment) {
            if ($comment->userId != 1) {
                $model = Yii::$app->DLL->getModelByUserId($comment->userId);
                $nl = $model::find()->where(['userId' => $comment->userId])->one();
                $comment->userName = $nl->name . ' ' . $nl->lastname;
                $comment->realId = $nl->id;
            } else {
                $comment->userName = 'Администратор DobroUA.com';
            }
        }
        return $comments;
    }
}