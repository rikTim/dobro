<?php

namespace app\components;


use app\models\shopComment;
use app\models\TaskComment;
use Yii;
use yii\base\Widget;



class ShopCommentWidget extends Widget
{
    public $shopId;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new shopComment();
        $comments = $model::find()->where(['shopId' => $this->shopId, 'statusId' => 2])->orderBy(['dateTime' => SORT_DESC])->all();

        return $this->render('shopcomment', [
            'taskId' => $this->shopId,
            'comment' => $model,
            'comments' => $this->getNameByUserId($comments),
//            'upload' => new UploadForm(),
        ]);
    }

    public function getNameByUserId($comments)
    {
        foreach ($comments as $comment) {
            if ($comment->userId != 1) {
                $model = Yii::$app->DLL->getModelByUserId($comment->userId);
                $nl = $model::find()->where(['userId' => $comment->userId])->one();
                $comment->userName = $nl->name . ' ' . $nl->lastname;
                $comment->realId = $nl->id;
            } else {
                $comment->userName = 'Администратор DobroUA.com';
            }
        }
        return $comments;
    }
}