<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_113154_add_time_field extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'time', 'VARCHAR(120) AFTER `email`');
        $this->addColumn('needies', 'time', 'VARCHAR(120) AFTER `email`');
        $this->addColumn('volunteers', 'time', 'VARCHAR(120) AFTER `email`');
    }

    public function down()
    {
        $this->dropColumn('angels', 'time');
        $this->dropColumn('needies', 'time');
        $this->dropColumn('volunteers', 'time');
    }
}
