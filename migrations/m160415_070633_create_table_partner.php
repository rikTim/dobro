<?php

use yii\db\Migration;

class m160415_070633_create_table_partner extends Migration
{
    public function up()
    {
        $this->createTable('partner', [
            'id' => $this->primaryKey(),
            'name' => $this->string(120)->notNull(),
            'phone' => $this->integer(15)->notNull(),
            'url' => $this->string(255)->notNull(),
            'avatarId' => $this->integer(11)->notNull(),
            'text' => $this->text()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('partner');
    }
}
