<?php

use yii\db\Migration;

class m160321_091722_add_image_col_table_board extends Migration
{
    public function up()
    {
        $this->addColumn('board', 'image', 'VARCHAR(250) AFTER `contacts`');

    }

    public function down()
    {
        $this->dropColumn('board', 'image');

    }
}
