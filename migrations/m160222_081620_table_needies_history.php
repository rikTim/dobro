<?php

use yii\db\Schema;
use yii\db\Migration;

class m160222_081620_table_needies_history extends Migration
{
    public function up()
    {
        $this->createTable('neediesHistory', [
           'id' => $this->primaryKey(),
            'userId' => $this->integer(11)->notNull(),
            'name' => $this->string(120)->notNull(),
            'age' => $this->integer(2)->notNull(),
            'city' => $this->string(120)->notNull(),
            'phone' => $this->string(120)->notNull(),
            'vk' => $this->string(120)->notNull(),
            'fb' => $this->string(120)->notNull(),
            'shortTrouble' => $this->string(120)->notNull(),
            'trouble' => $this->text(),
            'specificNeed' => $this->text(),
            'cardNum' => $this->string(120),
            'avatarId' => $this->integer(11),
        ]);

    }

    public function down()
    {
        $this->dropTable('neediesHistory');
    }
}
