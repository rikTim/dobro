<?php

use yii\db\Schema;
use yii\db\Migration;

class m160219_111433_add_specific_need extends Migration
{
    public function up()
    {
        $this->addColumn('needies', 'specificNeed', 'TEXT AFTER `trouble`');
    }

    public function down()
    {
        $this->dropColumn('needies', 'specificNeed');
    }

}
