<?php

use yii\db\Migration;

class m160401_135407_add_need_got_col_needies extends Migration
{
    public function up()
    {
        $this->addColumn('needies', 'need', 'INT(11)');
        $this->addColumn('needies', 'got', 'INT(11)');
    }

    public function down()
    {
        $this->dropColumn('needies', 'need');
        $this->dropColumn('needies', 'got');
    }
}
