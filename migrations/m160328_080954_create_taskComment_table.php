<?php

use yii\db\Migration;

class m160328_080954_create_taskComment_table extends Migration
{
    public function up()
    {
        $this->createTable('taskComment', [
            'id' => $this->primaryKey(),
            'taskId' => $this->integer(11)->notNull(),
            'userId' => $this->integer(11)->notNull(),
            'text' => $this->string(255)->notNull(),
            'dateTime' => $this->string(255)->notNull(),
            'statusId' => $this->integer(11)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('taskComment');
    }
}
