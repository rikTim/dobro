<?php

use yii\db\Migration;

class m160304_091257_add_needy_help_table extends Migration
{
    public function up()
    {
        $this->createTable('needyHelp', [
            'id' => $this->primaryKey(),
            'needyId' => $this->integer(11)->notNull(),
            'boardId' => $this->integer(11)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('board');
    }

}
