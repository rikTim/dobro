<?php

use yii\db\Schema;
use yii\db\Migration;

class m160222_115554_add_time_col_neediesHistory extends Migration
{
    public function up()
    {
        $this->addColumn('neediesHistory', 'time', 'VARCHAR(120) AFTER `phone`');
    }

    public function down()
    {
        $this->dropColumn('neediesHistory', 'time');
    }
}
