<?php

use yii\db\Schema;
use yii\db\Migration;

class m160222_083548_add_diff_col_neediesHistory extends Migration
{
    public function up()
    {
        $this->addColumn('neediesHistory', 'diff', 'TEXT');
    }

    public function down()
    {
        $this->dropColumn('neediesHistory', 'diff');
    }
}
