<?php

use yii\db\Migration;

class m160307_153957_add_countries extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'mainCountryId', 'VARCHAR(120) AFTER `cityId`');

        $this->addColumn('volunteers', 'mainCountryId', 'VARCHAR(120) AFTER `cityId`');

    }

    public function down()
    {
        $this->dropColumn('angels', 'mainCountryId');
        $this->dropColumn('volunteers', 'mainCountryId');
    }

}
