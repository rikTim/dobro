<?php

use yii\db\Schema;
use yii\db\Migration;

class m160222_114342_add_lastname_fiels_neediesHistory extends Migration
{
    public function up()
    {
        $this->addColumn('neediesHistory', 'lastname', 'VARCHAR(120) AFTER `name`');
    }

    public function down()
    {
        $this->dropColumn('neediesHistory', 'lastname');
    }
}
