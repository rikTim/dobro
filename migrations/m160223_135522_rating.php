<?php

use yii\db\Schema;
use yii\db\Migration;

class m160223_135522_rating extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'rating', 'INT(11) AFTER `email`');
        $this->addColumn('volunteers', 'rating', 'INT(11) AFTER `email`');
    }

    public function down()
    {
        $this->dropColumn('angels', 'rating');
        $this->dropColumn('volunteers', 'rating');
    }
}
