<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_094542_add_show_email_files extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'showEmail', 'VARCHAR(1) DEFAULT "0" NOT NULL AFTER `showPhone`');
        $this->addColumn('volunteers', 'showEmail', 'VARCHAR(1) DEFAULT "0" NOT NULL AFTER `showPhone`');
    }

    public function down()
    {
        $this->dropColumn('angels', 'showEmail');
        $this->dropColumn('volunteers', 'showEmail');
    }
}