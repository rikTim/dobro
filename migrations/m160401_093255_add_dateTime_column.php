<?php

use yii\db\Migration;

class m160401_093255_add_dateTime_column extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'createTime', 'VARCHAR(255) AFTER `statusId`');
        $this->addColumn('volunteers', 'createTime', 'VARCHAR(255) AFTER `statusId`');
    }

    public function down()
    {
        $this->dropColumn('angels', 'createTime');
        $this->dropColumn('volunteers', 'createTime');
    }


}
