<?php

use yii\db\Migration;

class m160302_092746_add_board_table extends Migration
{
    public function up()
    {
        $this->createTable('board', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer(11)->notNull(),
            'countryId' => $this->integer(11)->notNull(),
            'cityId' => $this->integer(11)->notNull(),
            'helpId' => $this->integer(11)->notNull(),
            'text' => $this->text()->notNull(),
            'contacts' => $this->string(250)->notNull(),
            'statusId' => $this->integer(11)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('board');
    }
}
