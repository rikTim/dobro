<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_144217_add_comment_text_field_userHelp extends Migration
{
    public function up()
    {
        $this->addColumn('userHelp', 'comment', 'TEXT AFTER `text`');
        $this->addColumn('userHelp', 'filePath', 'VARCHAR(120) AFTER `comment`');
    }

    public function down()
    {
        $this->dropColumn('userHelp', 'comment');
        $this->dropColumn('userHelp', 'filePath');
    }
}
