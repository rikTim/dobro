<?php

use yii\db\Migration;

class m160312_091152_add_region_col extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'regionId', 'INT(11) AFTER `cityId`');
        $this->addColumn('volunteers', 'regionId', 'INT(11) AFTER `cityId`');
        $this->addColumn('needies', 'regionId', 'INT(11) AFTER `cityId`');
        $this->addColumn('cities', 'regionId', 'INT(11) AFTER `countryId`');

    }

    public function down()
    {
        $this->dropColumn('angels', 'regionId');
        $this->dropColumn('volunteers', 'regionId');
        $this->dropColumn('needies', 'regionId');
        $this->dropColumn('cities', 'regionId');

    }


}
