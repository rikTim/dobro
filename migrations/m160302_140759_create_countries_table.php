<?php

use yii\db\Schema;
use yii\db\Migration;

class m160302_140759_create_countries_table extends Migration
{
    public function up()
    {
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'mainId' => $this->integer(11)->notNull(),
            'title' => $this->string(120),
            'order' => $this->integer(1)
        ]);

//        $this->insert('countries', [
//           'mainId' => '1',
//           'title' => 'Россия',
//           'order' => 2,
//        ]);
//
//        $this->insert('countries', [
//            'mainId' => '2',
//            'title' => 'Украина',
//            'order' => 1,
//        ]);
//
//        $this->insert('countries', [
//            'mainId' => '3',
//            'title' => 'Беларусь',
//            'order' => 3,
//        ]);
//
//        $this->insert('countries', [
//            'mainId' => '4',
//            'title' => 'Казахстан',
//            'order' => 6,
//        ]);
//
//        $this->insert('countries', [
//            'mainId' => '11',
//            'title' => 'Кыргызстан',
//            'order' => 7,
//        ]);
//
//        $this->insert('countries', [
//            'mainId' => '15',
//            'title' => 'Молдова',
//            'order' => 5,
//        ]);
//
//        $this->insert('countries', [
//            'mainId' => '39',
//            'title' => 'Болгария',
//            'order' => 4,
//        ]);


        $this->addColumn('needies', 'mainCountryId', 'INTEGER(1) AFTER `cityId`');
    }

    public function down()
    {
        $this->dropTable('countries');
        $this->dropColumn('needies', 'mainCountryId');
    }
}
