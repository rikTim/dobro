<?php

use yii\db\Schema;
use yii\db\Migration;

class m160331_091600_rename_volunteer_task_col extends Migration
{
    public function up()
    {
        $this->renameTable('volunteerTask', 'userTask');
        $this->renameColumn('userTask', 'volunteerId', 'userId');
    }

    public function down()
    {
        $this->renameTable('userTask', 'volunteerTask');
        $this->renameColumn('volunteerId', 'userTask', 'userId');
    }
}
