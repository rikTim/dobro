<?php

use yii\db\Migration;

class m160316_145029_add_board_regionId extends Migration
{
    public function up()
    {
        $this->addColumn('board', 'regionId', 'INT(11) AFTER `cityId`');
    }

    public function down()
    {
        $this->dropColumn('board', 'regionId');
    }


}
