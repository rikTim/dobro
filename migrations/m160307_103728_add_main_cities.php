<?php

use yii\db\Schema;
use yii\db\Migration;

class m160307_103728_add_main_cities extends Migration
{
    public function up()
    {
        $this->addColumn('cities', 'main', 'TINYINT(1) DEFAULT 0 NOT NULL AFTER `title`');
    }

    public function down()
    {
        $this->dropColumn('cities', 'main');
    }
}
