<?php

use yii\db\Schema;
use yii\db\Migration;

class m160222_112213_add_visit_time extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'visitTime', 'VARCHAR(120) AFTER `flags`');
    }

    public function down()
    {
        $this->dropColumn('user', 'visitTime');
    }
}
