<?php

use yii\db\Schema;
use yii\db\Migration;

class m160301_122506_create_needType_table extends Migration
{
    public function up()
    {
        $this->createTable('needType', [
            'id' => $this->primaryKey(),
            'title' => $this->string(120)->notNull(),
            'descr' => $this->string(120)
        ]);

        $this->insert('needType', [
            'title' => 'Финансовая поддержка'
        ]);

        $this->insert('needType', [
            'title' => 'Вещи (одежда, обувь)'
        ]);

        $this->insert('needType', [
            'title' => 'Продукты питания'
        ]);

        $this->insert('needType', [
            'title' => 'Медицинские услуги'
        ]);

        $this->insert('needType', [
            'title' => 'Медикаменты'
        ]);

        $this->insert('needType', [
            'title' => 'Психологическая помощь'
        ]);

        $this->insert('needType', [
            'title' => 'Помощь в трудоустройстве'
        ]);

        $this->insert('needType', [
            'title' => 'Помощь волонтеров'
        ]);

        $this->insert('needType', [
            'title' => 'Другое'
        ]);

        $this->addColumn('needies', 'needyTypeId', 'INTEGER(11) AFTER `cardNum`');
    }

    public function down()
    {
        $this->dropTable('needType');
        $this->dropColumn('needies', 'needyTypeId');
    }
}
