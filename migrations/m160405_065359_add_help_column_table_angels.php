<?php

use yii\db\Migration;

class m160405_065359_add_help_column_table_angels extends Migration
{
    public function up()
    {
        $this->addColumn('angels', 'help', 'VARCHAR(255)');
    }

    public function down()
    {
        $this->dropColumn('angels', 'help');
    }

}
