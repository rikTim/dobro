<?php

use yii\db\Migration;

class m160312_090903_add_table_regions extends Migration
{
    public function up()
    {

        $this->createTable('regions', [
            'id' => $this->primaryKey(),
            'mainId' => $this->integer(11)->notNull(),
            'countryId'=>$this->integer(11)->notNull(),
            'title' => $this->string(120),
            'main' => $this->integer(1)
        ]);
    }

    public function down()
    {
        $this->dropTable('regions');
    }
}
