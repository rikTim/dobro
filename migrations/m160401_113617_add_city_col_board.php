<?php

use yii\db\Schema;
use yii\db\Migration;

class m160401_113617_add_city_col_board extends Migration
{
    public function up()
    {
        $this->addColumn('board', 'city', 'VARCHAR(120) AFTER `cityId`');
    }

    public function down()
    {
        $this->dropColumn('board', 'city');
    }
}
