<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_135958_create_attachment_table extends Migration
{
    public function up()
    {
        $this->createTable('attachment', [
            'id' => $this->primaryKey(),
            'commentId' => $this->integer(11)->notNull(),
            'path' => $this->string(120)->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('attachment');
    }
}
