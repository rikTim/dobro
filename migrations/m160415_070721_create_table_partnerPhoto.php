<?php

use yii\db\Migration;

class m160415_070721_create_table_partnerPhoto extends Migration
{
    public function up()
    {
        $this->createTable('partnerPhoto', [
            'id' => $this->primaryKey(),
            'partnerId'=> $this->integer(11)->notNull(),
            'path' => $this->string(255)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('partnerPhoto');
    }
}
