<?php

use yii\db\Schema;
use yii\db\Migration;

class m160219_081730_add_attachment_type extends Migration
{
    public function up()
    {
        $this->addColumn('attachment', 'type', 'TINYINT(1)');
    }

    public function down()
    {
        $this->dropColumn('attachment', 'type');
    }
}
