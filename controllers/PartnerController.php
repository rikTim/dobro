<?php

namespace app\controllers;

use app\models\PartnerPhoto;
use app\models\UploadForm;
use Yii;
use app\models\Partner;
use app\models\PartnerSeach;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PartnerController implements the CRUD actions for Partner model.
 */
class PartnerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($action->id != 'partner' && $action->id != 'more') {
                if (!Yii::$app->user->can('adminIndex')) {
                    Yii::$app->session->setFlash('error', 'Доступ запрещен');
                    return $this->redirect(['site/index']);
                } else {
                    $this->layout = 'admin';
                }
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * Lists all Partner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnerSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => PartnerPhoto::find()->where(['partnerId'=>$model->id]),
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

         if(!empty($model->avatarId)) {
             $model->avatar->path = Yii::getAlias('@web') . '/images/partner/' . $model->id . '/cropped/' . $model->avatar->path;
         }
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Partner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partner();
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) ) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $model->save(false);
             $upload->partnerAvatar($model->id);
            $files = Yii::$app->request->post('UploadForm');
            $upload->file = UploadedFile::getInstances($upload, 'file');

            if (!empty($upload->file)) {

                $upload->partnerPhotos($model->id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
            ]);
        }
    }

    /**
     * Updates an existing Partner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        $dataProvider = new ActiveDataProvider([
            'query' => PartnerPhoto::find()->where(['partnerId'=>$model->id]),
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $model->save(false);
            $upload->partnerAvatar($model->id);
            $files = Yii::$app->request->post('UploadForm');
            $upload->file = UploadedFile::getInstances($upload, 'file');

            if (!empty($upload->file)) {

                $upload->partnerPhotos($model->id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing Partner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Partner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPartner()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Partner::find(),
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);
        return $this->render('partner', [
                'dataProvider' => $dataProvider
            ]);
    }

    public function actionMore($id)
    {
        $model = $this->findModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => PartnerPhoto::find()->where(['partnerId'=>$model->id]),
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        return $this->render('more',[
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelavatar()
    {
        $id = Yii::$app->request->post('avatarId');
        $partnerModel = new PartnerPhoto();
        $partner = Partner::find()->where(['avatarId'=>$id])->one();
        if(!empty($partner)){
            $partner->avatarId = '';
            $partner->save(false);
        }
        $avatar = $partnerModel->findOne($id);
        $avatar->delete();
    }
}
