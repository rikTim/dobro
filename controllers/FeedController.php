<?php

namespace app\controllers;

use app\models\Comments;
use app\models\Messages;
use app\models\Needies;
use app\models\Notifications;
use app\models\Tasks;
use app\models\Users;
use app\models\Volunteers;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\Url;

class FeedController extends Controller
{

    protected $userId;
    protected $volunteerId;

    public function __construct($id, $module, $config = [])
    {
        $this->userId = Yii::$app->user->id;
        $this->volunteerId = Volunteers::find()->where(['userId' => $this->userId])->one()->id;
        parent::__construct($id, $module, $config);
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    public function actionIndex()
    {
        $userId = Yii::$app->user->id;

        // убираем все уведомления
        Notifications::updateAll(['notification' => 0], 'userId =' . $userId);

        $tasks = Tasks::find()->where('(dateTime >=' . time() . ' OR dateTime = "" ) AND isActive = 1')->orderBy(['dateCreate' => SORT_DESC])->all();
        $messages = Messages::find()->where('fromUserId = ' . $userId . ' or toUserId = ' . $userId)->orderBy(['dateTime' => SORT_DESC])->all();
        $needies = Needies::find()->where(['statusId' => 2])->orderBy(['registerDate' => SORT_DESC])->all();
        $comments = Comments::find()->where(['toUserId' => $userId, 'statusId' => 2])->orderby(['dateTime' => SORT_DESC])->all();


        // types = { 1: новость, 2: задание, 3: сообщение, 4: новый нуждающийся, 5: сообщение на стене }

        $feeds = [];
        if (Yii::$app->DLL->getRoleByUserId($userId) != 'needy') {
            $feeds = $this->feedCreate($tasks, $feeds, 2);
        }

        $feeds = $this->feedCreate($messages, $feeds, 3);
        if (Yii::$app->DLL->getRoleByUserId($userId) != 'needy') {
            $feeds = $this->feedCreate($needies, $feeds, 4);
        }
        $feeds = $this->feedCreate($comments, $feeds, 5);

        ArrayHelper::multisort($feeds, 'dateTime', SORT_DESC);

        $pages = new Pagination(['totalCount' => sizeof($feeds), 'defaultPageSize' => 10]);
        $feeds = array_splice($feeds, $pages->offset, $pages->limit);
        return $this->render('index', [
            'feeds' => $feeds,
            'pages' => $pages
        ]);
    }

    protected function feedCreate($items, $feeds, $type)
    {
        $i = sizeof($feeds);
        foreach ($items as $item) {
            switch ($type) {
                case 1:
                    $feeds[$i]['id'] = $item->id;
                    $feeds[$i]['title'] = $item->title;
                    $feeds[$i]['text'] = $item->text;
                    $feeds[$i]['dateTime'] = $item->dateTime;
                    break;
                case 2:
                    $feeds[$i]['id'] = $item->id;
                    $feeds[$i]['title'] = $item->title;
                    $feeds[$i]['img'] = $item->filePath;
                    $feeds[$i]['text'] = $item->descr;
                    $feeds[$i]['dateTime'] = $item->dateCreate;
                    $feeds[$i]['count'] = count($item->userTask);
                    $feeds[$i]['volTask'] = !empty($this->userId) ? in_array($this->userId, ArrayHelper::map($item->userTask, 'id', 'userId')) : null;
                    break;
                case 3:
                    $feeds[$i]['id'] = $item->id;
                    $feeds[$i]['title'] = $item->fromUserId == $this->userId ? 'администратору' : 'от администратора';
                    $feeds[$i]['text'] = $item->text;
                    $feeds[$i]['dateTime'] = $item->dateTime;
                    break;
                case 4:
                    $feeds[$i]['id'] = $item->id;
                    $feeds[$i]['userId'] = $item->userId;
                    $feeds[$i]['img'] = $item->file->path;
                    $feeds[$i]['title'] = $item->name . ' ' . $item->lastname . ' ' . $item->age . ' ' . Yii::$app->DLL->getWordEnd('', 'лет', 'год', 'года', 'лет', $item->age) . ' ' . $item->city->title;
                    $feeds[$i]['text'] = $item->shortTrouble;
                    $feeds[$i]['dateTime'] = $item->registerDate;
                    break;
                case 5:
                    $userRole = Yii::$app->DLL->getRoleByUserId($item->fromUserId);
                    switch($userRole) {
                        case 'volunteer': $link = 'volunteers'; $id = $item->fromVolunteerUser->id; $from = $item->fromVolunteerUser->name . ' ' . $item->fromVolunteerUser->lastname; break;
                        case 'angel': $link = 'angels'; $id = $item->fromAngelUser->id; $from = $item->fromAngelUser->name . ' ' . $item->fromAngelUser->lastname; break;
                        case 'needy': $link = 'needies'; $id = $item->fromNeedyUser->id; $from = $item->fromNeedyUser->name . ' ' . $item->fromNeedyUser->lastname; break;
                    }

                    $feeds[$i]['id'] = $item->id;
                    $feeds[$i]['title'] = 'Сообщение на стене от <a href="'. Url::toRoute(['/' . $link . '/more', 'id' => $id]) . '">' . $from . '</a>';
                    $feeds[$i]['text'] = $item->text;
                    $feeds[$i]['dateTime'] = $item->dateTime;
            }


            $feeds[$i]['type'] = $type;
            $i++;
        }
        return $feeds;
    }
}
