<?php

namespace app\controllers;

use app\components\DLL;
use app\models\Angels;
use app\models\Attachment;
use app\models\Messages;
use app\models\Needies;
use app\models\Ratings;
use app\models\Statuses;
use app\models\Users;
use app\models\Volunteers;
use Yii;
use app\models\Comments;
use app\models\CommentsSeach;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\filters\VerbFilter;

/**
 * CommentsController implements the CRUD actions for Comments model.
 */
class CommentsController extends Controller
{
    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            if (!Yii::$app->user->can('adminIndex')) {
                Yii::$app->session->setFlash('error', 'Доступ запрещен');
                return $this->redirect(['site/index']);
            } else {
                $this->layout = 'admin';
                return true;
            }
        }
        return false;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Comments models.
     * @return mixed
     */
    public function actionIndex($type = '')
    {

        switch($type){
            case 'angel': $title = 'к Ангелам'; break;
            case 'volunteer': $title = 'к Волонтерам'; break;
            case 'needy': $title = 'к Кому помочь'; break;
        }

        $searchModel = new CommentsSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $type);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title'=>$title,
            'type'=>$type,
        ]);
    }

    /**
     * Displays a single Comments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $toUserModel = Yii::$app->DLL->getModelByUserId($model->toUserId);

        $type = Yii::$app->DLL->getRoleByUserId($model->toUserId);

        $toUserUrl = $this->createUrl($model->toUserId);

        if ($model->fromUserId == 1) {
            $fromUser = '';
            $fromUserUrl = '';
        } else {
            $fromUserUrl = $this->createUrl($model->fromUserId);
            $fromUserModel = Yii::$app->DLL->getModelByUserId($model->fromUserId);
            $fromUser = $fromUserModel->find()->where(['userId' => $model->fromUserId])->one();
        }

        $toUser = $toUserModel->find()->where(['userId'=>$model->toUserId])->one();



        return $this->render('view', [
            'model' => $model,
            'toUser'=>$toUser,
            'fromUser'=>$fromUser,
            'type'=>$type,
            'toUrl'=>$toUserUrl,
            'fromUrl'=>$fromUserUrl,

        ]);
    }

    protected function createUrl($userId)
    {
        $type = Yii::$app->DLL->getRoleByUserId($userId);

        switch($type){
            case 'angel': $url = 'angels';break;
            case 'volunteer': $url = 'volunteers';break;
            case 'needy' : $url = 'needies'; break;
        }
        return $url;
    }
    /**
     * Creates a new Comments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type)
    {
        $model = new Comments();
        $statusModel = new Statuses();
        $status = $statusModel->getAllStatuses();

        switch($type){
            case 'angel': $userModel = new Angels();break;
            case 'volunteer': $userModel = new Volunteers();break;
            case 'needy' : $userModel = new Needies(); break;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->dateTime = strtotime($model->dateTime);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'status' => $status,
            ]);
        }
    }

    /**
     * Updates an existing Comments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $toUserModel = Yii::$app->DLL->getModelByUserId($model->toUserId);

        $toUser = $toUserModel->find()->where(['userId'=>$model->toUserId])->one();
        if ($model->fromUserId == 1) {
            $fromUser = '';
        } else {
            $fromUserModel = Yii::$app->DLL->getModelByUserId($model->fromUserId);
            $fromUser = $fromUserModel->find()->where(['userId' => $model->fromUserId])->one();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->dateTime = strtotime($model->dateTime);

            $model->save(false);

            $role = Yii::$app->DLL->getRoleByUserId($model->toUserId);
            if(!$role== 'needy') {
                /** Пересчитываем рейтинг */
                $this->recountRating($model);
            }


            /** Если подтверждаем комментарий */
            if ($model->statusId == 2) {
                /** Отправляем email о новом сообщении на стене */
                Yii::$app->DLL->newWallCommentEmail($model);

                /** Добавляем уведомление о новом сообщении на стене */
                Yii::$app->DLL->addNotification($model->toUserId);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'toUser'=>$toUser,
                'fromUser'=>$fromUser,
            ]);
        }
    }

    /**
     * Deletes an existing Comments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        $type = Yii::$app->DLL->getRoleByUserId($toUserId);
        $model = new Comments();
        $model = $model->findOne(['id'=>$id]);
        $type = Yii::$app->DLL->getRoleByUserId($model->toUserId);
        $model->delete();


        return $this->redirect(['index','type'=>$type]);
    }

    /**
     * Finds the Comments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionApproved($id)
    {
        $model = new Comments();
        $model = $model->findOne(['id'=>$id]);
        $model->statusId = 2;
        $type = Yii::$app->DLL->getRoleByUserId($model->toUserId);

        $model->save(false);


        if ($type != 'needy') {
            /** Пересчитываем рейтинг */
            $this->recountRating($model);
        }

        /** Отправляем email о новом сообщении на стене */
        Yii::$app->DLL->newWallCommentEmail($model);

        /** Добавляем уведомление в меню о новом сообщении на стене */
        Yii::$app->DLL->addNotification($model->toUserId);


        return $this->redirect(['index', 'type'=>$type]);
    }

    public function actionRejected($id)
    {


        $model = new Comments();
        $model = $model->findOne(['id'=>$id]);
        $model->statusId = 3;
        $model->save(false);
        $type = Yii::$app->DLL->getRoleByUserId($model->toUserId);
        return $this->redirect(['index','type'=>$type]);
    }


    /**
     * Увеличение рейтинга от сообщений на стене
     * @param $model
     */
    protected function recountRating($model)
    {
        $messageModel = new Messages();
        $fromUserModel = Yii::$app->DLL->getModelByUserId($model->fromUserId);
        $fromUser = $fromUserModel->find()->where(['userId'=>$model->fromUserId])->one();

        /** Увеличиваем рейтинг */
        if ($model->statusId == 2 && $model->toUserId != $model->fromUserId) {
            $role = strtolower(explode('\\', $fromUserModel::className())[2]);
            $descr = 'Комментарий от <a href="' . Url::toRoute([$role . '/more', 'id' => $fromUser->id]) . '">' . $fromUser->name . ' ' . $fromUser->lastname . '</a>';

            $rating = new Ratings();
            $rating->setAttributes([
                'userId' => $model->toUserId,
                'rating' => 1,
                'descr' => $descr,
                'dateTime' => time()
            ]);
            $rating->save();

            $userModel =  Yii::$app->DLL->getModelByUserId($model->toUserId);
            $user = $userModel->find()->where(['userId'=>$model->toUserId])->one();


            if(!empty($user->rating)) {
                $user->rating += 1;
            }else{
                $user->rating = 1;
            }

            $user->save(false);


            /** Отправляем сообщение от админа на сайте, что добавлен рейтинг */
            $role = Yii::$app->DLL->getRoleByUserId($model->toUserId);
            switch($role){
                case 'needy'; $type = 4; break;
                case 'volunteer'; $type = 3; break;
                case 'angel'; $type = 2; break;
            }
            $text = "Вам добавленно 1 рейтинга. За комментарий";
            $messageModel->writeMessage(1, 1, $model->toUserId, $type, $text);
        }
    }


    public function actionDelavatar()
    {
        $attachmentModel = new Attachment();
        $attachmentId = Yii::$app->request->post('avatarId');
        $delete = $attachmentModel->findOne(['id' => $attachmentId]);
        $delete->delete();
    }
}
