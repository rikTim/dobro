<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use app\models\Subscribe;
use app\models\SubscribeSeach;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SubscribeController implements the CRUD actions for Subscribe model.
 */
class SubscribeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }

            if (!Yii::$app->user->can('adminIndex')) {
                Yii::$app->session->setFlash('error', 'Доступ запрещен');
                return $this->redirect(['/site/index']);
            } else {
                $this->layout = 'admin';
                return true;
            }
        }
        return true;
    }

    /**
     * Lists all Subscribe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Subscribe();
        $model->readMessage();
        $searchModel = new SubscribeSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subscribe model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


//    public function actionSendMessage()
//    {
//        $model = new Subscribe();
//        $users = $model->getEmail();
//        $i = 0;
//        foreach ($users as $user) {
//
//            $email = $user->email;
//            $message = 'Привет! Вот и настал момент, когда ты - часть большого и светлого проекта. Информационный портал для добрых дел dobroua.com открыт и ты можешь
//            быть ангелом или волонтером, попросить о помощи или помочь нам с организацией работы. Ангел - это человек, который имеет желание и возможность помочь людям.
//            Волонтер - человек, который проверяет достоверность информации в просьбе о помощи, чтобы оградить ангела от мошенничества. Мы стремимся к тому, чтобы Мир добрых
//            людей становился шире, поэтому, нам всегда нужны люди, которые помогут в организации работы портала. Неважно какие у тебя способности и профессия - дела найдутся
//             для всех. В нашей стране ужасающая статистика инвалидности, онко-заболеваний, детские приюты и дома престарелых переполнены людьми, которые нуждаются в тебе.
//              Вместе мы реализуем свое желание быть полезными миру без страха остаться обманутым! А еще, dobroua.com - место, где ценят помощь и мы обязательно поблагодарим
//              тебя за все, что ты сделаешь! Спасибо, что ты с нами!';
//
//            Yii::$app->mailer->compose()
//                ->setFrom('admin@dobroua.com')
//                ->setTo($email)
//                ->setSubject('Мир добрых людей')
//                ->setHtmlBody($message)
//                ->send();
//
//
////            $email = $user->email;
////            echo '<pre>';
////            print_r($email);
////            $i++;
//        }
////        print_r($i);
////        die();
//    }

    /**
     * Creates a new Subscribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscribe();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Subscribe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Subscribe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Subscribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscribe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
