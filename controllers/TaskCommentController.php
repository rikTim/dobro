<?php

namespace app\controllers;

use app\models\Statuses;
use Yii;
use app\models\TaskComment;
use app\models\TaskCommentSeach;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskCommentController implements the CRUD actions for TaskComment model.
 */
class TaskcommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }


    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($action->id != 'add-task-comment') {
                if (!Yii::$app->user->can('adminIndex')) {
                    Yii::$app->session->setFlash('error', 'Доступ запрещен');
                    return $this->redirect(['site/index']);
                } else {
                    $this->layout = 'admin';
                }
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * Lists all TaskComment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskCommentSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaskComment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model= $this->findModel($id);
        $model->taskId ='<a href=' . \yii\helpers\Url::toRoute(["tasks/view", "id"=>$model->taskId]) . '>' . $model->task->title . '</a>';
        $model->userId = $model->getFullName($model->userId);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new TaskComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new TaskComment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'status' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
                'taskId' => $id,
            ]);
        }
    }

    /**
     * Updates an existing TaskComment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'status' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
            ]);
        }
    }

    /**
     * Deletes an existing TaskComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaskComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaskComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddTaskComment()
    {
        $model = new TaskComment();

        $comment = Yii::$app->request->post('TaskComment');

        if (!empty($comment)) {

            $model->attributes = $comment;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('commentMessage', 'Спасибо за Ваше сообщение. Сразу после проверки администратор опубликует его на сайте.');
            }

//            $files = Yii::$app->request->post('UploadForm');
//            if (!empty($files->file)) {
//                $upload = new UploadForm();
//                $upload->file = UploadedFile::getInstances($upload, 'file');
//                $upload->wallFileUpload($model->id);
//            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
