<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\Cities;
use app\models\Comments;
use app\models\Countries;
use app\models\Files;
use app\models\NeediesHistory;
use app\models\Statuses;
use app\models\UserHelp;
use app\models\Users;
use Yii;
use app\models\Needies;
use app\models\NeediesSeach;
use yii\data\ActiveDataProvider;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\web\UploadedFile;
use app\models\UploadForm;
use yii\helpers\BaseFileHelper;

/**
 * NeediesController implements the CRUD actions for Needies model.
 */
class NeediesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($action->id != 'list' && $action->id != 'more') {
                if (!Yii::$app->user->can('adminIndex')) {
                    Yii::$app->session->setFlash('error', 'Доступ запрещен');
                    return $this->redirect('/site/index', []);
                } else {
                    $this->layout = 'admin';
                }
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * Lists all Needies models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NeediesSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Needies model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $filesModel = new Files();
        $model = $this->findModel($id);
        $historyModel = NeediesHistory::find()->where(['userId' => $model->userId])->one();
        $diff = $historyModel->diff;

        return $this->render('view', [
            'model' => $model,
            'modelHistory' => $historyModel,
            'diff' => json_decode($diff),
            'images' => $filesModel->getFileType($model->userId, 3),
            'videos' => $filesModel->getFileType($model->userId, 2),
        ]);
    }

    /**
     * Creates a new Needies model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Needies();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'cities' => ArrayHelper::map(Cities::find()->asArray()->all(), 'id', 'title'),
                'statuses' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title')
            ]);
        }
    }

    /**
     * Updates an existing Needies model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $filesModel = new Files();
        $model = $this->findModel($id);
        $oldStatus = $model->statusId;
        $upload = new UploadForm();
        $upload->scenario = 'avatarUpload';
        $images = $filesModel->getFileType($model->userId, 3);
        $videos = $filesModel->getFileType($model->userId, 2);
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if (!empty($upload->imageFile)) {
                $model->avatarId = $upload->upload($model->userId);
            }
            $model->fb = Yii::$app->DLL->getLinkId($model->fb);
            $model->vk = Yii::$app->DLL->getLinkId($model->vk);
            $model->updateDate = strtotime($model->updateDate);
            $model->registerDate = strtotime($model->registerDate);

            /** отправка уведомоений на почту.*/
            if ($model->save(false) && $model->statusId == 2  && $oldStatus == 1 ) {
                Yii::$app->DLL->newNeedyNotification($model->id,$model->name,$model->lastname);
            }

            return $this->redirect(['view',
                'id' => $model->id,
                'images' => $images,
                'videos' => $videos,]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
                'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                'cities' => ArrayHelper::map(Cities::find()->where(['countryId' => 2])->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
                'statuses' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
                'images' => $images,
                'videos' => $videos,

            ]);
        }
    }


    public function actionUpload($id,$user)
    {
        $upload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $upload->file = UploadedFile::getInstances($upload, 'file');
            $success = $upload->filesUpload($id);

            if($success == 1) {
                return $this->redirect(['descriptions',
                    'id' => $id,
                    'user'=>$user,
                ]);
            } else {
                return $this->render('upload', [
                    'userId' => $id,
                    'upload'=> $upload,
                    'error'=>$success['file'][0],
                    'user'=>$user
                ]);
            }
        }
        else return $this->render('upload', [
            'userId' => $id,
            'upload'=> $upload,
            'user'=>$user
        ]);
    }

    public function actionVideo($id)
    {
        $filesModel = new Files();
        $video = $filesModel->findOne(['userId'=>$id,'fileType'=>2]);
        $videoArr = Yii::$app->request->post('Files');
        if(!empty($videoArr)){
                $filesModel->userId = $id;
                $filesModel->path = $videoArr['path'];
                $filesModel->descr = $videoArr['descr'];
                $filesModel->fileType = 2;
                $filesModel->save();
            return $this->redirect(['index']);
            }
        return $this->render('video',[
            'model'=>$filesModel,
            'id'=>$id,
            'video'=>$video,

        ]);
    }

    public function actionDescriptions($id,$user)
    {

        $filesModel = new Files();
        $files = $filesModel->getAllFile($id);
        $filesArr = $_POST['Files'];
        if (!empty($filesArr)) {
            $filesId = $filesArr['id'];
            $filesDescr = $filesArr['descr'];
            $filesCount = count($filesId);
            for ($i = 0; $i < $filesCount; $i++) {
                if(!empty($filesDescr[$i])){
                    $file = $filesModel->find()->where(['id'=>$filesId[$i]])->one();
                    $file->descr = $filesDescr[$i];
                    $file->validate();
                    $file->update();
                }
            }
            return $this->redirect(['index']);
        }


        return $this->render('description', [
            'id' => $id,
            'files' => $files,
            'user'=>$user,
        ]);
    }

    /**
     * Deletes an existing Needies model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new Users();
        $userArr = $this->findModel($id);
        $user = $model->findOne(['id'=>$userArr->userId]);
        $user->delete();
        return $this->redirect(['index']);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $needies = new ActiveDataProvider([
            'query' =>  Needies::find()->where(['statusId' => 2])->orderBy(['id' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 9,
            ],
        ]);
        return $this->render('list', [
            'needies' => $needies,
        ]);
    }

    /**
     * @return string
     */
    public function actionMore($id)
    {
        $model = Needies::find()->where(['id' => $id])->one();
        if (!empty($model)) {

            $helps = new ActiveDataProvider([
                'query' => UserHelp::find()->where(['toUserId' => $model->userId, 'isVerify' => 1])->orderBy(['dateTime' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 4,
                ],
            ]);

            $victory =  Needies::find()->where(['statusId' => 6, 'id' => $id])->one();
            $memory =  Needies::find()->where(['statusId' => 7, 'id' => $id])->one();

            return $this->render('more', [
                'helps' => $helps,
                'model' => $model,
                'victory'=> $victory,
                'memory'=> $memory,
                'photos' => Files::find()->where(['userId' => $model->userId, 'fileType' => 3])->all(),
                'video' => Files::find()->where(['userId' => $model->userId, 'fileType' => 2])->one(),
                'comment' => new Comments()
            ]);
        } else {
            return $this->redirect(['list']);
        }
    }

    /**
     *
     */
    public function actionAddWallComment()
    {
        $model = new Comments();
        $comment = Yii::$app->request->post('Comments');
        if (!empty($comment)) {
            $model->attributes = $comment;

            if ($model->save(false)) {
                Yii::$app->session->setFlash('wallMessage', 'Спасибо за ваш отзыв. Ваш отзыв появится на сайте после подтверждения администратором');
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the Needies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Needies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Needies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelavatar()
    {
        $filesModel = new Files();
        $avatarId =Yii::$app->request->post('avatarId');
        $delete = $filesModel->findOne(['id' => $avatarId]);
        $delete->delete();

    }

    public function actionApproved($id)
    {
        $nediesModel = new Needies();
        $needies = $nediesModel->findOne(['id'=>$id]);
        $oldStatus = $needies->statusId;
        $needies->statusId = 2;

        if ($needies->save(false) && $oldStatus == 1) {
            Yii::$app->DLL->newNeedyNotification($needies->id,$needies->name,$needies->lastname);
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteVideo($id)
    {
        $video = Files::findOne(['id'=>$id,'fileType'=>2]);
        $userId = $video->userId;
        $video->delete();
        return $this->redirect(['video','id'=>$userId]);
    }

    public function actionRotateImg()
    {
        $id = Yii::$app->request->post('id');
        $side = Yii::$app->request->post('side');
        if ($id !== null || $side !== null) {
            $imagine = Image::getImagine();
            $file = Files::findOne($id);
            $path = realpath(Yii::getAlias('@app') . '/web/images/' . $file->userId . '/' . $file->path);
            $pathM = realpath(Yii::getAlias('@app') . '/web/images/' . $file->userId . '/m_' . $file->path);
            $image = $imagine->open($path);
            $imageM = $imagine->open($pathM);
            $angle = null;
            if ($side == 'left') {
                $angle = -90;
            } else {
                $angle = 90;
            }
            $image->rotate($angle)->save($path);
            $imageM->rotate($angle)->save($pathM);
        }
    }

    public function actionSetWatermark()
    {
        $id = Yii::$app->request->post('id');
        if ($id !== null) {
            $file = Files::findOne($id);
            $path = realpath(Yii::getAlias('@app') . '/web/images/' . $file->userId . '/' . $file->path);
            $pathM = realpath(Yii::getAlias('@app') . '/web/images/' . $file->userId . '/m_' . $file->path);
            \Yii::$app->DLL->watermark($path, 'images/dobro_wm.png');
            \Yii::$app->DLL->imgResize($path, 160, $pathM);
        }
    }

    public function actionComment($id)
    {
        $user = new Needies();
        $model = new Comments();
        $statusModel = new Statuses();
        $status = $statusModel->getAllStatuses();
        $needies = $user->find()->where(['id'=>$id])->one();

        if ($model->load(Yii::$app->request->post())) {

            $model->save();

            return $this->redirect(['index']);
        }
        return $this->render('comment',[
            'model'=>$model,
            'needies'=>$needies,
            'status'=>$status,
        ]);
    }

    public function actionCounter()
    {
        $searchModel = new NeediesSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('counter', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function  actionUpdateCounter($id)
    {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->save(false);

            return $this->redirect(['counter']);
        }
        return $this->render('update-counter', [
            'model'=>$model,
            ]);
    }

}
