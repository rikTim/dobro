<?php

namespace app\controllers;

use app\models\Cities;
use app\models\Files;
use app\models\UploadForm;
use app\models\Users;
use dektrium\user\models\User;
use Yii;
use app\models\News;
use app\models\NewsSeach;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($action->id != 'list' && $action->id != 'more' && $action->id != 'test') {
                if (!Yii::$app->user->can('adminIndex')) {
                    Yii::$app->session->setFlash('error', 'Доступ запрещен');
                    return $this->redirect(['/site/index']);
                } else {
                    $this->layout = 'admin';
                }
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) ) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $filePath = $upload->newsFile();
            $model->filePath = $filePath;
            $model->save(false);
            Yii::$app->DLL->newNews($model->id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload'=>$upload,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->dateTime = strtotime($model->dateTime);
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $filePath = $upload->newsFile();
            if(!empty($filePath)) {
                $model->filePath = $filePath;
            }
            $model->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload'=> $upload
            ]);
        }
    }


    /**
     * @return string
     */
    public function actionList()
    {

        $query = News::find();
        $news = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('list', [
            'news' => $news,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMore($id)
    {
        return $this->render('more', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelavatar()
    {
       $newsModel =new News();
        $newsId = Yii::$app->request->post('avatarId');
        $news = $newsModel->findOne($newsId);
        $news->filePath = '';
       $news->save(false);
    }


    public function actionTest()
    {
        return $this->render('test', [
            'model' => new User(),
            'cities' => ArrayHelper::map(Cities::find()->where(['regionId'=>1508808])->orderBy(['main' => SORT_DESC, 'title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
        ]);
    }
}
