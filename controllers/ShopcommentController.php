<?php

namespace app\controllers;

use app\models\Statuses;
use Yii;
use app\models\shopComment;
use app\models\ShopCommentSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShopCommentController implements the CRUD actions for ShopComment model.
 */
class ShopcommentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
                $this->layout = 'shop_admin';
            return true;
        }
        return false;
    }


    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'actions' => ['add-task-comment'],
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['moder'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopComment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopComment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ShopComment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'shopId' => $id,
                'status' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
            ]);
        }
    }

    /**
     * Updates an existing ShopComment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {

             $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'status' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
            ]);
        }
    }

    /**
     * Deletes an existing ShopComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionAddTaskComment()
    {
        $model = new ShopComment();

        $comment = Yii::$app->request->post('shopComment');

        if (!empty($comment)) {

            $model->attributes = $comment;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('commentMessage', 'Спасибо за Ваше сообщение. Сразу после проверки администратор опубликует его на сайте.');
            }


        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
