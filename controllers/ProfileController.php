<?php

namespace app\controllers;

use app\models\Angels;
use app\models\Cities;
use app\models\Comments;
use app\models\Countries;
use app\models\Files;
use app\models\Needies;
use app\models\NeediesHistory;
use app\models\Regions;
use app\models\SocialAccount;
use app\models\Users;
use app\models\VolunteerHelp;
use app\models\Volunteers;
use app\models\UploadForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
    protected $userId;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function __construct($id, $module, $config = [])
    {
        $this->userId = Yii::$app->user->id;
        parent::__construct($id, $module, $config);
    }

    /** если не авторизированы не можем перейти на страницу профайлв */
    public function beforeAction($action)
    {
        $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
        if (parent::beforeAction($action)) {

            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }

            if (Yii::$app->user->isGuest) {
                Yii::$app->session->setFlash('error', 'Доступ запрещен');
                return $this->redirect('/site/index', []);
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $currentModel = Yii::$app->DLL->getModelByUserId($this->userId);
        if (!empty($currentModel)) {
            $model = $currentModel::find()->where(['userId' => $this->userId])->one();
            if (!empty($model)) {
                return $this->render('index', [
                    'model' => $model,
                    'photos' => Files::find()->where(['userId' => $this->userId, 'fileType' => 3])->all(),
                    'video' => Files::find()->where(['userId' => $this->userId, 'fileType' => 2])->one(),
                    'comment' => new Comments()
                ]);
            } else {
                return $this->redirect(['edit']);
            }
        } else {
            return $this->redirect(['edit']);
        }
    }


    /**
     * @return string
     */
    public function actionEdit()
    {
        if (!Yii::$app->user->isGuest) {
            $needyModel = new Needies();
            $angelModel = new Angels();
            $volunteerModel = new Volunteers();
            $uploadModel = new UploadForm();

            $needyData = Yii::$app->request->post('Needies');
            $volunteerData = Yii::$app->request->post('Volunteers');
            $angelData = Yii::$app->request->post('Angels');

            $currentModel = Yii::$app->DLL->getModelByUserId($this->userId);
            $model = $currentModel::find()->where(['userId' => $this->userId])->one();

            if (!empty($angelData)) {
                //update angel data
                $model->attributes = $angelData;

                // avatar update
                $this->avatarUpload($uploadModel, $model, 'angelFile');

                $model->setAttributes([
                    'statusId' => 4,
                    'email' => Yii::$app->user->identity->email
                ]);
                $model->save(false);





                Yii::$app->session->setFlash('message', 'Вы изменили свой профиль. Ваш профиль станет активным после проверки его администратором.');
                return $this->redirect('index');


            } elseif (!empty($volunteerData)) {
                // update volunteer data
                $model->attributes = $volunteerData;

                // avatar update
                $this->avatarUpload($uploadModel, $model, 'volunteerFile');

                $model->setAttributes([
                    'statusId' => 4,
                    'email' => Yii::$app->user->identity->email
                ]);
                $model->save(false);

                Yii::$app->session->setFlash('message', 'Вы изменили свой профиль. Ваш профиль станет активным после проверки его администратором.');
                return $this->redirect('index');


            } elseif (!empty($needyData)) {
                // update needy data
                $model->attributes = $needyData;



                /** ПЕРЕПИСАТЬ */
                /** Сохранение изменений в профиле */
                $diff = array_diff($model->attributes, $model->oldAttributes);
                $neediesHistoryModel = NeediesHistory::find()->where(['userId' => $this->userId])->one();
                if (!empty($neediesHistoryModel)) {
                    $neediesHistoryModel->attributes = $model->oldAttributes;
                } else {
                    $neediesHistoryModel = new NeediesHistory();
                    $neediesHistoryModel->attributes = $model->oldAttributes;
                }
                $neediesHistoryModel->diff = json_encode($diff);
                $neediesHistoryModel->save(false);


                // avatar update
                $this->avatarUpload($uploadModel, $model, 'needyFile');

                if ($model->statusId != 1) {
                    $model->setAttribute('statusId', 4);
                }

                $model->setAttributes([
                    'email' => Yii::$app->user->identity->email,
                    'bank' => $needyData['bank'],
                ]);

                $model->save(false);




                Yii::$app->session->setFlash('message', 'Вы изменили свой профиль. Ваш профиль станет активным после проверки его администратором.');
                return $this->redirect('index');


            } elseif (!empty($currentModel)) {
                /**
                 * type { 1: needy, 2: angel, 3: volunteer}
                 */
                $model = $currentModel::find()->where(['userId' => $this->userId])->one();
                if (empty($currentModel->type)) {
                    switch ($currentModel::className()) {
                        case 'app\models\Needies':
                            $uploadModel->setScenario('needyUpload');
                            $type = 1;
                            break;
                        case 'app\models\Angels':
                            $uploadModel->setScenario('angelUpload');
                            $type = 2;
                            break;
                        case 'app\models\Volunteers':
                            $uploadModel->setScenario('volunteerUpload');
                            $type = 3;
                            break;
                    }
                } else {
                    $type = $currentModel->type;
                    switch ($currentModel::className()) {
                        case 'app\models\Needies':
                            $uploadModel->setScenario('needyUpload');
                            break;
                        case 'app\models\Angels':
                            $uploadModel->setScenario('angelUpload');
                            break;
                        case 'app\models\Volunteers':
                            $uploadModel->setScenario('volunteerUpload');
                            break;
                    }

                }

                if (!empty($model->name)) {
                    $uploadModel->setScenario('');
                    // view form data
                    return $this->render('edit', [
                        'model' => $model,
                        'isEdit' => true,
                        'uploadModel' => $uploadModel,
                        'type' => $type,
                        'regions'=> ArrayHelper::map(Regions::find()->orderBy(['main'=>SORT_DESC])->all(),'mainId','title'),
                        'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                        'cities' => ArrayHelper::map(Cities::find()->where(['countryId' => $model->mainCountryId])->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
                    ]);
                }
            }
        } else {
            echo 'permissions denied';
        }
    }

    /**
     *
     */
    public function actionAddWallComment()
    {
        $model = new Comments();
        $comment = Yii::$app->request->post('Comments');
        if (!empty($comment)) {
            $model->attributes = $comment;
            $model->toUserId = $this->userId;

            if ($model->save(false)) {
                Yii::$app->session->setFlash('wallMessage', 'Спасибо за ваш отзыв. Ваш отзыв появится на сайте после подтверждения администратором');
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @return string|void
     */
    public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                // file is uploaded successfully
                return;
            }
        }

        return $this->render('upload', ['model' => $model]);
    }


    /**
     * Avatar updater
     */
    public function avatarUpload($uploadModel, $model, $type)
    {
        $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, $type);
        if (!empty($uploadModel->imageFile)) {
            $avatarId = $uploadModel->upload($this->userId);
            $model->updateAll(['avatarId' => $avatarId], 'userId = ' . $this->userId);
        }
    }


}
