<?php

namespace app\controllers;


use app\models\Angels;
use app\models\Comments;
use app\models\Contacts;
use app\models\Messages;
use app\models\NeediesSeach;
use app\models\StaticPages;
use app\models\User;
use app\models\UserHelp;
use app\models\Users;
use app\models\Volunteers;
use app\models\Needies;
use app\models\News;
use app\models\VolunteersSeach;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use app\models\UploadForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;



class SiteController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }


    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }

//
            if ($role == 'admin') {
                return $this->redirect(['/admin/contacts']);
            }

            if ($role == 'moder') {
                return $this->redirect(['shop/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function actionIndex($message = '')
    {
        $this->layout = 'main_index';

        if (!empty($_SERVER['HTTP_X_REAL_IP'])) {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if($message == 1) {
            Yii::$app->session->setFlash('message', 'Добро пожаловать в Мир добрых людей! Благодаря Вам мы становимся сильнее. Вместе еще легче делать добрые дела, а счастливых людей и моментов становится больше! Спасибо, что Вы с нами!');
        }
        if($message == 2){
            Yii::$app->session->setFlash('warning','К сожелению регистрация прошла не успешно, обратитесь пожалуйста к администратору');
        }


        SetCookie('server', $ip);

        $topVolunteer = new ActiveDataProvider([
            'query' => Volunteers::find()->where(['statusId' => 2])->orderBy('rating  DESC'),
            'pagination' => [
                'pageSize' => 3,
            ],

        ]);
        $topAngels = new ActiveDataProvider([
            'query' => Angels::find()->where(['statusId' => 2])->orderBy('rating  DESC'),
            'pagination' => [
                'pageSize' => 3,
            ],

        ]);
        $news = new ActiveDataProvider([
            'query' => News::find()->orderBy('dateTime  DESC'),
            'pagination' => [
                'pageSize' => 4,
            ],

        ]);

        $needyMain = Needies::find()->where(['statusId' => 2])->limit(4)->orderBy('rand()')->all();
        $needyAll = Needies::find()->where(['statusId' => 2])->limit(4)->orderBy('rand()')->all();

        return $this->render('index', [
//            'volunteersCount' => Volunteers::find()->where(['statusId' => 2])->count(),
//            'angelsCount' => Angels::find()->where(['statusId' => 2])->count(),
//            'topAngels' => $this->getTop(Angels::find()->where(['statusId' => 2])->all()),
//            'topVolunteers' => $this->getTop(Volunteers::find()->where(['statusId' => 2])->all()),
//            'news' => News::find()->limit(3)->orderby(['dateTime' => SORT_DESC])->all(),
//            'needies' => Needies::find()->where(['statusId' => 2])->limit(3)->orderBy('rand()')->all()
            'news'=> $news,
            'needyMain' => $needyMain,
            'needyAll' => $needyAll,
            'topVolunteer' => $topVolunteer,
            'topAngels' => $topAngels,
        ]);
    }

    /**
     *
     */
    protected function getTop($model)
    {
        $top = [];
        foreach ($model as $key => $value) {
            $top[$key]['userId'] = $value->userId;
            $top[$key]['id'] = $value->id;
            $top[$key]['name'] = $value->name . ' ' . $value->lastname;
            $top[$key]['rating'] = $value->ratingCount;
            $top[$key]['img'] = $value->file->path;
        }
        ArrayHelper::multisort($top, 'rating', SORT_DESC);
        return array_splice($top, 0, 3);
    }

    /**
     * Отправка сообщений из попапа
     * @return bool|\yii\web\Response
     */
    public function actionContacts()
    {
        $data = Yii::$app->request->post('Contacts');
        if (!empty($data)) {
            $contactsModel = new Contacts();
            if ($contactsModel->message($data)) {
                Yii::$app->session->setFlash('message', 'Спасибо за ваше сообщение.');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        return true;
    }

    /**
     *
     */
    public function actionHelp()
    {
        $data = Yii::$app->request->post();
        if (!empty($data)) {

            $model = new UserHelp();

            $files = Yii::$app->request->post('UploadForm');


            $text = !empty($data['UserHelp']['money']) ? 'Перечислил ' . $data['UserHelp']['money'] . ' ' : '';
            $text .= !empty($data['UserHelp']['things']) ? $data['UserHelp']['things'] : '';
            $model->load($data);
            $model->setAttribute('text', $text);

            $model->save(false);

            if (!empty($files->file)) {

                $upload = new UploadForm();
                $upload->file = UploadedFile::getInstances($upload, 'file');
                $upload->helpFileUpload($model->id);
            }

            Yii::$app->session->setFlash('message', 'Дорогой друг, спасибо за то, что ты есть! Любая помощь важна человеку, когда он в беде! Возможно, именно сейчас благодаря тебе спасли чью-то жизнь! Не важно, как и чем ты помог, главное, что у тебя большое и доброе сердце. Сейчас ты сделал наш большой мир еще немного светлее и счастливее. Мы благодарны тебе за неоценимую помощь!');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionContactsRegister()
    {
        $data = Yii::$app->request->post('Messages');
        if (!empty($data)) {
            $model = new Messages();
            $userId = Yii::$app->user->id;
            $role = Yii::$app->DLL->getRoleByUserId($userId);
            switch ($role) {
                case 'needy': $fromUserType = 4; break;
                case 'volunteer': $fromUserType = 3; break;
                case 'angel': $fromUserType = 2; break;
            }
            $model->writeMessage($userId, $fromUserType, 1, 1, $data['text'], 0);
            Yii::$app->session->setFlash('message', 'Спасибо за Ваше сообщение.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param $id
     * @param $user
     * @return string|\yii\web\Response
     */
    public function actionUpload($id,$user)
    {
        $upload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $upload->file = UploadedFile::getInstances($upload, 'file');
            $success = $upload->filesUpload($id);

            if($success == 1) {
                return $this->redirect(['descriptions',
                    'id' => $id,
                    'user'=>$user,
                ]);
            } else {
                return $this->render('upload', [
                    'userId' => $id,
                    'upload'=> $upload,
                    'error'=>$success['file'][0],
                    'user'=>$user
                ]);
            }
        }
        else return $this->render('upload', [
            'userId' => $id,
            'upload'=> $upload,
            'user'=>$user
        ]);
    }

    /**
     *
     */
    public function actionAddWallComment()
    {
        $model = new Comments();
        $comment = Yii::$app->request->post('Comments');

        if (!empty($comment)) {
            $model->attributes = $comment;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('wallMessage', 'Спасибо за Ваше сообщение. Сразу после проверки администратор опубликует его на сайте.');
            }

//            $files = Yii::$app->request->post('UploadForm');
            $upload = new UploadForm();
            $upload->file = UploadedFile::getInstances($upload, 'file');
            if(!empty($upload->file)) {
                $upload->wallFileUpload($model->id);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();
        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about', [
            'model' => Staticpages::find()->where(['id' => '1'])->one(),
        ]);
    }

    /**
     * @return string
     */
    public function actionContact()
    {
        return $this->render('contact', [
            'model' => Staticpages::find()->where(['id' => '2'])->one(),
        ]);
    }

    public function actionVictory()
    {
        $needies = new ActiveDataProvider([
            'query' =>  Needies::find()->where(['statusId' => 6])->orderBy(['id' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);
        return $this->render('victory', [
            'needies' => $needies,
        ]);
    }
    public function actionMemory()
    {
        $needies = new ActiveDataProvider([
            'query' =>  Needies::find()->where(['statusId' => 7])->orderBy(['id' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);
        return $this->render('memory', [
            'needies' => $needies,
        ]);
    }
}
