<?php

namespace app\controllers;


use app\components\YandexMetrica;
use app\models\Angels;
use app\models\AngelsSeach;
use app\models\Attachment;
use app\models\Contacts;
use app\models\ContactsSeach;
use app\models\Messages;
use app\models\Needies;
use app\models\Ratings;
use app\models\UserHelp;
use app\models\Users;
use app\models\Volunteers;
use app\models\VolunteersSeach;
use dektrium\user\models\User;
use Yii;
use yii\web\Controller;

class AdminController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }

            if (!Yii::$app->user->can('adminIndex')) {
                Yii::$app->session->setFlash('error', 'Доступ запрещен');
                return $this->redirect(['/site/index']);
            }
            $this->layout = 'admin';
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $type
     * @param string $id
     * @return string
     */
    public function actionMessages($type, $id = '')
    {

        switch ($type) {
            case '2':
                $title = 'Ангел : ';
                break;
            case '3':
                $title = 'Волонтер : ';
                break;
            case '4':
                $title = 'Кому помочь : ';
                break;
        }
        $messageModel = new Messages();
        if (empty($id)) {
            $id = $messageModel->getOneMessage($type);


            if (empty($id)) {
                return $this->render('messages');
            }
        }
        $user = $messageModel->getMessageUserType($type, $id);
        $messages = $messageModel->getMessagesToAdmin($id, $type);
        $avatar = $user->avatar->path;
        $allMessage = $messageModel->getAllMessage($type);
        $fromuseridArr = $messageModel->getAllUseridInMessages($allMessage);
        $allInform = $messageModel->getAllInform($fromuseridArr, $type);
        $newMessage = $messageModel->newMessage($type);

        return $this->render('messages', [
            'messagesToAdmin' => $messages,
            'user' => $user,
            'avatar' => $avatar,
            'pathArr' => $allInform['0'],
            'texts' => $allInform['1'],
            'users' => $allInform['2'],
            'type' => $type,
            'model' => $messageModel,
            'newMessage' => $newMessage,
            'title' => $title,
            'id' => $id,
        ]);
    }


    /**
     * @return \yii\web\Response
     */
    public function actionAddmessage()
    {
        $messageModel = new Messages();
        $postArr = Yii::$app->request->post('Messages');
        $userId = $postArr['toUserId'];
        $type = $postArr['toUserType'];
        $text = $postArr['text'];
        if (!empty($userId) && !empty($type) && !empty($text)) {
            $messageModel->writeMessage(1, 1, $userId, $type, $text);
            $messageModel->readMessage(1, $userId);

            Yii::$app->DLL->addNotification($userId);
        }

        return $this->redirect(['messages',
            'type' => $type,
            'id' => $userId,
        ]);

    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();
        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionContacts()
    {
        $searchModel = new ContactsSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $contactsModel = new Contacts();
        $contactsModel->readMessage();


        return $this->render('contacts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionRatingAngels()
    {
        $searchModel = new AngelsSeach();
        $title = 'Ангелов';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('ratings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => 2,
            'title' => $title,
        ]);
    }

    /**
     * @return string
     */
    public function actionRatingVolunteer()
    {
        $searchModel = new VolunteersSeach();
        $title = 'Волонтеров';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('ratings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => 3,
            'title' => $title,
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @return string
     */
    public function actionView($id, $type)
    {
        $model = new Ratings();
        $userHelp = new UserHelp();

        switch ($type) {
            case 2:
                $user = Angels::findOne(['userId' => $id]);
                $url = 'angels';
                break;
            case 3:
                $user = Volunteers::findOne(['userId' => $id]);
                $url = 'volunteer';
                break;
        }

        $newHelps = $userHelp->find()->where(['fromUserId' => $id, 'isVerify' => 0])->all();
        $newNeedies = $this->getNeedies($newHelps);
        $helps = $userHelp->find()->where(['fromUserId' => $id, 'isVerify' => 1])->all();
        $needies = $this->getNeedies($helps);


        return $this->render('view', [
            'newHelps' => $newHelps,
            'model' => $model,
            'id' => $id,
            'newNeedies' => $newNeedies,
            'user' => $user,
            'needies' => $needies,
            'helps' => $helps,
            'type' => $type,
            'url' => $url,
            'ratings' => $model->find()->where(['userId' => $id])->all(),
        ]);
    }

    /**
     * @param $array
     * @return array
     */
    public function getNeedies($array)
    {
        foreach ($array as $arr) {
            $needie[] = Needies::findOne(['userId' => $arr->toUserId]);
        }
        return $needie;
    }

    public function actionUpdate($id, $type)
    {
        $rating = Yii::$app->request->post('Ratings');

        if (!empty($rating)) {
            $model = new Ratings();
            $messageModel = new Messages();

            switch ($type) {
                case 2:
                    $user = Angels::findOne(['userId' => $id]);
                    break;
                case 3:
                    $user = Volunteers::findOne(['userId' => $id]);
                    break;
            }

            $help = Yii::$app->request->post('UserHelp');
            if (!empty($help)) {
                $userHelp = new UserHelp();
                $userHelp->updateAll(['isVerify' => 1], ['id' => $help['id']]);
            }

            $model->attributes = $rating;

            $model->save();

            $count = $rating['rating'];
            $userId = $rating['userId'];
            $user->rating += $count;
            $user->save();
            $message = "Вам добавлено " . $count . "  рейтинга. " . $rating['descr'];
            $messageModel->writeMessage(1, 1, $userId, $type, $message);

            Yii::$app->DLL->addNotification($userId);
            $user = User::find()->where(['id' => $userId])->one();

            Yii::$app->mailer->compose()
                ->setFrom('admin@dobroua.com')
                ->setTo($user->email)
                ->setSubject("Вам начислен рейтинг")
                ->setHtmlBody("Вам добавлено " . $count . "  рейтинга. " . $rating['descr'])
                ->send();
        }
        return $this->redirect(['view', 'id' => $id, 'type' => $type]);
    }

    public function actionDeleteHelp($id, $type)
    {
        $userHelp = new UserHelp();
        $help = $userHelp->findOne(['id' => $id]);
        $userId = $help->fromUserId;
        $help->delete();
        return $this->redirect(['view', 'id' => $userId, 'type' => $type]);
    }

    public function actionDelete($id)
    {
        $model = Contacts::findOne($id);
        $model->delete();

        return $this->redirect(['contacts']);
    }

    public function actionViewContacts($id)
    {
        $model = new Contacts();
        $contacts = $model->find()->where(['id' => $id])->one();
        return $this->render('viewcontacts', [
            'model' => $contacts,
        ]);
    }

    public function actionMetrica()
    {
        $metrica = new YandexMetrica([
            'user_login' => Yii::$app->params['Yandex']['user_login'],
            'user_password' => Yii::$app->params['Yandex']['user_password'],
            'oauth_id' => Yii::$app->params['Yandex']['oauth_id'],
            'oauth_password' => Yii::$app->params['Yandex']['oauth_password']
        ]);

        $trafficSummary = $metrica->trafficSummary(Yii::$app->params['Yandex']['counter_id']);
        $trafficDeepness = $metrica->trafficDeepness(Yii::$app->params['Yandex']['counter_id']);
        $trafficHourly = $metrica->trafficHourly(Yii::$app->params['Yandex']['counter_id']);
        $trafficLoad = $metrica->trafficLoad(Yii::$app->params['Yandex']['counter_id']);
        $geo = $metrica->geo(Yii::$app->params['Yandex']['counter_id']);


        return $this->render('metrica', [
            'trafficSummary' => $trafficSummary,
            'trafficDeepness' => $trafficDeepness,
            'trafficHourly' => $trafficHourly,
            'trafficLoad' => $trafficLoad,
            'geo' => $geo
        ]);
    }

    /**
     * Удаление рейтинга
     */
    public function actionRemove($id)
    {
        $model = Ratings::findOne($id);
        $userModel = Yii::$app->DLL->getModelByUserId($model->userId);
        $user = $userModel->find()->where(['userId' => $model->userId])->one();
        $user->rating -= $model->rating;
        $user->save();
        $model->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRecount($type)
    {
        switch ($type) {
            case 2:
                $model = new Angels();
                $url = 'angels';
                break;
            case 3:
                $model = new Volunteers();
                $url = 'volunteer';
                break;
        }
        $users = $model->find()->all();

        foreach ($users as $user) {
            $user->rating = $user->ratingCount;
            $user->save();
        }
        $this->redirect('rating-' . $url);
    }

    public function actionEmail($error = '')
    {
        $userModel = new Users();

        $users = $userModel -> find()->select(['email'])->asArray()->all();

        $needyModel = new Needies();



        $needyEmail = $needyModel->find()->select(['email'])->asArray()->all();

        $angelsModel = new Angels();

        $angelsEmail = $angelsModel->find()->select(['email'])->asArray()->all();

        $volunteerModel = new Volunteers();

        $volunteerEmail = $volunteerModel->find()->select(['email'])->asArray()->all();

        $usersEmail =[];
        foreach($needyEmail as $needy)
        {
            $usersEmail [] = $needy['email'];
        }
        foreach($angelsEmail as $angels)
        {
            $usersEmail [] = $angels['email'];
        }
        foreach($volunteerEmail as $volunteer)
        {
            $usersEmail [] = $volunteer['email'];
        }
        $emails = Yii::$app->params['emails'];
        foreach( $emails as $ignore){
            $usersEmail [] = $ignore;
        }

        $email = [];
        foreach($users as $user)
        {
            $email [] = $user['email'];
        }
        $noUsers = array_diff($email,$usersEmail);

        return $this->render('nouser',[
            'noUsers'=> $noUsers,
            'error'=> $error,
        ]);

    }
    public function actionDeleteUser($email)
    {
        $model = new Users();
        $user = $model->find()->where(['email'=>$email])->one();
        $profile = Yii::$app->DLL->getProfileById($user->id);

        if(empty($profile)) {
            $user->delete();
            return $this->redirect(['admin/email']);
        } else{
            $error = $email . ' - ' .$profile->name . ' ' . $profile->lastname;
            return $this->redirect(['admin/email','error'=>$error]);
        }

    }
    public function actionGetRegion()
    {
        $lang = 0; // russian
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" . // Вероятно этот параметр ни на что не влияет
                    "Cookie: remixlang=$lang\r\n"
            )
        );
        $country =  [
            '1',
            '2',
            '3',
            '4',
            '11',
            '15',
            '39',

        ];
        $value = [];
        foreach($country as $countryId) {

            $methodUrl = 'http://api.vk.com/method/database.getRegions?v=5.5&need_all=1&offset=0&count=1000&country_id=' . $countryId;
            $streamContext = stream_context_create($headerOptions);
            $json = file_get_contents($methodUrl, false, $streamContext);
            $arr = json_decode($json, true);
            echo '<pre>';

            foreach($arr['response']['items'] as $title){
                $arrRegion['1'] = $title['id'];
                $arrRegion['2'] = $countryId;
                $arrRegion ['3'] = $title['title'];


                $value [] = '(' . $title['id'] . ', ' .$countryId .',\' ' .$title['title'] .'\' )';
            }
        }

        Yii::$app->db->createCommand('INSERT INTO `regions` ( `mainId` ,`countryId` , `title`) VALUES ' .implode(', '."", $value).'; ')->query();

    }

    public function actionGetCities()
    {


        $lang = 0;
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=$lang\r\n"
            )
        );



        $country = [
            '1',
            '2',
            '3',
            '4',
            '11',
            '15',
            '39',

        ];

        foreach ($country as $countryId) {

            $methodUrl = 'http://api.vk.com/method/database.getRegions?v=5.5&need_all=1&offset=0&count=1000&country_id=' . $countryId;
            $streamContext = stream_context_create($headerOptions);
            $json = file_get_contents($methodUrl, false, $streamContext);
            $arr = json_decode($json, true);


            foreach ($arr['response']['items'] as $regionId) {


                $lang = 0;
                $headerOptions = array(
                    'http' => array(
                        'method' => "GET",
                        'header' => "Accept-language: en\r\n" .
                            "Cookie: remixlang=$lang\r\n"
                    )
                );
                $methodUrl= 'http://api.vk.com/method/database.getCities?v=5.5&country_id='. $countryId . '&region_id='.$regionId['id'].'&offset=0&need_all=1&count=1000';
                $streamContext = stream_context_create($headerOptions);
                $json = file_get_contents($methodUrl, false, $streamContext);
                $arr = json_decode($json, true);


                $count = $arr['response']['count'];


                if($count>1000)
                {


                    $count = ceil($count/1000);
                    for($i = 0; $i < $count; $i++ ){

                        $lang = 0;
                        $headerOptions = array(
                            'http' => array(
                                'method' => "GET",
                                'header' => "Accept-language: en\r\n" .
                                    "Cookie: remixlang=$lang\r\n"
                            )
                        );
                        $offset = $i*1000;
                        echo '<pre>';
                        print_r($offset);
                        $methodUrl= 'http://api.vk.com/method/database.getCities?v=5.5&country_id='. $countryId . '&region_id='.$regionId['id'].'&offset='.$offset.'&need_all=1&count=1000';
                        $streamContext = stream_context_create($headerOptions);
                        $json = file_get_contents($methodUrl, false, $streamContext);
                        $arr = json_decode($json, true);

                        foreach($arr['response']['items'] as $title){
                            $value [] = '(' . $regionId['id'] . ', ' .$countryId .',\' ' .$title['title'] .'\' )';

                        }
                        Yii::$app->db->createCommand( 'INSERT INTO `cities` ( `regionId` ,`countryId` , `title`) VALUES ' .implode(', '."", $value).' ')->query();
                        $value= '';
                    }
                } else{
                    foreach($arr['response']['items'] as $title){
                        $value [] = '(' . $regionId['id'] . ', ' .$countryId .',\' ' .$title['title'] .'\' )';

                    }
                    Yii::$app->db->createCommand( 'INSERT INTO `cities` ( `regionId` ,`countryId` , `title`) VALUES ' .implode(', '."", $value).' ')->query();
                    $value= '';
                }
            }
        }



    }
}