<?php

namespace app\controllers;

use app\models\Needies;
use app\models\NeediesSeach;
use app\models\ShopPhoto;
use app\models\UploadForm;
use Yii;
use app\models\Shop;
use app\models\ShopSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;

/**
 * AdminShopController implements the CRUD actions for Shop model.
 */
class ShopController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (!Yii::$app->user->isGuest || Yii::$app->DLL->getRoleByUserId(Yii::$app->user->id) !== 'moder') {
            } else {
                $this->layout = 'shop_admin';
            }
            return true;
        }
        return false;
    }

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => true,
                        'actions' => ['shop', 'dress'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['shop', 'dress'],
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'watermark','counter','change'],
                        'allow' => true,
                        'roles' => ['moder'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Shop models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'shop_admin';
        $searchModel = new ShopSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWatermark($id)
    {
        $file = ShopPhoto::findOne($id);
        $path = Yii::$app->basePath . '/web/images/shop/' . $file->shopId . '/big/' . $file->path;
        $pathM = Yii::$app->basePath . '/web/images/shop/' . $file->shopId . '/small/' . $file->path;
        \Yii::$app->DLL->watermark($path, 'images/sale.png');
        Image::thumbnail($path, 200, 200, ManipulatorInterface::THUMBNAIL_INSET)
            ->save($pathM);

        $this->redirect(['index']);
    }

    /**
     * Displays a single Shop model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'shop_admin';
        $model = $this->findModel($id);
        $model->avatarId = Yii::getAlias('@web/images/shop/') . $id . '/small/' . $model->avatar->path;
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Shop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'shop_admin';
        $model = new Shop();
        $upload = new UploadForm();
        $uploadFile = new UploadForm();
        $upload->scenario = 'shopAvatar';

        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $model->save(false);
            $upload->shopAvatar($model->id);
            $uploadFile->file = UploadedFile::getInstances($upload, 'file');

            if (!empty($uploadFile->file)) {

                $uploadFile->shopPhotos($model->id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
                'uploadFile' => $uploadFile
            ]);
        }
    }

    /**
     * Updates an existing Shop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'shop_admin';
        $model = $this->findModel($id);
        $upload = new UploadForm();
        $uploadFile = new UploadForm();
        $upload->scenario = 'shopAvatar';
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');

            $model->save(false);
            $upload->shopAvatar($model->id);
            $upload->file = UploadedFile::getInstances($upload, 'file');
            if (!empty($upload->file)) {

                $upload->shopPhotos($model->id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
                'uploadFile' => $uploadFile,
            ]);
        }
    }

    /**
     * Deletes an existing Shop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->layout = 'shop_admin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shop::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//
    public function actionShop()
    {
        $searchModel = new ShopSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $needies = Needies::find()->where(['id' => 106])->one();

        return $this->render('dress', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'needies' => $needies,
        ]);
    }

    public function actionDress()
    {


        $id = $_GET['dressId'];
        $model = Shop::findOne($id);
        $files = ShopPhoto::find()->where(['shopId' => $id])->all();
        return $this->renderPartial('_view', [
            'model' => $model,
            'files' => $files,

        ]);
    }

    public function actionCounter()
    {
        $this->layout = 'shop_admin';
        $searchModel = new NeediesSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,464);

        return $this->render('counter', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function  actionChange($id)
    {

        $this->layout = 'shop_admin';
        $model = Needies::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save(false);

            return $this->redirect(['counter']);
        }
        return $this->render('change', [
            'model'=>$model,
        ]);
    }
}
