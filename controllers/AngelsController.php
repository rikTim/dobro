<?php

namespace app\controllers;

use app\models\Cities;
use app\models\Comments;
use app\models\Countries;
use app\models\Files;
use app\models\Statuses;
use app\models\UploadForm;
use app\models\UserHelp;
use app\models\Users;
use Yii;
use app\models\Angels;
use app\models\AngelsSeach;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * AngelsController implements the CRUD actions for Angels model.
 */
class AngelsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
          $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
          if ($action->id != 'list' && $action->id != 'more') {
              if (!Yii::$app->user->can('adminIndex')) {
                  Yii::$app->session->setFlash('error', 'Доступ запрещен');
                  return $this->redirect('/site/index', 302);
              } else {
                  $this->layout = 'admin';
              }
          } elseif ($role == 'admin') {
              return $this->redirect(['subscribe/index']);
          }
          return true;
        }
        return false;
    }

    /**
     * Lists all Angels models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AngelsSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Angels model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $filesModel = new Files();
        $model = $this->findModel($id);
        $images = $filesModel->getFileType($model->userId, 1);
        return $this->render('view', [
            'model' => $model,
            'images' => $images,
        ]);
    }

    /**
     * Creates a new Angels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Angels();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'cities' => ArrayHelper::map(Cities::find()->asArray()->all(), 'id', 'title'),
                'statuses' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title')
            ]);
        }
    }

    /**
     * Updates an existing Angels model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $filesModel = new Files();
        $model = $this->findModel($id);
        $upload = new UploadForm();
        $upload->scenario = 'avatarUpload';

        $images = $filesModel->getFileType($model->userId, 1);
        if ($model->load(Yii::$app->request->post()) ) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if (!empty($upload->imageFile)) {
                $model->avatarId = $upload->upload($model->userId);
            }
            $model->fb = Yii::$app->DLL->getLinkId($model->fb);
            $model->vk = Yii::$app->DLL->getLinkId($model->vk);
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                'cities' => ArrayHelper::map(Cities::find()->where(['countryId' => 2])->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
                'statuses' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
                'images' => $images,
                'upload'=>$upload,
            ]);
        }
    }

    /**
     * Deletes an existing Angels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = new Users();
        $userArr = $this->findModel($id);
        $user = $model->findOne(['id'=>$userArr->userId]);
        $user->delete();
        return $this->redirect(['index']);
    }

    /**
     * @return string
     */
    public function actionList()
    {

        $angels = new ActiveDataProvider([
            'query' => Angels::find()->where(['statusId' => 2])->orderBy(['rating' => SORT_DESC, 'id' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);
        return $this->render('list', [
                'angels' =>$angels
        ]);
    }


    /**
     * @return string
     */
    public function actionMore($id)
    {
        $model = $this->findModel($id);
        $helps = new ActiveDataProvider([
            'query' => UserHelp::find()->where(['fromUserId' => $model->userId, 'isVerify' => 1])->orderBy(['dateTime' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 4,
            ],
        ]);
        return $this->render('more', [
            'model' => $model,
            'helps' => $helps,
        ]);
    }

    /**
     *
     */
    public function actionAddWallComment()
    {
        $model = new Comments();
        $comment = Yii::$app->request->post('Comments');
        if (!empty($comment)) {
            $model->attributes = $comment;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('wallMessage', 'Спасибо за ваш отзыв. Ваш отзыв появится на сайте после подтверждения администратором');
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the Angels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Angels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Angels::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *
     */
    public function actionDelavatar()
    {
        $filesModel = new Files();
        $avatarId = Yii::$app->request->post('avatarId');
        $delete = $filesModel->findOne(['id' => $avatarId]);
        $delete->delete();
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionApproved($id)
    {
        $angelsModel = new Angels();
        $angel = $angelsModel->findOne(['id'=>$id]);
        $angel->statusId = 2;
        $angel->save(false);
        return $this->redirect(['index']);
    }

    public function actionRotateImg()
    {
        $id = Yii::$app->request->post('id');
        $side = Yii::$app->request->post('side');
        if ($id !== null || $side !== null) {
            $imagine = Image::getImagine();
            $file = Files::findOne($id);
            $path = realpath(Yii::getAlias('@app') . '/web/images/' . $file->userId . '/' . $file->path);
            $pathM = realpath(Yii::getAlias('@app') . '/web/images/' . $file->userId . '/m_' . $file->path);
            $image = $imagine->open($path);
            $imageM = $imagine->open($pathM);
            $angle = null;
            if ($side == 'left') {
                $angle = -90;
            } else {
                $angle = 90;
            }
            $image->rotate($angle)->save($path);
            $imageM->rotate($angle)->save($pathM);
        }
    }

    public function actionComment($id)
    {
        $user = new Angels();
        $model = new Comments();
        $statusModel = new Statuses();
        $status = $statusModel->getAllStatuses();
        $angels = $user->find()->where(['id'=>$id])->one();

        if ($model->load(Yii::$app->request->post())) {

            $model->save();

            return $this->redirect(['index']);
        }
        return $this->render('comment',[
            'model'=>$model,
            'angels'=>$angels,
            'status'=>$status,
        ]);
    }

}
