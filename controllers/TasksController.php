<?php

namespace app\controllers;

use app\models\AuthAssignment;
use app\models\Messages;
use app\models\Needies;
use app\models\UploadForm;
use app\models\UserTask;
use app\models\Volunteers;
use app\models\Angels;
use Yii;
use app\models\Tasks;
use app\models\TasksSeach;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\data\Pagination;

/**
 * TasksController implements the CRUD actions for Tasks model.
 */
class TasksController extends Controller
{

    protected $userId;
    protected $volunteerId;
    protected $angelId;

    public function __construct($id, $module, $config = [])
    {
        $this->userId = Yii::$app->user->id;
        $this->volunteerId = Volunteers::find()->where(['userId' => $this->userId])->one()->id;
        $this->angelId = Angels::find()->where(['userId' => $this->userId])->one()->id;
        parent::__construct($id, $module, $config);
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }

            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($action->id != 'list' && $action->id != 'more' && $action->id != 'apply') {
                if (!Yii::$app->user->can('adminIndex')) {
                    Yii::$app->session->setFlash('error', 'Доступ запрещен');
                    return $this->redirect('/site/index', []);
                } else {
                    $this->layout = 'admin';
                }
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TasksSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $isActive = [
            ['id'=>'0','name'=>'Не Активна'],
            ['id'=>'1','name'=>'Активна']
        ];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'isActive' => $isActive,
        ]);
    }

    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $userTasks = $model->userTask;
        $users= [];

        foreach ($userTasks as $userTask) {
            $role = Yii::$app->DLL->getRoleByUserId($userTask->userId);
            switch($role)
            {
                case 'angel': $users[] =  Angels::find()->where(['userId'=>$userTask->userId])->one(); $url []= 'angels/view'; break;
                case 'volunteer': $users[] = Volunteers::find()->where(['userId'=>$userTask->userId])->one(); $url []= 'volunteers/view'; break;
                case 'needy' : $users[] = Needies::find()->where(['userId'=>$userTask->userId])->one(); $url [] = 'needies/view'; break;
            }

        }
        switch($model->isActive){
            case 1: $isActive = 'Активная';break;
            case 0: $isActive = 'Не активная';break;
        }

        return $this->render('view', [
            'model' => $model,
            'users' => $users,
            'isActive' => $isActive,
            'url'=>$url
        ]);
    }

    /**
     * Creates a new Tasks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tasks();
        $upload = new UploadForm();
        $model->isActive = 1;
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $filePath = $upload->tasksFile();
            $model->filePath = $filePath;
            if(!empty($model->dateTime)){
                $model->dateTime = strtotime($model->dateTime);
            }
            $model->save(false);

            /** new task notifier */
            $messagesModel = new Messages();
            $messagesModel->taskNotification($model->title);

            /** уведомление в меню, о новом задании */
            $volunteersArray = AuthAssignment::find()->where(['item_name' => 'volunteer'])->all();
            foreach ($volunteersArray as $volunteer) {
                Yii::$app->DLL->addNotification($volunteer->user_id);
            }


            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload'=>$upload,
            ]);
        }
    }

    /**
     * Updates an existing Tasks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->isActive = 1;
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->dateTime = strtotime($model->dateTime);
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $filePath = $upload->tasksFile();
            if(!empty($filePath)) {
                $model->filePath = $filePath;
            }
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload'=>$upload,
            ]);
        }
    }

    /**
     * Deletes an existing Tasks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Просмотр списка заданий
     * @return string
     */
    public function actionList()
    {
//        if (\Yii::$app->user->can('task')) {
            $query = Tasks::find()->where(' (dateTime >=' . time() . ' OR dateTime = "" ) AND isActive = 1');
            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 9]);

            $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->orderBy(['id' => SORT_DESC])
                ->all();

            return $this->render('list', [
                'models' => $models,
                'pages' => $pages,
                'userId' => $this->userId,

            ]);
//        } else {
//            Yii::$app->session->setFlash('error', 'Доступ запрещен');
//            return $this->redirect('/site/index', []);
//        }
    }

    /**
     * Просмотр подбродной информации о задании
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionMore($id)
    {
//        if (\Yii::$app->user->can('task')) {
            $model = Tasks::find()->where(['id' => $id])->one();
            if ($model->isActive == 0) {
                return $this->redirect('list', []);
            }

            return $this->render('more', [
                'model' => Tasks::find()->where(['id' => $id])->one(),
                'userId' => $this->userId,
            ]);
//        } else {
//            Yii::$app->session->setFlash('error', 'Доступ запрещен');
//            return $this->redirect('/site/index', []);
//        }
    }

    /**
     * Подписка / отписка на задания
     * @param $id
     * @param $action
     * @return \yii\web\Response
     */
    public function actionApply($id, $action)
    {
        $userTaskModel = new UserTask();
        if (!empty($action)) {
            if ($action == 'subscribe') {
                $userTaskModel->setAttributes([
                    'userId' => $this->userId,
                    'taskId' => $id
                ]);
                $userTaskModel->save();
                Yii::$app->session->setFlash('message', 'Спасибо что откликнулись. Наш администратор свяжется с Вами в ближайшее время.');
            } elseif($action == 'unsubscribe') {
                $userTaskModel->deleteAll(['userId' => $this->userId, 'taskId' => $id ]);
                Yii::$app->session->setFlash('message', 'Вы больше не подписаны на выполнение задания.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
