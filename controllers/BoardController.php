<?php

namespace app\controllers;

use app\models\Angels;
use app\models\Cities;
use app\models\Countries;
use app\models\HelpTypes;
use app\models\Needies;
use app\models\NeedType;
use app\models\NeedyHelp;
use app\models\Regions;
use app\models\Statuses;
use app\models\UploadForm;
use app\models\Volunteers;
use Yii;
use app\models\Board;
use app\models\BoardSeach;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BoardController implements the CRUD actions for Board model.
 */
class BoardController extends Controller
{


    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(!Yii::$app->user->isGuest) {
                Yii::$app->DLL->onliner(Yii::$app->user->id);
            }
            $role = key(Yii::$app->authManager->getRolesByUser(Yii::$app->user->id));
            if ($action->id != 'board' && $action->id != 'more' && $action->id != 'create' && $action->id != 'user-board' && $action->id != 'update-board' && $action->id != 'apply' && $action->id != 'delete-board' ) {
                if (!Yii::$app->user->can('adminIndex')) {
                    Yii::$app->session->setFlash('error', 'Доступ запрещен');
                    return $this->redirect(['site/index']);
                } else {
                    $this->layout = 'admin';
                }
            } elseif ($role == 'admin') {
                return $this->redirect(['subscribe/index']);
            }
            return true;
        }
        return false;
    }

    /**
     * Lists all Board models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BoardSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Board model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $role =  Yii::$app->DLL->getRoleByUserId($model->userId);
        switch($role){
            case 'angel': $userModel = new Angels();  $url = 'angels';break;
            case 'volunteer': $userModel = new Volunteers(); $url = 'volunteers';break;
            case 'needy' : $userModel = new Needies(); $url = 'needy';break;
        }
        $user = $userModel->find()->where(['userId'=>$model->userId])->one();

        return $this->render('view', [
            'model' => $model,
            'user' => $user,
            'url' => $url,
        ]);
    }

    /**
     * Creates a new Board model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Board();
        $upload = new UploadForm();
        $upload->scenario = 'boardUpload';
        if ($model->load(Yii::$app->request->post()) ) {



            $model->save();
            $upload->boardFile = UploadedFile::getInstance($upload, 'boardFile');
            if (!empty($upload->boardFile)) {
                 $upload->boardFile($model->id);
            }

            Yii::$app->session->setFlash('message', 'Спасибо за Вашу активность. Ваше объявление появится на сайте после модерации администратором.');
            return $this->redirect(['user-board']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload'=> $upload,
                'help' => ArrayHelper::map(NeedType::find()->where('id != 1')->asArray()->all(), 'id', 'title'),
                'regions'=> ArrayHelper::map(Regions::find()->where(['countryId'=>2])->orderBy(['main'=>SORT_DESC])->all(),'mainId','title'),
                'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                'cities' => ArrayHelper::map(Cities::find()->where(['regionId'=>1508808])->orderBy(['main' => SORT_DESC, 'title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
            ]);
        }
    }

    /**
     * Updates an existing Board model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload' => $upload,
                'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                'cities' => ArrayHelper::map(Cities::find()->where(['countryId' => 2])->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
                'help' => ArrayHelper::map(NeedType::find()->where('id != 1')->asArray()->all(), 'id', 'title'),
                'statuses' => ArrayHelper::map(Statuses::find()->all(), 'id', 'title'),
                'regions'=> ArrayHelper::map(Regions::find()->orderBy(['main'=>SORT_DESC])->all(),'mainId','title'),
            ]);
        }
    }

    /**
     * Deletes an existing Board model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Board model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Board the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Board::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionBoard()
    {
        $modelBoard = new Board();
        $post = Yii::$app->request->post('Board');
        $count = Yii::$app->DLL->getSearchCount($post);

        $query = Yii::$app->DLL->getSearch($count, $post, $modelBoard);

//        $userBoard = $modelBoard->find()->where(['userId'=>Yii::$app->user->id])->one();

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 10]);
//        $models = $query->offset($pages->offset)
//            ->limit($pages->limit)
//            ->orderBy(['id' => SORT_DESC])
//            ->all();

        $board = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);
        return $this->render('board', [
            'modelBoard' => $modelBoard,
//            'models' => $models,
//            'pages' => $pages,
//            'userBoard' => $userBoard,
            'board'=>$board,
            'help' => ArrayHelper::map(NeedType::find()->where('id != 1')->asArray()->all(), 'id', 'title'),
            'regions'=> ArrayHelper::map(Regions::find()->where(['countryId'=>2])->orderBy(['main'=>SORT_DESC])->all(),'mainId','title'),
            'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
            'cities' => ArrayHelper::map(Cities::find()->where(['regionId'=>1508808])->orderBy(['main' => SORT_DESC, 'title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
        ]);
    }

    public function actionMore($id)
    {
        $needyHelpModel = new NeedyHelp();
        $isNeedHelp = $needyHelpModel->find()->where(['needyId'=>Yii::$app->user->id,'boardId'=>$id])->one();
        $boardModel = new Board();
        $model = $boardModel->findOne($id);
        $role =  Yii::$app->DLL->getRoleByUserId($model->userId);
        switch($role){
            case 'angel': $userModel = new Angels();  $url = 'angels';break;
            case 'volunteer': $userModel = new Volunteers(); $url = 'volunteers';break;
            case 'needy': $userModel = new Needies(); $url = 'needies'; break;
        }
        $user = $userModel->find()->where(['userId'=>$model->userId])->one();

        return $this->render('more', [
            'model' => $model,
            'user' => $user,
            'url' => $url,
            'isNeedyHelp'=> $isNeedHelp,
        ]);
    }

    public function actionApply($id, $action)
    {
        $userId = Yii::$app->user->id;
        $needyHelpModel = new NeedyHelp();
        if (!empty($action)) {
            if ($action == 'subscribe') {
                $needyHelpModel->setAttributes([
                    'needyId' =>  $userId,
                    'boardId' => $id
                ]);
                $needyHelpModel->save();
                Yii::$app->session->setFlash('message', 'Наш администратор свяжется с вам в ближайшее время.');
            } elseif($action == 'unsubscribe') {
                $needyHelpModel->deleteAll(['needyId' => $userId , 'boardId' => $id ]);
                Yii::$app->session->setFlash('message', 'Вы больше не нуждаетесь.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUserBoard()
    {
        $userId = Yii::$app->user->id;
        $query = Board::find()->where(['userId'=>$userId]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 10]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('board', [
            'models' => $models,
            'pages' => $pages,
        ]);
    }

    public function actionUpdateBoard($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        $upload->scenario = 'boardUpload';
        if ($model->load(Yii::$app->request->post()) ) {
            $model->statusId = 4;
            $model->save();
            $upload->boardFile = UploadedFile::getInstance($upload, 'boardFile');
            if (!empty($upload->boardFile)) {
                $upload->boardFile($model->id);
            }
            Yii::$app->session->setFlash('message', 'Спасибо за Вашу активность. Ваше объявление появится на сайте после модерации администратором.');
            return $this->redirect(['user-board', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload' => $upload,
                'help' => ArrayHelper::map(NeedType::find()->where('id != 1')->asArray()->all(), 'id', 'title'),
                'regions'=> ArrayHelper::map(Regions::find()->where(['countryId'=>2])->orderBy(['main'=>SORT_DESC])->all(),'mainId','title'),
                'countries' => ArrayHelper::map(Countries::find()->orderBy(['order' => SORT_ASC])->all(), 'mainId', 'title'),
                'cities' => ArrayHelper::map(Cities::find()->where(['regionId'=>1508808])->orderBy(['main' => SORT_DESC, 'title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
            ]);
        }
    }

    public function actionDeleteBoard($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['user-board']);
    }

    public function actionApproved($id)
    {
        $boardModel = new Board();
        $board = $boardModel->findOne(['id'=>$id]);
        $board->statusId = 2;
        $board->save(false);
        return $this->redirect(['index']);
    }

    public function actionNeedyHelp()
    {
        $searchModel = new BoardSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('needyhelp', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNeedyView($id)
    {
        $board =  $this->findModel($id);

        $needyHelps = NeedyHelp::find()->where(['boardId'=>$id])->all();

        $role =  Yii::$app->DLL->getRoleByUserId($board->userId);
        switch($role){
            case 'angel': $user = new Angels();  $url = 'angels';break;
            case 'volunteer': $user = new Volunteers(); $url = 'volunteers';break;
            case 'needy' : $user = new Needies(); $url = 'needy';break;
        }
        $user = $user->find()->where(['userId'=>$board->userId])->one();
        foreach ($needyHelps as $key=>$needyHelp)
        {
            $role =  $role =  Yii::$app->DLL->getRoleByUserId($needyHelp->needyId);

            switch($role){
                case 'angel': $userModel =  Angels::find()->where(['userId'=>$needyHelp->needyId])->one(); $url = 'angel/view';break;
                case 'volunteer': $userModel = Volunteers::find()->where(['userId'=>$needyHelp->needyId])->one();$url = 'volunteer/view';break;
                case 'needy' : $userModel = Needies::find()->where(['userId'=>$needyHelp->needyId])->one();$url = 'needy/view'; break;
            }

//            print_r($userModel);
            $avatar = $userModel->avatar->path;
            if(!empty($avatar)){
                $avatarArr[$key] = $avatar;
            }else{
                $avatarArr[$key] = 0;
            }
            $urlArr[$key] = $url;
            $needy[$key] = $userModel;
        }
        return $this->render('needyview',[
            'avatars' => $avatarArr,
            'url' => $urlArr,
            'board'=>$board,
            'user'=>$user,
            'needy'=>$needy,
        ]);

    }
}
